/*
 * MathVec.h
 *
 *  Created on: Nov 17, 2015
 *      Author: wenchia
 */

#ifndef MATHVEC_H_
#define MATHVEC_H_

#include <vector>
#include <ostream>

template <typename VType>
class MathVec {
public:
	MathVec(){;}
	MathVec(unsigned int size){ _vals.resize(size); }
	MathVec(unsigned int size, VType val){ _vals.assign(size,val); }
	virtual ~MathVec(){;}

	inline void resize( unsigned int size ){ _vals.resize( size ); }

	inline std::vector<VType>& stdvector(){ return _vals; }
	inline const std::vector<VType>& stdvector() const { return _vals; }
	inline unsigned int size() const { return _vals.size(); }
	bool assure_samesize(unsigned int insize) const ;

	template <typename Value>
	void assign(const std::vector<Value>& vals );
	template <typename Value>
	void assign(const MathVec<Value>& vals );
	template <typename Array>
	void assign(const Array& vals, unsigned int size );
	void assign(const VType& vals, unsigned int size );

	void zeros( unsigned int size );

	VType prod();
	VType sum();

	MathVec<VType>& bw_abs();
	template <typename Value>
	MathVec<VType>& bw_maxabs(const MathVec<Value>& vals);
	template <typename Value>
	MathVec<VType>& bw_cutout(const MathVec<Value>& errs);
	template <typename Value>
	MathVec<VType>& bw_mindis(const MathVec<Value>& vals);
	template <typename Value>
	MathVec<VType>& bw_maxdis(const MathVec<Value>& vals);
	template <typename Value>
	MathVec<VType>& bw_min(const MathVec<Value>& vals);
	template <typename Value>
	MathVec<VType>& bw_max(const MathVec<Value>& vals);

	template <typename Value, typename Bool>
	void bw_lt(Value val, std::vector<Bool>& ires);
	template <typename Value, typename Bool>
	void bw_gt(Value val, std::vector<Bool>& ires);
	template <typename Value, typename Bool>
	void bw_le(Value val, std::vector<Bool>& ires);
	template <typename Value, typename Bool>
	void bw_ge(Value val, std::vector<Bool>& ires);

	VType& operator[] (unsigned int Iadd){ return _vals[Iadd]; }
	const VType& operator[] (unsigned int Iadd) const { return _vals[Iadd]; }

	template <typename Value>
	MathVec<VType>& operator= ( const MathVec<Value>& vals){ return ((*this) =vals.stdvector() ); }
	template <typename Value>
	MathVec<VType>& operator+=( const MathVec<Value>& vals){ return ((*this)+=vals.stdvector() ); }
	template <typename Value>
	MathVec<VType>& operator-=( const MathVec<Value>& vals){ return ((*this)-=vals.stdvector() ); }
	template <typename Value>
	MathVec<VType>& operator*=( const MathVec<Value>& vals){ return ((*this)*=vals.stdvector() ); }
	template <typename Value>
	MathVec<VType>& operator/=( const MathVec<Value>& vals){ return ((*this)/=vals.stdvector() ); }
	template <typename Value>
	MathVec<VType> operator+ ( const MathVec<Value>& vals) const { return ((*this)+vals.stdvector() ); }
	template <typename Value>
	MathVec<VType> operator- ( const MathVec<Value>& vals) const { return ((*this)-vals.stdvector() ); }
	template <typename Value>
	MathVec<VType> operator* ( const MathVec<Value>& vals) const { return ((*this)*vals.stdvector() ); }
	template <typename Value>
	MathVec<VType> operator/ ( const MathVec<Value>& vals) const { return ((*this)/vals.stdvector() ); }

	template <typename Value>
	MathVec<VType>& operator=( const std::vector<Value>& vals);
	template <typename Value>
	MathVec<VType>& operator+=( const std::vector<Value>& vals);
	template <typename Value>
	MathVec<VType>& operator-=( const std::vector<Value>& vals);
	template <typename Value>
	MathVec<VType>& operator*=( const std::vector<Value>& vals);
	template <typename Value>
	MathVec<VType>& operator/=( const std::vector<Value>& vals);
	template <typename Value>
	MathVec<VType> operator+ ( const std::vector<Value>& vals) const;
	template <typename Value>
	MathVec<VType> operator- ( const std::vector<Value>& vals) const;
	template <typename Value>
	MathVec<VType> operator* ( const std::vector<Value>& vals) const;
	template <typename Value>
	MathVec<VType> operator/ ( const std::vector<Value>& vals) const;

	template <typename Value>
	MathVec<VType>& operator= ( const Value& val);
	template <typename Value>
	MathVec<VType>& operator+=( const Value& val);
	template <typename Value>
	MathVec<VType>& operator-=( const Value& val);
	template <typename Value>
	MathVec<VType>& operator*=( const Value& val);
	template <typename Value>
	MathVec<VType>& operator/=( const Value& val);
	template <typename Value>
	MathVec<VType> operator+ ( const Value& val) const;
	template <typename Value>
	MathVec<VType> operator- ( const Value& val) const;
	template <typename Value>
	MathVec<VType> operator* ( const Value& val) const;
	template <typename Value>
	MathVec<VType> operator/ ( const Value& val) const;

protected:
	typedef typename std::vector<VType>::iterator Vecite;
	std::vector<VType> _vals;
};

template <typename VType> template <typename Value>
void MathVec<VType>::assign(const std::vector<Value>& vals ){
	_vals.assign( vals.begin(), vals.end() );
}
template <typename VType> template <typename Value>
void MathVec<VType>::assign(const MathVec<Value>& vals ){
	this-> assign( vals.stdvector() );
}
template <typename VType> template <typename Array>
void MathVec<VType>::assign(const Array& vals, unsigned int size ){
	_vals.resize( size );
	for (unsigned int Iv =0; Iv != size; ++Iv) _vals[Iv] = vals[Iv];
}
template <typename VType>
void MathVec<VType>::assign(const VType& val, unsigned int size ){
	_vals.assign( size, val );
}
template <typename VType>
void MathVec<VType>::zeros( unsigned int size ){
	_vals.assign( size, 0 );
}

template <typename VType>
VType MathVec<VType>::prod(){
	VType val=1;
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) val *= (*iv);
	return val;
}
template <typename VType>
VType MathVec<VType>::sum(){
	VType val=0;
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) val += (*iv);
	return val;
}

template <typename VType>
MathVec<VType>& MathVec<VType>::bw_abs(){
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) (*iv) = std::abs((*iv));
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::bw_maxabs(const MathVec<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) _vals[Iv] = std::max( std::abs(vals[Iv]), std::abs(_vals[Iv]) );
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::bw_cutout(const MathVec<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv){
			if ( std::abs(_vals[Iv])<vals[Iv] ) _vals[Iv] = 0;
		}
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::bw_maxdis(const MathVec<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv){
			if( std::abs(vals[Iv]) > std::abs(_vals[Iv]) ) _vals[Iv] = vals[Iv];
		}
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::bw_mindis(const MathVec<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv){
			if ( _vals[Iv]*vals[Iv] > 0 ){
				if(  std::abs(vals[Iv]) < std::abs(_vals[Iv]) ) _vals[Iv] = vals[Iv];
			}
			else{
				_vals[Iv] = 0;
			}

		}
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::bw_max(const MathVec<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv){
			_vals[Iv] = std::max(_vals[Iv], vals[Iv]);
		}
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::bw_min(const MathVec<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv){
			_vals[Iv] = std::min(_vals[Iv], vals[Iv]);
		}
	}
	return *this;
}

template <typename VType> template <typename Value, typename Bool>
void MathVec<VType>::bw_lt(Value val, std::vector<Bool>& ires){
	unsigned int Nval = _vals.size();
	ires.resize(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) ires[Iv] = (_vals[Iv] < val);
}
template <typename VType> template <typename Value, typename Bool>
void MathVec<VType>::bw_gt(Value val, std::vector<Bool>& ires){
	unsigned int Nval = _vals.size();
	ires.resize(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) ires[Iv] = (_vals[Iv] > val);
}
template <typename VType> template <typename Value, typename Bool>
void MathVec<VType>::bw_le(Value val, std::vector<Bool>& ires){
	unsigned int Nval = _vals.size();
	ires.resize(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) ires[Iv] = (_vals[Iv] <= val);
}
template <typename VType> template <typename Value, typename Bool>
void MathVec<VType>::bw_ge(Value val, std::vector<Bool>& ires){
	unsigned int Nval = _vals.size();
	ires.resize(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) ires[Iv] = (_vals[Iv] >= val);
}

// operators to vector
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator=( const std::vector<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) _vals[Iv] = vals[Iv];
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator+=( const std::vector<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) _vals[Iv] += vals[Iv];
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator-=( const std::vector<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) _vals[Iv] -= vals[Iv];
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator*=( const std::vector<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) _vals[Iv] *= vals[Iv];
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator/=( const std::vector<Value>& vals){
	unsigned int Nval = vals.size();
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) _vals[Iv] /= vals[Iv];
	}
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator+ ( const std::vector<Value>& vals) const{
	unsigned int Nval = vals.size();
	MathVec<VType> temp(Nval);
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] + vals[Iv];
	}
	return temp;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator- ( const std::vector<Value>& vals) const{
	unsigned int Nval = vals.size();
	MathVec<VType> temp(Nval);
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] - vals[Iv];
	}
	return temp;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator* ( const std::vector<Value>& vals) const{
	unsigned int Nval = vals.size();
	MathVec<VType> temp(Nval);
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] * vals[Iv];
	}
	return temp;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator/ ( const std::vector<Value>& vals) const{
	unsigned int Nval = vals.size();
	MathVec<VType> temp(Nval);
	if ( assure_samesize( Nval ) ){
		for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] / vals[Iv];
	}
	return temp;
}


// operators to values
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator= (  const Value& val){
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) (*iv) = val;
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator+=(  const Value& val){
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) (*iv) += val;
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator-=(  const Value& val){
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) (*iv) -= val;
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator*=(  const Value& val){
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) (*iv) *= val;
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType>& MathVec<VType>::operator/=(  const Value& val){
	for(Vecite iv=_vals.begin(); iv != _vals.end(); ++iv) (*iv) /= val;
	return *this;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator+ (  const Value& val) const {
	unsigned int Nval = _vals.size();
	MathVec<VType> temp(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] + val;
	return temp;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator- (  const Value& val) const {
	unsigned int Nval = _vals.size();
	MathVec<VType> temp(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] - val;
	return temp;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator* (  const Value& val) const {
	unsigned int Nval = _vals.size();
	MathVec<VType> temp(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] * val;
	return temp;
}
template <typename VType> template <typename Value>
MathVec<VType> MathVec<VType>::operator/ (  const Value& val) const {
	unsigned int Nval = _vals.size();
	MathVec<VType> temp(Nval);
	for(unsigned int Iv=0; Iv != Nval; ++Iv) temp._vals[Iv] = _vals[Iv] / val;
	return temp;
}

template <typename VType>
bool MathVec<VType>::assure_samesize(unsigned int insize) const {
	bool isame = (insize == _vals.size());
	if (!isame) {
		std::cerr<<"ERROR: vector is not aligned."<<_vals.size()<<", "<<insize<<std::endl;
	}
	return isame;

}

#endif /* MATHVEC_H_ */
