#include "Matrix.h"
#include "stdlib.h"
#include <iostream>
#include "MVector.h"
#include "../TensorFunctions/GSymmTensor.h"
#include "../TensorFunctions/GTensor.h"

#define MATVECT_EPSILON 1.0e-15

//===========================================
//
//           class Matrix
//
//-------------------------------------------
//          definition of member-functions
//
//===========================================

//================================================
//  Constructor 1   Default Constructor
//================================================
Matrix::Matrix()
//:mRows(0), mCols(0), mSize(0), mData(0),isSquareMatrix(1)
{
    mRows = 0;
    mCols = 0;
    mSize = 0;
    mData = 0;
    isSquareMatrix = 1;
}

//================================================
//  Constructor 2   using Num of Rows and Num of Cols
//================================================
Matrix::Matrix (int nRows, int nCols):mRows(nRows),mCols(nCols),isSquareMatrix(0){

	mSize = nRows * nCols;
	mData = 0;
	if (mRows == mCols){
		isSquareMatrix = 1;	}

	if (mSize > 0){
		mData = new Scalar[mSize];
//		Scalar Data[mSize];
//		mData = Data;
		Scalar *dataPtr = mData;
		for (int i=0; i < mSize; i++){
			*dataPtr++ = 0.0;
		}

	}
}

//================================================
//  Constructor 3   Copy Constructor
//================================================
Matrix::Matrix(const Matrix &other):isSquareMatrix(0)
{
    mRows = other.mRows;
    mCols = other.mCols;
    mSize = other.mSize;
    mData = 0;
 	if (mRows == mCols)	isSquareMatrix = 1;
   
    if (mSize != 0) {
		mData = new double[mSize];
//    	Scalar Data[mSize];
//    	mData = Data;
		// copy the data
		Scalar *dataPtr = mData;
		Scalar *otherDataPtr = other.mData;
		for (int i=0; i<mSize; i++)
			*dataPtr++ = *otherDataPtr++;
    }
}

/////////// End of Constructor ////////////////////////////////////////
//================================================
//  Destructor 9   
//================================================
Matrix::~Matrix(){

	if(mData != 0){
		delete mData;
	}

}

//================================================
//  SetZero
//================================================
void Matrix::SetZero()
{
	for (int i = 0; i <mRows; i++) {
		for (int j = 0; j < mCols; j++)
			(*this)(i,j) = 0.0;
	}

}

//================================================
//  Tensor2ToMatrix(GTensor&)
//================================================
Matrix Matrix::Tensor2ToMatrix(const GTensor& GNN) const
{
	Matrix result(3,3);
	result(0,0) = GNN.Getm11();
	result(0,1) = GNN.Getm12();
	result(0,2) = GNN.Getm13();
	result(1,0) = GNN.Getm21();
	result(1,1) = GNN.Getm22();
	result(1,2) = GNN.Getm23();
	result(2,0) = GNN.Getm31();
	result(2,1) = GNN.Getm32();
	result(2,2) = GNN.Getm33();
	return result;
}

//================================================
//  SetComponent(int, int , double)
//================================================
void Matrix::SetComponent(int i, int j, double Val)
{
//	(this)(i,j) = Val;
	if (i <= mRows && j <= mCols)
		(*this)(i,j) = Val;
//		mData(i,j) = Val;
}


//================================================
//  SetIdentity(int Dimension) // Create Identity Matrix
//================================================
void Matrix::SetIdentity(int dim)
{
    if (mCols != mRows) {
		std::cerr << "Matrix::SetIdentity(dim): incompatable sizes\n";
		exit(0);
    };
	int theSize;
	if (dim == 0) 
		theSize = mRows;
	else
		theSize = dim;
	for (int i=0; i < theSize; i++)
		(*this)(i,i) = 1.0;
}

//================================================
//  GetPartMatrix(int, int, int, int)  Zero base index
//================================================
Matrix Matrix::GetPartMatrix(int i, int j, int k, int l) const // return Partial Matrix
{
	// Rows from i to j, Cols from k to l
	int newRows = j - i + 1;
	int newCols = l - k + 1;
	Matrix result(newRows, newCols);
	for (int r = 0; r < newRows; r++) {
		for (int c = 0; c < newCols; c++)
			result(r,c) = (*this)(r + i, c + k);

	}
	return result;
}
//================================================
//  SetPartMatrix(int, int, int, int, Matrix)  Zero base index
//================================================
void Matrix::SetPartMatrix(int i, int j, int k, int l, const Matrix other) // Mat(i:j, k:l)
{
	if ((mCols < other.mCols) || (mRows < other.mRows))
		std::cerr << "Matrix::SetPartMatrix(): incompatable sizes\n";
	for (int r = i; r < j+1; r++) {
		for (int c = k; c < l+1 ; c++)
			(*this)(r,c) = other(r-i, c-k);

	}
}

void Matrix::SetSize(int nRows, int nCols){

	// Set size information
	mRows = nRows;
	mCols = nCols;
	mSize = nRows * nCols;
	if(mData != 0){
		delete mData;
	}
	mData = 0;
	if (mRows == mCols){
		isSquareMatrix = 1;
	}

	if (mSize > 0){
		mData = new Scalar[mSize];
//		Scalar Data[mSize];
//		mData = Data;
		Scalar *dataPtr = mData;
		for (int i=0; i < mSize; i++)
			*dataPtr++ = 0.0;
	}
}

Matrix& Matrix::GetTranspose(){

	// Computes the transpose of a given matrix
	Matrix* result = sWorkMatrixRing.Next();
	result->SetSize(mCols, mRows);

	for (int i = 0; i < mRows; i++){
		for (int j = 0; j < mCols; j++)
			(*result)(j,i) = (*this)(i,j);
	}

	return *result;
}

//-------------------------------------------
//    Member Operators
//===========================================
//================================================
//  opeartor = (const Matrix&)
//================================================
Matrix& Matrix::operator=(const Matrix &other)
{
	// first check we are not trying other = other
	if (this == &other)
		return *this;
	if ((mCols != other.mCols) || (mRows != other.mRows)) {
		if (this->mData != 0){
			delete this->mData;
		}
		int theSize = other.mCols*other.mRows;
//		Scalar Data[theSize];
//		mData = Data;
		mData = new Scalar[theSize];
		this->mSize = theSize;
		this->mCols = other.mCols;
		this->mRows = other.mRows;
	}
	// now copy the data
	double *dataPtr = mData;
	double *otherDataPtr = other.mData;
	for (int i=0; i<mSize; i++)
		*dataPtr++ = *otherDataPtr++;
	return *this;
}

//================================================
//  opeartor - ()   itself Negative
//================================================
Matrix Matrix::operator-() const
{
    Matrix result(*this);
//	Scalar *dataPtr = result.mData;
	for (int i = 0; i < mRows; i++){
		for (int j = 0; j < mCols; j++)
			result(i,j) = -(*this)(i,j);
//			*dataPtr++ = -(*mData++);
	}
    return result;
}

//================================================
//  opeartor - (const Matrix&)
//================================================
Matrix Matrix::operator-(const Matrix &M) const
{
	Matrix result(*this);
    if (mCols != M.mCols || mRows != M.mRows) 
    {
		std::cerr << "Matrix::operator-(Matrix): incompatable sizes\n";
		return result;
    }; 
	for (int i = 0; i < mRows; i++){
		for (int j = 0; j < mCols; j++)
			result(i,j) = (*this)(i,j) - M(i,j);
	}
    return result;
}

//================================================
//  opeartor + (const Matrix&)
//================================================
Matrix Matrix::operator+(const Matrix &M) const
{
	Matrix result(*this);
    if (mCols != M.mCols || mRows != M.mRows) 
    {
		std::cerr << "Matrix::operator+(Matrix): incompatable sizes\n";
		return result;
    }; 
    
	for (int i = 0; i < mRows; i++)
	{
		for (int j = 0; j < mCols; j++)
			result(i,j) = (*this)(i,j) + M(i,j);
	}
    return result;
}

//================================================
//  opeartor * (const Matrix&) const 
//================================================
Matrix& Matrix::operator*(const Matrix& M){
	
//    Matrix result(mRows,M.mCols);
    
    Matrix* result = sWorkMatrixRing.Next();
    result->SetSize(mRows, M.mCols);

    if (mCols != M.mRows || result->mRows != mRows) {
		std::cerr << "Matrix::operator*(Matrix): incompatable sizes\n";
		return *result;
    } 

    double *resDataPtr = result->mData;

    int innerDim = mCols;
    int nCols = result->mCols;
    for (int i=0; i<nCols; i++) {
		double *aStartRowDataPtr = mData;
		double *bStartColDataPtr = &(M.mData[i*innerDim]);
		for (int j=0; j<mRows; j++) {
			double *bDataPtr = bStartColDataPtr;
			double *aDataPtr = aStartRowDataPtr +j;
			double sum = 0.0;
			for (int k=0; k<innerDim; k++) {
				sum += *aDataPtr * *bDataPtr++;
				aDataPtr += mRows;
			}
			*resDataPtr++ = sum;
		}
    }
    return *result;
}

//================================================
//  opeartor * (const MVector&) const 
//================================================
MVector Matrix::operator*(const MVector& V) const
{
	
	MVector result(mRows);
	Scalar tmp = 0;
	if (mCols != V.GetSize()) 
	{
		std::cerr << "Matrix::operator*(const MVector&): ERROR Matrix Size Incompatible\n";
		return result;
	} 
    //Scalar *dataPtr = mData;
    for (int i=0; i<mRows; ++i) 
    {
      for (int j=0; j<mCols; ++j)
	  {
		  tmp += (*this)(i,j) * V(j);
	  }
	  result(i) = tmp;
	//	  result(j) += *dataPtr++ * V(i);
	  tmp = 0;
	}
    return result;
}

//================================================
//  opeartor * (const Scalar&) const 
//================================================

Matrix& Matrix::operator*(const Scalar& a){

	Matrix* result = sWorkMatrixRing.Next();

    result->SetSize(mRows,mCols);

    Scalar *resultDataPtr = result->mData;
    Scalar *currentDataPtr = mData;

    for (int i=0; i<mSize; i++)
		*resultDataPtr++ = *currentDataPtr++*a;

    return *result;
}

Matrix& Matrix::operator+(const Scalar& a){

	Matrix* result = sWorkMatrixRing.Next();

    result->SetSize(mRows,mCols);

    Scalar *resultDataPtr = result->mData;
    Scalar *currentDataPtr = mData;

    for (int i=0; i<mSize; i++)
		*resultDataPtr++ = (*currentDataPtr++)+a;

    return *result;
}

//================================================
//  opeartor * (const Scalar&) const 
//================================================
Matrix& Matrix::operator*=(const Scalar fact)
{
	// check if quick return
	if (fact == 1.0)
		return *this;
	Scalar *dataPtr = mData;
	for (int i=0; i<mSize; i++)
		*dataPtr++ *= fact;
	return *this;
}

Matrix& Matrix::operator+= (const Matrix& other){

	// Verify compatable sizes
    if (  (mCols != other.mCols) ||
    	   (mRows != other.mRows) ) {
		std::cerr << "Matrix::operator*(Matrix): incompatable sizes\n";
		return *this;
    }
	for (int i = 0; i < mRows; i++){
		for (int j = 0; j < mCols; j++){
			(*this)(i,j) += other(i,j);
		}
	}
    return *this;
}

//================================================
// friend opeartor * (const Scalar fact, const Matrix& M);
//================================================
Matrix operator*(const Scalar a, const Matrix& M)
{
	Matrix result;
	result = M;
    Scalar *resDataPtr = result.mData;	    
//    int nRows = result.mRows;
//    int nCols = result.mCols;
    for (int i=0; i<result.mRows; i++) 
    {
		for (int j=0; j<result.mCols; j++) 
		{
			*resDataPtr++ *= a;
		}
    }
    return result;
}

//================================================
// friend opeartor * (const MVector& V, const Matrix& M);
//================================================
MVector operator*(const MVector& V, const Matrix& M)
{
	MVector result(M.mCols);
	Scalar tmp = 0;
	if (M.mRows != V.GetSize()) {
		std::cerr << "Matrix::operator*(const MVector&, const Matrix& M): ERROR Size incorrect\n";
		return result;
	} 
    for (int i=0; i<M.mCols; ++i) {
		for (int j=0; j<M.mRows; ++j)
		{
			tmp += (M(j,i) * V(j));
		}
	  result(i) = tmp;
	//	  result(j) += *dataPtr++ * V(i);
	  tmp = 0;
	}
    return result;
}

MVector NewtonRapson(const Matrix& aM, const MVector& aV)
{
	int numSize = aM.GetCols();
	// Be aware that we are dealing with aM
	Matrix* negInvTan = new Matrix(numSize, numSize);
	MVector ans(numSize);
	Scalar det;

	switch(numSize)
	{
 	  case 1:

		ans.SetComponent(0, aV.GetComponent(0)*(1/aM.GetValue(0,0)));
		break;

	  case 2:

		det = (aM.GetValue(0,0)*aM.GetValue(1,1) 
			 - aM.GetValue(0,1)*aM.GetValue(1,0));
		negInvTan->SetComponent(0, 0, aM.GetValue(1,1)*(1/det));
		negInvTan->SetComponent(1, 1, aM.GetValue(0,0)*(1/det));		
		negInvTan->SetComponent(0, 1, aM.GetValue(0,1)*(-1/det));		
		negInvTan->SetComponent(1, 0, aM.GetValue(1,0)*(-1/det));		
		ans = (*negInvTan)*aV;
		break;

	// I should add another case for numSize = 3
	}
	
	delete negInvTan;
	return ans;
}

//   ***********************< Print >************************
//   * Purpose:	Printing out the elements of a matrix       *
//   * Description: 	Uses the file bzzFileOut global     *
//   *              	in utility.hpp                      *
//   * Example: 	A.Print("Matrix A");                    *
//   ********************************************************
void Matrix::Print(){

	std::cout <<"mRows  " << mRows << "  mCols  " << mCols <<std::endl;
	for(int row = 0; row < mRows;++row){
		for(int column = 0;column < mCols;++column){

			Scalar thevalue = (*this)(row, column) ;
			if (std::abs(thevalue)<1.0e-15){
				thevalue = 0;
			}

		    std::cout <<thevalue <<"\t";
		}
		std::cout<<"\n";
	}

}

void Matrix::PrintCol(int colin){

	// Prints the column in as A ROW accross the screen
	for(int row = 0; row < mRows; row++){

		if(row != (mRows-1)){
			std::cout <<(*this)(row, colin) <<"\t";
		}
		else{
			std::cout <<(*this)(row, colin) <<std::endl;
		}

	}

}


////// Check this function out before it is used commented by Wookuen
// LU Decomposition from Mamir C. Shamma PP20
void Matrix::LUDecomp(Matrix& A, IntVector& Index, int numRows, int& rowSwapFlag)
{
	int i, j, k; 
	int iMax=0;   // This should be initialized   <-Why type this comment instead of just initializing the variable???
	double large, sum, z, z2;
	MVector scaleVect(A.GetCols());
	
	/* initialize row interchange flag */
	rowSwapFlag = 1;
	
	/* loop to obtain the scaling element */
	for (i = 0; i < numRows; i++) 
	{
		large = 0;
		for (j = 0; j < numRows; j++) 
		{
			z2 = fabs(A(i, j));
			large = (z2 > large) ? z2 : large;
		}
		/* no non-Zero large value? then exit with an error code */
		//if (large == 0)
		// return matErr_Singular;
		scaleVect[i] = 1 / large;
	}
	
	for (j = 0; j < numRows; j++) 
//	for (j = 1; j < numRows; j++)
		{
		for (i = 0; i < j; i++) 
		{
			sum = A(i, j);
			for (k = 0; k < i; k++)
				sum -= A(i, k) * A(k, j);
			A(i, j) = sum;
		}
		large = 0;
		for (i = j; i < numRows; i++) 
		{
			sum = A(i, j);
			for (k = 0; k < j; k++)
				sum -= A(i, k) * A(k, j);
			A(i, j) = sum;
			z = scaleVect[i] * fabs(sum);
			if (z >= large) 
			{
				large = z;
				iMax = i;
			}
		}
		if (j != iMax) 
		{
			for (k = 0; k < numRows; k++) 
			{
				z = A(iMax, k);
				A(iMax, k) = A(j, k);
				A(j, k) = z;
			}
			rowSwapFlag *= -1;
			scaleVect[iMax] = scaleVect[j];
		}
		Index[j] = iMax;
		
		if (A(j, j) == 0)
			A(j, j) = MATVECT_EPSILON;
			
		if (j != numRows) 
		{
			z = 1 / A(j, j);
			for (i = j+1; i < numRows; i++)
				A(i, j) *= z;
		}
	}
}

void Matrix::LUBackSubst(Matrix& A, IntVector& Index, int numRows, MVector& B)
{
	int i, j, idx, k = -1;
	double sum;
	for (i = 0; i < numRows; i++) {
		idx = Index[i];
		sum = B[idx];
		B[idx] = B[i];
		if (k > -1)
			for (j = k; j < i; j++)
				sum -= A(i, j) * B[j];
		else if (sum != 0)
			k = i;
		B[i] = sum;
	}
	for (i = numRows - 1; i >= 0; i--) {
		sum = B[i];
		for (j = i+1; j < numRows; j++)
			sum -= A(i, j) * B[j];
		B[i] = sum / A(i, i);
	}
}



//void Matrix::LUInverse(Matrix& A, Matrix& InvA, IntVector& Index)
void Matrix::LUInverse(Matrix& A, Matrix& InvA)
{
	Matrix temp = A;
	int numRows = temp.GetRows();
	MVector colVect(numRows);
	IntVector Index(numRows);
	int i, j;
	int myFlag;
	LUDecomp(temp, Index, temp.GetRows(), myFlag);
	for (j = 0; j < numRows; j++) {
		for (i = 0; i < numRows; i++)
			colVect[i] = 0;
		colVect[j] = 1;
		LUBackSubst(temp, Index, numRows, colVect);
		for (i = 0; i < numRows; i++)
			InvA(i, j) = colVect[i];
  }
}

bool Matrix::matrixSolveSymmBandwidthThree(Matrix& RHS){

	// Solves system for this matrix (assumes this is the left hand side)
	// Uses LDL^T decomposition. This is hardwired for a symmetric LHS and bandwidth 3

	// To figure out what is going on, write out L and D and L^T. Solve for coefficients and below is
	// what results

    if (  (mCols != mRows) ) {
		std::cerr << "LHS must be a square matrix\n";
	    // Only returns this if bad size given
	    return false;
    }
    else if(mRows != RHS.mRows) {
    	std::cerr << "Size of RHS is not compatible with the size of  the LHS\n";
        // Only returns this if bad size given
        return false;
    }
    else if(mRows < 2) {
    	std::cerr << "LDLT decomposition failure: less than 2 unknowsn\n";
        // Only returns this if bad size given
        return false;
    }
    else{

    	// Should be correct sizes
    	int i;
    	Scalar B;
    	/*** Visual Studio complained about this: ***
		Scalar D[mRows];
    	Scalar L[mRows-1];
    	Scalar F[mRows];
    	*/
		std::vector<Scalar> D(mRows);
    	std::vector<Scalar> L(mRows-1);
    	std::vector<Scalar> F(mRows);

    	Scalar tolerance = 1.0e-8;

    	// Populate D and L. Initially D is diagonal and L is the off diagonal.
    	D[0] = this->GetValue(0,0);

    	F[0] = RHS(0,0);
    	for ( i = 0; i < (mRows-1); i++){

    		// The diagonal term
    		D[i+1] = this->GetValue(i+1,i+1);

    		// The off diagonal term
    		L[i] = this->GetValue(i+1,i);

    		// RHS
    		F[i+1] = RHS(i+1,0);

    	}

    	// This is the actual D and L values
    	for ( i = 0; i < (mRows-1); i++){
    		B = L[i];
    		if(D[i]<tolerance){
    			// This will be singular
    			return false;
    		}
    		else{
    			L[i] /= D[i];
    		}
    		D[i+1] = D[i+1]-B*L[i];
    	}

    	// Our system is now LDL^T*x = F where x is the unknown and F = RHS. Let z = DL^T*x

    	// solve Lz = F   for z ...(F will become L^-1*F)
    	for ( i = 0; i < (mRows-1); i++){
    		F[i+1] = F[i+1]-L[i]*F[i];
    	}

    	// Now z = DL^T*x. Let y = L^T*x then solve for y. F will become D^-1*F = D^-1*L^-1*RHS
    	F[0] /= D[0];
    	for ( i = 0; i < (mRows-1); i++){
    		F[i+1] = F[i+1]/D[i+1];
    	}

    	// This loop goes in reverse. At the end of this loop the values in F will be the answer.
    	for ( i = (mRows-1); i > 0; i--){
    		F[i-1] = F[i-1]-L[i-1]*F[i];
    	}

    	// Put the answer in the vector in
    	for ( i = 0; i < mRows; i++){
    		RHS(i,0) = F[i];
    	}

    	// if it made it this far then all is good
    	return true;

    }// Closes size check

}// Closes function

Matrix& Matrix::Inverse3x3(bool& AllGood){

	// Computes the inverse of a 3x3
	AllGood = true;

	Matrix* result = sWorkMatrixRing.Next();
    result->SetSize(mRows,mCols);

	// Make sure right size
	if( (mRows != 3) && (mCols != 3)){
		AllGood = false;
		std::cout<<"Bad size given for Matrix::Inverse3x3()"<<std::endl;
	}
	else{

		Scalar tolerance = 1.0e-8;

		// Get components
		Scalar m11, m12, m13, m21, m22, m23, m31, m32, m33, det;

		m11 = mData[0];
		m12 = mData[1];
		m13 = mData[2];
		m21 = mData[3];
		m22 = mData[4];
		m23 = mData[5];
		m31 = mData[6];
		m32 = mData[7];
		m33 = mData[8];

		det  =  m11*(m33*m22-m32*m23)-m21*(m33*m12-m32*m13)+m31*(m23*m12-m22*m13);

//		std::cout<<det<<std::endl;
//		this->Print();

		if(det<tolerance){

//			std::cout<<det<<std::endl;

			// This is singular
			AllGood = false;

		}
		else{

			// Set result with the correct values
			result->SetComponent( 0,0, (m22*m33-m23*m32)/det );
			result->SetComponent( 0,1, (m13*m32-m12*m33)/det );
			result->SetComponent( 0,2, (m12*m23-m13*m22)/det );

			result->SetComponent( 1,0, (m23*m31-m21*m33)/det);
			result->SetComponent( 1,1, (m11*m33-m13*m31)/det );
			result->SetComponent( 1,2, (m13*m21-m11*m23)/det );

			result->SetComponent( 2,0, (m21*m32-m22*m31)/det );
			result->SetComponent( 2,1, (m12*m31-m11*m32)/det );
			result->SetComponent( 2,2, (m11*m22-m12*m21)/det );
		}

	}
	return *result;
}

Matrix& Matrix::LUInverse(){

	int numRows = this->GetRows();
//	Matrix result(numRows, numRows);  // Square Matrix Again

	Matrix* result = sWorkMatrixRing.Next();
    result->SetSize(mRows,mCols);

	Matrix temp = *this;
	MVector colVect(numRows);
	IntVector Index(numRows);

	int i, j;
	int myFlag;
	LUDecomp(temp, Index, numRows, myFlag);
	for (j = 0; j < numRows; j++) {
		for (i = 0; i < numRows; i++)
			colVect[i] = 0;
		colVect[j] = 1;
		LUBackSubst(temp, Index, numRows, colVect);
		for (i = 0; i < numRows; i++)
			(*result)(i, j) = colVect[i];
	}
	return *result;
}
// Using Static Ring 

//Matrix& Matrix::LUInverse(){
//
//	Matrix* result = Matrix::sWorkMatrixRing.Next();
//	result->resize(this->GetRows(), this->GetCols());
//	Matrix temp = *this;
//	int numRows = this->GetRows();
//	MVector colVect(numRows);
//	IntVector Index(numRows);
//	int i, j;
//	int myFlag;
//	LUDecomp(temp, Index, numRows, myFlag);
//	for (j = 0; j < numRows; j++) {
//		for (i = 0; i < numRows; i++)
//			colVect[i] = 0;
//		colVect[j] = 1;
//		LUBackSubst(temp, Index, numRows, colVect);
//		for (i = 0; i < numRows; i++)
//			(*result)(i, j) = colVect[i];
//	}
//	return *result;
//}


//================================================
//  	static ring stuff
//================================================
//---Static work space


MatrixRing Matrix::sWorkMatrixRing;

MatrixRing::MatrixRing(){
	int ringCirc = MATRIX_RING_DEPTH;

	for(int i = 0; i < ringCirc; i++)
		 mElts[i] = new MatrixRingElt();

	for(int j = 0; j < ringCirc - 1; j++)
		mElts[j]->SetNext(*mElts[j+1]);
	//---Close the loop
		mElts[ringCirc-1]->SetNext(*mElts[0]);
		mCurrentElt = mElts[0];
}

MatrixRing::~MatrixRing(){
	int ringCirc = MATRIX_RING_DEPTH;

	for(int i = 0; i < ringCirc; i++)
		delete mElts[i];
}


Matrix* MatrixRing::Next(){
	mCurrentElt = mCurrentElt->GetNext();
	return &(mCurrentElt->GetMatrix());
}



