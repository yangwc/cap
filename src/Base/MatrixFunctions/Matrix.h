#ifndef MATRIX_H_
#define MATRIX_H_

#include <stdio.h>
#include <string>
#include "../TensorFunctions/TensorDefs.h"

class IntVector;
class GTensor;
class GSymmTensor;

class MVector;
class MatrixRing;
class Matrix 
{
	private:
	
		Scalar* mData;
		int mRows;
		int mCols;
		int mSize;
		int isSquareMatrix;   // 1 if N*N   0 if N*M
	    // prepares assignments
		void LUDecomp(Matrix& A, IntVector& Index, int numRows, int& rowSwapFlag);
		void LUBackSubst(Matrix& A, IntVector& Index, int numRows, MVector& B);
	
	public:


		static MatrixRing sWorkMatrixRing;
	
	    Matrix();    
		Matrix (int nRows, int nCols);
	    Matrix(const Matrix &M);    
		~Matrix ();

		// Added (or Modified) by Carter
		void SetSize(int nRows, int nCols);
		Matrix& GetTranspose();
		Matrix& operator*(const Matrix& other);
		Matrix& operator*(const Scalar& other);
		Matrix& operator+(const Scalar& other);
		Matrix& operator+= (const Matrix& other);
		Matrix& LUInverse();
		bool matrixSolveSymmBandwidthThree(Matrix& RHS);
		Matrix& Inverse3x3(bool& AllGood);
		// End added by Carter

		// IF USING THE FUNCTIONS BELOW BE SURE TO ADD THE RING FUNCTIONS TO THEM!

		void SetZero();
	    Matrix Tensor2ToMatrix(const GTensor& GNN) const;

	    void SetComponent(int i, int j, double Val);  // Set components by Scalar value

		void SetIdentity(int dim);
		////////////////// initRowIndex, endRowIdnex, initColIndex, endColIndex /////
		Matrix GetPartMatrix(int i, int j, int k, int l) const ;  // return Partial Matrix
		void SetPartMatrix(int i, int j, int k, int l, const Matrix other); // Mat(i:j, k:l) = other
		void Resize (int NewRows, int NewCols);
		int GetRows() const { return mRows; }
		int GetCols() const { return mCols; }
		int GetSize() const { return mSize; }
		Scalar GetValue(int i, int j) const {return (*this)(i,j);} 
	   // operators
		Matrix& operator=(const Matrix &other);
		Matrix operator-() const;
		Matrix operator-(const Matrix &M) const;
		Matrix operator+(const Matrix &M) const; //Wookuen Shin added on 8/24/05

		MVector operator*(const MVector& V) const;

		Matrix& operator*=(const Scalar fact);

		friend Matrix operator*(const Scalar fact, const Matrix& M);
		friend MVector operator*(const MVector& V, const Matrix& M);
		friend MVector NewtonRapson(const Matrix& aM, const MVector& aV);

		void LUInverse(Matrix& A, Matrix& InvA);

		void Print();
		void PrintCol(int colin);
	    inline double &operator()(int row, int col);
	    inline double operator()(int row, int col) const;
	
};

inline double &
Matrix::operator()(int row, int col)
{ 
  return mData[col*mRows + row];
}

inline double 
Matrix::operator()(int row, int col) const
{ 
  return mData[col*mRows + row];
}

#define MATRIX_RING_DEPTH 50

//----------
class MatrixRingElt{
public:
	MatrixRingElt(){;};
	//~SymmTensorRingElt();
	void SetNext(MatrixRingElt& next)
		{mNextRingElt = &next;}
	Matrix& GetMatrix()
		{return mMyMatrix;}
	MatrixRingElt* GetNext()
		{return mNextRingElt;}

protected:
//	Matrix mMyMatrix(MATRIX_RING_DEPTH);
	Matrix mMyMatrix;
	MatrixRingElt* mNextRingElt;

};

class MatrixRing {
public:
	MatrixRing();
	~MatrixRing();
	Matrix* Next();

protected:
	MatrixRingElt* mCurrentElt;
	MatrixRingElt* mElts[MATRIX_RING_DEPTH];
};


#endif /*Matrix .h*/

