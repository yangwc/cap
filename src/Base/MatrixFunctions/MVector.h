#ifndef MVECTOR_H_
#define MVECTOR_H_

#include "../TensorFunctions/GSymmTensor.h"
#include "Matrix.h"

class IntVector;
class MVector
{
  public:
	MVector();
    MVector(int MaxSize);
	MVector(const GSymmTensor& aST2, const Scalar& s);
	MVector(const MVector &other);
    ~MVector();
	void SetComponent(int i, Scalar Val) 
	{ (*this)(i) = Val;}
	void SetComponent(const GSymmTensor& aST2, const Scalar& S); 
    void ReSize(int NewSize);
    void SetZero(int vecSize); //WooKuen added this function on 07/11/07
	MVector& operator=(const MVector& V);

    double& operator [](int index) const
      { return mData[index]; }
    double& operator ()(int index) const
      { return mData[index]; }
    int GetSize() const { return mSize; }
    int checkIndex(int index)
      { return  (index >= 0 && index < mSize) ? 1 : 0; }
	Scalar GetNorm() const;
	MVector extract(int i, int j) const;
	Scalar GetComponent(int i) const { return (*this)(i); }
	MVector GetPartVector(int i, int j) const;  // return Partial MVector
	void SetPartVector(int i, int j, const MVector other); // MVector(i:j) = other

	Scalar Sum() const;
	void Print();
	
	MVector &operator-=(const MVector &other);
    MVector operator-(const MVector &V) const;
	Scalar operator*(MVector& other) const;
	Scalar operator*(IntVector& v2) const;
	MVector &operator*=(double fact);
    MVector operator*(double fact) const;
    MVector operator/(double fact) const;
	MVector operator+(const MVector& v) const;
	MVector& operator+=(const MVector &other);
	Matrix	operator&(const MVector other);

	friend MVector operator*(Scalar fact, MVector& v);

  protected:
    int mSize;
    double* mData;
};




class IntVector
{
  public:
	IntVector();
    IntVector(int MaxSize);
	IntVector(int , int , int ,int , int , int ); // Index for Mixed Driver
	IntVector(const IntVector &other);   //Copy Constructor
   ~IntVector()
      { delete [] mData; }
 	void SetComponent(int i, int Val) 
		{ (*this)(i) = Val;}
    void ReSize(int NewSize);
    int& operator [](int index) const { return mData[index]; }
    int& operator ()(int index) const { return mData[index]; }
    int checkIndex(int index) { return  (index >= 0 && index < mSize) ? 1 : 0; }
    int getSize()  { return mSize; }
	IntVector &operator*=(int fact);
    IntVector operator*(int fact) const;
    MVector operator*(double fact) const;
	int Sum() const; 
    void Print();

  protected:
    int mSize;
    int* mData;
};

#endif
