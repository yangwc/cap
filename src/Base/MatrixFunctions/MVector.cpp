#include "MVector.h"
#include <iostream>
#include <stdlib.h>

MVector::MVector():mSize(0), mData(0)
{
    // does nothing
}

MVector::MVector (int nSize):mSize(nSize)
{
	mData = new Scalar[mSize];
	Scalar *dataPtr = mData;
	for (int i=0; i < mSize; i++)
			*dataPtr++ = 0.0;
}

MVector::MVector(const GSymmTensor& aST2, const Scalar& s)
{
	mSize = 7;
	mData = 0;
	mData = new Scalar[mSize];
	Scalar *dataPtr = mData;
	for (int i=0; i < mSize; i++)
		*dataPtr++ = 0.0;
	(*this)(0) = aST2.Getm11();
	(*this)(1) = aST2.Getm22();
	(*this)(2) = aST2.Getm33();
	(*this)(3) = aST2.Getm12();
	(*this)(4) = aST2.Getm13();
	(*this)(5) = aST2.Getm23();
	(*this)(6) = s;
}

MVector::MVector(const MVector &other)
{
    mSize = other.mSize;
    mData = 0;
    if (mSize != 0) {
		mData = new double[mSize];
		// copy the data
		Scalar *dataPtr = mData;
		Scalar *otherDataPtr = other.mData;
		for (int i=0; i<mSize; i++)
			*dataPtr++ = *otherDataPtr++;
    }

}


MVector::~MVector()
{ 
	if (mData != 0) 
		delete [] mData; 
}

Scalar MVector::GetNorm() const
{
	Scalar result = 0;
	for (int i=0; i < mSize; i++)
		result += (*this)(i) * (*this)(i);
	return sqrt(result);
}

void
MVector::SetComponent(const GSymmTensor& aST2, const Scalar& s)
{
	// flush the all data field
	Scalar *dataPtr = mData;
	for (int i=0; i < mSize; i++)
		*dataPtr++ = 0.0;
	// Assign new data
	this->SetComponent(0, aST2.Getm11());
	this->SetComponent(1, aST2.Getm22());
	this->SetComponent(2, aST2.Getm33());
	this->SetComponent(3, aST2.Getm12());
	this->SetComponent(4, aST2.Getm13());
	this->SetComponent(5, aST2.Getm23());
	this->SetComponent(6, s);
//	(*this)(0) = aST2.Getm11();
//	(*this)(1) = aST2.Getm22();
//	(*this)(2) = aST2.Getm33();
//	(*this)(3) = aST2.Getm12();
//	(*this)(4) = aST2.Getm13();
//	(*this)(5) = aST2.Getm23();
//	(*this)(6) = s;
}

// destroy old data and set zeros using new size
void MVector::ReSize(int NewSize)
{
	delete [] mData;
	mSize = NewSize;
    mData = new Scalar[mSize];
	Scalar *dataPtr = mData;
	for (int i=0; i < mSize; i++)
			*dataPtr++ = 0.0;
}

//** WooKuen Added this funciton
void MVector::SetZero(int vecSize)
{
	// destroy old data and set zeros
	mSize = vecSize;
	delete[] mData;
	
    mData = new Scalar[mSize];
	Scalar *dataPtr = mData;
	for (int i=0; i < mSize; i++)
			*dataPtr++ = 0.0;	
}

MVector&
MVector::operator-=(const MVector &other)
{
	if (mSize != other.mSize) {
		std::cerr << "MVector::operator-(MVector): incompatable sizes\n";
		return *this;
	}
	for (int i=0; i<mSize; i++)
		mData[i] -= other.mData[i];
	return *this;    
}

MVector&
MVector::operator=(const MVector& V) 
{
	// first check we are not trying other = other
	if (this == &V)
		return *this;
	if (mSize != V.mSize) {
		if (this->mData != 0)
			delete [] this->mData;
		int theSize = V.mSize;
		mData = new Scalar[theSize];
		this->mSize = theSize;
	}
	// now copy the data
	double *dataPtr = mData;
	double *otherDataPtr = V.mData;
	for (int i=0; i<mSize; i++)
		*dataPtr++ = *otherDataPtr++;
	return *this;
}

MVector MVector::operator-(const MVector &b) const
{
	if (mSize != b.mSize) {
		std::cerr << "MVector::operator-(MVector): incompatable sizes\n";
		return *this;
	}
	MVector result(*this);
	result -= b;
	return result;
}

MVector &MVector::operator*=(double fact)
{
	for (int i=0; i<mSize; i++)
		mData[i] *= fact;
	return *this;
}


MVector MVector::operator*(double fact) const
{
	MVector result(*this);
    result *= fact;
    return result;
}

MVector MVector::operator/(double factor) const
{
	for (int i=0; i<mSize; i++)
		mData[i] /= factor;
	return *this;
}

Scalar MVector::operator*(MVector& v2) const
{
	Scalar result = 0;
	if (mSize != v2.mSize) {
		std::cerr << "MVector::operator*(MVector): ERROR Size incompatiblet\n";
    	exit(0);
	} else {
		for (int i=0; i < mSize; i++)
			result += (*this)(i) * v2(i);
	}
	return result;
}

Scalar MVector::operator*(IntVector& v2) const
{
	Scalar result = 0;
	if (mSize != v2.getSize()) {
		std::cerr << "MVector::operator*(MVector): ERROR Size incompatiblet\n";
    	exit(0);
	} else {
		for (int i=0; i <mSize; i++)
			result += (*this)(i) * (double)v2(i);
	}
	return result;
}

MVector MVector::operator+(const MVector &b) const
{
    MVector result(*this);
	// check new MVector of correct size
	if (result.GetSize() != mSize) {
		std::cerr << "MVector::operator+(MVector): ERROR Size incompatiblet\n";
    	exit(0);
		return result;
	}
	result += b;
	return result;
}

MVector & MVector::operator+=(const MVector &other)
{
	for (int i=0; i<mSize; i++)
		mData[i] += other.mData[i];
	return *this;	    
}

///////////////////////////
MVector operator*(Scalar fact, MVector& v)
{
	MVector result(v.mSize);
    // check if quick return
    if (fact == 1.0)
		return v;
//	Scalar *dataPtr = result.mData;
//	for (int i=0; i<result.mSize; i++)
//		*dataPtr++ *= fact;
//	Scalar *dataPtr = result.mData;
	for (int i=0; i<result.mSize; i++)
		result(i) = fact * v(i);

	return result;
}


//** Wookuen Shin added on 8/24/05
// Outer Product
Matrix MVector::operator& (MVector other)
{
	Matrix result(mSize, other.mSize);
	double matrixEntry;
	for(int i = 0; i < mSize; i++)
	{
		for(int j = 0; j < other.mSize; j++)
		{
			matrixEntry = mData[i]*other.mData[j];
			result.SetComponent(i, j, matrixEntry);
		}	
	}

	return result;
}

MVector MVector::extract(int i, int j) const
{
	int newSize = j - i + 1;
	MVector result(newSize);
	for (int k=0; k < newSize; k++)
	{
		result(k) = (*this)(k);
	}
	return result;
}

Scalar MVector::Sum() const
{
	Scalar result=0.0;
	double *dataPtr = mData;
	for (int i=0; i<mSize; i++)
		result += *dataPtr++;
	return result;
}


MVector MVector::GetPartVector(int i, int j) const // return Partial MVector
{
	int newSize = j - i + 1;
	MVector result(newSize);
	for (int n = 0; n < newSize; n++) {
			result(n) = (*this)(n + i);
	}
	return result;
}
void MVector::SetPartVector(int i, int j, const MVector other) // MVector(i:j) = other
{
	if (mSize < other.mSize)
		std::cerr << "MVector::SetPartVector(): incompatable sizes\n";
	for (int n = i; n < j+1; n++) {
			(*this)(n) = other(n-i);
	}
}


void MVector::Print()
{
//	printf("\nSize %d", mSize);
	for(int i = 0; i < mSize; ++i)
	{
//		printf(" %5.7f",(*this)(i));
	    std::cout <<(*this)(i) <<"\t";
	}
//	printf("\n");
	std::cout<<"\n";
}


////////////////   InvVector Class //////////////////////////
//
//
/////////////////////////////////////////////////////////////

IntVector::IntVector()
:mSize(0), mData(0)
{
    // does nothing
}

IntVector::IntVector (int nSize):mSize(nSize)
{
	mData = new int[mSize];
	int *dataPtr = mData;
	for (int i=0; i < mSize; i++)
			*dataPtr++ = 0;

}


IntVector::IntVector(int i1, int i2, int i3,int i4, int i5, int i6)
{
	mSize = 6;
    mData = new int[mSize];
	int *dataPtr = mData;
	for (int i=0; i < mSize; i++)
		*dataPtr++ = 0;
	(*this)(0) = i1;
	(*this)(1) = i2;
	(*this)(2) = i3;
	(*this)(3) = i4;
	(*this)(4) = i5;
	(*this)(5) = i6;

}
// Copy Constructor
IntVector::IntVector(const IntVector &other)
{
    mSize = other.mSize;
    mData = 0;
    if (mSize != 0) {
		mData = new int[mSize];
		// copy the data
		int *dataPtr = mData;
		int *otherDataPtr = other.mData;
		for (int i=0; i<mSize; i++)
			*dataPtr++ = *otherDataPtr++;
    }

}

// destroy old data and set zeros using new size
void IntVector::ReSize(int NewSize)
{
	delete [] mData;
	mSize = NewSize;
    mData = new int[mSize];
	int *dataPtr = mData;
	for (int i=0; i < mSize; i++)
			*dataPtr++ = 0;
}

IntVector &IntVector::operator*=(int fact)
{
	for (int i=0; i<mSize; i++)
		mData[i] *= fact;
	return *this;
}


IntVector IntVector::operator*(int fact) const
{
	IntVector result(*this);
    result *= fact;
    return result;
}


MVector IntVector::operator*(double fact) const
{
	MVector result(mSize);
	int *dataPtr = mData;
	for (int i=0; i < mSize; i++)
		result(i) = (double)(*dataPtr++) * fact;

    return result;
}

int IntVector::Sum() const
{
	int result=0;
	int *dataPtr = mData;
	for (int i=0; i<mSize; i++)
		result += *dataPtr++;
	return result;
}

void IntVector::Print()
{
//	printf("\nSize %d", mSize);
	std::cout <<"mSize  " << mSize<< std::endl;
	for(int i = 0; i < mSize; ++i)
	{
//		printf(" %5i",(*this)(i));
	    std::cout <<(*this)(i) <<"\t";
	}
//	printf("\n");
	std::cout<<"\n";
	std::cout<< std::flush;
}
