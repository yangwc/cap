#include <math.h>

typedef double Scalar;

#ifndef False
#define False 0
#endif

#ifndef True
#define True 1
#endif 

#ifdef _win32_
	typedef BOOL Boolean;
#else
	typedef unsigned char 	Boolean;
#endif

#ifdef _win32_
#define AbsVal fabs
#else
#define AbsVal ::fabs
#endif
