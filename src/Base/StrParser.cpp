/**************************************************
 * StrParser.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (Jan 24, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.01.28)
 **************************************************/

#include "StrParser.h"
#include <iostream>
#include <ciso646>

StrParser::StrParser() {
	_mgins = " \t\n\r";
	_seps  = " ,\t";
}

const std::vector<std::string>& StrParser::split(const std::string& str){
	std::vector<std::string>& strs  = _temp_strs;
	strs.clear();
	const std::string& line = this -> clean(str);
	unsigned int Ie, Ll = line.size();
	for (unsigned int Is=0; Is < Ll; ++Is){
		if( _seps.find(line[Is]) == std::string::npos ){
			for (Ie=Is+1; Ie != Ll; ++Ie){
				if( _seps.find(line[Ie]) != std::string::npos ) break;
			}
			strs.push_back( line.substr(Is,(Ie-Is)) );
			Is = Ie;
		}
	}
	return strs;
}

const std::string& StrParser::clean(const std::string& str){

	unsigned int Lstr = str.size();
	unsigned int Is;
	for (Is=0; Is != Lstr; ++Is){
		if( _mgins.find( str[Is] ) == std::string::npos ) break;
	}
	if (Is == Lstr){
		_temp_str = "";
	}
	else{
		unsigned int Lline;
		for (Lline = Lstr-Is; Lline != 1; --Lline){
			if( _mgins.find( str[ Is+Lline-1 ] ) == std::string::npos ) break;
		}
		_temp_str = str.substr(Is,Lline);
	}
	return _temp_str;
}

const std::string& StrParser::uppercase(const std::string& str){
	_temp_str = str;
	int ichar;
	for (unsigned int i=0; i!= _temp_str.size(); ++i){
		ichar = (int) _temp_str[i];
		if (ichar > 96 and ichar < 123){
			ichar -= 32;
			_temp_str[i] = (char) ichar;
		}
	}
	return _temp_str;
}

const std::string& StrParser::lowercase(const std::string& str){
	_temp_str = str;
	int ichar;
	for (unsigned int i=0; i!= _temp_str.size(); ++i){
		ichar = (int) _temp_str[i];
		if (ichar > 64 and ichar < 91){
			ichar += 32;
			_temp_str[i] = (char) ichar;
		}
	}
	return _temp_str;
}
