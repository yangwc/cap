#ifndef GTENSOR_H
#define GTENSOR_H

#include "GSymmTensor.h"
//=================================================
//
//  CLASS->GTensor
//    - A class to build up a second order tensor 
//
//=================================================

class GTensor 
{

 public:
  	Scalar m11, m12, m13, m21, m22, m23, m31, m32, m33;

 public:
   	static GTensorRing sWorkTensorRing;
  	static GSymmTensorRing sWorkSymmTensorRing;
  	static Scalar	sInvertTol;
  	
  	// Constructors
	GTensor(char *);
 	GTensor() {m11=0.0; m12=0.0; m13=0.0; m21=0.0; 
 			   m22=0.0; m23=0.0; m31=0.0; m32=0.0; m33=0.0;}
  	GTensor(const Scalar& f11, const Scalar& f12, const Scalar& f13, 
  			const Scalar& f21, const Scalar& f22, const Scalar& f23,
	  		const Scalar& f31, const Scalar& f32, const Scalar& f33)
			{m11=f11; m12=f12; m13=f13; m21=f21;
			 m22=f22; m23=f23; m31=f31; m32=f32; m33=f33;}
  	GTensor(GVector& aGV1,GVector& aGV2);

	  inline const GTensor& admin_dumperror(const Scalar tol){
		  if ( std::abs(m11) < tol ) m11 = 0.;
		  if ( std::abs(m12) < tol ) m12 = 0.;
		  if ( std::abs(m13) < tol ) m13 = 0.;
		  if ( std::abs(m21) < tol ) m21 = 0.;
		  if ( std::abs(m22) < tol ) m22 = 0.;
		  if ( std::abs(m23) < tol ) m23 = 0.;
		  if ( std::abs(m31) < tol ) m31 = 0.;
		  if ( std::abs(m32) < tol ) m32 = 0.;
		  if ( std::abs(m33) < tol ) m33 = 0.;

		  return *this;
	  }
	  inline const GTensor& admin_dumperror(const GTensor& tols){
		  if ( std::abs(m11) < tols.m11 ) m11 = 0.;
		  if ( std::abs(m12) < tols.m12 ) m12 = 0.;
		  if ( std::abs(m13) < tols.m13 ) m13 = 0.;
		  if ( std::abs(m21) < tols.m22 ) m21 = 0.;
		  if ( std::abs(m22) < tols.m22 ) m22 = 0.;
		  if ( std::abs(m23) < tols.m23 ) m23 = 0.;
		  if ( std::abs(m31) < tols.m22 ) m31 = 0.;
		  if ( std::abs(m32) < tols.m22 ) m32 = 0.;
		  if ( std::abs(m33) < tols.m33 ) m33 = 0.;

		  return *this;
	  }

	  const GTensor& abs_max(const GTensor& aten) const;
	  Scalar abs_max() const;
	  const GTensor& abs() const;

  	// Writers
	void SetComponents(const Scalar& f11, const Scalar& f12, const Scalar& f13,
					   const Scalar& f21, const Scalar& f22, const Scalar& f23,
					   const Scalar& f31, const Scalar& f32, const Scalar& f33)
			{m11=f11; m12=f12; m13=f13; m21=f21; 
			 m22=f22; m23=f23; m31=f31; m32=f32; m33=f33;}

	void Setm11(Scalar aS) {m11 = aS;}
  	void Setm12(Scalar aS) {m12 = aS;}
  	void Setm13(Scalar aS) {m13 = aS;}
	void Setm21(Scalar aS) {m21 = aS;}
  	void Setm22(Scalar aS) {m22 = aS;}
  	void Setm23(Scalar aS) {m23 = aS;}
	void Setm31(Scalar aS) {m31 = aS;}
  	void Setm32(Scalar aS) {m32 = aS;}
  	void Setm33(Scalar aS) {m33 = aS;}

	void AddTom11(Scalar aS) {m11 += aS;}
  	void AddTom12(Scalar aS) {m12 += aS;}
  	void AddTom13(Scalar aS) {m13 += aS;}
	void AddTom21(Scalar aS) {m21 += aS;}
  	void AddTom22(Scalar aS) {m22 += aS;}
  	void AddTom23(Scalar aS) {m23 += aS;}
	void AddTom31(Scalar aS) {m31 += aS;}
  	void AddTom32(Scalar aS) {m32 += aS;}
  	void AddTom33(Scalar aS) {m33 += aS;}

  	void AddDiag( const Scalar& aS) { m11 += aS; m22 += aS; m33 += aS;}
  	void AddDiag( const GVector& aV) { m11 += aV.GetxComp(); m22 += aV.GetyComp(); m33 += aV.GetzComp();}
	const GVector& Diag() const { return GVector::sWorkVectorRing.Next()->SetComponents(m11,m22,m33); }
	inline void Diag(const GVector& aGV){ m11 = aGV.GetxComp(); m22 = aGV.GetyComp(); m33 = aGV.GetzComp(); }
	inline void Diag(const Scalar& aS){ m11 = m22 = m33 = aS; }

	const GTensor& deDiag() const;
//	inline void Diag(const GVector& aGV){ m11 = aGV.GetxComp(); m22 = aGV.GetyComp(); m33 = aGV.GetzComp(); }

  	// Wookuen added on 03/13/08
  	void SetComponent(int whichComp, Scalar val); 
  	void SetZeros();
  	void SetEye();

	// Readers
  	const Scalar& Getm11() const {return m11;}
  	const Scalar& Getm12() const {return m12;}
  	const Scalar& Getm13() const {return m13;}
  	const Scalar& Getm21() const {return m21;}
  	const Scalar& Getm22() const {return m22;}
  	const Scalar& Getm23() const {return m23;}
  	const Scalar& Getm31() const {return m31;}
  	const Scalar& Getm32() const {return m32;}
  	const Scalar& Getm33() const {return m33;}
  	
  	// WooKuen added on April 25, 2008
  	GVector GetRow(int row);
  	GVector GetCol(int col);
	
	// Tensor & Matrix operations
  	Scalar GetTrace() const;
	GTensor& GetTranspose();
  	GSymmTensor& GetSymmPart() const;
  	GTensor& GetAntiSymmPart() const;
  	GSymmTensor& DotItsTranspose() const;

  	GSymmTensor& DotTransposeRight() const;
  	GSymmTensor& DotTransposeLeft() const;

  	Scalar	GetDet() const ; //** Wookuen added this on 07/10/07
  	Scalar  DoubleDot(const GTensor& aT) const; //** Wookuen added this on 08/01/07
  	GTensor& GetInverse(bool& badinverse); //** Wookuen added this on 08/16/07
  	inline GTensor& GetInverse(){bool istat; return this -> GetInverse(istat);}
  	// Operators
  	GTensor& operator= (const GTensor& aT);
	GTensor& operator= (const GSymmTensor& aGST);
	GTensor& operator+ (const GTensor& aT) const;
	GTensor& operator+= (const GTensor& aT);
	GTensor& operator- (const GTensor& aT) const;
	GTensor& operator* (const Scalar& aS) const;
	GVector	 operator* (const GVector& aGV) const;
	GTensor& operator* (const GTensor& aT) const;
	GTensor& operator*= (const GTensor& aT);
	GTensor& operator*= (const Scalar& aS);
	GTensor& operator* (const GSymmTensor& aST) const;
	GTensor& operator/ (const Scalar& a) const;
	GTensor& operator/= (const Scalar& aS);

	bool any_GT(const Scalar& aS) const;
	bool any_LT(const Scalar& aS) const;

	void Print(std::ostream* file) const;
    void Print() const{std::cout<<m11<<"\t"<<m12<<"\t"<<m13<<"\t"<<m21<<"\t"<<m22<<"\t"<<m23<<"\t"<<m31<<"\t"<<m32<<"\t"<<m33<<"\t"<<std::endl;}
    void PrintBin(std::ostream* file) const;

	// Friend Functions
    friend  GVector& operator* (const GVector& av, const GTensor& aT);
	friend  GTensor& operator* (const Scalar& aS, const GTensor& aT);
	friend  GTensor& operator*= (const Scalar& aS, const GTensor& aT);

	// Carter
	GTensor& Transpose();

	void DotPlusAssign(GVector& aGV, GVector& target);

	void DotTransposeMinusAssign(GVector& aGV, GVector& target);

	GTensor& Equals(const GTensor &aGT);

	void MinusEqualTransposeDot(GTensor& a, GTensor& b);

	void DotMinusAssign(GVector& aGV, GVector& target);
};

#define TENSOR_RING_DEPTH 64

class TensorRingElt
{
	public:
		TensorRingElt(){;};
		//~TensorRingElt();
		void SetNext(TensorRingElt& next)
			{mNextRingElt = &next;}
		GTensor& GetTensor()
			{return mMyTensor;}
		TensorRingElt* GetNext()
			{return mNextRingElt;}
			
	protected:
		GTensor mMyTensor;
		TensorRingElt* mNextRingElt;
};

class GTensorRing 
{
	public:
		GTensorRing();
		~GTensorRing();
		GTensor* Next();
	protected:
		TensorRingElt* mCurrentElt;
		TensorRingElt*	mElts[TENSOR_RING_DEPTH];
};


#endif
