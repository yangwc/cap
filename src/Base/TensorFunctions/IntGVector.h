#ifndef INTGVECTOR_H
#define INTGVECTOR_H

class GVector;

class IntGVector
{
  private:

	int miX, miY, miZ;

  public:

	// Constructors & Destructor	
	IntGVector()	{miX = 0; miY = 0; miZ = 0;}
	IntGVector(int ix, int iy, int iz) {miX = ix; miY = iy; miZ = iz;}
	IntGVector(int unitDir);
	~IntGVector (){;}

	//---Readers
	int GetxComp() const {return miX;}
	int GetyComp() const {return miY;}
	int GetzComp() const {return miZ;}
	int GetComp(int whichDir) const;
	inline void GetComp(int& x, int& y, int& z) const {x= miX; y= miY; z= miZ;}
				
	// Writers
	void SetxComp(int i) {miX = i;}
	void SetyComp(int i) {miY = i;}
	void SetzComp(int i) {miZ = i;}
	void SetComp(int whichDir, int val); // WooKuen Added on 8/27/07
	inline void SetComponents(int i1, int i2, int i3) {miX = i1; miY = i2; miZ = i3;}
	inline void AddComponents(int i1, int i2, int i3) {miX += i1; miY += i2; miZ += i3;}
	void SetZeros() {miX = 0; miY = 0; miZ = 0;}
	void SetOnes(int unitDir);


	//---Basic algebra
	int operator [] (int Idir) const{ return this -> GetComp(Idir); }
	IntGVector& operator *=  (const IntGVector& iGV);
	IntGVector& operator=(int val);
	IntGVector& operator=(const IntGVector& iGV);
	IntGVector  operator+  (const IntGVector& iGV) const;
	IntGVector& operator+= (const IntGVector& iGV);
	IntGVector  operator-  (const IntGVector& iGV) const;
	IntGVector& operator-= (const IntGVector& iGV);
	IntGVector& operator-= (const int& val);
	GVector 	operator*  (const double& aS) const;
	bool		operator== (const IntGVector& iGV) const;

	//*** Component-wise operation
	GVector		operator*  (const GVector& aGV);

	//** Friend Function
	friend GVector operator*  (const double& aS, const IntGVector& iGV);
	
};

#endif

