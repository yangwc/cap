#include "IntGVector.h"
#include "GVector.h"

// Constructor for a unit vector
IntGVector::IntGVector(int unitDir)
{
	IntGVector::SetOnes(unitDir);
}

int IntGVector::GetComp(int whichDir) const
{
	int component = 0;
	
	switch(whichDir)
	{
		case 0:
			component = miX;
			break;

		case 1:
			component = miY;
			break;

		case 2:
			component = miZ;
			break;
			
		default:
		
			std::stringstream dirStr;
			dirStr << whichDir;
			std::string errMsg = "IntGVector::GetComp is called with the argument of "
			                   + dirStr.str();
			//ErrHandler::sErrMsg += "# "+ errMsg + ".\n";
			dirStr.str("");
			break;
	}
		
	return component;		
}

// WooKuen Added on 8/27/07
void IntGVector::SetComp(int whichDir, int val)
{
	switch(whichDir)
	{
		case 0:
			miX = val;
			break;
		
		case 1:
			miY = val;
			break;

		case 2:
			miZ = val;
			break;
	}
}

// This function is dedicated to construct nodes in a cell
// Therefore, unitDir are not usual!!!

void IntGVector::SetOnes(int unitDir)
{
	switch(unitDir)
	{
	  case 0: // special case...zero vector
	  	IntGVector::SetZeros();
	  	break;

	  case 1: // unit vector in z
		
		miX = 0;
		miY = 0;
		miZ = 1;
		break;
		
	  case 2: // unit vector in x & z
		
		miX = 1;
		miY = 0;
		miZ = 1;
		break;
			  		
	  case 3: // unit vector in x
		
		miX = 1;
		miY = 0;
		miZ = 0;
		break;
		
	  case 4: // unit vector in y
		
		miX = 0;
		miY = 1;
		miZ = 0;
		break;		

	  case 5: // unit vector in y & z
		
		miX = 0;
		miY = 1;
		miZ = 1;
		break;
		
	  case 6: // unit vector in x, y & z
		
		miX = 1;
		miY = 1;
		miZ = 1;
		break;	
						
	  case 7: // unit vector in x & y
		
		miX = 1;
		miY = 1;
		miZ = 0;
		break;
	}
}	

/////////////////////////////////////////////////
////////////		operators		 ////////////
/////////////////////////////////////////////////
IntGVector& IntGVector::operator *= (const IntGVector& iGV)
{
	miX *= iGV.miX;
	miY *= iGV.miY;
	miZ *= iGV.miZ;
	return *this;
}
IntGVector& IntGVector::operator=(int val)
{
	miX =  miY =  miZ = val;
	return *this;
}
IntGVector& IntGVector::operator=(const IntGVector& iGV)
{
	miX = iGV.miX;
	miY = iGV.miY;
	miZ = iGV.miZ;
	return *this;
}

IntGVector  IntGVector::operator+(const IntGVector& iGV) const
{
	IntGVector answer;
	answer.miX = miX + iGV.miX;
	answer.miY = miY + iGV.miY;
	answer.miZ = miZ + iGV.miZ;
	
	return answer;
}

IntGVector& IntGVector::operator+=(const IntGVector& iGV)
{
	miX += iGV.miX;
	miY += iGV.miY;
	miZ += iGV.miZ;
	return *this;
}

IntGVector  IntGVector::operator-(const IntGVector& iGV) const
{
	IntGVector answer;
	answer.miX = miX - iGV.miX;
	answer.miY = miY - iGV.miY;
	answer.miZ = miZ - iGV.miZ;
	
	return answer;
}

IntGVector& IntGVector::operator-=(const IntGVector& iGV)
{
	miX -= iGV.miX;
	miY -= iGV.miY;
	miZ -= iGV.miZ;
	return *this;
}

IntGVector& IntGVector::operator-=(const int& val)
{
	miX -= val;
	miY -= val;
	miZ -= val;
	return *this;
}

GVector IntGVector::operator*(const double& aS) const
{
     GVector answer;
     
     // preserve double precision
     answer.SetxComponent( ((Scalar) miX) * aS);
     answer.SetyComponent( ((Scalar) miY) * aS);
     answer.SetzComponent( ((Scalar) miZ) * aS);     
     
     return answer;
}

GVector IntGVector::operator*(const GVector& aGV)
{
     GVector answer;
     
     // preserve double precision
     answer.SetxComponent( ((Scalar) miX) * aGV.GetxComp());
     answer.SetyComponent( ((Scalar) miY) * aGV.GetyComp());
     answer.SetzComponent( ((Scalar) miZ) * aGV.GetzComp());     
     
     return answer;
}

bool IntGVector::operator==(const IntGVector& iGV) const
{
	return ( miX == iGV.GetxComp()
		  && miY == iGV.GetyComp()
		  && miZ == iGV.GetzComp() );
}

////////////////////////////////////////////////////////
////////////		Friend Function			////////////
////////////////////////////////////////////////////////
GVector operator*(const double& aS, const IntGVector& iGV)
{
     GVector answer;
     answer.SetxComponent( ((Scalar) iGV.miX) * aS);
     answer.SetyComponent( ((Scalar) iGV.miY) * aS);
     answer.SetzComponent( ((Scalar) iGV.miZ) * aS);     
     
     return answer;
}
