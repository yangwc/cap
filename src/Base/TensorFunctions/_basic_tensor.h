/*
 * _basic_tensor.h
 *
 *  Created on: 2015.10.07
 *      Author: YangWC
 */

#ifndef _BASIC_TENSOR_H_
#define _BASIC_TENSOR_H_
#include <vector>

template <class _vtype, class _double>
class _basic_tensor{
public:
	template <class _vt, class _db>
	inline int roundoff( const _basic_tensor<_vt,_db>& tols){
		if ( tols._elms.size() != _elms.size() ) return -1;

		for (unsigned int Iv = 0; Iv != _elms.size(); ++Iv){
			if ( std::abs(_elms[Iv]) < tols._elms[Iv] ) _elms[Iv] = 0;
		}
	}
	inline void roundoff(const _double tol){
		for (std::vector<_vtype*>::iterator ivec = _elms.begin(); ivec != _elms.end(); ++ivec){
			if ( std::abs(*(*ivec)) < tol ) *(*ivec) = 0;
		}
	}
protected:
	std::vector<_vtype*> _elms;
};



#endif /* _BASIC_TENSOR_H_ */
