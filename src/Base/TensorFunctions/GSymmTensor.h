#ifndef GSYMMTENSOR_H_
#define GSYMMTENSOR_H_

#include "TensorDefs.h"
#include "GVector.h"
#include <vector>

class GTensor;
class GTensorRing;
class GSymmTensorRing;

class GSymmTensor 
{

 protected:
 
	Scalar m11, m12, m13, m22, m23, m33; //--Tensor components
	//---This is not a typo -- it makes it easier to make full tensors
	static Scalar	sInvertTol;	// - call to 'Invert' fails if det < mInvertTol.

 public:

	static GTensorRing sWorkTensorRing;
	static GSymmTensorRing sWorkSymmTensorRing;
//	static std::vector<IntGVector> sIJK;
	static GSymmTensor sIdentity;	
	
	// Constructors	
	GSymmTensor(char *);
	GSymmTensor() {m11=0;m12=0;m13=0;m22=0;m23=0;m33=0;}
	GSymmTensor(const Scalar& f11, const Scalar& f22, const Scalar& f33,
				const Scalar& f12, const Scalar& f13, const Scalar& f23)
				  {m11=f11; m12=f12; m13=f13; m22=f22; m23=f23; m33=f33;}

	GSymmTensor(GVector& aGV);
	GSymmTensor(GVector& aGV1,GVector& aGV2);

	inline unsigned int size(){ return 6; }

	  inline const GSymmTensor& admin_dumperror(const Scalar tol){
		  if ( std::abs(m11) < tol ) m11 = 0.;
		  if ( std::abs(m22) < tol ) m22 = 0.;
		  if ( std::abs(m33) < tol ) m33 = 0.;
		  if ( std::abs(m12) < tol ) m12 = 0.;
		  if ( std::abs(m23) < tol ) m23 = 0.;
		  if ( std::abs(m13) < tol ) m13 = 0.;
		  return *this;
	  }
	  inline const GSymmTensor& admin_dumperror(const GSymmTensor& tols){
		  if ( std::abs(m11) < tols.m11 ) m11 = 0.;
		  if ( std::abs(m22) < tols.m22 ) m22 = 0.;
		  if ( std::abs(m33) < tols.m33 ) m33 = 0.;
		  if ( std::abs(m12) < tols.m12 ) m12 = 0.;
		  if ( std::abs(m23) < tols.m23 ) m23 = 0.;
		  if ( std::abs(m13) < tols.m13 ) m13 = 0.;
		  return *this;
	  }

	  const GSymmTensor& abs_max(const GSymmTensor& aten) const;
	  Scalar abs_max() const;
	  const GSymmTensor& bw_abs() const;
	  const GSymmTensor& bw_mindis(const GSymmTensor& aten);
	  const GSymmTensor& bw_max(const GSymmTensor& aten) const;
	  const GSymmTensor& bw_min(const GSymmTensor& aten) const;

	// Writers							
	void Setm11(Scalar aS) {m11 = aS;}
	void Setm12(Scalar aS) {m12 = aS;}
 	void Setm22(Scalar aS) {m22 = aS;}
	void Setm13(Scalar aS) {m13 = aS;}
  	void Setm23(Scalar aS) {m23 = aS;}
  	void Setm33(Scalar aS) {m33 = aS;}
  	void SetComponents (const Scalar& f11, const Scalar& f22, const Scalar& f33,
  	 				    const Scalar& f12, const Scalar& f13, const Scalar& f23)
  	 				{m11=f11; m12=f12; m13=f13; m22=f22; m23=f23; m33=f33;}
	void SetZeros();
	void SetEye();
	void SetDiag(const GVector& aGV);
	void SetDiag(const Scalar& aS);

	const GVector& Diag() const;
	const GSymmTensor& Diag(const GVector& aGV);
	const GSymmTensor& Diag(const Scalar& aS);
	const GSymmTensor& addDiag(const GVector& aGV);
	const GSymmTensor& addDiag(const Scalar& aS);

	const GSymmTensor& deDiag() const;

	// set component via vectorial representation   	 				
	void SetComponent (int i, Scalar val); 
  	
	
	// Readers  	 				    		
  	Scalar Getm11() const {return m11;}
  	Scalar Getm12() const {return m12;}
  	Scalar Getm22() const {return m22;}
  	Scalar Getm13() const {return m13;}
  	Scalar Getm23() const {return m23;}
  	Scalar Getm33() const {return m33;}
  	// Return component via vectorial representation 
	Scalar GetComponent(int i) const;
  	// Return component via matrix representation 
	Scalar GetComponent(int row, int col) const;

	// Added by Carter
	Scalar GetMax();

	// Tensor Operations
  	Scalar GetTrace() const {return (m11 + m22 + m33);}
  	GSymmTensor& GetDevPart() const;
	GSymmTensor& GetVolPart() const;
	GSymmTensor& GetVolDevSplit() const;
	GSymmTensor& ReconstructFromVolDevSplit() const;

  	GSymmTensor& GetDevPartPlane() const;
	Scalar       GetNorm() const;
	Scalar		 DoubleDot(GSymmTensor& aST);

	GSymmTensor& GetLogarithmicDecomposition() const;
	GSymmTensor& GetLogarithmicDecomposition(GVector &  eigVal, GTensor & eigTen);

	GSymmTensor& GetExponentialDecomposition() const;
	GSymmTensor& GetExponentialDecomposition(GVector &  eigVal, GTensor & eigTen);
  	
  	// Matrix Operations
  	Scalar  	Det ()  const ;
  	Boolean		Invert ();
	Boolean		Invert (Scalar tolerance);
    static  void	NotifyOfInvertTol (Scalar tol);
    
    // Log operations
    void EigenAnalysis(GVector& eigVal, GTensor& eigVec) const;
    
    // Operators
  	GSymmTensor& operator= (const GSymmTensor& aST);
  	GSymmTensor& operator= (const GTensor& aT);
  	GSymmTensor& operator= (const Scalar& as);
	GSymmTensor& operator+ (const GSymmTensor& aGT) const;
	GSymmTensor& operator+ (Scalar aS) const;
	GSymmTensor& operator- (const GSymmTensor& aGT) const;
	GSymmTensor& operator- () const;
	GSymmTensor& operator* (const Scalar& a) const;
	GVector& 	 operator* (const GVector& aGV) const;
	GTensor& 	 operator* (const GSymmTensor& aGT) const;
	GTensor& 	 operator* (const GTensor& aGT) const;
	GSymmTensor& operator/ (const Scalar& a) const;
  	GSymmTensor& operator+= (const GSymmTensor& aST);
  	GSymmTensor& operator-= (const GSymmTensor& aST);

  	GSymmTensor& operator*= (const Scalar& a);
  	GSymmTensor& operator*= (const GSymmTensor& aGT);
  	GSymmTensor& operator/= (const Scalar& a);
  	GTensor& GetFullTensor ();
    
    //--special operations
    GVector& 	 VecScalarMult(const GVector& aGV, Scalar a) const;
    void         PreDotPostTransposeDotEqual(const GTensor &aT);

    void Print(std::ostream* file) const;
    void Print(std::ostream* file, std::string sep) const;
  	void Print() const{std::cout<<m11<<"\t"<<m22<<"\t"<<m33<<"\t"<<m12<<"\t"<<m13<<"\t"<<m23<<"\t"<<std::endl;}
  	void PrintBin(std::ostream* file) const;
  	// friend functions
	friend  GSymmTensor& operator* (const Scalar& aS,GSymmTensor& aST);
	friend  GSymmTensor& operator*= (const Scalar& aS, GSymmTensor& aT);

	// ADDED BY CARTER
	void Zero();

	void DotAssign(GVector& aGV, GVector& target);
	void DotAssign(GVector& aGV);
	void DotAssign(GTensor& kij, GTensor& target);

	void GaussOpAssign(GTensor& kij, GSymmTensor& kjj);

};

#define SYMMTENSOR_RING_DEPTH 50

//----------
class SymmTensorRingElt
{
	public:
		SymmTensorRingElt(){;};
		//~SymmTensorRingElt();
		void SetNext(SymmTensorRingElt& next)
			{mNextRingElt = &next;}
		GSymmTensor& GetTensor()
			{return mMyTensor;}
		SymmTensorRingElt* GetNext()
			{return mNextRingElt;}
			
	protected:
		GSymmTensor mMyTensor;
		SymmTensorRingElt* mNextRingElt;
};

class GSymmTensorRing 
{
	public:
		GSymmTensorRing();
		~GSymmTensorRing();
		GSymmTensor* Next();
		
	protected:
		SymmTensorRingElt* mCurrentElt;
		SymmTensorRingElt* mElts[SYMMTENSOR_RING_DEPTH];
};

#endif

