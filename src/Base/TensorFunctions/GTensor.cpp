#include "GTensor.h"
#include <stdio.h>
#include <string.h>

Scalar GTensor::sInvertTol = 1e-10;

//================================================
//
//  GTensor::GTensor
//    - A constructor
//
//================================================
GTensor::GTensor(GVector& aGV1,GVector& aGV2)
{
  m11 = aGV1.GetxComp() * aGV2.GetxComp(); 
  m12 = aGV1.GetxComp() * aGV2.GetyComp(); 
  m13 = aGV1.GetxComp() * aGV2.GetzComp();
   
  m21 = aGV1.GetyComp() * aGV2.GetxComp(); 
  m22 = aGV1.GetyComp() * aGV2.GetyComp(); 
  m23 = aGV1.GetyComp() * aGV2.GetzComp(); 
  
  m31 = aGV1.GetzComp() * aGV2.GetxComp(); 
  m32 = aGV1.GetzComp() * aGV2.GetyComp(); 
  m33 = aGV1.GetzComp() * aGV2.GetzComp(); 
}

GTensor::GTensor(char* type)
{
	if (strcmp(type, "Identity") == 0) 
	{
		m11 = 1.0;
		m12 = 0.0;
		m13 = 0.0;
		
		m21 = 0.0;
		m22 = 1.0;
		m23 = 0.0;

		m31 = 0.0;
		m32 = 0.0;
		m33 = 1.0;
	}
}

const GTensor& GTensor::deDiag() const{
	GTensor& res = *GTensor::sWorkTensorRing.Next();
	res.m11 = res.m22 = res.m33 = 0;
	res.m12 = m12;
	res.m13 = m13;
	res.m21 = m21;
	res.m23 = m23;
	res.m31 = m31;
	res.m32 = m32;
	return res;
}

void GTensor::SetComponent(int whichComp, Scalar val)
{
	switch(whichComp)
	{
		case 0:
			
			m11 = val;
			break;
			
		case 1:
			
			m12 = val;
			break;			
			
		case 2:
			
			m13 = val;
			break;			

		case 3:
			
			m21 = val;
			break;			
			
		case 4:
			
			m22 = val;
			break;			
			
		case 5:
			
			m23 = val;
			break;			
			
		case 6:
			
			m31 = val;
			break;			
			
		case 7:
			
			m32 = val;
			break;			
			
		case 8:
			
			m33 = val;
			break;			
	}
}


//================================================
//  Others
//================================================
const GTensor& GTensor::abs_max(const GTensor& aten) const {
	GTensor* result = sWorkTensorRing.Next();
	result->m11 = std::max(std::abs(m11),std::abs(aten.m11));
	result->m12 = std::max(std::abs(m12),std::abs(aten.m12));
	result->m13 = std::max(std::abs(m13),std::abs(aten.m13));
	result->m21 = std::max(std::abs(m21),std::abs(aten.m21));
	result->m22 = std::max(std::abs(m22),std::abs(aten.m22));
	result->m23 = std::max(std::abs(m23),std::abs(aten.m23));
	result->m31 = std::max(std::abs(m31),std::abs(aten.m31));
	result->m32 = std::max(std::abs(m32),std::abs(aten.m32));
	result->m33 = std::max(std::abs(m33),std::abs(aten.m33));

	return *result;
}

Scalar GTensor::abs_max() const{
	Scalar absmax = std::max( std::max( std::max(std::abs(m11),std::max(std::abs(m22),std::abs(m33))),
			                            std::max(std::abs(m12),std::max(std::abs(m13),std::abs(m23)))),
									    std::max(std::abs(m21),std::max(std::abs(m31),std::abs(m32)))
							);
	return absmax;
}

const GTensor& GTensor::abs() const {
	GTensor* result = sWorkTensorRing.Next();
	result->m11 = std::abs(m11);
	result->m12 = std::abs(m12);
	result->m13 = std::abs(m13);
	result->m21 = std::abs(m21);
	result->m22 = std::abs(m22);
	result->m23 = std::abs(m23);
	result->m31 = std::abs(m31);
	result->m32 = std::abs(m32);
	result->m33 = std::abs(m33);

	return *result;
}

//================================================
//
//  GTensor::SetZeros
//
//================================================
void GTensor::SetZeros()
{
  m11 = 0.0; m12 = 0.0; m13 = 0.0;   
  m21 = 0.0; m22 = 0.0; m23 = 0.0; 
  m31 = 0.0; m32 = 0.0; m33 = 0.0; 
}

//-- WooKuen added this on 8/2/07
void GTensor::SetEye()
{
  m11 = 1.0; m12 = 0.0; m13 = 0.0;   
  m21 = 0.0; m22 = 1.0; m23 = 0.0; 
  m31 = 0.0; m32 = 0.0; m33 = 1.0; 
}

// WooKuen added on April 25, 2008
GVector GTensor::GetRow(int row)
{
	GVector rowVec;
	
	switch(row)
	{
		case 0:
		
			rowVec.SetComponents(m11, m12, m13);
			break;
			
		case 1:
		
			rowVec.SetComponents(m21, m22, m23);
			break;
			
		case 2:
		
			rowVec.SetComponents(m31, m32, m33);
			break;						
	}
	
	return rowVec;
}

// WooKuen added on April 25, 2008
GVector GTensor::GetCol(int col)
{
	GVector colVec;
	
	switch(col)
	{
		case 0:
		
			colVec.SetComponents(m11, m21, m31);
			break;
			
		case 1:
		
			colVec.SetComponents(m12, m22, m32);
			break;
			
		case 2:
		
			colVec.SetComponents(m13, m23, m33);
			break;						
	}
	
	return colVec;
}

//================================================
//
//  GTensor::Transpose
//    - A method to get the transpose tensor
//
//================================================
GTensor& GTensor::GetTranspose()
{
	GTensor* result = sWorkTensorRing.Next();
	Scalar temp12 = m12;
	Scalar temp21 = m21;
	Scalar temp13 = m13;
	Scalar temp31 = m31;
	Scalar temp23 = m23;
	Scalar temp32 = m32;


	//---Now set the values
	result->m11 = m11;
	result->m22 = m22;	
	result->m33 = m33;
		
	result->m12 = temp21;
	result->m21 = temp12;
	result->m13 = temp31;
	result->m23 = temp32;
	result->m31 = temp13;
	result->m32 = temp23;

	return *result;
}

//================================================
//
//  GTensor::GetSymmPart
//    - A method to get the symmetric part of a 
//      second order tensor
//
//================================================
GSymmTensor& GTensor::GetSymmPart() const {

	// Note that planestrain in is false by default

	GSymmTensor* result = sWorkSymmTensorRing.Next();
	result->Setm11(m11);
	result->Setm22(m22);
	result->Setm33(m33);
	result->Setm12( 0.5*(m12+m21) );
	result->Setm13( 0.5*(m13+m31) );
	result->Setm23( 0.5*(m23+m32) );

	return *result;
}

GTensor& GTensor::GetAntiSymmPart() const {
	GTensor* result = sWorkTensorRing.Next();
	result->m11 = 0;
	result->m22 = 0;
	result->m33 = 0;
	result->m12 = ( 0.5*(m12-m21) );
	result->m13 = ( 0.5*(m13-m31) );
	result->m23 = ( 0.5*(m23-m32) );

	result->m21 = -result->m12;
	result->m31 = -result->m13;
	result->m32 = -result->m23;

	return *result;
}

Scalar GTensor::GetDet() const {
	Scalar det = m11*m22*m33 + m12*m23*m31 + m13*m21*m32 - m11*m32*m23 - m21*m12*m33 - m31*m22*m13;
	return det;
}

Scalar GTensor::DoubleDot(const GTensor& aT) const
{
	return (m11*aT.m11 + m12*aT.m12 + m13*aT.m13
		  +	m21*aT.m21 + m22*aT.m22 + m23*aT.m23
		  +	m31*aT.m31 + m32*aT.m32 + m33*aT.m33);
}

GTensor& GTensor::GetInverse(bool& badinverse){
	GTensor& result = *GTensor::sWorkTensorRing.Next();

	Scalar det = this -> GetDet();
	badinverse = (det < sInvertTol);

	result.m11 = m22*m33 - m32*m23;
	result.m21 = m23*m31 - m21*m33;
	result.m31 = m21*m32 - m31*m22;
	result.m12 = m13*m32 - m12*m33;
	result.m22 = m11*m33 - m31*m13;
	result.m32 = m12*m31 - m11*m32;
	result.m13 = m12*m23 - m22*m13;
	result.m23 = m13*m21 - m11*m23;
	result.m33 = m11*m22 - m21*m12;
	result /= det;

	return result;
}

//================================================
//
//  GTensor::DotItsTranspose
//    - A method to get the product of a second 
//      order tensor itself and its transpose 
//
//================================================
GSymmTensor& GTensor::DotItsTranspose() const
{
//---This one should not be able to be called on sWorkSymmTensor
	GSymmTensor* result = sWorkSymmTensorRing.Next();
/*	
	result->Setm11(m11*m11 + m12*m12);
	result->Setm12(m11*m21 + m12*m22);
	result->Setm22(m21*m21 + m22*m22);
*/	
	result->Setm11(m11*m11 + m12*m12 + m13*m13);
	result->Setm12(m11*m21 + m12*m22 + m13*m23);
	result->Setm13(m11*m31 + m12*m32 + m13*m33);
	
	result->Setm22(m21*m21 + m22*m22 + m23*m23);
	result->Setm23(m21*m31 + m22*m32 + m23*m33);
	result->Setm33(m31*m31 + m32*m32 + m33*m33);
	return *result;
}

GSymmTensor& GTensor::DotTransposeRight() const{
	// For tensor T, return <T^T,T>
	GSymmTensor& result = this -> DotItsTranspose();
	return result;
}
GSymmTensor& GTensor::DotTransposeLeft() const{
	// For tensor T, return <T,T^T>
	GSymmTensor* result = sWorkSymmTensorRing.Next();
	result->Setm11(m11*m11 + m21*m21 + m31*m31);
	result->Setm12(m12*m11 + m22*m21 + m32*m31);
	result->Setm13(m13*m11 + m23*m21 + m33*m31);

	result->Setm22(m12*m12 + m22*m22 + m32*m32);
	result->Setm23(m13*m12 + m23*m22 + m33*m32);
	result->Setm33(m13*m13 + m23*m23 + m33*m33);
	return *result;
}

//================================================
//
//  GTensor::operator=
//
//================================================
GTensor& GTensor::operator=(const GTensor& aT)
{
	m11 = aT.m11;
	m12 = aT.m12;
	m21 = aT.m21;
	m22 = aT.m22;
	m13 = aT.m13;
	m23 = aT.m23;
	m31 = aT.m31;
	m32 = aT.m32;
	m33 = aT.m33;
	return *this;
}

GTensor& GTensor::operator=(const GSymmTensor& aST)
{
	m11 = aST.Getm11();
	m12 = aST.Getm12();
	m21 = aST.Getm12();
	m22 = aST.Getm22();
	m13 = aST.Getm13();
	m23 = aST.Getm23();
	m31 = aST.Getm13();
	m32 = aST.Getm23();
	m33 = aST.Getm33();
	
	return *this;
}

GTensor& GTensor::operator+(const GTensor& aT) const
{
	 GTensor* result = GTensor::sWorkTensorRing.Next();
     result->m11 = m11 + aT.m11;
     result->m12 = m12 + aT.m12;
     result->m21 = m21 + aT.m21;
     result->m22 = m22 + aT.m22;
     result->m13 = m13 + aT.m13;
     result->m23 = m23 + aT.m23;
     result->m31 = m31 + aT.m31;
     result->m32 = m32 + aT.m32;
     result->m33 = m33 + aT.m33;
    return  *result;
}


GTensor& GTensor::operator+=(const GTensor& aT)
{
     m11 += aT.m11;
     m12 += aT.m12;
     m21 += aT.m21;
     m22 += aT.m22;
     m13 += aT.m13;
     m23 += aT.m23;
     m31 += aT.m31;
     m32 += aT.m32;
     m33 += aT.m33;
    return  *this;
}
  
GTensor& GTensor::operator-(const GTensor& aT) const
{
	 GTensor* result = GTensor::sWorkTensorRing.Next();
     result->m11 = m11 - aT.m11;
     result->m12 = m12 - aT.m12;
     result->m21 = m21 - aT.m21;
     result->m22 = m22 - aT.m22;
     result->m13 = m13 - aT.m13;
     result->m23 = m23 - aT.m23;
     result->m31 = m31 - aT.m31;
     result->m32 = m32 - aT.m32;
     result->m33 = m33 - aT.m33;
    return  *result;
}

GTensor& GTensor::operator*(const Scalar& aS) const
{
	 GTensor* result = GTensor::sWorkTensorRing.Next();
   	 result->m11 = m11 * aS;
     result->m12 = m12 * aS;
     result->m21 = m21 * aS;
     result->m22 = m22 * aS;
     result->m13 = m13 * aS;
     result->m23 = m23 * aS;
     result->m31 = m31 * aS;
     result->m32 = m32 * aS;
     result->m33 = m33 * aS;
     return  *result;
}

GTensor& GTensor::operator*(const GTensor& aT) const
{
    GTensor* result = GTensor::sWorkTensorRing.Next();
    result->m11 = m11 * aT.m11 + m12 * aT.m21 + m13 * aT.m31;
    result->m12 = m11 * aT.m12 + m12 * aT.m22 + m13 * aT.m32;
    result->m13 = m11 * aT.m13 + m12 * aT.m23 + m13 * aT.m33;
    result->m21 = m21 * aT.m11 + m22 * aT.m21 + m23 * aT.m31;
    result->m22 = m21 * aT.m12 + m22 * aT.m22 + m23 * aT.m32;
    result->m23 = m21 * aT.m13 + m22 * aT.m23 + m23 * aT.m33;
    result->m31 = m31 * aT.m11 + m32 * aT.m21 + m33 * aT.m31;
    result->m32 = m31 * aT.m12 + m32 * aT.m22 + m33 * aT.m32;
    result->m33 = m31 * aT.m13 + m32 * aT.m23 + m33 * aT.m33;

	return *result;
}


GTensor& GTensor::operator*=(const GTensor& aT)
{
    Scalar tempm11 = m11 * aT.m11 + m12 * aT.m21 + m13 * aT.m31;
    Scalar tempm12 = m11 * aT.m12 + m12 * aT.m22 + m13 * aT.m32;
    Scalar tempm13 = m11 * aT.m13 + m12 * aT.m23 + m13 * aT.m33;
    Scalar tempm21 = m21 * aT.m11 + m22 * aT.m21 + m23 * aT.m31;
    Scalar tempm22 = m21 * aT.m12 + m22 * aT.m22 + m23 * aT.m32;
    Scalar tempm23 = m21 * aT.m13 + m22 * aT.m23 + m23 * aT.m33;
    Scalar tempm31 = m31 * aT.m11 + m32 * aT.m21 + m33 * aT.m31;
    Scalar tempm32 = m31 * aT.m12 + m32 * aT.m22 + m33 * aT.m32;
    Scalar tempm33 = m31 * aT.m13 + m32 * aT.m23 + m33 * aT.m33;
    m11 = tempm11;
    m12 = tempm12;
    m13 = tempm13;
    m21 = tempm21;
    m22 = tempm22;
    m23 = tempm23;
    m31 = tempm31;
    m32 = tempm32;
    m33 = tempm33;
	return *this;
}

GTensor& GTensor::operator*=(const Scalar& aS)
{

    m11 *= aS; m12 *= aS; m13 *= aS;
    m21 *= aS; m22 *= aS; m23 *= aS;
    m31 *= aS; m32 *= aS; m33 *= aS;
	return *this;
}

GVector GTensor::operator*(const GVector& aGV) const
{
    Scalar s1 = m11*aGV.GetxComp() + m12*aGV.GetyComp() + m13*aGV.GetzComp();
    Scalar s2 = m21*aGV.GetxComp() + m22*aGV.GetyComp() + m23*aGV.GetzComp();
    Scalar s3 = m31*aGV.GetxComp() + m32*aGV.GetyComp() + m33*aGV.GetzComp();
    GVector resultVec(s1, s2, s3);
	return resultVec;
}


GTensor& GTensor::operator*(const GSymmTensor& aST) const
{
    GTensor* result = GTensor::sWorkTensorRing.Next();
    result->m11 = m11 * aST.Getm11() + m12 * aST.Getm12() + m13 * aST.Getm13();
    result->m12 = m11 * aST.Getm12() + m12 * aST.Getm22() + m13 * aST.Getm23();
    result->m13 = m11 * aST.Getm13() + m12 * aST.Getm23() + m13 * aST.Getm33();
    
    result->m21 = m21 * aST.Getm11() + m22 * aST.Getm12() + m23 * aST.Getm13();
    result->m22 = m21 * aST.Getm12() + m22 * aST.Getm22() + m23 * aST.Getm23();
    result->m23 = m21 * aST.Getm13() + m22 * aST.Getm23() + m23 * aST.Getm33();
    
    result->m31 = m31 * aST.Getm11() + m32 * aST.Getm12() + m33 * aST.Getm13();
    result->m32 = m31 * aST.Getm12() + m32 * aST.Getm22() + m33 * aST.Getm23();
    result->m33 = m31 * aST.Getm13() + m32 * aST.Getm23() + m33 * aST.Getm33();

	return *result;
}

GTensor& GTensor::operator/(const Scalar& aS) const
{
	GTensor* result = GTensor::sWorkTensorRing.Next();
	result->m11 /= aS;
    result->m12 /= aS;
    result->m21 /= aS;
    result->m22 /= aS;
    result->m13 /= aS;
    result->m23 /= aS;
    result->m31 /= aS;
    result->m32 /= aS;
    result->m33 /= aS;
    return *result;
}

GTensor& GTensor::operator/=(const Scalar& aS)
{
    m11 /= aS; m12 /= aS; m13 /= aS;
    m21 /= aS; m22 /= aS; m23 /= aS;
    m31 /= aS; m32 /= aS; m33 /= aS;
	return *this;
}

bool GTensor::any_GT(const Scalar& aS) const{
	bool istat = false;
	if( m11 > aS ||
		m12 > aS ||
		m13 > aS ||
		m21 > aS ||
		m22 > aS ||
		m23 > aS ||
		m31 > aS ||
		m32 > aS ||
		m33 > aS )
	{ istat = true; }
	return istat;
}
bool GTensor::any_LT(const Scalar& aS) const{
	bool istat = false;
	if( m11 < aS ||
		m12 < aS ||
		m13 < aS ||
		m21 < aS ||
		m22 < aS ||
		m23 < aS ||
		m31 < aS ||
		m32 < aS ||
		m33 < aS )
	{ istat = true; }
	return istat;
}

void GTensor::Print(std::ostream* file) const{
    (*file) << m11 <<"\t" << m12 <<"\t"<< m13 <<"\t"
               << m21 <<"\t"<< m22 <<"\t"<< m23 <<"\t"
               << m31 <<"\t"<< m32 <<"\t"<< m33 <<"\t";
}

void GTensor::PrintBin(std::ostream* file) const{
	unsigned int Ldat = sizeof(m11);
	file->write( (char*) &m11, Ldat);
	file->write( (char*) &m12, Ldat);
	file->write( (char*) &m13, Ldat);
	file->write( (char*) &m21, Ldat);
	file->write( (char*) &m22, Ldat);
	file->write( (char*) &m23, Ldat);
	file->write( (char*) &m31, Ldat);
	file->write( (char*) &m32, Ldat);
	file->write( (char*) &m33, Ldat);
}

Scalar GTensor::GetTrace() const
{
	return (m11 + m22 + m33);
}

//------------------------
//
// friend functions
//
//------------------------
GVector& operator* (const GVector& av, const GTensor& aT){
	GVector& res = *GVector::sWorkVectorRing.Next();
    res.vx = av.vx*aT.m11 + av.vy*aT.m21 + av.vz*aT.m31;
    res.vy = av.vx*aT.m12 + av.vy*aT.m22 + av.vz*aT.m32;
    res.vz = av.vx*aT.m13 + av.vy*aT.m23 + av.vz*aT.m33;
   return  res;
}

GTensor& operator*(const Scalar& aS, const GTensor& aT)
{
	 GTensor* result = GTensor::sWorkTensorRing.Next();
     result->m11 = aT.m11 * aS;
     result->m12 = aT.m12 * aS;
     result->m21 = aT.m21 * aS;
     result->m22 = aT.m22 * aS;
     result->m13 = aT.m13 * aS;
     result->m23 = aT.m23 * aS;
     result->m31 = aT.m31 * aS;
     result->m32 = aT.m32 * aS;
     result->m33 = aT.m33 * aS;
    return  *result;
}

GTensor& operator*=(const Scalar& aS,const GTensor& aT)
{
	 GTensor* result = GTensor::sWorkTensorRing.Next();
     result->m11 = aT.m11 * aS;
     result->m12 = aT.m12 * aS;
     result->m21 = aT.m21 * aS;
     result->m22 = aT.m22 * aS;
     result->m13 = aT.m13 * aS;
     result->m23 = aT.m23 * aS;
     result->m31 = aT.m31 * aS;
     result->m32 = aT.m32 * aS;
     result->m33 = aT.m33 * aS;
     return  *result;
}

//================================================
//     static ring stuff
//================================================
GTensorRing GTensor::sWorkTensorRing;
GSymmTensorRing GTensor::sWorkSymmTensorRing;

GTensorRing::GTensorRing()
{
	int ringCirc = TENSOR_RING_DEPTH;
	
	for(int i = 0; i < ringCirc; i++)
		 mElts[i] = new TensorRingElt(); 
	
	for(int j = 0; j < ringCirc - 1; j++)
		mElts[j]->SetNext(*mElts[j+1]);
	//---Close the loop
		mElts[ringCirc-1]->SetNext(*mElts[0]);
		mCurrentElt = mElts[0];
}

GTensorRing::~GTensorRing()
{
	int ringCirc = TENSOR_RING_DEPTH;
	
	for(int i = 0; i < ringCirc; i++)
		delete mElts[i];
}

GTensor* GTensorRing::Next()
{
	mCurrentElt = mCurrentElt->GetNext();
	return &(mCurrentElt->GetTensor());
}		

GTensor& GTensor::Transpose()
{
	GTensor* result = sWorkTensorRing.Next();
#ifdef _2D_
	double temp12 = m12;
	double temp21 = m21;
	result->m11 = m11;
	result->m12 = temp21;
	result->m21 = temp12;
	result->m22 = m22;
#else
	double temp12 = m12;
	double temp21 = m21;
	double temp13 = m13;
	double temp23 = m23;
	double temp31 = m31;
	double temp32 = m32;
	result->m11 = m11;
	result->m12 = temp21;
	result->m13 = temp31;
	result->m21 = temp12;
	result->m22 = m22;
	result->m23 = temp32;
	result->m31 = temp13;
	result->m32 = temp23;
	result->m33 = m33;
#endif
	return *result;
}

void GTensor::DotMinusAssign(GVector& aGV, GVector& target)
{
#ifdef _2D_
	target.vx -= m11*aGV.vx + m12*aGV.vy;
	target.vy -= m21*aGV.vx + m22*aGV.vy;
#else
	target.vx -= m11*aGV.vx + m12*aGV.vy + m13*aGV.vz;
	target.vy -= m21*aGV.vx + m22*aGV.vy + m23*aGV.vz;
	target.vz -= m31*aGV.vx + m32*aGV.vy + m33*aGV.vz;

//	target.SetxComponent(  m11*aGV.GetxComp() + m12*aGV.GetyComp() + m13*aGV.GetzComp()  -  target.GetxComp()  );
//	target.SetyComponent(  m21*aGV.GetxComp() + m22*aGV.GetyComp() + m23*aGV.GetzComp()  -  target.GetyComp()  );
//	target.SetzComponent(  m31*aGV.GetxComp() + m32*aGV.GetyComp() + m33*aGV.GetzComp()  -  target.GetzComp()  );

#endif
}

void GTensor::DotTransposeMinusAssign(GVector& aGV, GVector& target)
{
#ifdef _2D_
	target.vx -= m11*aGV.vx + m21*aGV.vy;
	target.vy -= m12*aGV.vx + m22*aGV.vy;
#else
	target.vx -= m11*aGV.vx + m21*aGV.vy + m31*aGV.vz;
	target.vy -= m12*aGV.vx + m22*aGV.vy + m32*aGV.vz;
	target.vz -= m13*aGV.vx + m23*aGV.vy + m33*aGV.vz;

//	target.SetxComponent(  m11*aGV.GetxComp() + m21*aGV.GetyComp() + m31*aGV.GetzComp()  -  target.GetxComp()  );
//	target.SetyComponent(  m12*aGV.GetxComp() + m22*aGV.GetyComp() + m32*aGV.GetzComp()  -  target.GetyComp()  );
//	target.SetzComponent(  m13*aGV.GetxComp() + m23*aGV.GetyComp() + m33*aGV.GetzComp()  -  target.GetzComp()  );
#endif
}

GTensor& GTensor::Equals(const GTensor &aGT)
{
	m11 = aGT.m11; m12 = aGT.m12;
	m21 = aGT.m21; m22 = aGT.m22;
#ifndef _2D_
	m13 = aGT.m13;
	m23 = aGT.m23;
	m31 = aGT.m31; m32 = aGT.m32; m33 = aGT.m33;
#endif
	return *this;
}

void GTensor::MinusEqualTransposeDot(GTensor& a, GTensor& b)
{
#ifdef _2D_
	m11 -= (a.m11*b.m11 + a.m21*b.m21);
	m12 -= (a.m11*b.m12 + a.m21*b.m22);
	m21 -= (a.m12*b.m11 + a.m22*b.m21);
	m22 -= (a.m12*b.m12 + a.m22*b.m22);
#else
	m11 -= (a.m11*b.m11 + a.m21*b.m21 + a.m31*b.m31);
	m12 -= (a.m11*b.m12 + a.m21*b.m22 + a.m31*b.m32);
	m13 -= (a.m11*b.m13 + a.m21*b.m23 + a.m31*b.m33);
	m21 -= (a.m12*b.m11 + a.m22*b.m21 + a.m32*b.m31);
	m22 -= (a.m12*b.m12 + a.m22*b.m22 + a.m32*b.m32);
	m23 -= (a.m12*b.m13 + a.m22*b.m23 + a.m32*b.m33);
	m31 -= (a.m13*b.m11 + a.m23*b.m21 + a.m33*b.m31);
	m32 -= (a.m13*b.m12 + a.m23*b.m22 + a.m33*b.m32);
	m33 -= (a.m13*b.m13 + a.m23*b.m23 + a.m33*b.m33);
#endif
}
