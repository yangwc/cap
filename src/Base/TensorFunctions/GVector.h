#ifndef GVECTOR_H
#define GVECTOR_H

#include <cmath>
#include "TensorDefs.h"
#include "IntGVector.h"
#include <fstream>
#include <sstream>
#include <iostream>

//#include "_basic_tensor.h"

class GVectorRing;
class ScalarRing;
class GTensor;
class GSymmTensor;

class GVector{// : public _basic_tensor<Scalar, Scalar>{
	public:
		Scalar vx,vy,vz;
		static GVectorRing sWorkVectorRing;
		static ScalarRing  sWorkScalarRing;
		static GVector sIdentity;
	  
	  // Constructors & Destructors	
	  GVector()	{vx=0.0; vy=0.0; vz=0.0;}
	  GVector(Scalar s1,Scalar s2,Scalar s3){vx = s1; vy = s2; vz = s3;}
	  GVector(const IntGVector& avec){vx = avec.GetxComp(); vy = avec.GetyComp(); vz = avec.GetzComp();}
	  GVector(const GVector& avec){vx = avec.vx; vy = avec.vy; vz = avec.vz;}
	  ~GVector (){;}

	  //---Readers
	  inline const Scalar& GetxComp() const {return vx;}
	  inline const Scalar& GetyComp() const {return vy;}
	  inline const Scalar& GetzComp() const {return vz;}
	  inline void GetComp( Scalar& v1, Scalar& v2, Scalar& v3) const { v1 = vx; v2 = vy; v3 = vz;}
	  Scalar GetComp(int whichDir) const; // WooKuen added on 7/16/07
	  				
	  Scalar GetNorm() const;
	  
	  inline GVector& admin_dumperror(const Scalar tol){
		  if ( std::abs(vx) < tol ) vx = 0.;
		  if ( std::abs(vy) < tol ) vy = 0.;
		  if ( std::abs(vz) < tol ) vz = 0.;
		  return *this;
	  }
	  inline GVector& admin_dumperror(const GVector& tols){
		  if ( std::abs(vx) < tols.vx ) vx = 0.;
		  if ( std::abs(vy) < tols.vy ) vy = 0.;
		  if ( std::abs(vz) < tols.vz ) vz = 0.;
		  return *this;
	  }

	  GVector& abs_max(const GVector& avec) const;
	  GVector& abs_max(const Scalar& val) const;

	  // Writers
	  void SetxComponent(Scalar s) {vx = s;}
	  void SetyComponent(Scalar s) {vy = s;}
	  void SetzComponent(Scalar s) {vz = s;}
	  void SetComp(int whichDir, Scalar val); // WooKuen Added on 8/23/07
	  inline const GVector& SetComponents(Scalar s1, Scalar s2, Scalar s3)
	  {vx = s1; vy = s2; vz = s3; return *this;}
	  // WooKuen Added on 8/27/07
	  void SetRevSign() {vx = -vx; vy = -vy; vz = -vz;}
	  void SetZeros() {vx = 0.0; vy = 0.0; vz = 0.0;}
	  GSymmTensor DyodicSelf(); // WooKuen Added on 9/14/07
	  void Print(std::ostream* file) const;
	  void Print(std::ostream* file, std::string sep) const;
	  void PrintBin(std::ostream* file) const;

	  void SetFloor()
	  		{vx = std::floor(vx); vy = std::floor(vy); vz = std::floor(vz);}

	  //---Bitwise algebra
	  GVector& bw_inv () const;
	  GVector& bw_sqrt () const;
	  GVector& bw_log () const;
	  GVector& bw_exp () const;
	  Scalar bw_sum () const;
	  Scalar bw_product () const;
	  Scalar bw_absmax () const;
	  Scalar bw_max () const;
	  Scalar bw_min () const;
	  GVector& bw_max (const GVector& vec) const;
	  GVector& bw_min (const GVector& vec) const;

	  //---Basic algebra
	  template <typename Val>
	  GVector& operator= (const Val& val){ vx = val; vy = val; vz = val; return *this; }

	  GVector& operator+  (const GVector &aGV) const;
	  GVector& operator+= (const GVector &aGV);
	  // WooKuen added this function on 01/02/08
	  GVector& operator+  (const Scalar &aS) const;
	  // WooKuen added this function on 01/02/08
  	  GVector& operator+= (const Scalar &aS);
	  // WooKuen added this function on 01/02/08
	  GVector& operator-  (const Scalar &aS) const;
	  // WooKuen added this function on 01/02/08
  	  GVector& operator-= (const Scalar &aS);  	  	  
	  GVector& operator-  (const GVector &aGV) const;
	  GVector& operator-  () const;
	  GVector& operator-= (const GVector &aGV);
	  GVector& operator*  (const Scalar &aS) const;
	  GVector& operator*= (const Scalar &aS);
	  GVector& operator/  (const Scalar &aS) const;
	  GVector& operator/= (const Scalar  &aS);
	  	  
	  //--- Component-wise operations
	  GVector& operator*  (const GVector &aGV) const;
	  GVector& operator*= (const GVector &aGV);
	  GVector& operator*  (const IntGVector &iGV) const;
	  GVector& operator/  (const GVector &aGV) const;
	  GVector& operator/= (const GVector &aGV);
	  Scalar operator[] (int Idir) const {return this->GetComp(Idir); }

	  //** Wookuen Shin added this on 02/28/06(inner product)
	  Scalar   operator&& (const GVector &aGV) const;	  
  	  //** Wookuen Shin added this (dyodic product)
	  const GTensor&  operator&  (const GVector &aGV) const;
	  //** Wookuen Shin added this (cross product) on 9/19/07
	  GVector  operator^ (const GVector &aGV) const;
	  
	  // WARNING: Be careful about these operations... WS 08/01/07
	  GVector& operator%  (const GVector &aGV) const;	  
	  bool	   operator!= (const GVector &aGV);
	  
	  //*** Yang add on Apr 25 2014
	  bool operator< (const GVector& vec) const; // all
	  bool operator> (const GVector& vec) const; // all
	  bool operator< (const Scalar& val) const; // all
	  bool operator> (const Scalar& val) const; // all
	  bool  any_GT (const Scalar& aval) const;
	  bool  any_LT (const Scalar& aval) const;

	  IntGVector floor();
	  IntGVector round();

	  //** GTensor or GSymmTensor on Principal Plane
	  GVector& TakeLog(const GVector &aGV);
	  GVector& TakeExp(const GVector &aGV);
	  GVector& GetDevPart() const;
	  Scalar GetTrace() const {return vx + vy + vz;}
	  
	  //** Friend Functions
	  friend GVector& operator*  (const Scalar& aNumber, const GVector& aGV);
	  friend const GVector& operator/ (const Scalar& aNumber, const GVector& aGV);
	  friend GVector& operator*= (const Scalar& aNumber, GVector& aGV);
	  friend IntGVector GetBaseIndex(const GVector &aGV, const GVector& mesh);
	  friend IntGVector DivAndGetInt(GVector aGV, GVector bGV);

	  // Carter
		void Print() const;
		void Zero();


};


#define RING_DEPTH 96

//-----------------
class VectorRingElt
{
	public:
		VectorRingElt(){mNextRingElt = 0;}
		//~VectorRingElt();
		void SetNext(VectorRingElt& next)
			{mNextRingElt = &next;}
		GVector& GetVector()
			{return mMyVector;}
		VectorRingElt* GetNext()
			{return mNextRingElt;}
			
	protected:
		GVector mMyVector;
		VectorRingElt* mNextRingElt;
};

class GVectorRing 
{
	public:
		GVectorRing();
		~GVectorRing();
		void Init();
		GVector* Next();
	protected:
		VectorRingElt* mCurrentElt;
		VectorRingElt  mVectorRingElts[RING_DEPTH];
		Boolean mInitedp;
};

class ScalarRingElt
{
	public:
		ScalarRingElt(){mNextRingElt = 0; mMyScalar = 0;}
		//~ScalarRingElt();
		void SetNext(ScalarRingElt& next)
			{mNextRingElt = &next;}
		Scalar& GetScalar()
			{return mMyScalar;}
		ScalarRingElt* GetNext()
			{return mNextRingElt;}
			
	protected:
		Scalar mMyScalar;
		ScalarRingElt* mNextRingElt;
};

class ScalarRing 
{
	public:
		ScalarRing();
		~ScalarRing();
		void Init();
		Scalar* Next();
	protected:
		ScalarRingElt* mCurrentElt;
		ScalarRingElt* mScalarRingElts[RING_DEPTH];
};

//---Bitwise algebra
inline GVector& GVector::bw_inv () const{
    GVector* result = sWorkVectorRing.Next();
    result->vx = 1.0/vx;
    result->vy = 1.0/vy;
    result->vz = 1.0/vz;
    return *result;
}
inline GVector& GVector::bw_sqrt() const{
    GVector* result = sWorkVectorRing.Next();
    result->vx = std::sqrt(vx);
    result->vy = std::sqrt(vy);
    result->vz = std::sqrt(vz);
    return *result;
}

inline GVector& GVector::bw_log() const{
    GVector* result = sWorkVectorRing.Next();
    result->vx = std::log(vx);
    result->vy = std::log(vy);
    result->vz = std::log(vz);
    return *result;
}

inline GVector& GVector::bw_exp() const{
    GVector* result = sWorkVectorRing.Next();
    result->vx = std::exp(vx);
    result->vy = std::exp(vy);
    result->vz = std::exp(vz);
    return *result;
}

inline Scalar GVector::bw_sum() const{
	return (vx+vy+vz);
}
inline Scalar GVector::bw_product() const{
	return (vx*vy*vz);
}

inline Scalar GVector::bw_absmax () const{
	Scalar max = std::max( std::max(std::abs(vx),std::abs(vy)),std::abs(vz) );
	return max;
}

inline Scalar GVector::bw_max () const{
	Scalar max = std::max( std::max(vx,vy),vz);
	return max;
}

inline Scalar GVector::bw_min () const{
	Scalar min = std::min( std::min(vx,vy),vz);
	return min;
}
inline GVector& GVector::bw_max (const GVector& vec) const{
    GVector* result = sWorkVectorRing.Next();
    result->vx = std::max(vx, vec.vx);
    result->vy = std::max(vy, vec.vy);
    result->vz = std::max(vz, vec.vz);
    return *result;
}

inline GVector& GVector::bw_min (const GVector& vec) const{
    GVector* result = sWorkVectorRing.Next();
    result->vx = std::min(vx, vec.vx);
    result->vy = std::min(vy, vec.vy);
    result->vz = std::min(vz, vec.vz);
    return *result;
}

inline GVector& GVector::operator*(const Scalar &aS) const
{
     GVector* result = sWorkVectorRing.Next();
     result->vx = vx * aS;
     result->vy = vy * aS;
     result->vz = vz * aS;
     return *result;
}

inline GVector& operator*(const Scalar& aNumber, const GVector& aGV)
{
     GVector* result = aGV.sWorkVectorRing.Next();
     result->vx = aGV.vx * aNumber;
     result->vy = aGV.vy * aNumber;
     result->vz = aGV.vz * aNumber;
     return *result;
}

inline const GVector& operator/(const Scalar& aNumber, const GVector& aGV)
{
     GVector* result = aGV.sWorkVectorRing.Next();
     result->vx = aNumber/aGV.vx;
     result->vy = aNumber/aGV.vy;
     result->vz = aNumber/aGV.vz;
     return *result;
}

inline // WooKuen added this operator on 8/17/07
GVector& GVector::operator*(const IntGVector &iGV) const
{
// #ifdef _COUNT_FLOPS_
//   sFlopCount += 3;
// #endif
     GVector* result = sWorkVectorRing.Next();
     result->vx = vx * ( (Scalar) iGV.GetxComp() );
     result->vy = vy * ( (Scalar) iGV.GetyComp() );
     result->vz = vz * ( (Scalar) iGV.GetzComp() );
     return *result;
}

inline GVector& GVector::operator+(const GVector &aGV) const
{
// #ifdef _COUNT_FLOPS_
//   sFlopCount += 3;
// #endif
	GVector* result = sWorkVectorRing.Next();
	result->vx = vx + aGV.vx;
	result->vy = vy + aGV.vy;
	result->vz = vz + aGV.vz;
	return *result;
}


inline GVector& GVector::operator/(const GVector &aGV) const
{
// #ifdef _COUNT_FLOPS_
//   sFlopCount += 3;
// #endif
     GVector* result = sWorkVectorRing.Next();
     result->vx = vx / aGV.vx;
     result->vy = vy / aGV.vy;
     result->vz = vz / aGV.vz;
     return *result;
}

inline GVector& GVector::operator*(const GVector &aGV) const
{
     GVector* result = sWorkVectorRing.Next();
     result->vx = vx * aGV.vx;
     result->vy = vy * aGV.vy;
     result->vz = vz * aGV.vz;
     return *result;
}



#endif // GVECTOR_H
