#ifndef TENSORDEFS_H
#define TENSORDEFS_H

#include <math.h>

typedef double Scalar;
const double GLOBAL_TOLERANCE_PRECISION = 1.e-15;
const double GLOBAL_TOLERANCE_ERROR     = 1.e-12;
const double PI = 4.0*atan(1.0);

#ifndef False
#define False 0
#endif

#ifndef True
#define True 1
#endif 

#ifdef _win32_
	typedef BOOL Boolean;
#else
	typedef unsigned char 	Boolean;
#endif

#ifdef _win32_
#define AbsVal fabs
#else
#define AbsVal ::fabs
#endif

#endif
