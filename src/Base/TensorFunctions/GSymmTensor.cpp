//---Automatically includes GSymmTensor.h
#include "GTensor.h"
#include <stdio.h>
#include <iostream>
#include<cstring>
#include <time.h>

//==========
//  Static members
//==========
Scalar GSymmTensor::sInvertTol = 0.0;
// permutation for eigen values
//std::vector<IntGVector> GSymmTensor::sIJK; // defined in Modelling class
GSymmTensor GSymmTensor::sIdentity(1, 1, 1, 0, 0, 0);

//===========================================
//
//           class GSymmTensor
//
//-------------------------------------------
//          definition of member-functions
//
//===========================================

//================================================
//  Constructor 1
//================================================
GSymmTensor::GSymmTensor(GVector& aGV)
{
	m11 = aGV.GetxComp() * aGV.GetxComp(); 
	m12 = aGV.GetxComp() * aGV.GetyComp(); 
	m22 = aGV.GetyComp() * aGV.GetyComp(); 
	m13 = aGV.GetxComp() * aGV.GetzComp(); 
	m23 = aGV.GetyComp() * aGV.GetzComp(); 
	m33 = aGV.GetzComp() * aGV.GetzComp(); 
}

//================================================
//  Constructor 2
//================================================
// bug fix by Wookuen on 08/01/2008
GSymmTensor::GSymmTensor(GVector& aGV1,GVector& aGV2)
{
	m11 = aGV1.GetxComp() * aGV2.GetxComp(); 
	m22 = aGV1.GetyComp() * aGV2.GetyComp(); 
	m33 = aGV1.GetzComp() * aGV2.GetzComp(); 

	m12 = 0.5*(aGV1.GetxComp() * aGV2.GetyComp() + aGV2.GetxComp() * aGV1.GetyComp()); 
	m13 = 0.5*(aGV1.GetxComp() * aGV2.GetzComp() + aGV2.GetxComp() * aGV1.GetzComp()); 
	m23 = 0.5*(aGV1.GetyComp() * aGV2.GetzComp() + aGV2.GetyComp() * aGV1.GetzComp()); 
//	m23 = 0.5*(aGV1.GetyComp() * aGV2.GetzComp() + aGV1.GetyComp() * aGV2.GetzComp()); 
}

//================================================
//  Constructor 3
//================================================
GSymmTensor::GSymmTensor(char* type)
{
	if (std::strcmp(type, "Identity") == 0) 
	{
		m11 = 1.0;
		m22 = 1.0;
		m33 = 1.0;
		m12 = 0;
		m13 = 0;
		m23 = 0;
	}
}

//================================================
//  Others
//================================================
const GSymmTensor& GSymmTensor::abs_max(const GSymmTensor& aten) const {
	GSymmTensor* result = sWorkSymmTensorRing.Next();
	result->m11 = std::max(std::abs(m11),std::abs(aten.m11));
	result->m22 = std::max(std::abs(m22),std::abs(aten.m22));
	result->m33 = std::max(std::abs(m33),std::abs(aten.m33));
	result->m12 = std::max(std::abs(m12),std::abs(aten.m12));
	result->m13 = std::max(std::abs(m13),std::abs(aten.m13));
	result->m23 = std::max(std::abs(m23),std::abs(aten.m23));
	return *result;
}

Scalar GSymmTensor::abs_max() const{
	Scalar absmax = std::max( std::max(std::abs(m11),std::max(std::abs(m22),std::abs(m33))),
			                  std::max(std::abs(m12),std::max(std::abs(m13),std::abs(m23)))
							);
	return absmax;
}

const GSymmTensor& GSymmTensor::bw_abs() const{
	GSymmTensor* result = sWorkSymmTensorRing.Next();
	result->m11 = std::abs(m11);
	result->m22 = std::abs(m22);
	result->m33 = std::abs(m33);
	result->m12 = std::abs(m12);
	result->m13 = std::abs(m13);
	result->m23 = std::abs(m23);
	return *result;
}

const GSymmTensor& GSymmTensor::bw_mindis(const GSymmTensor& aten){
	if( aten.m11*m11 <= 0 ) m11 = 0;
	else if( std::abs(aten.m11) < std::abs(m11) ) m11 = aten.m11;
	if( aten.m22*m22 <= 0 ) m22 = 0;
	else if( std::abs(aten.m22) < std::abs(m22) ) m22 = aten.m22;
	if( aten.m33*m33 <= 0 ) m33 = 0;
	else if( std::abs(aten.m33) < std::abs(m33) ) m33 = aten.m33;
	if( aten.m12*m12 <= 0 ) m12 = 0;
	else if( std::abs(aten.m12) < std::abs(m12) ) m12 = aten.m12;
	if( aten.m13*m13 <= 0 ) m13 = 0;
	else if( std::abs(aten.m13) < std::abs(m13) ) m13 = aten.m13;
	if( aten.m23*m23 <= 0 ) m23 = 0;
	else if( std::abs(aten.m23) < std::abs(m23) ) m23 = aten.m23;
	return *this;
}
const GSymmTensor& GSymmTensor::bw_max(const GSymmTensor& aten) const{
	GSymmTensor* result = sWorkSymmTensorRing.Next();
	result->m11 = std::max(m11, aten.m11);
	result->m22 = std::max(m22, aten.m22);
	result->m33 = std::max(m33, aten.m33);
	result->m12 = std::max(m12, aten.m12);
	result->m13 = std::max(m13, aten.m13);
	result->m23 = std::max(m23, aten.m23);
	return *result;
}
const GSymmTensor& GSymmTensor::bw_min(const GSymmTensor& aten) const{
	GSymmTensor* result = sWorkSymmTensorRing.Next();
	result->m11 = std::min(m11, aten.m11);
	result->m22 = std::min(m22, aten.m22);
	result->m33 = std::min(m33, aten.m33);
	result->m12 = std::min(m12, aten.m12);
	result->m13 = std::min(m13, aten.m13);
	result->m23 = std::min(m23, aten.m23);
	return *result;
}
//================================================
//  GSymmTensor::Zero
//================================================
void GSymmTensor::SetZeros()
{
	m11 = 0.0; 
	m12 = 0.0; 
	m22 = 0.0; 
	m13 = 0.0; 
	m23 = 0.0;
	m33 = 0.0;
}

void GSymmTensor::SetEye()
{
	m11 = 1.0; m12 = 0.0; m13 = 0.0;
	m22 = 1.0; m23 = 0.0;
	m33 = 1.0;
}

//================================================
//  GSymmTensor::Det
//================================================
Scalar GSymmTensor::Det() const
{
	return(   - m13 * m13 * m22 
			 + 2.0*m12 * m13 * m23 
			   - m11 * m23 * m23 
			   - m12 * m12 * m33 
			 + 	 m11 * m22 * m33);			
}

//================================================
//  GSymmTensor::NotifyOfInvertTol
//================================================
void GSymmTensor::NotifyOfInvertTol (Scalar tol)
{
	if (tol < sInvertTol)
		sInvertTol = tol;
}

//================================================
//  GSymmTensor::Invert
//================================================
Boolean GSymmTensor::Invert () 
{
  Scalar det =   - m13 * m13 * m22 
			 	+ 2.0*m12 * m13 * m23 
			   - m11 * m23 * m23 
			   - m12 * m12 * m33 
			 + 	 m11 * m22 * m33;
  //---To avoid overwriting
  Scalar tempm11 = m11;
  Scalar tempm12 = m12;
  Scalar tempm13 = m13;
  Scalar tempm22 = m22;
  Scalar tempm23 = m23;
  Scalar tempm33 = m33;
  
  if (AbsVal(det) < sInvertTol) {
  	return False; 
  	//---Should do some nice exception handling here
  	std::cout<< "WARNING: AbsVal(det) < sInvertTol "<<std::endl;
  }
  
  det = 1/det;
  m11 = (tempm22*tempm33 - tempm23*tempm23)*det;
  m12 = (tempm13*tempm23 - tempm12*tempm33)*det;
  m13 = (tempm12*tempm23 - tempm13*tempm22)*det;
  m22 = (tempm11*tempm33 - tempm13*tempm13)*det;
  m23 = (tempm12*tempm13 - tempm11*tempm23)*det;
  m33 = (tempm11*tempm22 - tempm12*tempm12)*det;

  return True;
}

///////////// order issue???
// Return component via vector notation
// m11 = 0, m22 =1, m33 = 2, m12 =3, m13 = 4, m23 = 5
// Changho add May 24, 2002 
Scalar GSymmTensor::GetComponent (int i) const
{
	Scalar res = 0.0;
	
	switch (i) 
	{
		case 0:
			res = m11; 
			break;
			
		case 1:
			res = m22; 
			break;
			
		case 2:
			res = m33; 
			break;
			
		case 3:
			res = m12; 
			break;
			
		case 4:
			res = m13; 
			break;
			
		case 5:
			res = m23; 
			break;
			
		default:
		
			std::stringstream dirStr;
			dirStr << i;
			std::string errMsg = "GSymmTensor::GetComp is called with the argument of "
			                   + dirStr.str();
//			ErrHandler::sErrMsg += "# "+ errMsg + ".\n";
			dirStr.str("");				
	}
	
	return res;
}

///////////// order issue???
// Return component via matrix notation
// m11 = (0,0), m22 = (1,1), m33 = (2,2), 
// m12 = (0,1) or (1,0), m13 = (0,2) or (2,0), m23 = (1,2) or (2,1)
// Wookuen added on April 25, 2008
Scalar GSymmTensor::GetComponent(int row, int col) const
{
	Scalar a = 0.0;
	if(0 > row || row > 2 || 0 > col || col > 2)
	{
		std::stringstream rowStr;
		rowStr << row;
		std::stringstream colStr;
		colStr << col;
		
		std::string errMsg = "GSymmTensor::GetComponent is called with the argument of "
		                   + rowStr.str() + " and " + colStr.str();
//		ErrHandler::sErrMsg += "# "+ errMsg + ".\n";
		rowStr.str("");		
		colStr.str("");		
	}
	else
	{
		int index = 3*row + col;	
		switch(index)
		{
			case 0:
			
				a = m11;
				break;
				
			case 1:
			
				a = m12;
				break;

			case 2:
			
				a = m13;
				break;

			case 3:
			
				a = m12;
				break;
				
			case 4:
			
				a = m22;
				break;

			case 5:
			
				a = m23;
				break;

			case 6:
			
				a = m13;
				break;
				
			case 7:
			
				a = m23;
				break;

			case 8:
			
				a = m33;
				break;
		}
	}
	return a;
}

///////////// order issue???
// Set component via vector notation
// m11 = 0, m22 =1, m33 = 2, m12 =3, m13 = 4, m23 = 5
// Changho add May 24, 2002 
void GSymmTensor::SetComponent (int i, Scalar val)
{
	switch (i) 

	{
		case 0:
			m11 = val; 
			break;
			
		case 1:
			m22 = val; 
			break;
			
		case 2:
			m33 = val; 
			break;
			
		case 3:
			m12 = val;
			break;
			
		case 4:
			m13 = val; 
			break;
			
		case 5:
			m23 = val; 
			break;
	}
}

void GSymmTensor::SetDiag(const GVector& aGV)
{
	m11 = aGV.GetxComp();
	m22 = aGV.GetyComp();
	m33 = aGV.GetzComp();
	m12 = 0.0;
	m13 = 0.0;
	m23 = 0.0;
}

void GSymmTensor::SetDiag(const Scalar& aS)
{
	m11 = aS;
	m22 = aS;
	m33 = aS;
	m12 = 0.0;
	m13 = 0.0;
	m23 = 0.0;
}

const GVector& GSymmTensor::Diag() const{
	GVector* result = GVector::sWorkVectorRing.Next();
	result -> SetComponents(m11,m22,m33);
	return *result;
}

const GSymmTensor& GSymmTensor::Diag(const GVector& aGV){
	m11 = aGV.GetxComp();
	m22 = aGV.GetyComp();
	m33 = aGV.GetzComp();
	return *this;
}

const GSymmTensor& GSymmTensor::Diag(const Scalar& aS){
	m11 = m22 = m33 = aS;
	return *this;
}

const GSymmTensor& GSymmTensor::addDiag(const GVector& aGV){
	m11 += aGV.GetxComp();
	m22 += aGV.GetyComp();
	m33 += aGV.GetzComp();
	return *this;
}

const GSymmTensor& GSymmTensor::addDiag(const Scalar& aS){
	m11 += aS;
	m22 += aS;
	m33 += aS;
	return *this;
}

const GSymmTensor& GSymmTensor::deDiag() const{
	GSymmTensor& res = *GSymmTensor::sWorkSymmTensorRing.Next();
	res.m11 = res.m22 = res.m33 = 0;
	res.m12 = m12;
	res.m13 = m13;
	res.m23 = m23;
	return res;
}

Scalar GSymmTensor::GetNorm() const
{
	Scalar norm;
	norm = m11*m11 + m22*m22 + m33*m33
	  + 2.0*(m12*m12 + m13*m13 + m23 * m23);
	  
	return std::sqrt(norm);
}

Scalar GSymmTensor::GetMax(){

	Scalar maxnumber = 0.0;

	if(std::abs(m11)>maxnumber){
		maxnumber = std::abs(m11);
	}
	if(std::abs(m22)>maxnumber){
		maxnumber = std::abs(m22);
	}
	if(std::abs(m33)>maxnumber){
		maxnumber = std::abs(m33);
	}
	if(std::abs(m12)>maxnumber){
		maxnumber = std::abs(m12);
	}
	if(std::abs(m13)>maxnumber){
		maxnumber = std::abs(m13);
	}
	if(std::abs(m23)>maxnumber){
		maxnumber = std::abs(m23);
	}

	return maxnumber;

}

//================================================
//  GSymmTensor::Invert w/ tolerance
//================================================
Boolean GSymmTensor::Invert (Scalar tolerance) 
{
  Scalar det = - m13 * m13 * m22 + 2.0 * m12 * m13 * m23 
			   - m11 * m23 * m23 
			   - m12 * m12 * m33 
			   + m11 * m22 * m33;
  //---To avoid overwriting
  Scalar tempm11 = m11;
  Scalar tempm12 = m12;
  Scalar tempm13 = m13;
  Scalar tempm22 = m22;
  Scalar tempm23 = m23;
  Scalar tempm33 = m33;
  
  if (AbsVal(det) < tolerance) {
  	return False; 
  	//---Should do some nice exception handling here
  }
  
  det = 1/det;
  m11 = (tempm22*tempm33 - tempm23*tempm23)*det;
  m12 = (tempm13*tempm23 - tempm12*tempm33)*det;
  m13 = (tempm12*tempm23 - tempm13*tempm22)*det;
  m22 = (tempm11*tempm33 - tempm13*tempm13)*det;
  m23 = (tempm12*tempm13 - tempm11*tempm23)*det;
  m33 = (tempm11*tempm22 - tempm12*tempm12)*det;

  return True;
}

GSymmTensor& GSymmTensor::GetDevPart() const
{
	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
	Scalar thetrace;

	thetrace = this->GetTrace()/3;
	result->m11 = (m11 - thetrace);
	result->m22 = (m22 - thetrace);
	result->m33 = (m33 - thetrace);
	result->m12 = m12;
	result->m13 = m13;
	result->m23 = m23;
	return* result;
}

GSymmTensor& GSymmTensor::GetVolPart() const
{
	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();

	Scalar thetrace;
	thetrace = this->GetTrace()/3;
	result->m11 = thetrace;
	result->m22 = thetrace;
	result->m33 = thetrace;
	result->m12 = 0.0;
	result->m13 = 0.0;
	result->m23 = 0.0;
	return* result;
}

GSymmTensor& GSymmTensor::GetVolDevSplit() const{

	// returns the tensor split into the vol and dev components.

	// Gives {theta, e_x, e_y, e_xy, e_xz, e_yz} where theta is the trace of the original tensor
	// and the e components are the deviatoric values. Note that e_z = -(e_x + e_y)

	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
	Scalar thetrace = this->GetTrace();
	GSymmTensor theDev = this->GetDevPart();

	result->m11 = thetrace;
	result->m22 = theDev.Getm11();
	result->m33 = theDev.Getm22();
	result->m12 = theDev.Getm12();
	result->m13 = theDev.Getm13();
	result->m23 = theDev.Getm23();
	return* result;

}

GSymmTensor& GSymmTensor::ReconstructFromVolDevSplit() const{

	// This reconstructs the original tensor after the above function deconstructed the original tensor

	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();

	Scalar volavg = m11/3.0;

	result->m11 = volavg + m22;
	result->m22 = volavg + m33;
	result->m33 = volavg - m22 - m33;
	result->m12 = m12;
	result->m13 = m13;
	result->m23 = m23;

	return* result;
}

GSymmTensor& GSymmTensor::GetDevPartPlane() const
{
	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
	Scalar thetrace = (m11+m22)/2.0;
	result->m11 = (m11 - thetrace);
	result->m22 = (m22 - thetrace);
	result->m33 = m33;
	result->m12 = m12;
	result->m13 = m13;
	result->m23 = m23;
	return* result;
}

GSymmTensor& GSymmTensor::GetLogarithmicDecomposition() const{

	// Computes the log portion of the tensor in question
	GVector eigVal;
	GTensor eigTen;

	this->EigenAnalysis(eigVal, eigTen);

    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();

	// Sets to zero (the eigenvalues of the original tensor are contained in eigVal)
	result->SetZeros();

	result->m11 = ( 0.5 * std::log(eigVal.GetxComp()) );
	result->m22 = ( 0.5 * std::log(eigVal.GetyComp()) );
	result->m33 = ( 0.5 * std::log(eigVal.GetzComp()) );
	result->PreDotPostTransposeDotEqual(eigTen);

	return* result;
}

GSymmTensor& GSymmTensor::GetLogarithmicDecomposition(GVector &  eigVal, GTensor & eigTen){

	// ASSUMES THE PRINCIPAL DIRECTIONS (AND VALUES) ARE ALREADY KNOWN!!!!!!!!!!!!!!!

    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();

	// Sets to zero (the eigenvalues of the original tensor are contained in eigVal)
	result->SetZeros();

	result->m11 = ( 0.5 * std::log(eigVal.GetxComp()) );
	result->m22 = ( 0.5 * std::log(eigVal.GetyComp()) );
	result->m33 = ( 0.5 * std::log(eigVal.GetzComp()) );
	result->PreDotPostTransposeDotEqual(eigTen);

	return* result;

}

GSymmTensor& GSymmTensor::GetExponentialDecomposition() const{

	// Computes the exp of of the tensor in question
	GVector eigVal;
	GTensor eigTen;

    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = m11;
    result->m12 = m12;
    result->m22 = m22;
	result->m13 = m13;
    result->m23 = m23;
    result->m33 = m33;

    result->EigenAnalysis(eigVal, eigTen);

	// Sets to zero (the eigenvalues of the original tensor are contained in eigVal)
	result->SetZeros();

	result->m11 = ( std::exp( 2*eigVal.GetxComp() ) );
	result->m22 = ( std::exp( 2*eigVal.GetyComp() ) );
	result->m33 = ( std::exp( 2*eigVal.GetzComp() ) );
	result->PreDotPostTransposeDotEqual(eigTen);

	return* result;

}

GSymmTensor& GSymmTensor::GetExponentialDecomposition(GVector &  eigVal, GTensor & eigTen){

	// ASSUMES THE PRINCIPAL DIRECTIONS  (AND VALUES)  ARE ALREADY KNOWN!!!!!!!!!!!!!!!

    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();

	// Sets to zero (the eigenvalues of the original tensor are contained in eigVal)
	result->SetZeros();

	result->m11 = ( std::exp( 2*eigVal.GetxComp() ) );
	result->m22 = ( std::exp( 2*eigVal.GetyComp() ) );
	result->m33 = ( std::exp( 2*eigVal.GetzComp() ) );
	result->PreDotPostTransposeDotEqual(eigTen);

	return* result;

}



Scalar GSymmTensor::DoubleDot(GSymmTensor& aST)
{   
    return ( m11*aST.m11 + m22*aST.m22 + m33*aST.m33
      + 2.0*(m12*aST.m12 + m13*aST.m13 + m23*aST.m23) );
}

//================================================
//
//  GSymmTensor::operator=
//
//================================================
GSymmTensor& GSymmTensor::operator=(const GSymmTensor& aST)
{
  m11 = aST.m11;
  m12 = aST.m12;
  m22 = aST.m22;
  m13 = aST.m13;
  m23 = aST.m23;
  m33 = aST.m33;
  return *this;
}

GSymmTensor& GSymmTensor::operator=(const GTensor& aT)
{
  m11 = aT.Getm11();
  m12 = aT.Getm12();
  m22 = aT.Getm22();
  m13 = aT.Getm13();
  m23 = aT.Getm23();
  m33 = aT.Getm33();
  return *this;
}

GSymmTensor& GSymmTensor::operator=(const Scalar& val)
{
  m11 = m12 = m22 = m13 = m23 = m33 = val;
  return *this;
}

GSymmTensor& GSymmTensor::operator+(const GSymmTensor& aST) const
{
	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = m11 + aST.m11;
    result->m12 = m12 + aST.m12;
    result->m22 = m22 + aST.m22;
	result->m13 = m13 + aST.m13;
    result->m23 = m23 + aST.m23;
    result->m33 = m33 + aST.m33;
    return *result;
}

GSymmTensor& GSymmTensor::operator+ (Scalar aS) const{
	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = m11 + aS;
    result->m12 = m12 + aS;
    result->m22 = m22 + aS;
	result->m13 = m13 + aS;
    result->m23 = m23 + aS;
    result->m33 = m33 + aS;
    return *result;
}

GSymmTensor& GSymmTensor::operator-(const GSymmTensor& aST) const
{
    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = m11 - aST.m11;
    result->m12 = m12 - aST.m12;
    result->m22 = m22 - aST.m22;
	result->m13 = m13 - aST.m13;
    result->m23 = m23 - aST.m23;
    result->m33 = m33 - aST.m33;
    return *result;
}

GSymmTensor& GSymmTensor::operator-() const
{
    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = -m11;
    result->m12 = -m12;
    result->m22 = -m22;
	result->m13 = -m13;
    result->m23 = -m23;
    result->m33 = -m33;
    return *result;
}
GSymmTensor& GSymmTensor::operator*(const Scalar& a) const
{
	GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = m11 * a;
    result->m12 = m12 * a;
    result->m22 = m22 * a;
	result->m13 = m13 * a;
    result->m23 = m23 * a;
    result->m33 = m33 * a;
    return *result;
}

GVector& GSymmTensor::operator*(const GVector& aGV) const
{
	GVector* result = aGV.sWorkVectorRing.Next();
    Scalar s1 = m11*aGV.GetxComp() + m12*aGV.GetyComp() 
              + m13*aGV.GetzComp();
           
    Scalar s2 = m12*aGV.GetxComp() + m22*aGV.GetyComp()
    		  + m23*aGV.GetzComp();
    		  
    Scalar s3 = m13*aGV.GetxComp() + m23*aGV.GetyComp() 
    		  + m33*aGV.GetzComp();
    		  
	result->SetComponents(s1,s2,s3);
    return *result;
}

GVector& GSymmTensor::VecScalarMult(const GVector& aGV, Scalar a) const
{
  GVector* result = aGV.sWorkVectorRing.Next();
    Scalar s1 = m11*aGV.GetxComp() + m12*aGV.GetyComp() 
              + m13*aGV.GetzComp();
           
    Scalar s2 = m12*aGV.GetxComp() + m22*aGV.GetyComp()
    		  + m23*aGV.GetzComp();
    		  
    Scalar s3 = m13*aGV.GetxComp() + m23*aGV.GetyComp() 
    		  + m33*aGV.GetzComp();
    		  
	result->SetComponents(s1*a,s2*a,s3*a);
    return *result;  
}

GTensor& GSymmTensor::operator*(const GSymmTensor& aST) const
{
//    GTensor* result = GSymmTensor::sWorkTensorRing.Next();
	GTensor* result = GTensor::sWorkTensorRing.Next(); // Wookuen modified
	 
    result->SetComponents( m11*aST.m11 + m12*aST.m12 + m13*aST.m13,
                 		   m11*aST.m12 + m12*aST.m22 + m13*aST.m23,
   						   m11*aST.m13 + m12*aST.m23 + m13*aST.m33,
    
						   m12*aST.m11 + m22*aST.m12 + m23*aST.m13,
		   				   m12*aST.m12 + m22*aST.m22 + m23*aST.m23,
		 				   m12*aST.m13 + m22*aST.m23 + m23*aST.m33,
    
		      		  	   m13*aST.m11 + m23*aST.m12 + m33*aST.m13,
   						   m13*aST.m12 + m23*aST.m22 + m33*aST.m23,
   						   m13*aST.m13 + m23*aST.m23 + m33*aST.m33);
    return *result;
}

GTensor& GSymmTensor::operator*(const GTensor& aT) const
{
//    GTensor* result = GSymmTensor::sWorkTensorRing.Next();
	GTensor* result = GTensor::sWorkTensorRing.Next(); // Wookuen modified
		 
    result->SetComponents( m11*aT.Getm11() + m12*aT.Getm21() + m13*aT.Getm31(),
                 		   m11*aT.Getm12() + m12*aT.Getm22() + m13*aT.Getm32(),
   						   m11*aT.Getm13() + m12*aT.Getm23() + m13*aT.Getm33(),
    
						   m12*aT.Getm11() + m22*aT.Getm21() + m23*aT.Getm31(),
		   				   m12*aT.Getm12() + m22*aT.Getm22() + m23*aT.Getm32(),
		 				   m12*aT.Getm13() + m22*aT.Getm23() + m23*aT.Getm33(),
    
		      		  	   m13*aT.Getm11() + m23*aT.Getm21() + m33*aT.Getm31(),
   						   m13*aT.Getm12() + m23*aT.Getm22() + m33*aT.Getm32(),
   						   m13*aT.Getm13() + m23*aT.Getm23() + m33*aT.Getm33());
    return *result;
}

GSymmTensor& GSymmTensor::operator/(const Scalar& a) const
{
    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
    result->m11 = m11 / a;
    result->m12 = m12 / a;
    result->m22 = m22 / a;
	result->m13 = m13 / a;
    result->m23 = m23 / a;
    result->m33 = m33 / a;
    return *result;
}

GSymmTensor& GSymmTensor::operator+=(const GSymmTensor& aST)
{
	m11 += aST.m11;
	m12 += aST.m12;
	m22 += aST.m22;
	m13 += aST.m13;
	m23 += aST.m23;
	m33 += aST.m33;
	return *this;
}

GSymmTensor& GSymmTensor::operator-=(const GSymmTensor& aST)
{
	m11 -= aST.m11;
	m12 -= aST.m12;
	m22 -= aST.m22;
	m13 -= aST.m13;
	m23 -= aST.m23;
	m33 -= aST.m33;
	return *this;
}

GSymmTensor& GSymmTensor::operator*=(const Scalar& a)
{
	 m11 *= a;
	 m12 *= a;
	 m22 *= a;
	 m13 *= a;
	 m23 *= a;
	 m33 *= a;
	 return *this;
}

GSymmTensor& GSymmTensor::operator*=(const GSymmTensor& aST)
{
	 m11 *= aST.m11;
	 m12 *= aST.m12;
	 m22 *= aST.m22;
	 m13 *= aST.m13;
	 m23 *= aST.m22;
	 m33 *= aST.m33;
	 return *this;
}

GSymmTensor& GSymmTensor::operator/=(const Scalar& a)
{
	m11 /= a;
	m12 /= a;
	m22 /= a;
	m13 /= a;
	m23 /= a;
	m33 /= a;
	return *this;
}

//================================================
//
//  GSymmTensor::GetFullTensor
//  (Could just write a constructor GTensor(GSymmTensor)?)
//
//================================================
GTensor& GSymmTensor::GetFullTensor()
{
	GTensor* result = sWorkTensorRing.Next();
	result->SetComponents(m11,m12,m13,m12,m22,m23,m13,m23,m33);
	return *result;
}

// get eigen values and the tensor product of eigen vectors
void GSymmTensor::EigenAnalysis(GVector& eigVals, GTensor& eigVecs) const
{
	GSymmTensor eigen = (*this);
	
 	int p, q, r;
 	int sign;
 	Scalar diag_p;
 	Scalar diag_q;
 	Scalar eigen_pq;
 	GTensor tenR(1,0,0, 0,1,0, 0,0,1);
 	GTensor eye (1,0,0, 0,1,0, 0,0,1);
 	GTensor Q;
 	Scalar theta = 0.0;
 	GVector vecA, vecS, vecT;
 	
 	std::vector<GVector>  SijkVec;

    // define GSymmTensor::sIJK to compute eigenvalues
    GVector ijk(2, 0, 1);
    SijkVec.push_back(ijk);

    ijk.SetComponents(1, 2, 0);
    SijkVec.push_back(ijk);

    ijk.SetComponents(0, 1, 2);
    SijkVec.push_back(ijk);

	for(int i = 0; i < 3; i++)
	{
		for(unsigned int j = 0; j < 3; j++)
		{
			p = (int)  SijkVec[j].GetxComp();
			q = (int)  SijkVec[j].GetyComp();
			r =  (int)  SijkVec[j].GetzComp();
			
			diag_p = eigen.GetComponent(p, p);
			diag_q = eigen.GetComponent(q, q);
			eigen_pq = eigen.GetComponent(p, q);
			
			if(std::abs(eigen_pq) < 1e-12) // avoid singularity
				continue;
			
			if(std::abs(diag_p - diag_q) < 1e-12) // avoid singularity
			{
				sign = eigen_pq >= 0.0 ? 1 : -1;
				theta = sign*0.25*PI;
			}
			else
			{
			    theta = 0.5*std::atan( 2.0*eigen_pq/(diag_p - diag_q) );
			}           
			vecA = tenR.GetTranspose() * tenR.GetCol(r);
			
			if(r < 2)
				vecS = tenR.GetTranspose() * tenR.GetCol(r + 1);
			else
				vecS = tenR.GetTranspose() * tenR.GetCol(0);
			
			vecT = vecA^vecS; // cross product
			GTensor tenTS(vecT, vecS); // tensor product
			GTensor tenST(vecS, vecT); // tensor product
			GTensor tenAA(vecA, vecA); // tensor product
			
			Q = eye*std::cos(theta) + tenAA*(1-std::cos(theta)) 
			  + (tenTS - tenST) * std::sin(theta);
			
			eigen = Q.GetTranspose() * eigen * Q;
			tenR *= Q;
		}
		
	}
	
	eigVals.SetComponents(eigen.Getm11(), eigen.Getm22(), eigen.Getm33());
	eigVecs = tenR;
}

//----computes s = aT.s.aT^T
void GSymmTensor::PreDotPostTransposeDotEqual(const GTensor &aT)
{
    //---post transpose dot
    Scalar tempm11 = m11*aT.m11 + m12*aT.m12 + m13*aT.m13;
	Scalar tempm12 = m11*aT.m21 + m12*aT.m22 + m13*aT.m23;
	Scalar tempm13 = m11*aT.m31 + m12*aT.m32 + m13*aT.m33;
	
	Scalar tempm21 = m12*aT.m11 + m22*aT.m12 + m23*aT.m13;
	Scalar tempm22 = m12*aT.m21 + m22*aT.m22 + m23*aT.m23;
	Scalar tempm23 = m12*aT.m31 + m22*aT.m32 + m23*aT.m33;
	
	Scalar tempm31 = m13*aT.m11 + m23*aT.m12 + m33*aT.m13;
	Scalar tempm32 = m13*aT.m21 + m23*aT.m22 + m33*aT.m23;
	Scalar tempm33 = m13*aT.m31 + m23*aT.m32 + m33*aT.m33;
	
    //---predot (symmetry implied)
    m11 = aT.m11 * tempm11 + aT.m12 * tempm21 + aT.m13 * tempm31;
    m12 = aT.m11 * tempm12 + aT.m12 * tempm22 + aT.m13 * tempm32;
    m13 = aT.m11 * tempm13 + aT.m12 * tempm23 + aT.m13 * tempm33;
    
    m22 = aT.m21 * tempm12 + aT.m22 * tempm22 + aT.m23 * tempm32;
    m23 = aT.m21 * tempm13 + aT.m22 * tempm23 + aT.m23 * tempm33;
    
    m33 = aT.m31 * tempm13 + aT.m32 * tempm23 + aT.m33 * tempm33;

}

//---------------------------
//
// friend functions
//
//---------------------------

GSymmTensor& operator*(const Scalar& aS,GSymmTensor& aST)
{
    GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
	result->m11 = aST.m11 * aS;
    result->m12 = aST.m12 * aS;
    result->m22 = aST.m22 * aS;
    result->m13 = aST.m13 * aS;
    result->m23 = aST.m23 * aS;
    result->m33 = aST.m33 * aS;
    return *result;
}

GSymmTensor& operator*=(const Scalar& aS,GSymmTensor& aT)
{
	 GSymmTensor* result = GSymmTensor::sWorkSymmTensorRing.Next();
     result->m11 = aT.m11 * aS;
     result->m12 = aT.m12 * aS;
     result->m13 = aT.m13 * aS;
     result->m22 = aT.m22 * aS;
     result->m23 = aT.m23 * aS;
     result->m33 = aT.m33 * aS;
     return  *result;
}


void GSymmTensor::Print(std::ostream* file) const{
    (*file) << m11 <<"\t" << m22 << "\t" << m33 << "\t"
               << m12 << "\t"<< m13 << "\t"<< m23 <<"\t";
}

void GSymmTensor::Print(std::ostream* file, std::string sep) const{
    (*file) << m11 << sep << m22 << sep << m33 << sep
            << m12 << sep << m13 << sep << m23;
}
void GSymmTensor::PrintBin(std::ostream* file) const{
	unsigned int Ldat = sizeof(m11);
	file->write( (char*) &m11, Ldat);
	file->write( (char*) &m22, Ldat);
	file->write( (char*) &m33, Ldat);
	file->write( (char*) &m12, Ldat);
	file->write( (char*) &m13, Ldat);
	file->write( (char*) &m23, Ldat);
}


//================================================
//  	static ring stuff
//================================================
//---Static work space
GSymmTensorRing GSymmTensor::sWorkSymmTensorRing;
GTensorRing GSymmTensor::sWorkTensorRing;
GVectorRing GVector::sWorkVectorRing;

GSymmTensorRing::GSymmTensorRing(){
	int ringCirc = SYMMTENSOR_RING_DEPTH;
	
	for(int i = 0; i < ringCirc; i++)
		 mElts[i] = new SymmTensorRingElt(); 
	
	for(int j = 0; j < ringCirc - 1; j++)
		mElts[j]->SetNext(*mElts[j+1]);
	//---Close the loop
		mElts[ringCirc-1]->SetNext(*mElts[0]);
		mCurrentElt = mElts[0];
}

GSymmTensorRing::~GSymmTensorRing(){
	int ringCirc = SYMMTENSOR_RING_DEPTH;
	
	for(int i = 0; i < ringCirc; i++)
		delete mElts[i];
}


GSymmTensor* GSymmTensorRing::Next(){
	mCurrentElt = mCurrentElt->GetNext();
	return &(mCurrentElt->GetTensor());
}		

void GSymmTensor::Zero()
{
	m11 = m12 = m22 = m13 = m23 = m33 = 0;
}

void  GSymmTensor::DotAssign(GVector& aGV, GVector& target)
{
#ifdef _2D_
	target.vx = m11*aGV.vx + m12*aGV.vy;
	target.vy = m12*aGV.vx + m22*aGV.vy;
#else
//	target.vx = m11*aGV.vx + m12*aGV.vy + m13*aGV.vz;
//	target.vy = m12*aGV.vx + m22*aGV.vy + m23*aGV.vz;
//	target.vz = m13*aGV.vx + m23*aGV.vy + m33*aGV.vz;

	target.SetxComponent(  m11*aGV.GetxComp() + m12*aGV.GetyComp() + m13*aGV.GetzComp()  );
	target.SetyComponent(  m12*aGV.GetxComp() + m22*aGV.GetyComp() + m23*aGV.GetzComp()  );
	target.SetzComponent(  m13*aGV.GetxComp() + m23*aGV.GetyComp() + m33*aGV.GetzComp()  );

#endif
}

void GSymmTensor::DotAssign(GVector& aGV)
{
#ifdef _2D_
	double q1, q2;
	q1 = m11*aGV.vx + m12*aGV.vy;
	q2 = m12*aGV.vx + m22*aGV.vy;
	aGV.vx = q1;
	aGV.vy = q2;
#else
//	double q1, q2, q3;
//	q1 = m11*aGV.vx + m12*aGV.vy + m13*aGV.vz;
//	q2 = m12*aGV.vx + m22*aGV.vy + m23*aGV.vz;
//	q3 = m13*aGV.vx + m23*aGV.vy + m33*aGV.vz;
//	aGV.vx = q1;
//	aGV.vy = q2;
//	aGV.vz = q3;

	aGV.SetxComponent(  m11*aGV.GetxComp() + m12*aGV.GetyComp() + m13*aGV.GetzComp()  );
	aGV.SetyComponent(  m12*aGV.GetxComp() + m22*aGV.GetyComp() + m23*aGV.GetzComp()  );
	aGV.SetzComponent(  m13*aGV.GetxComp() + m23*aGV.GetyComp() + m33*aGV.GetzComp()  );

#endif
}
void GSymmTensor::DotAssign(GTensor& kij, GTensor& target)
{
#ifdef _2D_
	target.m11 = m11*kij.m11 + m12*kij.m21;
	target.m12 = m11*kij.m12 + m12*kij.m22;
	target.m21 = m12*kij.m11 + m22*kij.m21;
	target.m22 = m12*kij.m12 + m22*kij.m22;
#else
	target.m11 = m11*kij.m11 + m12*kij.m21 + m13*kij.m31;
	target.m12 = m11*kij.m12 + m12*kij.m22 + m13*kij.m32;
	target.m13 = m11*kij.m13 + m12*kij.m23 + m13*kij.m33;

	target.m21 = m12*kij.m11 + m22*kij.m21 + m23*kij.m31;
	target.m22 = m12*kij.m12 + m22*kij.m22 + m23*kij.m32;
	target.m23 = m12*kij.m13 + m22*kij.m23 + m23*kij.m33;

	target.m31 = m13*kij.m11 + m23*kij.m21 + m33*kij.m31;
	target.m32 = m13*kij.m12 + m23*kij.m22 + m33*kij.m32;
	target.m33 = m13*kij.m13 + m23*kij.m23 + m33*kij.m33;
#endif
}

void GSymmTensor::GaussOpAssign(GTensor& kij, GSymmTensor& kjj)
{
#ifdef _2D_
	double q11, q12, q21, q22;

	q11 = m11*kij.m11 + m12*kij.m21;
	q12 = m11*kij.m12 + m12*kij.m22;
	q21 = m12*kij.m11 + m22*kij.m21;
	q22 = m12*kij.m12 + m22*kij.m22;

	kjj.m11 -= kij.m11 * q11 + kij.m21 * q21;
	kjj.m12 -= kij.m11 * q12 + kij.m21 * q22;
	kjj.m22 -= kij.m12 * q12 + kij.m22 * q22;
#else
	double q11, q12, q13, q21, q22, q23, q31, q32, q33;

	q11 = m11*kij.m11 + m12*kij.m21 + m13*kij.m31;
	q12 = m11*kij.m12 + m12*kij.m22 + m13*kij.m32;
	q13 = m11*kij.m13 + m12*kij.m23 + m13*kij.m33;

	q21 = m12*kij.m11 + m22*kij.m21 + m23*kij.m31;
	q22 = m12*kij.m12 + m22*kij.m22 + m23*kij.m32;
	q23 = m12*kij.m13 + m22*kij.m23 + m23*kij.m33;

	q31 = m13*kij.m11 + m23*kij.m21 + m33*kij.m31;
	q32 = m13*kij.m12 + m23*kij.m22 + m33*kij.m32;
	q33 = m13*kij.m13 + m23*kij.m23 + m33*kij.m33;

	//---and now kjj -= kijT . q  (which is  symm)
	kjj.m11 -= kij.m11 * q11 + kij.m21 * q21 + kij.m31 * q31;
	kjj.m12 -= kij.m11 * q12 + kij.m21 * q22 + kij.m31 * q32;
	kjj.m13 -= kij.m11 * q13 + kij.m21 * q23 + kij.m31 * q33;

	kjj.m22 -= kij.m12 * q12 + kij.m22 * q22 + kij.m32 * q32;
	kjj.m23 -= kij.m12 * q13 + kij.m22 * q23 + kij.m32 * q33;

	kjj.m33 -= kij.m13 * q13 + kij.m23 * q23 + kij.m33 * q33;
#endif
}

