#include <stdio.h>
#include <time.h>

#include "GVector.h"
#include "GTensor.h"
#include "GSymmTensor.h"

GVector GVector::sIdentity(1.0, 1.0, 1.0);

//// #ifdef _COUNT_FLOPS_
//double GVector::sFlopCount = 0.0;
////  #endif

// WooKuen added on 7/16/07
Scalar GVector::GetComp(int whichDir) const
{
	Scalar component = 0.0;
	
	switch(whichDir)
	{
		case 0:
			component = vx;
			break;

		case 1:
			component = vy;
			break;

		case 2:
			component = vz;
			break;
			
		default:
			
			std::stringstream dirStr;
			dirStr << whichDir;
			std::string errMsg = "GVector::GetComp is called with the argument of "
			                   + dirStr.str();
//			ErrHandler::sErrMsg += "# "+ errMsg + ".\n";
			dirStr.str("");
			break;
	}
		
	return component;		
}

Scalar GVector::GetNorm() const
{
	Scalar norm = vx*vx + vy*vy + vz*vz;
	return std::sqrt(norm);
}

GVector& GVector::abs_max(const GVector& avec) const {
	GVector* result = sWorkVectorRing.Next();
	result->vx = std::max(std::abs(vx),std::abs(avec.vx));
	result->vy = std::max(std::abs(vy),std::abs(avec.vy));
	result->vz = std::max(std::abs(vz),std::abs(avec.vz));
	return *result;
}

GVector& GVector::abs_max(const Scalar& val) const {
	GVector* result = sWorkVectorRing.Next();
	Scalar absv = std::abs(val);
	result->vx = std::max(std::abs(vx),absv);
	result->vy = std::max(std::abs(vy),absv);
	result->vz = std::max(std::abs(vz),absv);
	return *result;
}

// WooKuen Added on 8/23/07
void GVector::SetComp(int whichDir, Scalar val)
{
	switch(whichDir)
	{
		case 0:
			vx = val;
			break;
		
		case 1:
			vy = val;
			break;

		case 2:
			vz = val;
			break;
	}
}

//=====================================================================
//	operator Definition (GVector)
//=====================================================================
GVector& GVector::operator+=(const GVector &aGV)
{
      vx += aGV.vx;
      vy += aGV.vy;
      vz += aGV.vz;
      return *this;
}

// WooKuen added this function on 01/02/08
GVector& GVector::operator+(const Scalar &aS) const
{
	GVector* result = sWorkVectorRing.Next();
	result->vx = vx + aS;
	result->vy = vy + aS;
	result->vz = vz + aS;
	return *result;
}

// WooKuen added this function on 01/02/08
GVector& GVector::operator+=(const Scalar &aS)
{
      vx += aS;
      vy += aS;
      vz += aS;
      return *this;
}

GVector& GVector::operator-(const GVector &aGV) const
{
     GVector* result = aGV.sWorkVectorRing.Next();
     result->vx = vx - aGV.vx;
     result->vy = vy - aGV.vy;
     result->vz = vz - aGV.vz;
     return *result;
}

GVector& GVector::operator-() const
{
     GVector* result = sWorkVectorRing.Next();
     result->vx = -vx;
     result->vy = -vy;
     result->vz = -vz;
     return *result;
}

GVector& GVector::operator-=(const GVector &aGV)
{
// #ifdef _COUNT_FLOPS_
////  sFlopCount += 3;
//  #endif
      vx -= aGV.vx;
      vy -= aGV.vy;
      vz -= aGV.vz;
      return *this;
}

// WooKuen added this function on 01/02/08
GVector& GVector::operator-(const Scalar &aS) const
{
	GVector* result = sWorkVectorRing.Next();
	result->vx = vx - aS;
	result->vy = vy - aS;
	result->vz = vz - aS;
	return *result;
}

// WooKuen added this function on 01/02/08
GVector& GVector::operator-=(const Scalar &aS)
{
      vx -= aS;
      vy -= aS;
      vz -= aS;
      return *this;
}


GVector& GVector::operator*=(const Scalar &aS)
{
// #ifdef _COUNT_FLOPS_
////  sFlopCount += 3;
//  #endif
      vx *= aS;
      vy *= aS;
      vz *= aS;
      return *this;
}

GVector& GVector::operator/(const Scalar &aS) const
{
     GVector* result = sWorkVectorRing.Next();
     result->vx = vx / aS;
     result->vy = vy / aS;
     result->vz = vz / aS;
     return *result;
}


GVector& GVector::operator/=(const Scalar &aS)
{
// #ifdef _COUNT_FLOPS_
////  sFlopCount += 3;
//  #endif
      vx /= aS;
      vy /= aS;
      vz /= aS;
      return *this;
}

/*
GVector& GVector::operator*(const GVector &aGV) const
{
// #ifdef _COUNT_FLOPS_
////  sFlopCount += 3;
//  #endif
     GVector* result = sWorkVectorRing.Next();
     result->vx = vx * aGV.vx;
     result->vy = vy * aGV.vy;
     result->vz = vz * aGV.vz;
     return *result;
}
*/

GVector& GVector::operator*=(const GVector &aGV)
{
// #ifdef _COUNT_FLOPS_
////  sFlopCount += 3;
//  #endif
      vx *= aGV.vx;
      vy *= aGV.vy;
      vz *= aGV.vz;
      return *this;
} 


GVector& GVector::operator/=(const GVector &aGV)
{
      vx /= aGV.vx;
      vy /= aGV.vy;
      vz /= aGV.vz;
      return *this;
}


//** Wookuen Shin added this on 02/28/06(inner product)
Scalar GVector::operator &&(const GVector &aGV) const
{
	return (vx*aGV.vx + vy*aGV.vy + vz*aGV.vz);	
}

// cross product
GVector GVector::operator^ (const GVector &aGV) const
{
	GVector* result = sWorkVectorRing.Next();
    result->vx = vy * aGV.vz - vz * aGV.vy;
    result->vy = vz * aGV.vx - vx * aGV.vz;
    result->vz = vx * aGV.vy - vy * aGV.vx;
    return *result;
}

// dyodic product
const GTensor& GVector::operator& (const GVector &aGV) const
{
	GTensor& resultTensor = *GTensor::sWorkTensorRing.Next();
	resultTensor.Setm11(vx*aGV.vx);
	resultTensor.Setm12(vx*aGV.vy);
	resultTensor.Setm13(vx*aGV.vz);
	
	resultTensor.Setm21(vy*aGV.vx);
	resultTensor.Setm22(vy*aGV.vy);
	resultTensor.Setm23(vy*aGV.vz);
	
	resultTensor.Setm31(vz*aGV.vx);
	resultTensor.Setm32(vz*aGV.vy);
	resultTensor.Setm33(vz*aGV.vz);	
		
	return resultTensor;
}

GSymmTensor GVector::DyodicSelf()
{
// #ifdef _COUNT_FLOPS_
//  sFlopCount += 6;
//  #endif
    GSymmTensor symTen;

    symTen.Setm11(vx*vx);
    symTen.Setm12(vx*vy);
    symTen.Setm13(vx*vz);
    
    symTen.Setm22(vy*vy);
    symTen.Setm23(vy*vz);
    
    symTen.Setm33(vz*vz);
    
    return symTen;
}

// WARNING: Be careful about this operation... WS 08/01/07
//** Wookuen Shin added on 9/14/05
GVector& GVector::operator%(const GVector &aGV) const
{
	GVector* result = sWorkVectorRing.Next();
	int intPart;
	
	double row = vx/aGV.vx;
	intPart = (int)(row);
	if(intPart != 0 && row == intPart)	// in the case of 0 remainder
		intPart -= 1;
		
	result->vx = vx - intPart*(aGV.vx);
	
	row = vy/aGV.vy;
	intPart = (int)(row);
	if(intPart != 0 && row == intPart)	// in the case of 0 remainder
		intPart -= 1;
			
	result->vy = vy - intPart*(aGV.vy);
	
	row = vz/aGV.vz;
	intPart = (int)(row);
	if(intPart != 0 && row == intPart)	// in the case of 0 remainder
		intPart -= 1;
			
	result->vz = vz - intPart*(aGV.vz);
	
	return *result;
}

// WARNING: Be careful about this operation... WS 08/01/07
bool GVector::operator!= (const GVector &aGV)
{
	int IsXsame, IsYsame, IsZsame, IsItSame;
	IsXsame = vx==aGV.vx ? 1:0;		// 1 means they are the same
	IsYsame = vy==aGV.vy ? 1:0;
	IsZsame = vz==aGV.vz ? 1:0;
	IsItSame = IsXsame*IsYsame*IsZsame;

	if(0 == IsItSame) return true;	// 0 means they are different -> true
	else			  return false;
}

bool GVector::operator< (const GVector &vec) const{
	bool istat = (  vx < vec.vx &&
					vy < vec.vy &&
					vz < vec.vz);
	return istat;
}
bool GVector::operator> (const GVector &vec) const{
	bool istat = (  vx > vec.vx &&
					vy > vec.vy &&
					vz > vec.vz);
	return istat;
}
bool GVector::operator< (const Scalar& val) const{
	bool istat = (  vx < val &&
					vy < val &&
					vz < val);
	return istat;
}
bool GVector::operator> (const Scalar& val) const{
	bool istat = (  vx > val &&
					vy > val &&
					vz > val);
	return istat;
}

//*** Yang add on Apr 25 2014
bool GVector::any_GT (const Scalar& aval) const{
	bool istat = (  vx > aval ||
					vy > aval ||
					vz > aval);
	return istat;
}

//*** Yang add on Apr 25 2014
bool GVector::any_LT (const Scalar& aval) const{
	bool istat = (  vx < aval ||
					vy < aval ||
					vz < aval);
	return istat;
}

IntGVector GVector::floor(){
	int ix = (int) std::floor(vx);
	int iy = (int) std::floor(vy);
	int iz = (int) std::floor(vz);
	return IntGVector(ix, iy, iz);
}
IntGVector GVector::round(){
	int ix,iy,iz;
	if (vx < 0 ){ix = (int) (vx-0.5);} else{ix = (int) (vx+0.5);}
	if (vy < 0 ){iy = (int) (vy-0.5);} else{iy = (int) (vy+0.5);}
	if (vz < 0 ){iz = (int) (vz-0.5);} else{iz = (int) (vz+0.5);}
	return IntGVector(ix, iy, iz);
}



GVector& GVector::TakeLog(const GVector &aGV) 
{
     vx = std::log(aGV.vx);
     vy = std::log(aGV.vy);
     vz = std::log(aGV.vz);     
     return *this;
}

GVector& GVector::TakeExp(const GVector &aGV) 
{
     vx = std::exp(aGV.vx);
     vy = std::exp(aGV.vy);
     vz = std::exp(aGV.vz);     
     return *this;
}

GVector& GVector::GetDevPart() const
{
     GVector* result = sWorkVectorRing.Next();
     Scalar trace = vx + vy + vz;
     result->vx = vx - trace / 3.0;
     result->vy = vy - trace / 3.0;
     result->vz = vz - trace / 3.0;          
     return *result;
}

//=====================================================================
//	Friend Function  Operator*(Scalar, GVector)
//=====================================================================


GVector& operator*=(const Scalar& aNumber, GVector& aGV)
{
// #ifdef _COUNT_FLOPS_
//   GVector::sFlopCount += 3;
//  #endif
      aGV.vx = aGV.vx * aNumber;
      aGV.vy = aGV.vy * aNumber;
      aGV.vz = aGV.vz * aNumber;
      return aGV;
}

// This function is to get THE BASE PT OF A CELL from ANY coordinates
IntGVector GetBaseIndex(const GVector &aGV, const GVector& mesh)
{
 // #ifdef _COUNT_FLOPS_
//   GVector::sFlopCount += 3;
//  #endif
   Scalar sx, sy, sz;
    int ix, iy, iz;

	sx = aGV.vx/mesh.vx;
	ix = (int)sx;
	if(sx < 0)
		ix--;
	
	sy = aGV.vy/mesh.vy;
	iy = (int)sy;
	if(sy < 0)
		iy--;

	sz = aGV.vz/mesh.vz;
	iz = (int)sz;
	if(sz < 0)
		iz--;	

    IntGVector ans(ix, iy, iz);
    return ans;
}
//=====================================================================
//	other Function  (GVector)
//=====================================================================

void GVector::Print(std::ostream* file) const {
   (*file) << vx << "\t" << vy << "\t " << vz << "\t";
}
void GVector::Print(std::ostream* file, std::string sep) const {
   (*file) << vx << sep << vy << sep << vz;
}

void GVector::PrintBin(std::ostream* file) const{
	unsigned int Ldat = sizeof(vx);
	file->write( (char*) &vx, Ldat);
	file->write( (char*) &vy, Ldat);
	file->write( (char*) &vz, Ldat);
}
//=====================================================================
//	Vector and Scalar Ring Stuff
//=====================================================================

GVectorRing::GVectorRing()
{
	mInitedp = false;
	Init();
}

void GVectorRing::Init()
{
	if(mInitedp) return;
	int ringCirc = RING_DEPTH;
	
	for(int i = 0; i < ringCirc - 1; i++)
		mVectorRingElts[i].SetNext(mVectorRingElts[i+1]);
	//---Close the loop
		mVectorRingElts[ringCirc-1].SetNext(mVectorRingElts[0]);
		mCurrentElt = &mVectorRingElts[0];
		mInitedp = true;
}


GVectorRing::~GVectorRing()
{
}

GVector* GVectorRing::Next()
{
	mCurrentElt = mCurrentElt->GetNext();
	return &(mCurrentElt->GetVector());
}		



ScalarRing::ScalarRing()
{
	ScalarRing::Init();
}

void ScalarRing::Init()
{
	int ringCirc = RING_DEPTH;
	
	for(int i = 0; i < ringCirc; i++)
		 mScalarRingElts[i] = new ScalarRingElt(); 
	
	for(int j = 0; j < ringCirc - 1; j++)
		mScalarRingElts[j]->SetNext(*mScalarRingElts[j+1]);
	//---Close the loop
		mScalarRingElts[ringCirc-1]->SetNext(*mScalarRingElts[0]);
		mCurrentElt = mScalarRingElts[0];
}

ScalarRing::~ScalarRing()
{
	int ringCirc = RING_DEPTH;
	
	
	for(int i = 0; i < ringCirc; i++)
		delete mScalarRingElts[i];
}

Scalar* ScalarRing::Next()
{
	mCurrentElt = mCurrentElt->GetNext();
	return &(mCurrentElt->GetScalar());
}
void GVector::Print() const
{
//#ifdef _2D_
//	std::cout << "[" << vx << ", " << vy << "]" << std::endl;
//#else
//	std::cout << "[" << vx << ", " << vy << ", " << vz << "]" << std::endl;
	std::cout <<  vx << "\t" << vy << "\t" << vz << std::endl;
//#endif
}

void GVector::Zero()
{
	vx = 0;vy = 0;
//#ifndef _2D_
	vz = 0;
//#endif
}

