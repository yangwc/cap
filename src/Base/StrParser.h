/**************************************************
 * StrParser.h
 * 	   - StrParser()
 * ------------------------------------------------
 * 	+ useful functions for string
 * ================================================
 * Created by: YWC @ CEE, UW (Jan 24, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.01.28)
 **************************************************/

#ifndef _STRPARSER_H_
#define _STRPARSER_H_

#include <sstream>
#include <vector>
#include <string>


class StrParser {
private:
	std::string _mgins, _seps;
	std::string _temp_str;
	std::vector<std::string> _temp_strs;
public:
	StrParser();
	virtual ~StrParser(){;}

	inline void set_seperator(const std::string& seps){_seps = seps; }
	inline void set_margin(const std::string& mgins){_mgins = mgins; }

	const std::vector<std::string>& split(const std::string& str);
	const std::string& clean(const std::string& str);
	const std::string& uppercase(const std::string& str);
	const std::string& lowercase(const std::string& str);

	template <class _type>
	inline std::vector<_type> batch_convert(const std::vector<std::string>& strs){
		unsigned int Nv = strs.size();
		std::stringstream strconv;
		std::vector<_type> types( Nv );
		for(unsigned int i=0; i!= Nv; ++i){
			strconv.clear();
			strconv.str( strs[i] );
			strconv>>types[i];
		}
		return types;
	}

	template <class _type>
	inline _type convert(const std::string& str){
		std::stringstream strconv;
		_type val;
		strconv.str( str );
		strconv>>val;
		return val;
	}

};

#endif /* _STRPARSER_H_ */
