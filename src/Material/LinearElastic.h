/**************************************************
 * LinearElastic.h
 * 	   - LinearElastic()
 * ------------------------------------------------
 * 	+ Linear elastic material model;
 * 	+ small strain and rotation
 * ================================================
 * Created by: YWC @ CEE, UW (Sep 23, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/
#ifndef LINEARELASTIC_H_
#define LINEARELASTIC_H_

#include "Material.h"

class LinearElastic:public Material{

public:
	LinearElastic(unsigned int id, const std::vector<Scalar>& props);
	LinearElastic(LinearElastic* proto);
	~LinearElastic(){;}

	int set_state( const std::vector<Scalar>& statevals );

	void update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv );
	void get_state( std::vector<Scalar>& statevals ) const;

	inline Material* get_new_obj(){ return new LinearElastic(this); }

	inline Scalar get_wavespeed() const { return std::sqrt( _M/_rho ); }
	inline Scalar get_spec_shear_modulus() const { return _G/_rho; }
	inline Scalar get_spec_bulk_modulus() const { return _K/_rho; }
	inline Scalar get_spec_viscosity() const { return 0; }

	const GSymmTensor& get_stress();
	const GSymmTensor& get_specstress();
	virtual std::vector<Scalar> get_property() const;
	void get_property(std::vector<Scalar>& props) const;

	inline unsigned int _get_code() const {return 10000;}
protected:
	//	unsigned int _id;
	/*** Property ***/
	// Scalar _rhoo;
	Scalar _K, _G;

	/***State ***/
	//	int _Nval_state;
	//	Scalar _rho;
	GSymmTensor _stress;

	Scalar _lbd, _M;
	GSymmTensor _temp_symt;

};

#endif /* LINEARELASTIC_H_ */
