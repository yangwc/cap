/**************************************************
 * Material.h
 * 	   - Material()
 * ------------------------------------------------
 * 	+ interface of Material objects
 * ================================================
 * Created by: YWC @ CEE, UW (Sep 22, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.09.22)
 **************************************************/
#ifndef MATERIAL_H_
#define MATERIAL_H_

#include <string>
#include <map>

#include "../GB_Tensor.h"

class Material{
public:
	virtual ~Material(){;}

	virtual int set_state( const std::vector<Scalar>& statevals ) = 0;

	virtual void update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv ) = 0;
	virtual void get_state( std::vector<Scalar>& statevals ) const = 0;

	virtual Material* get_new_obj() = 0;

	// for artificial viscosity
	virtual Scalar get_wavespeed() const = 0;
	virtual Scalar get_spec_shear_modulus() const = 0; // G/rho
	virtual Scalar get_spec_bulk_modulus() const = 0;  // K/rho
	virtual Scalar get_spec_viscosity() const = 0;     // mu/rho

	virtual const GSymmTensor& get_stress() = 0;
	virtual const GSymmTensor& get_specstress() = 0;
	virtual std::vector<Scalar> get_property() const = 0;
	virtual void get_property(std::vector<Scalar>& props) const = 0;
	virtual unsigned int _get_code() const = 0;



	inline const Scalar& get_density() const {return _rho;}
	inline unsigned int get_Nstatevals() const { return _Nval_state; }
	inline unsigned int get_Nproperty() const { return _Nval_prop; }
	inline unsigned int get_id() const { return _id; }

protected:
	unsigned int _id;
	/*** Property ***/
	Scalar _rhoo;

	/*** State ***/
	unsigned int _Nval_state, _Nval_prop;
	Scalar _rho;
};

#endif /* MATERIAL_H_ */
