#include "IncompFluid.h"

//******************CONSTRUCTOR******************************************
IncompFluid::IncompFluid(unsigned int id, const std::vector<Scalar>& props){
	_id   = id;

	_Nval_prop = 3;
	_rhoo = props[0];
	_K    = props[1];
	_mu   = props[2];
	_rho  = _rhoo;

	/*****************************************************
	 * State:
	 *        1). p ( pressure );
	 *****************************************************/
	_Nval_state = 1;
	_p   = 0;
}

IncompFluid::IncompFluid(IncompFluid* proto){
	_id   = proto->_id;

	_Nval_prop = 3;
	_rhoo = proto->_rhoo;
	_rho  = _rhoo;
	_K    = proto->_K;
	_mu   = proto->_mu;

	_Nval_state = 1;

	_p     = proto->_p;
}

int IncompFluid::set_state(const std::vector<Scalar>& vals){
	if(  vals.size() != _Nval_state ){ return -1; }

	_p   = vals[ 0];
	return 0;
}

void IncompFluid::update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv ){
	_p    -= gradv.GetTrace()*dt*_K; // or (Jt - 1)K
}

void IncompFluid::get_state(std::vector<Scalar>& statevals) const{
	statevals.assign( _Nval_state, 0);
	statevals[ 0] = _p;
}

const GSymmTensor&  IncompFluid::get_stress(){
	GSymmTensor& sig = _temp_symt;
	sig.SetDiag( -_p );
	return sig;
}

const GSymmTensor&  IncompFluid::get_specstress(){
	GSymmTensor& sigb = _temp_symt;
	sigb = this -> get_stress() / _rhoo;
	return sigb;
}

std::vector<Scalar> IncompFluid::get_property() const{
	std::vector<Scalar> props;
	this -> get_property(props);
	return props;
}
void IncompFluid::get_property(std::vector<Scalar>& props) const{
	props.assign(_Nval_prop,0);
	props[0] = _rhoo;
	props[1] = _K;
	props[2] = _mu;
}

