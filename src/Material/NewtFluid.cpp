#include "NewtFluid.h"

//******************CONSTRUCTOR******************************************
NewtFluid::NewtFluid(unsigned int id, const std::vector<Scalar>& props){
	_id   = id;

	_Nval_prop = 3;
	_rhoo = props[0];
	_K    = props[1];
	_mu   = props[2];

	/*****************************************************
	 * State:
	 *        1). rho
	 *        2). p ( pressure );
	 *****************************************************/
	_Nval_state = 2;
	_rho = _rhoo;
	_p   = 0;
}

NewtFluid::NewtFluid(NewtFluid* proto){
	_id   = proto->_id;

	_Nval_prop = 3;
	_rhoo = proto->_rhoo;
	_K    = proto->_K;
	_mu   = proto->_mu;

	_Nval_state = 11;

	_rho   = proto->_rho;
	_p     = proto->_p;
}

int NewtFluid::set_state(const std::vector<Scalar>& vals){
	if(  vals.size() != _Nval_state ){ return -1; }

	_rho = vals[ 0];
	_p   = vals[ 1];
	return 0;
}

void NewtFluid::update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv ){
	_rho  /= Jt;
	_p    -= gradv.GetTrace()*dt*_K; // or Jt - 1
}

void NewtFluid::get_state(std::vector<Scalar>& statevals) const{
	statevals.assign( _Nval_state, 0);
	statevals[ 0] = _rho;
	statevals[ 1] = _p;
}

const GSymmTensor&  NewtFluid::get_stress(){
	GSymmTensor& sig = _temp_symt;
	sig.SetDiag( -_p );
	return sig;
}

const GSymmTensor&  NewtFluid::get_specstress(){
	GSymmTensor& sigb = _temp_symt;
	sigb = this -> get_stress() / _rhoo;
	return sigb;
}

std::vector<Scalar> NewtFluid::get_property() const{
	std::vector<Scalar> props;
	this -> get_property(props);
	return props;
}
void NewtFluid::get_property(std::vector<Scalar>& props) const{
	props.assign(_Nval_prop,0);
	props[0] = _rhoo;
	props[1] = _K;
	props[2] = _mu;
}

