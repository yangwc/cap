/**************************************************
 * IncompFluid.h
 * 	   - IncompFluid()
 * ------------------------------------------------
 * 	+ material model for Incompressible Newtonian fluid
 * ================================================
 * Created by: YWC @ CEE, UW (Jan 15, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.01.15)
 **************************************************/
#ifndef INCOMPFLUID_H_
#define INCOMPFLUID_H_

#include "Material.h"

class IncompFluid: public Material{
public:
	IncompFluid(unsigned int id, const std::vector<Scalar>& props);
	IncompFluid(IncompFluid* proto);
	~IncompFluid(){;}

	int set_state( const std::vector<Scalar>& statevals );

	void update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv );
	void get_state( std::vector<Scalar>& statevals ) const;

	inline Material* get_new_obj(){ return new IncompFluid(this); }

	inline Scalar get_wavespeed() const { return std::sqrt( _K/_rho ); }
	inline Scalar get_spec_shear_modulus() const { return 0; }
	inline Scalar get_spec_bulk_modulus() const { return _K/_rho; }
	inline Scalar get_spec_viscosity() const { return _mu/_rho; }

	const GSymmTensor& get_stress();
	const GSymmTensor& get_specstress();
	std::vector<Scalar> get_property() const;
	void get_property(std::vector<Scalar>& props) const;
	inline unsigned int _get_code() const { return 21000; }

protected:
//	void cal_ssd();

	/*** Property ***/
	Scalar _K, _mu;

	/*** State ***/
	Scalar  _p;

	GSymmTensor _temp_symt;
};

#endif /* INCOMPFLUID_H_ */
