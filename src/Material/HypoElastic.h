/**************************************************
 * HypoElastic.h
 * 	   - HypoElastic()
 * ------------------------------------------------
 * 	+ Hypo-elastic material model
 * ================================================
 * Created by: YWC @ CEE, UW (Sep 22, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.09.22)
 **************************************************/

#include "Material.h"

#ifndef HYPOELASTIC_H_
#define HYPOELASTIC_H_

class HypoElastic : public Material{

public:
	HypoElastic( unsigned int id, const std::vector<Scalar>& props);
	HypoElastic( HypoElastic* proto );
	~HypoElastic(){;}

	int set_state( const std::vector<Scalar>& statevals );

	void update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv );
	void get_state( std::vector<Scalar>& statevals ) const;

	inline Material* get_new_obj(){ return new HypoElastic(this); }

	inline Scalar get_wavespeed() const { return std::sqrt( _M/_rho ); }
	inline Scalar get_spec_shear_modulus() const { return _G/_rho; }
	inline Scalar get_spec_bulk_modulus() const { return _K/_rho; }
	inline Scalar get_spec_viscosity() const { return 0; }

	inline const GSymmTensor& get_stress() { return _stress; }
	inline const GSymmTensor& get_specstress(){ _temp_symt = _stress / _rhoo; return _temp_symt; }
	std::vector<Scalar> get_property() const;
	void get_property(std::vector<Scalar>& props) const;
	inline unsigned int _get_code() const {return 40000;}

protected:
	//	unsigned int _id;
	/*** Property ***/
	// Scalar _rhoo;
	Scalar _K, _G;

	/***State ***/
	//	int _Nval_state;
	//	Scalar _rho;
	GSymmTensor _stress;

	Scalar _lbd, _M;
	GSymmTensor _temp_symt;

};

#endif /* HYPOELASTIC_H_ */
