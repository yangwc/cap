#include "LinearElastic.h"

LinearElastic::LinearElastic(unsigned int id, const std::vector<Scalar>& props){
	_id   = id;

	_Nval_prop = 3;
	_rhoo = props[0];
	_K    = props[1];
	_G    = props[2];

	_lbd = _K - _G/1.5;
	_M   = _K + _G/0.75;



	/*****************************************************
	 * State:
	 *        1)-6). stress s (s11, s22, s33, s12, s13, s23);
	 *****************************************************/
	_Nval_state = 6;
//	_stress.SetZeros();
}

LinearElastic::LinearElastic(LinearElastic* proto){
	_id   = proto->_id;

	_Nval_prop = 3;
	_rhoo = proto->_rhoo;
	_K    = proto->_K;
	_G    = proto->_G;

	_lbd = _K - _G/1.5;
	_M   = _K + _G/0.75;

	_rho = _rhoo;

	_Nval_state = 6;

	_stress = proto->_stress;
}

int LinearElastic::set_state(const std::vector<Scalar>& statevals){
	if(  statevals.size() != _Nval_state ){ return -1; }

	_stress.Setm11( statevals[0] );
	_stress.Setm22( statevals[1] );
	_stress.Setm33( statevals[2] );
	_stress.Setm12( statevals[3] );
	_stress.Setm13( statevals[4] );
	_stress.Setm23( statevals[5] );

	return 0;
}

void LinearElastic::update_state( const Scalar& dt, const Scalar& tnow, const Scalar& Jt, const GTensor& gradv ){
	// change caused by strain rate
	Scalar dsvol = _lbd* gradv.GetTrace() *dt;
	_stress +=  ( (2.0*_G*dt)*gradv.GetSymmPart() ).addDiag( dsvol );
}

void LinearElastic::get_state(std::vector<Scalar>& statevals) const{
	statevals.assign( _Nval_state, 0 );
	statevals[0] = _stress.Getm11();
	statevals[1] = _stress.Getm22();
	statevals[2] = _stress.Getm33();
	statevals[3] = _stress.Getm12();
	statevals[4] = _stress.Getm13();
	statevals[5] = _stress.Getm23();
}

const GSymmTensor& LinearElastic::get_stress(){
	return _stress;
}
const GSymmTensor& LinearElastic::get_specstress(){
	GSymmTensor& sigb = _temp_symt;
	sigb = _stress/_rho;
	return sigb;
}

std::vector<Scalar> LinearElastic::get_property() const{
	std::vector<Scalar> props;
	this->get_property(props);
	return props;
}

void LinearElastic::get_property(std::vector<Scalar>& props) const{
	props.assign(_Nval_prop,0);
	props[0] = _rhoo;
	props[0] = _K;
	props[0] = _G;
}


