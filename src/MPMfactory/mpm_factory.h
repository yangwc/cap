/**************************************************
 * mpm_factory.h
 * 	   - mpmfactory()
 * ------------------------------------------------
 * 	+ The MPM factory
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 21, 2013)
 * ------------------------------------------------
 * Modified by: YWC (15.01.21)
 **************************************************/
#ifndef MPM_FACTORY_H_
#define MPM_FACTORY_H_

#include "../MPMcomputer/MPMcomputer.h"
#include "../MPMdatabase/O_err.h"
#include <string>

#include "mpmfac_computer.h"

class mpmfactory{
public:
	mpmfactory(){_oerr = 0;}
	~mpmfactory(){delete _oerr; _oerr = 0;}

	void set_order(const std::string& ifilename);
	void set_order(const std::string& ifilename, int& Istat);
	MPM* make_new_model();
	MPM* make_new_model(int& Istat);

	inline const std::string& get_mpm_version(){ return _modelfac.get_srcversion(); }
	inline const std::string& get_infilename(){ return _modelfac.get_infilename(); }

private:
	std::string statmessage(const int& Istat);
	outerr* _oerr;
	MFACcomp _modelfac;
};


#endif /* MPM_FACTORY_H_ */
