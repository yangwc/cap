#ifndef MPMFAC_FUNC_H
#define MPMFAC_FUNC_H

#include "../Function/Function.h"
#include "pattern/comfac_general.h"

class mpmfuncfac : public mpmgenrfac<function>{
public:
	mpmfuncfac();
	~mpmfuncfac(){;}

protected:
	function* get_new_prototype(int Ikey, int obID, int Is, vecsca* dptr);
};
#endif /* MPMFAC_FUNC_H */
