#include "mpmfac_bd.h"

#include "../MPMobject/GeoObject/_PK_geoobject.h"

/*************************************
 * input data
 *************************************/
enum boundaryKeywords {
	Planar = 1,
	SlipSurf,
	Surface,

	Fixed,
	Freecut,
	DVel,
	DAcc,

	LamiFlow,

	Size,
	Phase,
	DLayer,
	FLayer
};

// *** major keys *** //
unsigned int fac_Nm = 8;
int fac_idmain[]={	Planar, SlipSurf, Surface,
					Fixed, Freecut,
					DVel, DAcc,
					LamiFlow,
					};
std::string fac_nmmain[]={"Planar", "SlipSurf", "Surface",
							"Fixed", "Freecut",
							"VelBC", "AccBC",
							"LamiFlow",
							};
int fac_Ldatmain[]={ 7,7, 7,7, 8,8,
						7,7, 7,7,
						8,8, 8,8,
						4,4,
					};

// *** minor (side) keys *** //
unsigned int fac_Ns = 4;
int fac_idside[]={Size, Phase, DLayer, FLayer};
std::string fac_nmside[]={"Size", "Owner", "DLayer", "FLayer"};
int fac_Ldatside[]={ 3,4, 2,2, 10,11, 11,12 };

/*************************************
 * Auto Initialization
 *************************************/
mpmbdfac::mpmbdfac(){
	mykeystr = "Boundary";
	this -> build_keymap();
	this -> build_inputmap();
	this -> set_default();
}

mpmbdfac::~mpmbdfac(){
	for (std::vector<BC*>::iterator bcite = vecbcp.begin(); bcite != vecbcp.end(); ++bcite){
		delete (*bcite); (*bcite) = 0;
	}
}

void mpmbdfac::build_keymap(){
	/*
	 * map for string keyword to its ID defined in "enum boundaryKeywords{}".
	 */
    _Nkey = fac_Nm + fac_Ns;

    kgpmain.assign(fac_idmain, fac_idmain+fac_Nm);
    kgpside.assign(fac_idside, fac_idside+fac_Ns);
    _Ikeys.assign(fac_idmain, fac_idmain+fac_Nm);
    _Ikeys.insert(_Ikeys.end(), fac_idside, fac_idside+fac_Ns);

    unsigned int II;
    for (II=0; II<fac_Nm; II++){
    	_Ikey_map[fac_nmmain[II]] = fac_idmain[II];
    	_Skey_map[fac_idmain[II]]  = fac_nmmain[II];
    }
    for (II=0; II<fac_Ns; II++){
    	_Ikey_map[fac_nmside[II]] = fac_idside[II];
    	_Skey_map[fac_idside[II]]  = fac_nmside[II];
    }
}

void mpmbdfac::build_inputmap(){
	int Iend, Ista=0;
	for (unsigned int II=0; II< fac_Nm; II++){
		Iend = Ista + 2;
		expnum_map[fac_idmain[II]] = (*new std::vector<int>(fac_Ldatmain+Ista, fac_Ldatmain+Iend));
		Ista = Iend;
	}
	Ista=0;
	for (unsigned int II=0; II< fac_Ns; II++){
		Iend = Ista + 2;
		expnum_map[fac_idside[II]] = (*new std::vector<int>(fac_Ldatside+Ista, fac_Ldatside+Iend));
		Ista = Iend;
	}
}// end function

bool mpmbdfac::organize_data(){
	bool istat1 = this -> fill_infomap( this -> kgpside, this -> IDpower);
	bool istat2 = this -> fill_infomap( this -> kgpmain, this -> IDskip);
	return (istat1 && istat2);
}

BC* mpmbdfac::get_new_object(unsigned int Ibc, int objID){
	BC *bcptr = 0, *bcproto = vecbcp[Ibc];
	if( bcproto->is_accessible(objID) ){
		bcptr = bcproto-> get_new_boundary();
		bcptr-> set_owner(objID);
	}
	return bcptr;
}

/*************************************
 ***  ***  ***  ***  ***  ***  ***  **
 * functions about MAKING boundaries
 **  ***  ***  ***  ***  ***  ***  ***
 *************************************/
bool mpmbdfac::make_boundaries(mpmfuncfac* funcfac){
	this->make_planars(funcfac);
	this->make_slipsurfaces(funcfac);
	this->make_surfaces(funcfac);
	this->make_fixeds(funcfac);
	this->make_freecuts(funcfac);
	this->make_Dvels(funcfac);
	this->make_Daccs(funcfac);
	this->make_laminarflow(funcfac);
	return true;
}// end function

BC* mpmbdfac::general_setting(int ID, mpmfuncfac* funcfac, BC* bobj, Cuboid*& cubgeo){
	this -> set_size(ID, cubgeo);
	this -> set_phase(ID, bobj);

	bool ifound = this -> set_dlayer(ID, funcfac, cubgeo, bobj);
	if (not ifound) ifound = this -> set_flayer(ID, funcfac, cubgeo, bobj);
	if (not ifound) bobj -> assign_zone( cubgeo ); cubgeo = 0;

	return bobj;
}

/*************************************
 * major factory functions
 *************************************/
bool mpmbdfac::make_planars(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[Planar];
	mapivp& ibox = infomap[Planar];

	int ID, Il, Is;

	GVector x, n;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCplanar(ID, n);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;

		vecbcp.push_back(bc); bc = 0;
	}
	return true;
}// end function

bool mpmbdfac::make_slipsurfaces(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[SlipSurf];
	mapivp& ibox = infomap[SlipSurf];

	int ID, Il, Is;

	GVector x, n;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCplanarPre(ID, n);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;


		vecbcp.push_back(bc);
	}
	return true;
}// end function

bool mpmbdfac::make_surfaces(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[Surface];
	mapivp& ibox = infomap[Surface];

	int ID, Il, Is;

	GVector x, n;
	Scalar mu;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		mu = data[Is]; Is++;
		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCplanwfri(ID, n, mu);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;

		vecbcp.push_back(bc);
	}
	return true;
}// end function

bool mpmbdfac::make_fixeds(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[Fixed];
	mapivp& ibox = infomap[Fixed];

	int ID, Il, Is;

	GVector x, n;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);


		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCfixed(ID);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;

		vecbcp.push_back(bc);
	}
	return true;
}// end function

bool mpmbdfac::make_freecuts(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[Freecut];
	mapivp& ibox = infomap[Freecut];

	int ID, Il, Is;

	GVector x, n;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);


		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCfreecut(ID, n);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;

		vecbcp.push_back(bc);
	}
	return true;
}// end function

bool mpmbdfac::make_Dvels(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[DVel];
	mapivp& ibox = infomap[DVel];

	int ID, Il, Is;

	GVector x, n;
	function* func;
	int fid;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		fid  = (int) ( data[Is] + 0.01 ); Is++;
		func = funcfac -> get_new_object(fid);
		if(func == 0){
			std::cout<<"WARNING: unknown function ID "<<fid<<"."<<std::endl;
			continue;
		}

		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCvel(ID, n, func);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;

		vecbcp.push_back(bc);
	}
	return true;
}// end function

bool mpmbdfac::make_Daccs(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[DAcc];
	mapivp& ibox = infomap[DAcc];

	int ID, Il, Is;

	GVector x, n;
	function* func;
	int fid;

	Cuboid* geo;
	BClocal* bcraw;
	BCalgorithm* alg;
	BC* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		fid  = (int) ( data[Is] + 0.01 ); Is++;
		func = funcfac -> get_new_object(fid);
		if(func == 0){
			std::cout<<"WARNING: unknown function ID "<<fid<<"."<<std::endl;
			continue;
		}

		x.SetComponents( data[Is  ],data[Is+1],data[Is+2] );
		n.SetComponents( data[Is+3],data[Is+4],data[Is+5] );

		/*** create zone ***/
		geo = new Cuboid( x, n);
		n = geo -> get_normal();

		/*** create algorithm prototype ***/
		alg = new BCacc(ID, n, func);

		/*** create boundary condition ***/
		bcraw = new BClocal();
		bcraw -> assign_algorithm( alg ); alg = 0;

		bc = this -> general_setting( ID, funcfac, bcraw, geo );
		geo = 0;

		vecbcp.push_back(bc);
	}
	return true;
}// end function

bool mpmbdfac::make_laminarflow(mpmfuncfac* funcfac){
	vecsvp& dbox = alldata_map[LamiFlow];
	mapivp& ibox = infomap[LamiFlow];

	int ID, Il, Is;

	int Idir, Ind, Iflow;
	Scalar Indo, Iflo;

	LaminarFlow* bc;

	for (mapivp::iterator iite=ibox.begin(); iite != ibox.end(); iite++){
		/*** read restored data ***/
		ID            = iite-> first;
		vecint& Iinfo = (*iite-> second);
		Il            = Iinfo[0];
		Is            = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		Idir = (int) ( data[Is] + 0.01 ); Is++;
		Indo = data[Is]; Is++;
		Iflo = data[Is];

		if (Idir <0 || Idir >2){
			std::cout<<"ERROR: direction index must be between 0~2, "<<Idir<<" is given."<<std::endl;
			continue;
		}

		if(Indo < 0){
			Ind = (int) (Indo - 0.01);
		}
		else{
			Ind = (int) (Indo + 0.01);
		}

		if(Iflo < 0){
			Iflow = -1;
		}
		else{
			Iflow = 1;
		}

		/*** create boundary condition ***/
		bc = new LaminarFlow( Idir, Ind, Iflow);
		this -> set_phase(ID, bc);

		vecbcp.push_back(bc);
	}
	return true;
}// end function

/*************************************
 * factory setup functions
 *************************************/
bool mpmbdfac::set_size(int ID, Cuboid* cubgeo){
	vecsvp& dbox = alldata_map[Size];
	mapivp& ibox = infomap[Size];

	mapivp::iterator iibox = ibox.find(ID);
	bool ifound = (iibox != ibox.end());
	if (!ifound){
		iibox  = ibox.find(IDpower);
		ifound = (iibox != ibox.end());
	}// end if

	if (ifound){
		vecint& Iinfo = (*iibox-> second);
		int Il        = Iinfo[0];
		int Is        = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		GVector dim( data[Is], data[Is+1], data[Is+2]);
		cubgeo->set_size( dim );
	}// end if
	return ifound;
}// end function

bool mpmbdfac::set_phase(int ID, BC* bobj){
	vecsvp& dbox = alldata_map[Phase];
	mapivp& ibox = infomap[Phase];

	mapivp::iterator iibox = ibox.find(ID);
	bool ifound = (iibox != ibox.end());
	if (!ifound){
		iibox  = ibox.find(IDpower);
		ifound = (iibox != ibox.end());
	}// end if

	if (ifound){
		vecint& Iinfo = (*iibox-> second);
		int Il        = Iinfo[0];
		int Is        = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		int Iphase = (int) data[Is];
		bobj-> set_owner(Iphase);
	}// end if
	return ifound;
}// end function

bool mpmbdfac::set_dlayer(int ID, mpmfuncfac* funcfac, Cuboid* cubgeo, BC*& bobj){
	vecsvp& dbox = alldata_map[DLayer];
	mapivp& ibox = infomap[DLayer];

	mapivp::iterator iibox = ibox.find(ID);
	bool ifound = (iibox != ibox.end());
	if (!ifound){
		iibox  = ibox.find(IDpower);
		ifound = (iibox != ibox.end());
	}// end if

	if (ifound){
		vecint& Iinfo = (*iibox-> second);
		int Il        = Iinfo[0];
		int Is        = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		std::vector<function*> lfs(5);
		std::vector<Scalar>    lts(5);
		int fid;

		for (unsigned int II = 0; II != 5; ++II){
			lts[II] = data[Is]; Is++;
			if(lts[II] < 0){
				lts[II] = 0;
				std::cout<<"WARNING: thickness should be positive, 0 is used.("<<II<<"th)"<<std::endl;
			}

			fid     = (int) ( data[Is] + 0.01 ); Is++;
			lfs[II] = funcfac-> get_new_object(fid);
			if(lfs[II] == 0){
				if (lts[II] > 1.e-12) lfs[II] = new FNcos(-1, 2*lts[II], 0.5, 0, 0.5);
				std::cout<<"WARNING: unknown function ID "<<fid<<", using default.("<<II<<"th)"<<std::endl;
			}
		}

		CuboidLayer* zone = new CuboidLayer(*cubgeo, lts); delete cubgeo; cubgeo = 0;
		bobj = new BClayer(bobj, zone, lfs); zone = 0;
	}// end if
	return ifound;
}// end function

bool mpmbdfac::set_flayer(int ID, mpmfuncfac* funcfac, Cuboid* cubgeo, BC*& bobj){
	vecsvp& dbox = alldata_map[FLayer];
	mapivp& ibox = infomap[FLayer];

	mapivp::iterator iibox = ibox.find(ID);
	bool ifound = (iibox != ibox.end());
	if (!ifound){
		iibox  = ibox.find(IDpower);
		ifound = (iibox != ibox.end());
	}// end if

	if (ifound){
		vecint& Iinfo = (*iibox-> second);
		int Il        = Iinfo[0];
		int Is        = Iinfo[1];
		vecsca& data  = (*dbox[Il]);

		int fid = (int) ( data[Is] + 0.01 ); Is++;
		function * surfunc = funcfac->get_new_object(fid);
		if(surfunc == 0){
			surfunc = new FNconstant(fid, 0);
			std::cout<<"WARNING: unknown function ID "<<fid<<", using default."<<std::endl;
		}

		std::vector<function*> lfs(5);
		std::vector<Scalar>    lts(5);

		for (unsigned int II = 0; II != 5; ++II){
			lts[II] = data[Is]; Is++;
			if(lts[II] < 0){
				lts[II] = 0;
				std::cout<<"WARNING: thickness should be positive, 0 is used.("<<II<<"th)"<<std::endl;
			}

			fid     = (int) ( data[Is] + 0.01 ); Is++;
			lfs[II] = funcfac-> get_new_object(fid);
			if(lfs[II] == 0){
				if (lts[II] > 1.e-12) lfs[II] = new FNcos(-1, 2*lts[II], 0.5, 0, 0.5);
				std::cout<<"WARNING: unknown function ID "<<fid<<", using default.("<<II<<"th)"<<std::endl;
			}
		}

		SurfCubLayer* zone = new SurfCubLayer(*cubgeo, lts, surfunc);
		delete cubgeo; cubgeo = 0;
		surfunc = 0;

		bobj = new BClayer(bobj, zone, lfs); zone = 0;
	}// end if
	return ifound;
}// end function
