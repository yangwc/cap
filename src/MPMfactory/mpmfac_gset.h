#ifndef MPMFAC_GENRSET_H
#define MPMFAC_GENRSET_H

#include "../MPMcomputer/MPMcomputer.h"
#include "pattern/comfac_cmd.h"

class mpmgsetfac : public comcmdfac{
public:
	mpmgsetfac();
	~mpmgsetfac();

	int set_computer(MPM* mpmc) const;

	inline void
	get_obj_step_time_w(std::vector<Scalar>& stptimes) const
	{ this -> get_step_time_w(_Nstpout, stptimes); }

	void get_step_time_w( unsigned int Nostep, std::vector<Scalar>& stptimes) const;

	inline bool if_output_obj(){return _io_obj;}
	inline bool if_output_odb(){return _io_odb;}
	inline bool if_output_gnd(){return _io_gnd;}
protected:
	int make_prototype(int Ikey, int ownerid, int Is, vecsca* dptr);

	Scalar _dt, _tend, _tsta, _trec;
	unsigned int _Nstpout;
	bool _io_gnd, _io_obj, _io_odb;
};
#endif /* MPMFAC_GENRSET_H */
