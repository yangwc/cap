#ifndef MPMFAC_PT_H_
#define MPMFAC_PT_H_


#include "mpmfac_objinfo.h"
#include "subfac_pt/ptfac.h"
#include "subfac_pt/ptic.h"
#include "subfac_pt/ptic_gb.h"
#include "subfac_pt/ptic_vel.h"
#include "subfac_pt/ptic_bf.h"
#include "subfac_pt/ptic_state.h"

class mpmptfac {
private:
	typedef std::vector<Particle*> ptpvec;
	typedef std::map<unsigned int, ptpvec > optsmap;
	typedef std::vector<ptic*> pticvec;

public:
	mpmptfac(){
		_Iptvel = _Iptbf = _Iptstate = -1; // _Iptgb =
	}
	~mpmptfac(){
		for(pticvec::iterator ivec = _ptics.begin(); ivec != _ptics.end(); ++ivec){
			delete (*ivec);
		}
	}

	inline int read_particle(inpParser& inpreader){ return _ptfac.read_value(inpreader); }

	inline int read_velocity(inpParser& inpreader){
		if (_Iptvel == -1){
			_Iptvel = _ptics.size();
			_ptics.push_back( new mpmptvel() );
		}
		return _ptics[_Iptvel]->read_value(inpreader);
	}
	inline int read_bforce(inpParser& inpreader){
		if (_Iptbf == -1){
			_Iptbf = _ptics.size();
			_ptics.push_back( new mpmptbf() );
		}
		return _ptics[_Iptbf]->read_value(inpreader);
	}
	inline int read_state(inpParser& inpreader){
		if (_Iptstate == -1){
			_Iptstate = _ptics.size();
			_ptics.push_back( new mpmptstate() );
		}
		return _ptics[_Iptstate]->read_value(inpreader);
	}

	inline int make_particles(const mpmobjinfo& oinffac){
		int Istat = 0;
		Istat = _ptfac.make_particles_w(_opmap);
		if(Istat != 0) return Istat;

		for(optsmap::iterator imap = _opmap.begin(); imap != _opmap.end(); ++imap){
			const unsigned int& objID = imap -> first;
			ptpvec& pts = (imap -> second);

			Objinfo* oinf = oinffac.get_new_object(objID);
			for (ptpvec::iterator iptv = pts.begin(); iptv != pts.end(); ++iptv ){
				oinf->get_object_info((*iptv),0);
			}
			delete oinf; oinf = 0;

			for(pticvec::iterator ivec = _ptics.begin(); ivec != _ptics.end(); ++ivec){
				Istat = (*ivec) -> set_particles( objID, pts);
				if(Istat != 0) break;
			}
			if(Istat != 0) break;
		}
		return Istat;
	}

	inline optsmap& get_objptmap(){ return _opmap; }

	inline unsigned int get_Npt() const{
		unsigned int Npt=0;
		for(optsmap::const_iterator imap = _opmap.begin(); imap != _opmap.end(); ++imap){
			Npt += (imap->second).size();
		}
		return Npt;
	}

private:
	mpmpt _ptfac;

	int _Iptvel, _Iptbf, _Iptstate; //_Iptgb,
	pticvec _ptics;

	optsmap _opmap;
};

#endif /* MPMFAC_PT_H_ */
