/*
 * linefac.cpp
 *
 *  Created on: Nov 21, 2013
 *      Author: wenchia
 */

#include "linefac.h"

int linefac::clean_line(std::string& filename ){
	unsigned int Ista, Iend, Ichar;
	for (Ista = 0; Ista<filename.length(); Ista++){
		Ichar = int(filename[Ista]);
		if (Ichar != 32 && Ichar != 9) break;
	}
	if (Ista == filename.length()) return -1;

	for (Iend = filename.length(); Iend>Ista; Iend--){
		Ichar = int(filename[(Iend-1)]);
		if (Ichar != 32 && Ichar != 9) break;
	}
	filename = filename.substr(Ista, (Iend-Ista));
	return 0;
}// end function (clean_line)

bool linefac::extract_basename(std::string& filename, std::string& basename){
	unsigned int Ista, Iend;
	bool isame;
	Ista = 0;
	Iend = filename.find_last_of(".");
	isame = (Iend != std::string::npos);
	if (isame){ basename = filename.substr(Ista, (Iend-Ista)); }
	else{ basename = filename; }
	return isame;
}// end function (extract_basename)
