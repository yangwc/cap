#ifndef GEOTOOL_H_
#define GEOTOOL_H_

#include "../../GB_Tensor.h"
class xy2Drotator {
public:
	xy2Drotator();
	xy2Drotator(const Scalar& angle);
	~xy2Drotator(){;}

	bool set_rotangle(const Scalar& angle);
	bool coord_trans(GVector& coord);
private:
	bool build_transfmtx();

	Scalar rrad;
	GTensor rmtx;
};

#endif /* GEOTOOL_H_ */
