#include "geotool.h"
//	Scalar rangle;
//	GTensor rmtx;
xy2Drotator::xy2Drotator(){
	this->rmtx.SetEye();
	this->rrad = 0.0;
}

xy2Drotator::xy2Drotator(const Scalar& angle){
	Scalar pi = std::acos(-1.0);
	this->rrad = angle;
	this->rrad *= pi/180.0;
	this->build_transfmtx();
}

bool xy2Drotator::build_transfmtx(){
	Scalar costheta = std::cos(this->rrad);
	Scalar sintheta = std::sin(this->rrad);
	this->rmtx.SetComponents(costheta, -sintheta, 0.0,
							 sintheta,  costheta, 0.0,
							      0.0,       0.0, 1.0 );
	return true;
}

bool xy2Drotator::set_rotangle(const Scalar& angle){
	Scalar pi = std::acos(-1.0);
	this->rrad = angle;
	this->rrad *= pi/180.0;
	bool istat = this->build_transfmtx();
	return istat;
}

bool xy2Drotator::coord_trans(GVector& coord){
	coord = this->rmtx*coord;
	return true;
}
