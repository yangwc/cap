/*
 * linefac.h
 *
 *  Created on: Nov 21, 2013
 *      Author: wenchia
 */

#ifndef LINEFAC_H_
#define LINEFAC_H_

#include <string>

class linefac {
public:
	linefac(){}
	~linefac(){}
	int clean_line(std::string& filename );
	bool extract_basename(std::string& filename, std::string& basename);
};

#endif /* LINEFAC_H_ */
