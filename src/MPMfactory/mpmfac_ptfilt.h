#ifndef MPMFAC_PTFILT_H
#define MPMFAC_PTFILT_H

#include "../MPMobject/PTFilter/_PK_PTFilter.h"
#include "mpmfac_func.h"
#include "mpmfac_objinfo.h"
#include "pattern/comfac.h"

class mpmptfiltfac : public mpmcomfac{
protected:
	typedef PTFilter _typ_obj;
	typedef std::map<int, PTFilter*> objmap;
	typedef std::vector<PTFilter*>   objvec;
	typedef objmap::iterator     omapite;
	typedef objvec::iterator     ovecite;

public:
	mpmptfiltfac();
	~mpmptfiltfac();

	bool organize_data();
	bool make_objects(mpmfuncfac* funcfacptr, mpmobjinfo* oinffacptr);
	_typ_obj* get_new_object(unsigned int Ibc, int objID);
	unsigned int get_new_objects(int ownerID, objvec& ovec);

	inline unsigned int get_Nobject(){ return vecobp.size(); }
	inline int get_powerId(){return IDpower;}

protected:
	/* ===========================
	 * functions
	 =========================== */
//	void boundary_default();

	void build_keymap();
	void build_inputmap();

	bool make_eaters(mpmfuncfac* funcfacptr);
	bool make_generators(mpmfuncfac* funcfacptr, mpmobjinfo* oinffacptr);
	bool make_CTgenerators(mpmfuncfac* funcfacptr, mpmobjinfo* oinffacptr);

//	bool set_size(int& ID, GeoObject* gobj);
	bool set_owner(int& ID, _typ_obj* gobj);

	bool set_staID(int& ID, PFproducer* bobj);
	bool set_mesh(int& ID, PFproducer* bobj);

	/* ===========================
	 * personal values
	 =========================== */
	std::vector<int> kgpside,kgpmain;

	/* ===========================
	 * final product and recorder
	 =========================== */
	objmap mapobp;
	objvec vecobp;
};
#endif /* MPMFAC_PTFILT_H */
