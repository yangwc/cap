/**************************************************
 * subfac_gspa.h
 * 	   - FACspace()
 * ------------------------------------------------
 * 	+ Sub-factory for creating MPM space.
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.04.29)
 **************************************************/

#ifndef MPMFAC_GSPA_H_
#define MPMFAC_GSPA_H_

#include "../MPMobject/ObjManager/Gmanager.h"
#include "../MPMcomputer/MPMos.h"
#include "../MPMobject/Grid/MPMGrid.h"

#include "mpmfac_bd.h"
#include "mpmfac_ptfilt.h"
#include "pattern/comfac_cmd.h"

class FACspace : public comcmdfac{
public:
	FACspace();
	~FACspace(){;}

	Space* get_new_space(int objID, GBridge* brdglink, mpmbdfac& bdfac, mpmptfiltfac& ptffac);

	/******************
	 *  global setup
	 ******************/
	GBridge* get_new_bridge();
	inline const GVector& get_spacing() const { return _RCGspacing; }

protected:
	int make_prototype(int Ikey, int ownerid, int Is, vecsca* dptr);

	void check_generalonly(int ownerid, int Ikey);
	void check_duplicate(int ownerid);

	template<typename _elm, typename _nd, typename _elmmaker, typename _ndmaker>
	Gmanager<_elm,_nd>* create_gridmanager(int objID, mpmbdfac& bdfac);

	template< template<typename, typename> class _grid, typename _elm, typename _nd, template<typename> class _elmmaker, template<typename> class _ndmaker>
	Space* create_new_space(int objID, GBridge* brdglink, mpmbdfac& bdfac, mpmptfiltfac& ptffac);

	GVector _RCGspacing;
	Scalar _c_tintg, _c_dNmin, _c_Nmin, _c_Lcut;

	std::map<int,int> _map_objgridtype;
	std::map< int, MathVec<Scalar> > _map_objinput;

};
#endif /* MPMFAC_GSPA_H_ */
