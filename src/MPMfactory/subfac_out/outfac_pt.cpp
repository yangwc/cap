#include "outfac_pt.h"

enum outptKeywords {
	PMass,
	PDensity,
	PVelocity,
	PStress
};

/*************************************
 * Initialization
 *************************************/
outptfac::outptfac(){
	mykeystr = "PTOutput";
	unsigned int Nk = 4;
	int keys[] = { PMass, PDensity, PVelocity, PStress};
    std::string knames[]={"PMass", "PDensity","PVelocity", "PStress"};
    int Ninvals[]={ 0,1, 0,1, 0,1, 0,1 };

	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
	this -> IDdefault = this -> IDpower;
}

PTPVal* outptfac::get_new_prototype(int Ikey, int Is, vecsca* dptr) const{
	PTPVal* optr = 0;
	switch(Ikey){
	case PMass:
		optr = new PTPmass();
		break;
	case PDensity:
			optr = new PTPdensity();
			break;
	case PVelocity:
		optr = new PTPvel();
		break;
	case PStress:
		optr = new PTPstress();
		break;
	}// end switch (function key)
	return optr;
}

std::vector<PTPVal*>* outptfac::get_new_objects(int objid) const{
	std::vector<PTPVal*>* newvec = 0;

	for(mapstorage::const_iterator idmap=alldata_map.begin(); idmap !=alldata_map.end(); ++idmap){
		const int& Ikey    = idmap->first;
		const vecsvp& dbox = idmap->second;

		std::map<int, mapivp >::const_iterator iinfomap = infomap.find( Ikey );
		const mapivp& imap = iinfomap -> second;

		mapivp::const_iterator iimap = imap.find( objid );
		if( iimap == imap.end() ) iimap = imap.find( this->IDpower );
		if( iimap == imap.end() ) continue;

		vecint* const & iptr = iimap->second;
		int Il = (*iptr)[0];
		int Is = (*iptr)[1];
		vecsca* const & dptr = dbox[Il];

		PTPVal* optr = this -> get_new_prototype(Ikey, Is, dptr);

		if( newvec == 0 ) newvec = new std::vector<PTPVal*>();
		newvec->push_back(optr);
		optr = 0;
	}// end for (all data)

	return newvec;
}
