#ifndef OUTFAC_PT_H
#define OUTFAC_PT_H

#include "../../MPMdatabase/pval_pt.h"
#include "../pattern/comfac_cmd.h"

class outptfac : public comcmdfac{
public:
	outptfac();
	~outptfac(){;}

	std::vector<PTPVal*>* get_new_objects(int objid) const;
protected:
	inline int make_prototype(int Ikey, int ownerid, int Is, vecsca* dptr){return 0;}
	PTPVal* get_new_prototype(int Ikey, int Is, vecsca* dptr) const;

};
#endif /* OUTFAC_PT_H */
