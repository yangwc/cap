#ifndef MPMFAC_PTBF_H
#define MPMFAC_PTBF_H

#include "comfac_pt.h"

class mpmptbf : public ptcomfac {
public:
	mpmptbf(){
		_Lvalmin = 4; _Lvalmax = 4;
		this -> set_default();
		_errmsgmap[_Ierr_title]   = "*** error(s) when reading particle body force. ***";
	}

	virtual ~mpmptbf(){;}

protected:
	inline int set_particle(const valvec& vals, Particle& pt){
		GVector gvec(vals[0], vals[1], vals[2]);
		pt.set_bodyforce(gvec);
		return 0;
	}
};

#endif /* MPMFAC_PTBF_H */
