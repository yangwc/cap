#ifndef MPMFAC_PTSTATE_H
#define MPMFAC_PTSTATE_H

#include "comfac_pt.h"

class mpmptstate : public ptcomfac {
public:
	mpmptstate(){
		_Lvalmin = 2; _Lvalmax = 1000;
		this -> set_default();
		_errmsgmap[_Ierr_title]   = "*** error(s) when reading particle state. ***";
	}

	virtual ~mpmptstate(){;}

protected:
	inline int set_particle(const valvec& vals, Particle& pt){
		int Istat = -1;
		bool ierr = this -> checkerr_para_minnumber(pt.get_Nstatevals(), vals.size(), _Ierr_minpara);
		if ( !ierr ){
			pt.set_state(vals);
			Istat = 0;
		}
		return Istat;
	}
};

#endif /* MPMFAC_PTSTATE_H */
