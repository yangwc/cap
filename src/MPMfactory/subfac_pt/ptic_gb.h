#ifndef MPMFAC_PTGB_H
#define MPMFAC_PTGB_H

#include "../../MPMcomputer/Particle.h"
#include "../subfac_base/geotool.h"
#include "../pattern/comfac_cmd.h"
#include "ptic.h"

class mpmptgb : public ptic, public comcmdfac{
public:
	mpmptgb();
	virtual ~mpmptgb(){;}

	int read_value(inpParser& inpreader);
	int set_particles(unsigned int objID, ptpvec& pts);

protected:
	inline int make_prototype(int Ikey, int id, int Is, vecsca* dptr){return 0;}
	void set_global(int Ikey, int Is, vecsca* dptr, ptpvec& pts);
};

#endif /* MPMFAC_PTGB_H */
