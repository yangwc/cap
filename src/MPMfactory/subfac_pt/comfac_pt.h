#ifndef COMFAC_PT_H
#define COMFAC_PT_H

#include "../../MPMcomputer/Particle.h"
#include "../pattern/fackit_err.h"

#include "../../GB_Tensor.h"
#include "../../MPMfstream/inpParser.h"

#include "ptic.h"

class ptcomfac : public ptic, public errfackit {
protected:
	typedef std::vector<Scalar> valvec;
	typedef std::map<unsigned int, valvec* > ptvalmap;
	typedef std::map<unsigned int, ptvalmap* > objmap;
	typedef std::vector<Particle*> ptpvec;

public:
	virtual ~ptcomfac();

	virtual int read_value(inpParser& inpreader);
	virtual int set_particles(unsigned int objID, ptpvec& pts);

protected:
	virtual int record_ptdata(inpParser& inpreader, unsigned int Ldmin, unsigned int Ldmax, ptvalmap& datamap);
	virtual int set_particle(const valvec& vals, Particle& ptptr) = 0;

	unsigned int  _Lvalmin, _Lvalmax;
	objmap _omap;
};

#endif /* COMFAC_PT_H */
