#include "ptfac.h"

int mpmpt::read_value(inpParser& inpreader){
	/****************************************
	 * Return:
	 *       0: good state.
	 *      -1: other errors.
	 ****************************************/
	unsigned int objID = inpreader.get_next_value<unsigned int>();
	objmap::iterator imap = _omap.find(objID);
	if( imap == _omap.end() ){
		_omap[objID] = new ptvalmap();
		imap = _omap.find(objID);
	}
	unsigned int Nerr = this -> record_ptdata( inpreader, _Lvalmin, _Lvalmax, *(imap->second) );

	if (Nerr != 0) return -1;
	return 0;
}

int mpmpt::make_particles_w(optsmap& opmap){
	int Jstat, Istat = 0;
	unsigned int Npt, objID, Ipt;

	ptpvec* ptsptr;
	optsmap::iterator ioptmap;
	for (objmap::iterator imap = _omap.begin(); imap != _omap.end(); ++imap){
		objID = (imap->first);

		ioptmap = opmap.find( objID );
		if (ioptmap != opmap.end() ){
			std::cout<<"CODE ERROR: duplicated object ID found at creating particles."<<std::endl;
			Istat = -1;
			continue;
		}
		ptvalmap& ptvmap = *(imap->second);
		Npt = ptvmap.size();

		ptsptr = new ptpvec(Npt, 0);
		Ipt = 0;
		for (ptvalmap::iterator iptv = ptvmap.begin(); iptv != ptvmap.end(); ++iptv ){
			Particle*& ptptr = ptsptr->at(Ipt);
			ptptr = new Particle( (iptv -> first), Ipt );
			ptptr -> assign_objectID(objID);
			++Ipt;
		}

		Jstat = this -> set_particles(objID,  *ptsptr);
		if (Jstat != 0){
			Istat = -1;
			delete ptsptr;
			continue;
		}

		opmap[ objID ] = *ptsptr;
		delete ptsptr; ptsptr = 0;
	}
	return Istat;
}

int mpmpt::set_particle(const valvec& vals, Particle& pt){
	const Scalar& vol = vals[0];
	GVector coord(vals[1], vals[2], vals[3]);
	pt.assign_volume(vol);
	pt.set_coord(coord);
	return 0;
}
