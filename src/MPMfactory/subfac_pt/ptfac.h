#ifndef PTFAC_H
#define PTFAC_H

#include "comfac_pt.h"

class mpmpt : public ptcomfac {
private:
	typedef std::map<unsigned int, unsigned int> omatmap;
	typedef std::map<unsigned int, ptpvec >      optsmap;

public:
	mpmpt(){
		_Lvalmin = 5; _Lvalmax = 5;
		this -> set_default();
	}

	virtual ~mpmpt(){;}

	int read_value(inpParser& inpreader);
	int make_particles_w(optsmap& opmap);

protected:
	int set_particle(const valvec& vals, Particle& ptptr);

//	omatmap _matmap;
};

#endif /* PTFAC_H */
