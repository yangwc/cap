#include "comfac_pt.h"

ptcomfac::~ptcomfac(){
	for (objmap::iterator iomap = _omap.begin(); iomap != _omap.end(); ++iomap){
		ptvalmap*& pvmap = iomap -> second;
		for (ptvalmap::iterator ipmap = pvmap->begin(); ipmap != pvmap->end(); ++ipmap){
			delete (ipmap->second); (ipmap->second) = 0;
		}
		delete pvmap; pvmap = 0;
	}
}

int ptcomfac::read_value(inpParser& inpreader){
	/****************************************
	 * Return:
	 *       0: good state.
	 *    -100: no particle ID.
	 *     -50: particle ID should be an
	 *          non-negative integer.
	 *      -1: other errors.
	 ****************************************/
	unsigned int objID = inpreader.get_next_value<unsigned int>();

	objmap::iterator imap = _omap.find(objID);
	if( imap == _omap.end() ){
		_omap[objID] = new ptvalmap();
		imap = _omap.find(objID);
	}
	unsigned int Nerr = this -> record_ptdata( inpreader, _Lvalmin, _Lvalmax, *(imap->second) );

	if (Nerr != 0) return -1;

	return 0;
}

int ptcomfac::record_ptdata(inpParser& inpreader, unsigned int Ldmin, unsigned int Ldmax, ptvalmap& datamap){
	/*********************************************************************
	 * Record all input data in Scalar except the first one, particle ID,
	 * which is an unsigned integer key of map.
	 *    -- throw out inappropriate input commends.
	 *    -- ignore redundant input numbers.
	 *********************************************************************/
	bool istat, iend = false;
	unsigned int Nval, ptID, Nerr = 0;
	valvec* svptr = 0;
    std::string  inword;
    while ( inpreader.read_next_line() ){
    	iend = inpreader.is_endcmd();
    	if ( iend ) break;

        Nval  = inpreader.get_Ninput();
        istat = this -> checkerr_para_minnumber(Ldmin, Nval, _Ierr_minpara);
        if (istat) { Nerr++; continue; }// *** ERROR 1001

        ptID  = inpreader.get_next_value<unsigned int>();
        svptr = inpreader.get_new_surplus<Scalar>();

    	datamap[ptID] = (svptr);
    	svptr = 0;
    }// end while (read line)
    if (!iend){ // *** ERROR 1003
    	this -> record_error(_Ierr_end);
    	std::cout<<"ERROR: commend block ended unexpectedly (lack of 'End')."<<std::endl;
    	Nerr++;
    }
    return Nerr;
}// end function

int ptcomfac::set_particles(unsigned int objID, ptpvec& pts){
	int Jstat, Istat = 0;
	objmap::iterator iomap = _omap.find(objID);
	if ( iomap == _omap.end() ) return Istat;

	ptvalmap& valmap = *(iomap -> second);
	ptvalmap::iterator ipmap;
	unsigned int ptID;
	for (ptpvec::iterator ipt = pts.begin(); ipt != pts.end(); ++ipt){
		Particle& pt = *(*ipt);
		ptID  = pt.get_ID();

		ipmap = valmap.find( ptID );
		if ( ipmap == valmap.end() ) continue;

		valvec& vals = *(ipmap -> second);

		Jstat = this -> set_particle(vals, pt);
		if ( Jstat != 0 ) Istat = -1;
	}
	return Istat;
}
