#ifndef PTIC_H_
#define PTIC_H_

#include <vector>

class Particle;
class inpParser;

class ptic{
protected:
	typedef std::vector<Particle*> ptpvec;

public:
	virtual ~ptic(){;}
	virtual int read_value(inpParser& inpreader) = 0;
	virtual int set_particles(unsigned int objID, ptpvec& pts) = 0;
};


#endif /* PTIC_H_ */
