#ifndef PTFAC_VEL_H
#define PTFAC_VEL_H

#include "comfac_pt.h"

class mpmptvel : public ptcomfac {
public:
	mpmptvel(){
		_Lvalmin = 4; _Lvalmax = 4;
		this -> set_default();
		_errmsgmap[_Ierr_title]   = "*** error(s) when reading particle velocity. ***";
	}

	virtual ~mpmptvel(){;}

protected:
	inline int set_particle(const valvec& vals, Particle& pt){
		GVector ptvel(vals[0], vals[1], vals[2]);
		pt.set_velocity(ptvel);
		return 0;
	}
};

#endif /* PTFAC_VEL_H */
