#include "ptic_gb.h"
enum pticKeywords{
	Gvelocity,
	Gbforce,
	Gshift,
};

mpmptgb::mpmptgb() {
	mykeystr = "PTSetup";

	int Nk = 3;
	int keys[]={Gvelocity, Gbforce, Gshift};
    std::string knames[]={"globalVelocity", "globalBodyForce", "globalPosition"};
    int Ninvals[]={ 3,4, 3,4, 3,4 };


	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
	this -> IDdefault = this -> IDpower;
}

void mpmptgb::set_global(int Ikey, int Is, vecsca* dptr, ptpvec& pts){
	GVector avec, bvec;
	switch(Ikey){
	case Gvelocity:
		avec.SetComponents( (*dptr)[Is], (*dptr)[Is+1], (*dptr)[Is+2] );
		for (ptpvec::iterator ipt = pts.begin(); ipt != pts.end(); ++ipt){
			(*ipt)->set_velocity(avec);
		}
		break;
	case Gbforce:
		avec.SetComponents( (*dptr)[Is], (*dptr)[Is+1], (*dptr)[Is+2]);
		for (ptpvec::iterator ipt = pts.begin(); ipt != pts.end(); ++ipt){
			(*ipt)->set_bodyforce(avec);
		}
		break;
	case Gshift:
		avec.SetComponents( (*dptr)[Is], (*dptr)[Is+1], (*dptr)[Is+2]);
		for (ptpvec::iterator ipt = pts.begin(); ipt != pts.end(); ++ipt){
			bvec = (*ipt)->get_coord();
			bvec += avec;
			(*ipt)->set_coord(bvec);
		}
		break;
	default:
		std::cout<<"WARNING: undefined key "<<Ikey<<" ."<<std::endl;
		break;
	}// end switch (function key)
}// end function

int mpmptgb::read_value(inpParser& inpreader){
	bool igood = this -> read_inp(inpreader);
	int Istat = -1;
	if (igood) Istat = 0;
	return Istat;
}

int mpmptgb::set_particles(unsigned int objID, ptpvec& pts){
	int Istat = 0;
	mapstorage::iterator mite;
	int objkey;
	vecsca* dptr;
	vecint* iptr;
	mapivp::iterator iite;

	int Il, Is;

	for(mite = alldata_map.begin(); mite!=alldata_map.end(); ++mite){
		objkey = mite->first;
		vecsvp& dbox = mite->second;
		mapivp& ibox = infomap[objkey];
		iite = ibox.find( objID );
		if(iite == ibox.end()) iite = ibox.find( this -> IDpower );
		if(iite == ibox.end()) continue;

		iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];

		this -> set_global(objkey, Is, dptr, pts);
	}// end for (all data)

	return Istat;
}

