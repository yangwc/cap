/**************************************************
 * mpmfac.h
 * ------------------------------------------------
 * 	+ factory of mpm program (computer)
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.11.10)
 **************************************************/

#ifndef MPMFAC_CP_H_
#define MPMFAC_CP_H_

// basic tools
#include <string>
#include <map>
#include <vector>
#include <sstream>

#include "../GB_Tensor.h"

// input file reader
#include "../MPMfstream/inpParser.h"

// product
#include "../MPMcomputer/MPMcomputer.h"

// sub factories
#include "mpmfac_func.h"
#include "subfac_out/outfac_pt.h"
#include "mpmfac_gset.h"
#include "mpmfac_bd.h"
#include "mpmfac_space.h"
#include "mpmfac_mat.h"
#include "mpmfac_objinfo.h"
#include "mpmfac_pt.h"
#include "mpmfac_ptfilt.h"

// parents
#include "pattern/fackit_err.h"
#include "pattern/fackit_kmap.h"

class MFACcomp : public errfackit, public FACkmap{
protected:
	typedef std::vector<int> intvec;
	typedef std::vector<Scalar> scavec;

public:
	MFACcomp();
	~MFACcomp(){;}

	int open_ifile(const std::string& srcname, const std::string& projname);
	MPM* get_new_computer(int& Istat);

	std::string get_statmsg();
	inline const std::string& get_srcversion()  const {return _mpmversion;}
	inline const std::string& get_projectname() const {return _projname;}
	inline const std::string& get_infilename()  const {return _ifname;}

protected:
	int read_inp();
	int read_keyblock(int Ikey);

	MPM*   create_computer();
	MPMos* create_os();

	inpParser _inpreader;
	std::string _ifname, _projname;
	std::string _mpmversion;
    intvec _rstat_keys;

    // *** sub factories
	mpmfuncfac _funcfac; 	// function
	outptfac   _ptofac;  	// output (particle)
	mpmgsetfac _gsetfac; 	// general setup
	mpmbdfac   _bdfac;   	// boundary
	FACspace   _gspafac; 	// space setup
	mpmmatfac  _matfac;  	// material
	mpmobjinfo _oinffac; 	// object information & setup
	mpmptfac   _ptfac;   	// particles & their I.C.
	mpmptfiltfac _pfiltfac; // particle filter
};
#endif // MPMFAC_CP_H_
