#include "mpmfac_space.h"

#include "../MPMobject/Shape/Cub8.h"

#include "../MPMobject/Interact/Contact.h"

#include "../MPMobject/LiteFac/_PK_LiteFac.h"

#include "../MPMobject/Space/PK_Space.h"
#include "../MPMobject/Grid/_PK_Grid.h"
#include "../MPMobject/GridElem/_PK_GridElem.h"
#include "../MPMobject/GridNode/_PK_GridNode.h"

enum gridspaceKeywords {
	FRegCuboid,
	CoeffTIntg,
	REShape,

	MPM,
	MPMCAL,
	MPMFLOW,

	SmoothMPMPWO0,
	SmoothMPMO0Fx,
	SmoothMPMO0FxSm,

	SmoothMPMO0FxO1E,
	SmoothMPMO0FxO2E,

	SmoothMPMO0FxOSCO0,
	SmoothMPMO0FxOSCO1,
	SmoothMPMO0FxOSCO2,

	SmoothMPMPWO1,
	SmoothMPMO1Fx,
	SmoothMPMO1FxS,
};
// ==========================================================================================
// Initialization ---------------------------------------------------------------------------
// ==========================================================================================
FACspace::FACspace(){
	mykeystr = "GridSpace";
	unsigned int Nk = 17;
	int keys[] = { FRegCuboid, CoeffTIntg, REShape,
					MPM, MPMCAL, MPMFLOW,
					SmoothMPMPWO0,SmoothMPMO0Fx,SmoothMPMO0FxSm,
					SmoothMPMO0FxO1E,SmoothMPMO0FxO2E,
					SmoothMPMO0FxOSCO0, SmoothMPMO0FxOSCO1, SmoothMPMO0FxOSCO2,
					SmoothMPMPWO1,SmoothMPMO1Fx,SmoothMPMO1FxS
					};
    std::string knames[]={ "FRegCuboid", "CoeffTIntg", "MODShape",
    						"MPM", "MPMCAL", "MPMFLOW",
    						"SMPMPWO0","SMPMO0Fx","SMPMO0FxSm",
    						"SMPMO0FxO1E","SMPMO0FxO2E",
    						"SMPMO0FxOSCO0","SMPMO0FxOSCO1","SMPMO0FxOSCO2",
    						"SMPMPWO1","SMPMO1Fx","SMPMO1FxS",
    						};
    int Ninvals[]={ 4,5, 1,2, 3,4,
    				0,1, 0,1, 0,1,
    				0,1, 0,1, 0,1,
    				0,1, 0,1,
    				0,1, 0,1, 0,1,
    				0,1, 0,1, 0,1,
    				};

	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
	this -> IDdefault = this -> IDpower;

	_c_tintg = 1.0;

	_c_Lcut  = 0.01;
	_c_Nmin  = 1e-3;
	_c_dNmin = 0;
}

int FACspace::make_prototype(int Ikey, int ownerid, int Is, vecsca* dptr){
	MathVec<Scalar> dat;
	switch(Ikey){
	case FRegCuboid:
		this -> check_generalonly(ownerid, Ikey);
		_RCGspacing.SetComponents( dptr->at(Is), dptr->at(Is+1), dptr->at(Is+2) );
		break;
	case CoeffTIntg:
		_c_tintg = dptr->at(Is);
		break;
	case REShape:
		_c_Lcut  = dptr->at(Is);
		_c_Nmin  = dptr->at(Is+1);
		_c_dNmin = dptr->at(Is+2);
		break;

	default:
		this -> check_duplicate(ownerid);
		_map_objgridtype[ownerid] = Ikey;
		break;

	}// end switch (function key)
	return 0;
}

// ==========================================================================================
// Global object ---------------------------------------------------------------------------
// ==========================================================================================

GBridge* FACspace::get_new_bridge(){
	OInteract* algptr = new Contact();
	GBridge* brdgptr  = new GBridge( new FLNKmngr<Gnode>(), algptr );  algptr = 0;
	return brdgptr;
}
// ==========================================================================================
// Warnings ---------------------------------------------------------------------------------
// ==========================================================================================
inline void FACspace::check_generalonly(int ownerid, int Ikey){
	if ( ownerid != this->IDpower ){
		std::cout<<"(CODE) ERROR: "<< _Skey_map[Ikey] <<"always apply to all object."<<std::endl;
	}
}

inline void FACspace::check_duplicate(int ownerid){
	std::map<int,int>::iterator iid = _map_objgridtype.find( ownerid );
	if ( iid != _map_objgridtype.end() ){
		std::cout<<"WARNING: one space (object) can only have one grid type."<<std::endl;
	}
}

// ==========================================================================================
// ------------------------------------------------------------------------------------------
// ==========================================================================================
Space* FACspace::get_new_space(int objID, GBridge* brdglink, mpmbdfac& bdfac, mpmptfiltfac& ptffac){
	std::map<int,int>::iterator iid = _map_objgridtype.find( objID );
	if ( iid == _map_objgridtype.end() ){
		iid = _map_objgridtype.find( this -> IDpower );
	}
	int Ikey = MPM;
	if ( iid != _map_objgridtype.end() ){
		Ikey = iid->second;
	}
	Space* spptr=0;
	switch(Ikey){
	case MPM:
		spptr = this -> create_new_space<MPMGrid, MPMelem, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case MPMCAL:
		spptr = this -> create_new_space<MPMGrid,  SMPMelemPWO0, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case MPMFLOW:
		spptr = this -> create_new_space<MPMGridFLOW,  MPMelemFLOW, GnodeFLOW, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;

	case SmoothMPMPWO0:
		spptr = this -> create_new_space<MPMGrid,  SMPMelemPWO0, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case SmoothMPMO0Fx:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0Flux, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;

	case SmoothMPMO0FxSm:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0FluxSm, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;

	case SmoothMPMO0FxO1E:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0FluxO1E, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case SmoothMPMO0FxO2E:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0FluxO2E, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;

	case SmoothMPMO0FxOSCO0:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0FluxOSCO0, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case SmoothMPMO0FxOSCO1:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0FluxOSCO1, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case SmoothMPMO0FxOSCO2:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO0FluxOSCO2, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;

	case SmoothMPMPWO1:
		spptr = this -> create_new_space<MPMGrid,  SMPMelemPWO1, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case SmoothMPMO1Fx:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO1Flux, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;
	case SmoothMPMO1FxS:
		spptr = this -> create_new_space<MPMGridFlux,  SMPMelemO1FluxS, Gnode, BasicElmMaker, BasicNodeMaker>(objID, brdglink, bdfac, ptffac);
		break;

	default:
		std::cout<<"ERROR: unknown key @ FACspace"<<std::endl;
		break;
	}// end switch (function key)

	return spptr;
}

template< template<typename, typename> class _grid, typename _elm, typename _nd, template<typename> class _elmmaker, template<typename> class _ndmaker>
Space* FACspace::create_new_space(int objID, GBridge* brdglink, mpmbdfac& bdfac, mpmptfiltfac& ptffac){

	// create grid manager
	Gmanager<_elm,_nd>* gmptr = this -> create_gridmanager<_elm,_nd,_elmmaker<_elm>,_ndmaker<_nd> >(objID, bdfac);

	// create & link particle filters to grid manager
	std::vector<PTFilter*> ptfs;
	ptffac.get_new_objects( objID, ptfs);
	for (unsigned int Ipf = 0; Ipf != ptfs.size(); ++Ipf){
		gmptr-> link_ptfilters( ptfs[Ipf] );
	}

	// create grid
	_grid<_nd,_elm>* gptr = new _grid<_nd,_elm>();
	gptr -> link_bridge(brdglink);
	gptr -> assign_gridmanager(gmptr); gmptr = 0;

	// create space
	Space* spptr = new MPMSpace(objID);
	spptr -> assign_gridmodel(gptr); gptr = 0;
	spptr -> assign_ptfilters( ptfs );

	return spptr;
}

template<typename _elm, typename _nd, typename _elmmaker, typename _ndmaker >
Gmanager<_elm,_nd>* FACspace::create_gridmanager(int objID, mpmbdfac& bdfac){
	/***********************************
	 * Only for the manager of
	 * floating regular cuboid grids
	 ***********************************/

	// Maker : Grid Node
	_ndmaker* gmkptr = new _ndmaker();
	gmkptr -> set_Cintg( _c_tintg );

	// Maker : Particle-Grid Adaptor
	_elmmaker* cmkptr = new _elmmaker();
	cmkptr -> set_dimension( _RCGspacing );

	// Grid Manager
	ShpCCP* shccp = new Cub8CCP( _c_Lcut, _c_Nmin, _c_dNmin);
	Gmanager<_elm,_nd>* gmptr = new FRCGmanager<_elm,_nd,_elmmaker,_ndmaker>( _RCGspacing, cmkptr, gmkptr, shccp );
	cmkptr = 0; gmkptr = 0; shccp = 0;

	// boundary
	unsigned int Nbc = bdfac.get_Nboundary();
	for (unsigned int Ibc = 0; Ibc != Nbc; ++Ibc){
		BC* bcptr = bdfac.get_new_object(Ibc, objID);
		if( bcptr != 0 ){
			gmptr -> add_bconditions(bcptr);
			bcptr = 0;
		}
	}

	return gmptr;
}


