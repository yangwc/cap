#include "mpmfac_mat.h"
#include "../Material/matpack.h"

enum materialKeywords {
	NonStressFake,
	HypoElasticSolid,
	LinearElasticSolid,
	NewtonianFluid,
	IncompressibleFluid,
};

mpmmatfac::mpmmatfac(){
	mykeystr = "Material";

	unsigned int Nk = 5;
	int keys[]={ NonStressFake, HypoElasticSolid, LinearElasticSolid, NewtonianFluid, IncompressibleFluid };
    std::string knames[]={ "NonStress", "HypoElastic", "LinearElastic", "NewtonianFluid", "IncompressibleFluid" };
    int Ninvals[]={ 2,2, 4,4, 4,4, 4,4, 4,4};

	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
}

Material* mpmmatfac::get_new_prototype(int Ikey, int id, int Is, vecsca* dptr){
	Material* optr = 0;
	vecsca vals;
	switch(Ikey){
	case HypoElasticSolid:
		this -> get_values_w(3, Is, dptr, vals);
		optr = new HypoElastic(id, vals);
		break;
	case LinearElasticSolid:
		this -> get_values_w(3, Is, dptr, vals);
		optr = new LinearElastic(id, vals);
		break;
	case NewtonianFluid:
		this -> get_values_w(3, Is, dptr, vals);
		optr = new NewtFluid(id, vals);
		break;
	case IncompressibleFluid:
		this -> get_values_w(3, Is, dptr, vals);
		optr = new IncompFluid(id, vals);
		break;
//	case NonStressFake:
//		this -> get_values_w(1, Is, dptr, vals);
//		optr = new NonStress(id, vals);
//		break;
	}// end switch (key)
	return optr;
}// end function
