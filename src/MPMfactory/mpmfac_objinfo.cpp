#include "mpmfac_objinfo.h"

//#include "DeformObjs.h"

enum pticKeywords{
	Gvelocity,
	Gbforce,
	Gshift,

	kMaterial,
	kDampingRatio,
};

mpmobjinfo::mpmobjinfo() {
	mykeystr = "ObjInfo";
	_link_matfac = 0;

	int Nk = 5;
	int keys[]={Gvelocity, Gbforce, Gshift, kMaterial, kDampingRatio };

    std::string knames[]={"globalVelocity", "globalBodyForce", "globalPosition",
    						"Material","DRatio"};

    int Ninvals[]={ 3,4, 3,4, 3,4, 1,2, 1,2, };

	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
	this -> IDdefault = this -> IDpower;
}

void mpmobjinfo::read_info(int Ikey, int Is, vecsca* dptr, Objinfo* oinfo) const{
	switch(Ikey){
	case Gvelocity:
		oinfo->set_velocity( new GVector((*dptr)[Is], (*dptr)[Is+1], (*dptr)[Is+2]) );
		break;
	case Gbforce:
		oinfo->set_bodyforce(new GVector( (*dptr)[Is], (*dptr)[Is+1], (*dptr)[Is+2]));
		break;
	case Gshift:
		oinfo->reset_original_point(new GVector( (*dptr)[Is], (*dptr)[Is+1], (*dptr)[Is+2]));
		break;

	case kMaterial:
		if (_link_matfac != 0){
			int Imat = int ((*dptr)[Is]+0.0001);
			oinfo->assign_material( _link_matfac->get_new_object(Imat) );
		}
		else{
			std::cout<<"WARNING: no material factory is linked."<<std::endl;
		}
		break;

	case kDampingRatio:
		oinfo-> set_damping_ratio( (*dptr)[Is] );
		break;

	default:
		std::cout<<"WARNING: undefined key "<<Ikey<<" ."<<std::endl;
		break;
	}// end switch (function key)
}// end function

int mpmobjinfo::read_value(inpParser& inpreader){
	bool igood = this -> read_inp(inpreader);
	int Istat = -1;
	if (igood) Istat = 0;
	return Istat;
}

Objinfo* mpmobjinfo::get_new_object(unsigned int objID) const {
	Objinfo* oinfo = new Objinfo(objID);
	mapstorage::const_iterator mite;
	int objkey;
	vecsca* dptr;
	vecint* iptr;
	mapivp::const_iterator iite;

	int Il, Is;

	for(mite = alldata_map.begin(); mite!=alldata_map.end(); ++mite){
		objkey = mite->first;
		const vecsvp& dbox = mite->second;
		const mapivp& ibox = infomap.at(objkey);
		iite = ibox.find( objID );
		if(iite == ibox.end()) iite = ibox.find( this -> IDpower );
		if(iite == ibox.end()) continue;

		iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];

		this -> read_info(objkey, Is, dptr, oinfo);
	}// end for (all data)

	return oinfo;
}

