#ifndef FACKIT_ERR_H
#define FACKIT_ERR_H

#include <string>
#include <map>
#include <vector>
#include <sstream>

class errfackit{
public:
	errfackit(){ this->set_default(); this->build__errmsgmap();}
	virtual ~errfackit(){;}
	inline std::string get_errstring(){
		std::string strrec = "";
		strrec += _errmsgmap[_Ierr_title] + '\n';
		for (unsigned int II =0; II != _Ierrs.size(); ++II){
			strrec += "    - " + _errmsgmap[_Ierrs[II]] + '\n';
		}
		return strrec;
	}//{return errstrrec.str();}

protected:
	inline void set_default(){
		_Ierr_title = 0; _Ierr_minpara = 1001; _Ierr_key = 1002; _Ierr_end= 1003;
		_Ierr_ignore = -1;
	}// end function

	inline void build__errmsgmap(){
		_errmsgmap[_Ierr_title]   = "*** error(s) when creating objects. ***";
		_errmsgmap[_Ierr_minpara] = "Given data are insufficient. (input error)";
		_errmsgmap[_Ierr_key]     = "There is no such key in the database. (input error)";
		_errmsgmap[_Ierr_end]     = "Commend block ended unexpectedly (lack of 'End'). (input warning)";
	}//end function

	inline void record_error(int inIerr){ _Ierrs.push_back(inIerr); }

	inline bool checkerr_para_minnumber(unsigned int len, unsigned int inlen, int inIerr){
		bool ierr=( inlen < len );
		if(ierr) record_error(inIerr);
		return ierr;
	}

protected:
	int _Ierr_title, _Ierr_key, _Ierr_end, _Ierr_minpara, _Ierr_ignore;
	std::map<int, std::string> _errmsgmap;

	std::vector<int> _Ierrs;
};
#endif /* FACKIT_ERR_H */
