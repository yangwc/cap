#ifndef COMFAC_GENR_H
#define COMFAC_GENR_H

#include "comfac_cmd.h"

template <class _obj>
class mpmgenrfac : public comcmdfac{
protected:
	typedef typename std::map<int, _obj*>   objmap;
	typedef typename objmap::iterator       omapite;
	typedef typename objmap::const_iterator omapcite;

public:
	virtual ~mpmgenrfac(){
		for(omapite imap =mapobp.begin();  imap != mapobp.end(); ++imap){
			delete (imap->second);
			(imap->second) = 0;
		}
	}

	inline _obj* get_new_object(int id) const{
		_obj* newobj = 0;
		omapcite mapite;
		mapite = mapobp.find(id);
		if (mapite != mapobp.end()){
			newobj = mapite->second->get_new_obj();
		}
		return newobj;
	}

protected:
	virtual inline int make_prototype(int Ikey, int id, int Is, vecsca* dptr){
		_obj* optr = this -> get_new_prototype( Ikey, id, Is, dptr);
		if(optr != 0) this -> save_new_obj(id, optr);
		return 0;
	}

	virtual inline void save_new_obj(int Id, _obj* optr){
		omapite imap = mapobp.find( Id );
		if( imap != mapobp.end() ){
			delete (imap->second);
			(imap->second) = optr;
		}
		else{
			mapobp[Id] = optr;
		}
	}// end function

	virtual _obj* get_new_prototype(int Ikey, int id, int Is, vecsca* dptr) = 0;

	objmap mapobp; // ( ID: object pointer )
};
#endif /* COMFAC_GENR_H */
