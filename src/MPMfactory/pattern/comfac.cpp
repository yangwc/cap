#include "comfac.h"

mpmcomfac::~mpmcomfac(){
	for (std::map<int, mapivp >::iterator iimap = infomap.begin(); iimap != infomap.end(); ++iimap){
		mapivp& ivpmap = iimap -> second;
		for (mapivp::iterator imap = ivpmap.begin(); imap != ivpmap.end(); ++imap){
			delete (imap -> second); (imap -> second) = 0;
		}
	}
	for (mapstorage::iterator ismap = alldata_map.begin(); ismap != alldata_map.end(); ++ismap){
		vecsvp& spvecs = ismap -> second;
		for (vecsvp::iterator ivec = spvecs.begin(); ivec != spvecs.end(); ++ivec){
			delete (*ivec); (*ivec) = 0;
		}
	}
}
/*************************************
 * Initialization
 *************************************/
void mpmcomfac::set_default(){
	_Ierr_title = 0; _Ierr_minpara = 1001; _Ierr_key = 1002; _Ierr_end = 1003;
	IDpower = -1; IDskip = -100; IDdefault = IDskip;

	mapIkey::iterator keyite;
	int Ikey;
	for (keyite= _Ikey_map.begin(); keyite != _Ikey_map.end(); ++keyite){
		Ikey = keyite->second;
		alldata_map[Ikey] = (*new vecsvp);
	}
}// end function

/*************************************
 * functions about READING file
 *************************************/
bool mpmcomfac::read_inp(inpParser& inpreader){
	bool istat;
	istat = record_alllines(inpreader);
	if (!istat) return istat;
	istat = organize_data();
	if (!istat) return istat;
	istat = make_inpstr();
	return istat;
}// end function

bool mpmcomfac::record_alllines(inpParser& inpreader){
	/*
	 * record all input data in Scalar.
	 *    -- throw out inappropriate input commends.
	 *    -- ignore redundant input numbers.
	 */
	bool istat, iend;
	int Ikey;
	unsigned int Nval, Nmin;

	mapstorage::iterator site;
	vecint* ivptr;
	vecsca* svptr;

    std::string  inword;

    while ( inpreader.read_next_line()  ){
    	iend = inpreader.is_endcmd();
    	if ( iend ) break;
    	inword = inpreader.get_next_value();
        Ikey = get_keyID(inword);
        if (Ikey < 0){// *** ERROR 1002
        	record_error(_Ierr_key);
        	std::cout<<"WARNING: Unknown key word: "<<inword<<'.'<<std::endl;
        	continue;
        }

        ivptr = (&expnum_map[Ikey]);
        Nmin  = (*ivptr)[0];
        Nval  = inpreader.get_Ninput();
        istat = checkerr_para_minnumber(Nmin, Nval, _Ierr_minpara);
        if (istat) continue;// *** ERROR 1001

        svptr = inpreader.get_new_surplus<Scalar>( (*ivptr)[1] );

    	alldata_map[Ikey].push_back(svptr);
    }// end while (read line)
    if (!iend && _Ierr_end != _Ierr_ignore ){ // *** ERROR 1003
    	record_error(_Ierr_end);
    	std::cout<<"WARNING: commend block ended unexpectedly (lack of 'End')."<<std::endl;
    }
    return true;
}// end function

bool mpmcomfac::make_inpstr(){
	inpstrrec.str("");
	inpstrrec.clear();

	if(mykeystr.size() != 0) inpstrrec<<mykeystr<<std::endl;

	vecsca* dataptr;
	vecint* infoptr;
	int ID, Is, Lv;
	int Il, Iv;

	//*** keys
	for (vecint::iterator ik = this -> _Ikeys.begin(); ik !=  this -> _Ikeys.end(); ++ik){
		int& Ikey = (*ik);
		vecsvp& dbox = alldata_map[Ikey];
		mapivp& ibox = infomap[Ikey];
		mapivp::iterator iite;

		//*** command lines of each key
		for (iite=ibox.begin(); iite != ibox.end(); ++iite){
			ID = iite->first; infoptr = iite->second;
			Il = (*infoptr)[0]; Is = (*infoptr)[1]; Lv = (*infoptr)[2];
			dataptr = dbox[Il];

			inpstrrec<<"\t"<<_Skey_map[Ikey];

			if( ID != this->IDskip && ID != this->IDpower ) inpstrrec<<"\t"<<ID;

			//*** values in each command line
			for (Iv=Is; Iv<Lv; Iv++){
				inpstrrec<<"\t"<<(*dataptr)[Iv];
			}// end for (values)

			inpstrrec<<std::endl;
		}// end for (command lines)
	}// end for (keys)

	if(mykeystr.size() != 0) inpstrrec<<"End"<<std::endl;
	return true;
}// end function

bool mpmcomfac::fill_infocontainer(unsigned int& Ikey, int& negID, mapivp& info){
	std::vector< vecsca* >* boxptr;
	vecsca* dataline;
	unsigned int Il, Nmax, Ldata;
	int ID;
	const unsigned int Larr = 3;
	int arr[Larr]; // [Il, Ista, Ldata]
	Scalar val;

	boxptr = (&alldata_map[Ikey]);
	Nmax   = expnum_map[Ikey][1];

	for (Il=0; Il < (*boxptr).size(); Il++){
		dataline = (*boxptr)[Il];
		Ldata = (*dataline).size();
		ID = negID; arr[0] = Il; arr[1] = 0; arr[2] = Ldata;
		if ( Ldata == Nmax){
			val    = (*dataline)[0];
			if(val >= 0) ID = (int) (val + 0.01 );
			arr[1] = 1;
		}// end if
		if (ID != IDskip){
			info[ID] = (new vecint(arr, arr+Larr)); // the latest one will be used.
		}// end if
	}// end for
	return true;
}// end function

bool mpmcomfac::fill_infomap(const vecint& keys, int negID){
	mapivp* infptr;
	unsigned int Ikey;
	for  (vecint::const_iterator iv = keys.begin(); iv != keys.end(); ++iv){
		Ikey   = (*iv);
		infptr = new mapivp();
		this -> fill_infocontainer(Ikey, negID, (*infptr));
		infomap[Ikey] = (*infptr);
		delete infptr;
	}
	return true;
}// end function

bool mpmcomfac::organize_data(){
	bool istat = fill_infomap( this -> _Ikeys, this -> IDdefault);
	return istat;
}// end function
