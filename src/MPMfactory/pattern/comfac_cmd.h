#ifndef COMFAC_CMD_H
#define COMFAC_CMD_H

#include "comfac.h"

class comcmdfac : public mpmcomfac{
public:
	virtual ~comcmdfac(){;}

	virtual inline bool make_objects(){
		mapstorage::iterator mite;
		int objkey;
		vecsca* dptr;
		vecint* iptr;
		mapivp::iterator iite;

		int ID, Il, Is;
		for(mite=alldata_map.begin(); mite!=alldata_map.end(); mite++){
			objkey = mite->first;
			vecsvp& dbox = mite->second;
			mapivp& ibox = infomap[objkey];

			for (iite=ibox.begin(); iite != ibox.end(); iite++){
				ID = iite->first; iptr = iite->second;
				Il = (*iptr)[0]; Is = (*iptr)[1];
				dptr = dbox[Il];
				this -> make_prototype(objkey, ID, Is, dptr);
			}// end for (same kinds)
		}// end for (all data)
		return true;
	}// end function

protected:
	inline void build_inputmap( int Nvals[] ){
		int Iend, Ista=0;
		for (unsigned int II=0; II< _Nkey; ++II){
			Iend = Ista + 2;
			this -> expnum_map[ _Ikeys[II] ] =
					(*new std::vector<int>(Nvals+Ista, Nvals+Iend));
			Ista = Iend;
		}
	}

	virtual int make_prototype(int Ikey, int id, int Is, vecsca* dptr) = 0;
};
#endif /* COMFAC_CMD_H */
