#ifndef FACKIT_KMAP_H
#define FACKIT_KMAP_H

#include <string>
#include <map>
#include <vector>

class FACkmap{
public:
	typedef std::map<std::string, int> mapIkey;
	typedef std::map<int, std::string> mapSkey;

public:
	FACkmap(){ _Nkey = 0; }
	~FACkmap(){;}

	int get_keyID(std::string& inword){
		/*
		 * covert input keyword from string to integer.
		 * --------------------------------------------
		 * RETURN: int Ikey (-1: failed)
		 */
	    int Ikey = -1;
	    mapIkey::iterator iter;

	    iter = _Ikey_map.find(inword);
		if(iter != _Ikey_map.end()){ Ikey = iter->second; }
	    return Ikey;
	}// end function

protected:
	inline void build_keymap( unsigned int Nk, int keys[], std::string knames[] ){
		this -> _Nkey = Nk;
		this -> _Ikeys.assign( keys, keys+Nk);

	    for (unsigned int Ik=0; Ik < Nk; ++Ik){
	    	this -> _Ikey_map[knames[Ik]] = keys[Ik];
	    	this -> _Skey_map[keys[Ik]]   = knames[Ik];
	    }
	}

	unsigned int _Nkey;

	std::vector<int> _Ikeys;
	mapIkey _Ikey_map;
	mapSkey _Skey_map;
};



#endif /* FACKIT_KMAP_H */
