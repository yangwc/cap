#ifndef COMFAC_H
#define COMFAC_H

#include "fackit_err.h"
#include "fackit_kmap.h"

#include "../../GB_Tensor.h"
#include "../../MPMfstream/inpParser.h"

class mpmcomfac : public errfackit, public FACkmap {
protected:
	typedef std::vector<int> vecint;
	typedef std::vector<Scalar> vecsca;
	typedef std::vector<vecsca*> vecsvp;
	typedef std::vector<vecint*> vecivp;
	typedef std::map<int, vecsca*> mapsvp;
	typedef std::map<int, vecint*> mapivp;
	typedef std::map<std::string, int> mapIkey;
	typedef std::map<int, std::string> mapSkey;
	typedef std::map<int, vecsvp > mapstorage;

public:
	virtual ~mpmcomfac();
	bool read_inp(inpParser& inpreader);

	bool make_inpstr();

	std::string get_inpstring(){return inpstrrec.str();}

protected:

	bool record_alllines(inpParser& inpreader);
	bool organize_data();
	/* ===========================
	 * functions
	 =========================== */
	void set_default();
	bool fill_infomap(const vecint& keys, int negID);
	bool fill_infocontainer(unsigned int& Ikey, int& negID, mapivp& info);

	/* ===========================
	 * personal values
	 =========================== */
	int IDpower, IDskip, IDdefault;
	std::string mykeystr;
//	unsigned int Nkey;
//	std::vector<int> kgpall;
//	mapIkey Ikey_map;
//	mapSkey Skey_map;
	std::map<int, vecint > expnum_map; // [min. input, max. read values]

	/* ===========================
	 * operational members
	 =========================== */
	mapstorage alldata_map;         // ( Ikey: [ [input values], ] )
	std::map<int, mapivp > infomap; // ( Ikey: (ID: [address, Ista, Ldata] ) )

	/* ===========================
	 * final product and recorder
	 =========================== */
    std::ostringstream inpstrrec;
};
#endif /* COMFAC_H */
