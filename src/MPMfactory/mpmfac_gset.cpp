#include "mpmfac_gset.h"

enum genrKeywords {
	Timestep,
	StartTime,
	EndTime,
	STARecTime,
	OUTStep,
	IFODB,
	IFOBJ,
	IFGND
};

/*************************************
 * Initialization
 *************************************/
mpmgsetfac::mpmgsetfac(){
	_dt   = -1;
	_tend = 1;
	_tsta = 0;
	_trec = 0;
	_Nstpout = 0;
	_io_gnd = _io_obj = _io_odb = false;

	mykeystr = "GeneralSetup";
	unsigned int Nk = 8;
	int keys[] = { Timestep, StartTime, EndTime, STARecTime, OUTStep, IFODB, IFOBJ, IFGND};
    std::string knames[]={"TimeStep", "StaTime", "EndTime",
    		              "RecStaTime", "MPMRecStep", "SWodb", "SWobj", "SWgnd"};
    int Ninvals[]={ 1,2, 1,2, 1,2, 1,2, 1,2, 1,2 ,1,2 ,1,2 };

	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
	this -> IDdefault = this -> IDpower;
}

mpmgsetfac::~mpmgsetfac(){;}

int mpmgsetfac::make_prototype(int Ikey, int ownerid, int Is, vecsca* dptr){
	if ( ownerid != this->IDpower ){
		std::cout<<"(CODE) ERROR: general setups always apply to all object."<<std::endl;
	}
	switch(Ikey){
	case Timestep:
		_dt = dptr -> at(Is);
		break;
	case StartTime:
		_tsta = dptr -> at(Is);
		break;
	case  EndTime:
		_tend = dptr -> at(Is);
		break;
	case  STARecTime:
		_trec = dptr -> at(Is);
		break;
	case  OUTStep:
		_Nstpout = (unsigned int) (dptr -> at(Is) + 0.1);
		_io_gnd = _io_obj = true;
		break;
	case  IFODB:
		_io_odb = ( (int)(dptr->at(Is) + 0.1) ) == 1;
		break;
	case  IFOBJ:
		_io_obj = ( (int)(dptr->at(Is) + 0.1) ) == 1;
		break;
	case  IFGND:
		_io_gnd = ( (int)(dptr->at(Is) + 0.1) ) == 1;
		break;
	}// end switch (function key)
	return 0;
}

int  mpmgsetfac::set_computer(MPM* mpmc) const{
	if (_dt <= 0 ){
		std::cout << "!!! ERROR !!! Time interval dt should larger than 0." << std::endl;
		return -1;
	}
	if(_tend <= _tsta){
		std::cout << "!!! ERROR !!! Analysis ends before or when it starts! NOTHING will be done." << std::endl;
		return -2;
	}
	if(_tend < _trec){
		std::cout << "!!! ERROR !!! Data start being recorded after all analysis is done! NOTHING will be done." << std::endl;
		return -3;
	}

	if (_dt <= 0 ) return -1;
	mpmc->assign_timestep(_dt);
	mpmc->assign_duration(_tend);
	mpmc->set_starttime(_tsta);
	mpmc->set_rectimeo(_trec);
	mpmc->set_recstep(_Nstpout);
//	mpmc->set_rststep(_Nstprst);
	return 0;
}

void mpmgsetfac::get_step_time_w(unsigned int Nostep, std::vector<Scalar>& stptimes) const{
	// time step in MPM can be a very small number compared to analyzing duration
	unsigned long long int Ntstep = (unsigned long long int) ((_tend - _tsta)/_dt +0.01);
	unsigned long long int Istep  = 0;

	int Nrecon = (int) ((_trec - _tsta)/_dt +0.01);
	if (Nrecon < 0){ Nrecon = 0;}
	unsigned int Iostep = Nostep-Nrecon;

	stptimes.clear();
	while (  Istep != Ntstep  ){
		if(   Iostep == Nostep ){
			stptimes.push_back( (_tsta + _dt*Istep) );
			Iostep = 0;
		}
		Istep++;
		Iostep++;
	}
	stptimes.push_back( (_tsta + _dt*Ntstep) );
}
