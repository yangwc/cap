#ifndef MPMFAC_OBJINFO_H
#define MPMFAC_OBJINFO_H

#include "../MPMobject/LiteFac/Objinfo.h"
#include "pattern/comfac_cmd.h"
#include "mpmfac_mat.h"

class mpmobjinfo : public comcmdfac{
protected:
	typedef std::vector<Particle*> ptpvec;

public:
	mpmobjinfo();
	virtual ~mpmobjinfo(){;}

	int link_matfactory(mpmmatfac* matfac){_link_matfac = matfac; return 0; }
	int read_value(inpParser& inpreader);
	Objinfo* get_new_object(unsigned int objID) const;

protected:
	inline int make_prototype(int Ikey, int id, int Is, vecsca* dptr){return 0;}
	void read_info(int Ikey, int Is, vecsca* dptr, Objinfo* oinfo)  const;


	mpmmatfac* _link_matfac;
};

#endif /* MPMFAC_OBJINFO_H */
