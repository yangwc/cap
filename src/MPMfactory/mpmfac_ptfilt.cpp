#include "mpmfac_ptfilt.h"

enum ptfilterKeywords {
	Eater = 1,
	Generator,   // center, constant in time
	CTGenerator, // corner, time-dependent

	Owner,
	G_staID,
	G_mesh
};

/*************************************
 * Initialization
 *************************************/
mpmptfiltfac::mpmptfiltfac(){
	mykeystr = "PTFilter";
	this -> build_keymap();
	this -> build_inputmap();
	this -> set_default();
}

mpmptfiltfac::~mpmptfiltfac(){
	for (ovecite iite = vecobp.begin(); iite != vecobp.end(); ++iite){
		delete (*iite); (*iite) = 0;
	}
}

void mpmptfiltfac::build_keymap(){
	/*
	 * map for string keyword to its ID defined in "enum ptfilterKeywords{}".
	 */
	int Nm = 3, Ns = 3;
	int gpmain[]={Eater, Generator, CTGenerator};
    int gpside[]={Owner, G_staID, G_mesh};
    std::string gpmains[]={"Eater", "Generator", "CTGenerator"};
    std::string gpsides[]={"Owner", "G_staID", "G_mesh"};

    _Nkey = Nm + Ns;

    kgpmain.assign(gpmain, gpmain+Nm);
    kgpside.assign(gpside, gpside+Ns);
    _Ikeys.assign(gpmain, gpmain+Nm);
    _Ikeys.insert(_Ikeys.end(), gpside, gpside+Ns);

    int II;
    for (II=0; II<Nm; II++){
    	_Ikey_map[gpmains[II]] = gpmain[II];
    	_Skey_map[gpmain[II]]  = gpmains[II];
    }
    for (II=0; II<Ns; II++){
    	_Ikey_map[gpsides[II]] = gpside[II];
    	_Skey_map[gpside[II]]  = gpsides[II];
    }
}

void mpmptfiltfac::build_inputmap(){
	int arr[]={ 10,10, 10,10, 10,10,
				2,2, 1,2, 4,5 };
	int Iend, Ista=0;
	for (unsigned int II=0; II< _Nkey; II++){
		Iend = Ista + 2;
		expnum_map[_Ikeys[II]] = (*new std::vector<int>(arr+Ista, arr+Iend));
		Ista = Iend;
	}
}// end function

bool mpmptfiltfac::organize_data(){
	bool istat1 = this -> fill_infomap( this -> kgpside, this -> IDpower);
	bool istat2 = this -> fill_infomap( this -> kgpmain, this -> IDskip);
	return (istat1 && istat2);
}

mpmptfiltfac::_typ_obj* mpmptfiltfac::get_new_object(unsigned int Ibc, int objID){
	_typ_obj *optr = 0, *oproto = vecobp[Ibc];
	if( oproto->is_accessible(objID) ){
		optr = oproto->get_new_filter();
		optr-> set_owner(objID);
	}
	return optr;
}

unsigned int mpmptfiltfac::get_new_objects(int ownerID, objvec& objs){
	_typ_obj *optr = 0;
	unsigned int Nobj = 0;
	for (ovecite ivec=vecobp.begin(); ivec != vecobp.end(); ++ivec){
		_typ_obj *& oproto = (*ivec);
		if( oproto->is_accessible(ownerID) ){
			optr = oproto->get_new_filter();
			optr-> set_owner(ownerID);
			objs.push_back(optr);
			optr = 0;
			Nobj++;
		}
	}
	return Nobj;
}

/*************************************
 ***  ***  ***  ***  ***  ***  ***  **
 * functions about MAKING boundaries
 **  ***  ***  ***  ***  ***  ***  ***
 *************************************/
bool mpmptfiltfac::make_objects(mpmfuncfac* funcfacptr, mpmobjinfo* oinffacptr){
	this->make_eaters(funcfacptr);
	this->make_generators(funcfacptr, oinffacptr);
	this->make_CTgenerators(funcfacptr, oinffacptr);
	return true;
}// end function

bool mpmptfiltfac::make_eaters(mpmfuncfac* funcfacptr){
	typedef PFeater OBJ;
	vecsvp& dbox = alldata_map[Eater];
	mapivp& ibox = infomap[Eater];
	vecsca* dptr;
	vecint* iptr;
	mapivp::iterator iite, iite2;

	int ID, Il, Is;

	GVector x, n, dim;
	Cuboid* geo;
	OBJ* optr;

	for (iite=ibox.begin(); iite != ibox.end(); iite++){
		ID = iite->first; iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];
		x.SetComponents((*dptr)[Is  ],(*dptr)[Is+1],(*dptr)[Is+2]);
		n.SetComponents((*dptr)[Is+3],(*dptr)[Is+4],(*dptr)[Is+5]);
		dim.SetComponents((*dptr)[Is+6],(*dptr)[Is+7],(*dptr)[Is+8]);

		/*** create zone ***/
		geo = new Cuboid( x, n);
		geo-> set_size(dim);

		/*** create pt filter ***/
		optr = new OBJ();
		optr->assign_zone( geo ); geo = 0;

		this-> set_owner(ID, optr);

		vecobp.push_back(optr); optr = 0;
	}
	return true;
}// end function

bool mpmptfiltfac::make_generators(mpmfuncfac* funcfacptr, mpmobjinfo* oinffacptr){
	typedef PFproducer OBJ;
	vecsvp& dbox = alldata_map[Generator];
	mapivp& ibox = infomap[Generator];
	vecsca* dptr;
	vecint* iptr;
	mapivp::iterator iite, iite2;

	int ID, Il, Is;

	int funID;
	GVector x, n, dim;
	Cuboid* geo;
	OBJ* optr;

	for (iite=ibox.begin(); iite != ibox.end(); iite++){
		ID = iite->first; iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];

		funID = (int) ((*dptr)[Is] + 0.1);
		x.SetComponents((*dptr)[Is+1],(*dptr)[Is+2],(*dptr)[Is+3]);
		n.SetComponents((*dptr)[Is+4],(*dptr)[Is+5],(*dptr)[Is+6]);
		dim.SetComponents(0,(*dptr)[Is+7],(*dptr)[Is+8]);


		/*** create zone ***/
		geo = new Cuboid( x, n);
		geo-> set_size(dim);

		/*** create pt filter ***/
		optr = new OBJ();
		optr-> assign_zone( geo ); geo = 0;
		optr-> set_vn_func( funcfacptr->get_new_object(funID) );

		set_owner(ID, optr);
		set_staID(ID, optr);
		set_mesh(ID, optr);
		Objinfo* oinfo = oinffacptr->get_new_object(optr->get_ownerID());
		if (oinfo == 0){
			std::cout<<"ERROR: can not find material setup for object "
					<<optr->get_ownerID()
					<<". @ PT generator"<<std::endl;
			delete optr;
			continue;
		}
		optr->assign_object_info(oinfo); oinfo=0;


		vecobp.push_back(optr); optr = 0;
	}
	return true;
}// end function

bool mpmptfiltfac::make_CTgenerators(mpmfuncfac* funcfacptr, mpmobjinfo* oinffacptr){
	typedef PFprodCT OBJ;
	vecsvp& dbox = alldata_map[CTGenerator];
	mapivp& ibox = infomap[CTGenerator];
	vecsca* dptr;
	vecint* iptr;
	mapivp::iterator iite, iite2;

	int ID, Il, Is;

	int vnfunID, lt1funID, lt2funID;
	GVector x, n;
	Cuboid* geo;
	OBJ* optr;

	for (iite=ibox.begin(); iite != ibox.end(); iite++){
		ID = iite->first; iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];

		vnfunID  = (int) ((*dptr)[Is] + 0.1);
		lt1funID = (int) ((*dptr)[Is+1] + 0.1);
		lt2funID = (int) ((*dptr)[Is+2] + 0.1);
		x.SetComponents((*dptr)[Is+3],(*dptr)[Is+4],(*dptr)[Is+5]);
		n.SetComponents((*dptr)[Is+6],(*dptr)[Is+7],(*dptr)[Is+8]);

		/*** get functions ***/
		function* vnfunc = funcfacptr->get_new_object(vnfunID);
		function* lt1func = funcfacptr->get_new_object(lt1funID);
		function* lt2func = funcfacptr->get_new_object(lt2funID);

		/*** create zone ***/
		geo = new Cuboid( x, n);

		/*** create pt filter ***/
		optr = new OBJ();
		optr-> set_vn_func( vnfunc ); vnfunc = 0;
		optr-> set_t1length_func(lt1func); lt1func = 0;
		optr-> set_t2length_func(lt2func); lt2func = 0;

		set_owner(ID, optr);
		set_staID(ID, optr);
		set_mesh(ID, optr);

		Objinfo* oinfo = oinffacptr->get_new_object(optr->get_ownerID());
		if (oinfo == 0){
			std::cout<<"ERROR: can not find material setup for object "
					<<optr->get_ownerID()
					<<". @ PT generator"<<std::endl;
			delete optr;
			continue;
		}
		optr->assign_object_info(oinfo); oinfo=0;


		vecobp.push_back(optr); optr = 0;
	}
	return true;
}// end function

bool mpmptfiltfac::set_owner(int& ID, _typ_obj* gobj){
	vecsvp& dbox = alldata_map[Owner];
	mapivp& ibox = infomap[Owner];
	mapivp::iterator iite;
	vecint* iptr;
	int Il, Is;
	bool ifound;

	iite = ibox.find(ID);
	ifound = (iite != ibox.end());
	if (!ifound){
		iite = ibox.find(IDpower);
		ifound = (iite != ibox.end());
	}// end if

	if (ifound){
		vecsca* dptr;
		iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];
		int Iobj = (int) (*dptr)[Is];
		gobj-> set_owner( Iobj );
	}// end if
	return true;
}// end function

bool mpmptfiltfac::set_staID(int& ID, PFproducer* bobj){
	vecsvp& dbox = alldata_map[G_staID];
	mapivp& ibox = infomap[G_staID];
	mapivp::iterator iite;
	vecint* iptr;
	int Il, Is;
	bool ifound;

	iite = ibox.find(ID);
	ifound = (iite != ibox.end());
	if (!ifound){
		iite = ibox.find(IDpower);
		ifound = (iite != ibox.end());
	}// end if

	if (ifound){
		vecsca* dptr;
		iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];
		long long int Ista = (long long int) (*dptr)[Is];
		bobj->set_start_ID(Ista);
	}// end if
	return true;
}// end function

bool mpmptfiltfac::set_mesh(int& ID, PFproducer* bobj){
	vecsvp& dbox = alldata_map[G_mesh];
	mapivp& ibox = infomap[G_mesh];
	mapivp::iterator iite;
	vecint* iptr;
	int Il, Is;
	bool ifound;

	iite = ibox.find(ID);
	ifound = (iite != ibox.end());
	if (!ifound){
		iite = ibox.find(IDpower);
		ifound = (iite != ibox.end());
	}// end if

	if (ifound){
		vecsca* dptr;
		iptr = iite->second;
		Il = (*iptr)[0]; Is = (*iptr)[1];
		dptr = dbox[Il];
		Scalar rnfront = (*dptr)[Is];
		Scalar drn = (*dptr)[Is+1];
		int Nsegt1 = (int) (*dptr)[Is+2];
		int Nsegt2 = (int) (*dptr)[Is+3];
		bobj->set_discretize_info(drn, Nsegt1, Nsegt2);
		bobj->set_front_dist(rnfront);
	}// end if
	return true;
}// end function
