#include "mpmfac_func.h"

enum functionKeywords {
	Const = 1,
	Sin,
	Cos,
	LRamp,
	Line,
	O2poly,
	EXP,
	Rect,
	HCir
};

/*************************************
 * Initialization
 *************************************/
mpmfuncfac::mpmfuncfac(){
	mykeystr = "Function";

	unsigned int Nk = 9;
	int keys[]={Const, Sin, Cos, LRamp, Line, O2poly, EXP, Rect, HCir};
    std::string knames[]={"Const", "Sin", "Cos", "LRamp", "Line", "O2poly",
    						"EXP", "Rect", "HCircle"};
    int Ninvals[]={ 2,2, 5,5, 5,5, 5,5, 4,4, 4,4, 4,4, 5,5, 3,3 };

	this -> build_keymap( Nk, keys, knames);
	this -> build_inputmap( Ninvals );
	this -> set_default();
}

function* mpmfuncfac::get_new_prototype(int Ikey, int id, int Is, vecsca* dptr){
	function* fptr = NULL;
	switch(Ikey){
	case Const:
		fptr = new FNconstant(id, (*dptr)[Is]);
		break;
	case Sin:
		fptr = new FNsin(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2],(*dptr)[Is+3]);
		break;
	case Cos:
		fptr = new FNcos(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2],(*dptr)[Is+3]);
		break;
	case LRamp:
		fptr = new FNlramp(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2],(*dptr)[Is+3]);
		break;
	case Line:
		fptr = new FNline(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2]);
		break;
	case O2poly:
		fptr = new FNO2poly(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2]);
		break;
	case EXP:
		fptr = new FNexp(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2]);
		break;
	case Rect:
		fptr = new FNrect(id, (*dptr)[Is],(*dptr)[Is+1],(*dptr)[Is+2],(*dptr)[Is+3]);
		break;
	case HCir:
		fptr = new FNhcir(id, (*dptr)[Is],(*dptr)[Is+1]);
		break;
	}// end switch (function key)
	return fptr;
}// end function
