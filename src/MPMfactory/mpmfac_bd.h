#ifndef MPMFAC_BD_H
#define MPMFAC_BD_H

#include "../MPMobject/Boundary/_PK_BC.h"
#include "mpmfac_func.h"
#include "pattern/comfac.h"

class Cuboid;

class mpmbdfac : public mpmcomfac{
protected:
	typedef BC _obj;
	typedef std::map<int, BC*> objmap;
	typedef std::vector<BC*>   objvec;
	typedef objmap::iterator   omapite;
	typedef objvec::iterator   ovecite;
public:
	mpmbdfac();
	~mpmbdfac();

	bool organize_data();
	bool make_boundaries(mpmfuncfac* funcfacptr);
	BC* get_new_object(unsigned int Ibc, int objID);

	inline unsigned int get_Nboundary(){ return vecbcp.size(); }
	inline int get_powerId(){return IDpower;}

protected:
	/* ===========================
	 * functions
	 =========================== */
//	void boundary_default();

	void build_keymap();
	void build_inputmap();

	bool make_planars(mpmfuncfac* funcfacptr);
	bool make_slipsurfaces(mpmfuncfac* funcfacptr);
	bool make_surfaces(mpmfuncfac* funcfacptr);
	bool make_fixeds(mpmfuncfac* funcfacptr);
	bool make_freecuts(mpmfuncfac* funcfacptr);
	bool make_Dvels(mpmfuncfac* funcfacptr);
	bool make_Daccs(mpmfuncfac* funcfacptr);
	bool make_laminarflow(mpmfuncfac* funcfacptr);

	BC* general_setting(int ID, mpmfuncfac* funcfacptro, BC* bobj, Cuboid*& cubge);

	bool set_size(int ID, Cuboid* cubgeo);
	bool set_phase(int ID, BC* bobj);
	bool set_dlayer(int ID, mpmfuncfac* funcfacptr, Cuboid* cubgeo, BC*& bobj);
	bool set_flayer(int ID, mpmfuncfac* funcfacptr, Cuboid* cubgeo, BC*& bobj);

	/* ===========================
	 * personal values
	 =========================== */
	std::vector<int> kgpside,kgpmain;

	/* ===========================
	 * final product and recorder
	 =========================== */
	std::vector<BC*> vecbcp;

	objmap mapobp;
	objvec vecobp;
};
#endif /* MPMFAC_BD_H */
