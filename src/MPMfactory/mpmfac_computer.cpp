/**************************************************
 * mpmfac.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.01.27)
 **************************************************/

#include "mpmfac_computer.h"
#include "../MPMcomputer/MPMinfo.h"

enum keywords {
	GenrSetup = 1,

	Boundaries,
	GridSpace,

	MaterialKey,
	ObjectInfo,
	Particles,
	PTState,
	PTVelocity,
	PTBodyForce,
	PTFilters,

	OutputPT,
	Function,
};

MFACcomp::MFACcomp(){
    _mpmversion = _version_;
    this -> set_default();

	unsigned int Nk = 12;
	int keys[] = { GenrSetup, GridSpace, Boundaries, MaterialKey, ObjectInfo,
			       Particles, PTState, PTVelocity, PTBodyForce, PTFilters,
			       OutputPT, Function};
    std::string knames[]={"GeneralSetup", "GridSpace", "Boundary", "Material", "ObjInfo",
    		              "Particles", "ParticleState", "ParticleVelocity", "ParticleBodyForce", "PTFilter",
    		              "OutputPT", "Function"};

	this -> build_keymap( Nk, keys, knames);
	this -> build__errmsgmap();
}

int MFACcomp::read_keyblock(int Ikey){
	bool igood = false;
    int Istat = -1;
    switch(Ikey) {
        case GenrSetup:
        	igood = _gsetfac.read_inp( _inpreader );
            break;

        case GridSpace:
            igood = _gspafac.read_inp( _inpreader );
            break;

        case MaterialKey:
        	igood = _matfac.read_inp(_inpreader);
            break;

        case Boundaries:
        	igood = _bdfac.read_inp(_inpreader);
            break;

        case PTFilters:
        	igood = _pfiltfac.read_inp(_inpreader);
            break;

        case Function:
        	igood = _funcfac.read_inp(_inpreader);
            break;

        case OutputPT:
            igood = _ptofac.read_inp(_inpreader);
            break;

        case ObjectInfo:
        	igood = _oinffac.read_inp(_inpreader);
            break;

        case Particles:
        	Istat = _ptfac.read_particle(_inpreader);
            igood = (Istat == 0);
            break;

        case PTVelocity:
        	Istat = _ptfac.read_velocity(_inpreader);
            igood = (Istat == 0);
            break;

        case PTBodyForce:
        	Istat = _ptfac.read_bforce(_inpreader);
            igood = (Istat == 0);
            break;

        case PTState:
        	Istat = _ptfac.read_state(_inpreader);
            igood = (Istat == 0);
            break;

        default:
        	std::cout<< "WARNING: undefined key "<< Ikey <<'.'<<std::endl;
        	break;
    }
    if(igood){
    	_rstat_keys.push_back(Ikey);
    	Istat = 0;
    }
    return Istat;
}

int MFACcomp::open_ifile(const std::string& ifilename, const std::string& ibasename){
	int Istat = -1;
	std:: string filenow = ifilename;
	if ( _inpreader.open(ifilename) ) {
		_ifname   = ifilename;
		_projname = ibasename;
		Istat = 0;
	}
	return Istat;
}

MPM* MFACcomp::get_new_computer(int& Istat){
	MPM* mpmptr = 0;

    Istat = this -> read_inp();
    bool igood = (Istat == 0);

    if(igood){ igood = _funcfac.make_objects();}

    if(igood){ igood = _matfac.make_objects();}
    if(igood){ _oinffac.link_matfactory(&_matfac); igood = _oinffac.make_objects();}
    if(igood){ Istat = _ptfac.make_particles(_oinffac); igood = (Istat == 0); }

    if(igood){ igood = _ptofac.make_objects();}
    if(igood){ igood = _bdfac.make_boundaries( &_funcfac );}
    if(igood){ igood = _pfiltfac.make_objects( &_funcfac,  &_oinffac);}
    if(igood){ igood = _gspafac.make_objects();}

    if(igood){ igood = _gsetfac.make_objects();}

    if(igood){ mpmptr = this -> create_computer(); }

    if ( !igood ){ Istat = -100; }
    return mpmptr;
}// end function

int MFACcomp::read_inp(){
	std::string inword;
	int Ikey, Istat;
	while ( _inpreader.read_next_line() ){
	   	/* ----------------------------------------------------------------------------- *
	   	 * Any contents between key blocks will be ignored.
	   	 * (key block: chunk of input lines starts with a key word and ends with "End")
	   	 * ----------------------------------------------------------------------------- */
		inword = _inpreader.get_next_value();
		Ikey   = get_keyID(inword);

		// *** ERROR 1002
		if (Ikey < 0){
			record_error(_Ierr_key);
			std::cout<<"WARNING: Unknown key word: "<<inword<<'.'<<std::endl;
			continue;
		}

		Istat = this -> read_keyblock( Ikey );
		if( Istat != 0 ) break;
	}// end while
	return Istat;
}

MPMos* MFACcomp::create_os(){
	/************************************************************
	 * This function creates and returns a MPM operation system,
	 * which include whole universe of the MPM world, including
	 * spaces, bridges, grids, particles and particle filters.
	 ************************************************************/
	typedef std::vector<Particle*>          ptpvec;
	typedef std::map<unsigned int, ptpvec > opsmap;

	// -------------------------------- //
	// new operation system ----------- //
	MPMos* uniptr = new MPMos();

	// -------------------------------- //
	// new empty bridge --------------- //
	GBridge* brdgptr  = _gspafac.get_new_bridge();

	// -------------------------------- //
	// create empty spaces ------------ //
	Space* spptr=0;
	opsmap optsmap = _ptfac.get_objptmap();

	for (opsmap::iterator iopmap = optsmap.begin(); iopmap!=optsmap.end(); ++iopmap){
		/****************************
		 * 		create space,
		 * 		assign grids and
		 * 		connect with bridges
		 ****************************/
		const unsigned int& objID = iopmap->first;
		spptr = _gspafac.get_new_space( objID, brdgptr, _bdfac, _pfiltfac);

		/****************************
		 * 		assign particles
		 ****************************/
		ptpvec& pts = iopmap->second;
		spptr -> assign_particles( pts );

		/****************************
		 * 		output file
		 ****************************/
		std::ostringstream oname;
		oname << _projname << "_"<< objID;

		/****************************
		 * 		obj (output) file
		 ****************************/
		if (_gsetfac.if_output_obj()){

			outobj* oobjptr = new outobj(oname.str().data());
			std::vector<PTPVal*>* opts = _ptofac.get_new_objects(objID);
			if(opts == 0){
				std::cout<<"WARNING: no particle output (OutputPT) for object "<<objID<<"."<<std::endl;
			}
			else{
				oobjptr->set_outkeys( *opts );
				delete opts; opts = 0;
			}

			std::vector<Scalar> objstimes;
			_gsetfac.get_obj_step_time_w(objstimes);
			oobjptr->print_header( objID, objstimes );
			spptr->assign_objfile(oobjptr);
			oobjptr = 0;
		}// end if (ouput obj)

		/****************************
		 * 		gnd (output) file
		 ****************************/
		if (_gsetfac.if_output_gnd()){
			outgnd* ogndptr = new outgnd(oname.str().data());
			std::vector<Scalar> objstimes;
			_gsetfac.get_obj_step_time_w(objstimes);
			ogndptr->print_header( objID, objstimes );
			spptr->assign_gndfile(ogndptr);
			ogndptr = 0;
		}// end if (ouput obj)

		/****************************
		 * 		odb (output) file
		 ****************************/
		if (_gsetfac.if_output_odb()){

			// material info
			Objinfo* oinfo = _oinffac.get_new_object( objID );
			unsigned int Nval_state = oinfo->get_Nstatevals();
			unsigned int matcode    = oinfo->get_matcode();

			std::vector<Scalar> props;
			oinfo -> get_property( props );

			outodb* oodbptr = new outodb(oname.str().data());
			std::vector<Scalar> objstimes;
			_gsetfac.get_obj_step_time_w(objstimes);
			oodbptr->print_header( objID, matcode, props, Nval_state, objstimes );
			spptr->assign_odbfile(oodbptr);
			oodbptr = 0;
		}// end if (ouput obj)
		/****************************
		 * 		assign space
		 ****************************/
		uniptr -> add_space(spptr);
	}

	uniptr->set_gridbridge(brdgptr); brdgptr = 0;
	return uniptr;
}

MPM* MFACcomp::create_computer(){
	/****************************
	 * create empty MPM computer
	 ****************************/
	MPM* mpmptr = new MPM();

	/****************************
	 * setup analysis schedule
	 ****************************/
	int Istat = _gsetfac.set_computer( mpmptr );

	/****************************
	 * install operation system if
	 * the schedule has no problem
	 ****************************/
	if( Istat != 0){
		std::cout<<"ERROR: cannot setup computer."<<std::endl;
	}
	else{
		MPMos* osptr = this -> create_os();
		mpmptr -> assign_operatingsystem(osptr);
	}

	/****************************
	 * return the new computer
	 ****************************/
	return mpmptr;
}


std::string MFACcomp::get_statmsg(){
	/************************************************************
	 * return current status
	 ************************************************************/
    std::string msg = "*** Reading Status Report ***\n";
    for(intvec::iterator ivec = _rstat_keys.begin(); ivec != _rstat_keys.end(); ++ivec){
    	msg += "    - Reading key ";
    	msg += _Skey_map[ (*ivec) ];
    	msg += " is finished.\n";
    }
    return msg;
}// end function


