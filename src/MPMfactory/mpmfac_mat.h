#ifndef MPMFAC_MAT_H
#define MPMFAC_MAT_H

#include "../Material/Material.h"
#include "pattern/comfac_general.h"

class mpmmatfac : public mpmgenrfac<Material>{
public:
	mpmmatfac();
	virtual ~mpmmatfac(){;}

protected:
	Material* get_new_prototype(int Ikey, int obID, int Is, vecsca* dptr);

	inline void get_values_w(unsigned int Nval, unsigned int Is, vecsca* dptr, vecsca& vals){
		vecsca::iterator iv = (*dptr).begin();
		vals.assign(iv+Is, iv+(Is+Nval));
	}
};
#endif /* MPMFAC_MAT_H */
