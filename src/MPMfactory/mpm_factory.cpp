/**************************************************
 * mpm_factory.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 21, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#include "mpm_factory.h"

#include <ctime>
#include "subfac_base/linefac.h"

inline void report_error(const std::string& fnm){
	std::cout << "Facing errors! read "<< fnm <<" for more information."<<std::endl;
}

void mpmfactory::set_order(const std::string& ifilename){
	int Istat;
	set_order(ifilename,Istat);
	if (Istat < 0){ std::cout<<statmessage(Istat)<<std::endl; }
}// end function (set_order)

void mpmfactory::set_order(const std::string& ifilename, int& oIstat){
    int Istat;
	std::string basename, ifname = ifilename;
	/****************************
	 *		identify filename
	 ****************************/
	linefac lfac;
	Istat = lfac.clean_line(ifname);
	if (Istat == -1){ oIstat = -101; return;}// *** ERROR: empty input

	lfac.extract_basename(ifname, basename);
	std::cout << "*** current project: "<<basename<<std::endl;
	std::cout << "--- mpm-v"<< _modelfac.get_srcversion() <<" ---"<<std::endl;

	/****************************
	 *	open input file
	 ****************************/
	const int Next    = 1;
	std::string ext[] = {".inp"};

	Istat = _modelfac.open_ifile(ifname, basename);
	if (Istat == -1){
		for (int II = 0; II < Next; ++II){
			ifname = basename + ext[II];
			Istat  = _modelfac.open_ifile(ifname,basename);
			if (Istat == 0) break;
		}
	}// end if

	if (Istat == 0){
		std::cout<< "Reading input file " << ifname <<std::endl;
	}
	else{ // *** ERROR: no such file
		_oerr = new outerr(basename);
		ifname = ifilename;
		for (int II = 0; II < Next; ++II) {ifname = ifname+" or "+basename+ext[II];}
		_oerr->add_msg("Can not open "+ifname +".\n");
		_oerr->print();

		report_error(_oerr->get_filename());
		_oerr->close(); delete _oerr; _oerr = 0;
		oIstat = -102;
		return;
	}// end if

	oIstat = 0;
}// end function (set_order)

MPM* mpmfactory::make_new_model(){
	int Istat;
	MPM* mpmptr = this -> make_new_model(Istat);
	if (Istat < 0){ std::cout<<statmessage(Istat)<<std::endl; }
	return mpmptr;
}// end function (make_model)

MPM* mpmfactory::make_new_model(int& Istat){
	/****************************
	 *		setup MPM model
	 ****************************/
	std::clock_t startTime = std::clock();
	// --------------->
	MPM* mpmptr = _modelfac.get_new_computer(Istat);

	// *** ERROR
	if( Istat != 0 ){
		_oerr = new outerr(_modelfac.get_projectname());
		_oerr->add_msg( "    "+_modelfac.get_errstring() );
		_oerr->add_msg( "    "+_modelfac.get_statmsg() );
		_oerr->add_msg("Can not build a MPM model from the input.\n");
		_oerr->add_msg("Terminating Program.\n");
		_oerr->print();

		report_error(_oerr->get_filename());
		_oerr->close(); delete _oerr; _oerr = 0;
		Istat = -1;
		return mpmptr;
	}
	// <---------------
	std::clock_t endTime = std::clock();
	double elapsedTime = (endTime  - startTime) * 1.0 / CLOCKS_PER_SEC;
	std::cout << "model build time = " << elapsedTime << " seconds" << std::endl;

	return mpmptr;
}// end function (make model)

std::string mpmfactory::statmessage(const int& Istat){
	std:: string msg;
	switch(Istat){
		case -101:
			msg = "WARNING: input filename is empty.";
			break;
		case -102:
			msg = "WARNING: file can not be found.";
			break;
		case -1:
			msg = "WARNING: error happens.";
			break;
		case 0:
			msg = "*** Everything works well ***";
			break;
		default:
			msg = "WARNING: undefined Istat value.";
			break;
	}
	return msg;
}// end function (statmessage)
