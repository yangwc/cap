/*
 * IVmap.h
 *
 *  Created on: May 1, 2014
 *      Author: wenchia
 */

#ifndef IVSTRMAP_H_
#define IVSTRMAP_H_

#include <map>
#include <string>

#include "../GB_Tensor.h"

namespace strkey{

template <class _mapped_val>
class IVmap {
public:
	typedef std::string keytype;
	typedef _mapped_val valtype;
	typedef std::map<keytype, valtype> maptype;
	typedef typename maptype::iterator iterator;

private:
	keytype make_key(const IntGVector& intv){
		std::ostringstream strconverter;
		strconverter<<intv.GetxComp()<<','
					<<intv.GetyComp()<<','
					<<intv.GetzComp();
		return strconverter.str();
	}

public:
	IVmap(){;}
	virtual ~IVmap(){;}

	int get_w(const IntGVector& intv, valtype& oval){
		int Istat = -1;
		iterator it = find(intv);
		if (it != end()){
			oval = it->second;
			Istat = 0;
		}
		return Istat;
	}

	int set(const IntGVector& intv, const valtype& val){
		int Istat = 0;
		keytype key = make_key(intv);
		_map[key] = val;
		return Istat;
	}

private:
	iterator find(const keytype& key){
		return _map.find(key);
	}
	iterator find(const IntGVector& intv){
		keytype key = make_key(intv);
		return _map.find(key);
	}
	iterator begin(){
		return _map.begin();
	}
	iterator end(){
		return _map.end();
	}

	valtype& operator[] (const keytype& key){
		return _map[key];
	}
	valtype& operator[] (const IntGVector& intv){
		keytype key = make_key(intv);
		return _map[key];
	}

	maptype _map;
};

};
#endif /* IVSTRMAP_H_ */
