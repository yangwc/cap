/*
 * IVmap.h
 *
 *  Created on: May 1, 2014
 *      Author: wenchia
 */

#ifndef IVNESTMAP_H_
#define IVNESTMAP_H_

#include <map>
#include <string>
#include <utility>

#include "../GB_Tensor.h"

namespace nestmap{

template <class _mapped_val>
class IVmap {
public:
	typedef _mapped_val valtype;
	typedef typename std::map<int, valtype> nest3;
	typedef typename std::map<int, nest3>   nest2;
	typedef typename std::map<int, nest2>   maptype;
	typedef typename nest3::iterator   nest3itr;
	typedef typename nest2::iterator   nest2itr;
	typedef typename maptype::iterator nest1itr;

private:
	maptype _map;

public:
	IVmap(){;}
	virtual ~IVmap(){;}

	int get_w(const IntGVector& intv, valtype& oval){
		int Istat = -1;
		nest1itr itn1 = _map.find(intv.GetComp(0));
		if( itn1 != _map.end() ){
			nest2& nt2 = itn1->second;
			nest2itr itn2 = nt2.find(intv.GetComp(1));
			if( itn2 != nt2.end() ){
				nest3& nt3 = itn2->second;
				nest3itr itn3 = nt3.find(intv.GetComp(2));
				if( itn3 != nt3.end() ){
					oval = itn3->second;
					Istat = 0;
				} // end nest 3
			} // end nest 2
		} // end nest 1
		return Istat;
	}

	int set(const IntGVector& intv, const valtype& val){
		nest1itr itn1 = _map.find(intv.GetComp(0));
		if( itn1 == _map.end() ){
			nest3 n3;
			n3[ intv.GetComp(2) ] = val;
			nest2 n2;
			n2[ intv.GetComp(1) ] = n3;
			std::pair<int, nest2> p(intv.GetComp(0), n2);
			_map.insert(itn1, p);
			return 0;
		}

		nest2& nt2 = itn1->second;
		nest2itr itn2 = nt2.find(intv.GetComp(1));
		if( itn2 == nt2.end() ){
			nest3 n3;
			n3[ intv.GetComp(2) ] = val;
			std::pair<int, nest3> p(intv.GetComp(1), n3);
			nt2.insert(itn2, p);
			return 0;
		}

		nest3& nt3 = itn2->second;
		nest3itr itn3 = nt3.find(intv.GetComp(2));
		if( itn3 == nt3.end() ){
			std::pair<int, valtype> p( intv.GetComp(2), val);
			nt3.insert(itn3, p);
		}
		else{
			itn3->second = val;
		}
		return 0;
	}

	nest1itr begin(){
		return _map.begin();
	}
	nest1itr end(){
		return _map.end();
	}
	nest1itr find(const valtype& val){
		return _map.find(val);
	}
//	int set(const IntGVector& intv, const valtype& val){
//		typename nest3::iterator itn3;
//		typename nest2::iterator itn2;
//		typename maptype::iterator itn1 = _map.find(intv.GetComp(0));
//		if( itn1 != _map.end() ){
//			nest2& nt2 = itn1->second;
//			itn2 = nt2.find(intv.GetComp(1));
//			if( itn2 != nt2.end() ){
//				nest3& nt3 = itn2->second;
//				itn3 = nt3.find(intv.GetComp(2));
//				if( itn3 != nt3.end() ){
//					itn3->second = val;
//				} // end nest 3
//				if( itn3 == nt3.end() ){
//					std::pair<int, valtype> p( intv.GetComp(2), val);
//					nt3.insert(itn3, p);
//				} // end nest 3 (end)
//			} // end nest 2
//			else{
//				nest3 n3;
//				n3[ intv.GetComp(2) ] = val;
//				std::pair<int, nest3> p(intv.GetComp(1), n3);
//				nt2.insert(itn2, p);
//			} // end nest 2 (end)
//		} // end nest 1
//		else{
//			nest3 n3;
//			n3[ intv.GetComp(2) ] = val;
//			nest2 n2;
//			n2[ intv.GetComp(1) ] = n3;
//			std::pair<int, nest2> p(intv.GetComp(0), n2);
//			_map.insert(itn1, p);
//		} // end nest 1 (end)
//		return 0;
//	}
//private:
//	valtype& operator[] (const IntGVector& intv){
//		typename nest3::iterator itn3;
//		typename nest2::iterator itn2;
//		typename maptype::iterator itn1 = _map.find(intv.GetComp(0));
//		if( itn1 != _map.end() ){
//			nest2& nt2 = itn1->second;
//			itn2 = nt2.find(intv.GetComp(1));
//			if( itn2 != nt2.end() ){
//				nest3& nt3 = itn2->second;
//				itn3 = nt3.find(intv.GetComp(2));
//				if( itn3 == nt3.end() ){
//					valtype newval;
//					std::pair<int, valtype> p(intv.GetComp(2), newval);
//					nt3.insert(itn3, p);
//					itn3--;
//				} // end nest 3 (end)
//			} // end nest 2
//			else{
//				valtype newval;
//				nest3 n3;
//				n3[ intv.GetComp(2) ] = newval;
//				std::pair<int, nest3> p(intv.GetComp(1), n3);
//				nt2.insert(itn2, p);
//				itn2--;
//				itn3 = (itn2->second).begin();
//			} // end nest 2 (end)
//		} // end nest 1
//		else{
//			valtype newval;
//			nest3 n3;
//			n3[ intv.GetComp(2) ] = newval;
//			nest2 n2;
//			n2[ intv.GetComp(1) ] = n3;
//			std::pair<int, nest2> p(intv.GetComp(0), n2);
//			_map.insert(itn1, p);
//			itn1--;
//			itn2 = (itn1->second).begin();
//			itn3 = (itn2->second).begin();
//		} // end nest 1 (end)
//		return itn3->second;
//	}
};

};
#endif /* IVNESTMAP_H_ */
