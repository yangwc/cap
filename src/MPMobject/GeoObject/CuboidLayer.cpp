
#include "CuboidLayer.h"

CuboidLayer::CuboidLayer(){
	_ts.assign(0.0, 5);
}

CuboidLayer::CuboidLayer( const GVector& location, const GVector& normal, const SVec& layers) : Cuboid(location, normal){
	_ts.resize(5); _ts = layers;
}

CuboidLayer::CuboidLayer(const Cuboid& cub, const SVec& layers) : Cuboid(cub){
	_ts.resize(5); _ts = layers;
}

CuboidLayer::CuboidLayer(const CuboidLayer& proto) : Cuboid(proto){
	_ts.assign(proto._ts);
}

bool CuboidLayer::if_covered(const GVector& loc){
	bool icover = this-> if_covered(loc, _rec_maploc);
	return icover;
}// end function

bool CuboidLayer::if_covered(const GVector& loc, GVector& locmap){
	this-> get_mapx(loc, locmap);
	Scalar bd = 1+_tol;
	bool icover = std::abs( locmap.GetxComp() ) < bd;
	if (icover){
		icover = std::abs( locmap.GetyComp() ) < bd;
		if (icover) icover = std::abs( locmap.GetzComp() ) < bd;
	}

//	std::cout<<"Layer ("<<icover<<")"<<std::endl;
//	std::cout<<"xp:"; loc.Print();
//	std::cout<<"xo:"; _loc.Print();
//	std::cout<<"rp:"; locmap.Print();
//	std::cout<<"----------------"<<std::endl;

	return icover;
}

inline void CuboidLayer::get_mapx(const GVector& loc, GVector& locmap){
	GVector r = loc - _loc;
	transform2nts(r);

	/**********************************
	 * layer applied to -n dir has same
	 * thickness as the one in +n dir,
	 * with const. amp. value 1.0
	 **********************************/
	Scalar xx = 0.5*std::max( _size.GetxComp(), 0.0);
	this-> get_mapx( (r.GetxComp() + xx) , _size.GetxComp(), _ts[0], _ts[0], xx);
	locmap.SetxComponent( xx );

	this-> get_mapx( r.GetyComp(), _size.GetyComp(), _ts[1], _ts[2], xx);
	locmap.SetyComponent( xx );

	this-> get_mapx( r.GetzComp(), _size.GetzComp(), _ts[3], _ts[4], xx);
	locmap.SetzComponent( xx );

//	std::cout<<"rp:"; r.Print();
//	std::cout<<"mx:"; locmap.Print();

}

void CuboidLayer::get_mapx( Scalar r, Scalar l, const Scalar& tp, const Scalar& tn, Scalar& xi) const {
	xi = 0;

	if ( l < -_tol ) return;

	l *= 0.5; l = std::max( 0.0, l );
	if(r > 0){
		r -= l;
		if ( r > _tol ) xi = r/tp;
	}
	else{
		r = -r;
		r -= l;
		if ( r > _tol ) xi = -r/tn;
	}
}
