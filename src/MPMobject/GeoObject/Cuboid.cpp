/*
 * Cuboid.cpp
 */
#include "Cuboid.h"
/***********************************************
 *	Cuboid
 ***********************************************/
Cuboid::Cuboid( const GVector& location, const GVector& normal) {
	/*
	 * for orthogonal global coordinate system (fixed)
	 *   ei = (1.0 , 0.0, 0.0);
	 *   ej = (0.0 , 1.0, 0.0);
	 *   ek = (0.0 , 0.0, 1.0);
	 */
	this-> reset_data( location, normal);
}// _end construction

Cuboid::Cuboid(const Cuboid& protot) {
	this-> reset_data( protot._loc, protot._en, protot._size);
}// _end construction

int Cuboid::reset_data( const GVector& location, const GVector& normal){
	return this-> reset_data( location, normal, GVector(0,-1,-1));
}

int Cuboid::reset_data( const GVector& location, const GVector& normal, const GVector& size){
	_loc  = location;
	_size = size;
	_en   = normal;
	_en  /= _en.GetNorm();
	_tol  = GLOBAL_TOLERANCE_ERROR;
	build_localsystem();
	return 0;
}

bool Cuboid::if_covered(const GVector& ptloc){
	/*
	 * check if the giv_en coordinate in the boundary domain (a rectangular cuboid).
	 * -------------------------------
	 * RETURN: bool (if covered)
	 */
	GVector& rnode = _rec_maploc;
	rnode = ptloc - _loc;
	transform2nts(rnode);

	bool icovered;
	icovered = 	( rnode.GetxComp() <= _tol );
	if (icovered && _size.GetxComp() > -_tol)
			{icovered = ( rnode.GetxComp() >= -_size.GetxComp()-_tol );}
	if (icovered && _size.GetyComp() > -_tol)
			{ icovered = ( std::abs(rnode.GetyComp()) <= 0.5*_size.GetyComp()+_tol );}
	if (icovered && _size.GetzComp() > -_tol)
			{ icovered = ( std::abs(rnode.GetzComp()) <= 0.5*_size.GetzComp()+_tol );}

//	std::cout<<"Origin ("<<icovered<<")"<<std::endl;
//	std::cout<<"xp:"; ptloc.Print();
//	std::cout<<"xo:"; _loc.Print();
//	std::cout<<"rp:"; rnode.Print();
//	std::cout<<"----------------"<<std::endl;

	return icovered;
}// _end function

int Cuboid::reset_globalsystem(const GVector& gei,const GVector& gej,const GVector& gek){
	//*** check input vector
	double dotvalue;
	dotvalue = gei&&gei;
	if (dotvalue < _tol){return -1;}
	dotvalue = gej&&gej;
	if (dotvalue < _tol){return -1;}
	dotvalue = gek&&gek;
	if (dotvalue < _tol){return -1;}
	dotvalue = gei&&gej;
	if (std::abs(dotvalue) > _tol){return -1;}
	dotvalue = gei&&gek;
	if (std::abs(dotvalue) > _tol){return -1;}

	//*** normalize
	GVector eit, ejt, ekt;
	eit = gei; ejt = gej; ekt = gek;
	eit /= eit.GetNorm();
	ejt /= ejt.GetNorm();
	ekt /= ekt.GetNorm();

	//*** transform curr_ent coordinate system to the new global one
	Scalar ival, jval, kval;
	ival = eit&&_en; jval = ejt&&_en; kval = ekt&&_en;
	_en.SetComponents(ival, jval, kval);
	ival = eit&&_et; jval = ejt&&_et; kval = ekt&&_et;
	_et.SetComponents(ival, jval, kval);
	ival = eit&&_es; jval = ejt&&_es; kval = ekt&&_es;
	_es.SetComponents(ival, jval, kval);

	//*** build transformation matrix
	build_systransmatrix();
	return 0;
}// _end function

void Cuboid::build_localsystem(){
	/*
	 * This function s_ets the ori_entation of the boundary object. This formulation will lead to a
	 * local orthogonal coordinate system (n, t, s) with giv_en n and arbitrary t & s.
	 */

	// *** g_ets the vector s
	GVector ei(1,0,0);
	_es  = ei^_en;
	Scalar snorm = _es.GetNorm();
	if( snorm < _tol ){ // *** th_en normal vector is parallel with the global i-vector.
		if (_en.GetxComp() > 0 ) {
			_en.SetComponents( 1.0, 0.0, 0.0);
			_et.SetComponents( 0.0, 1.0, 0.0);
			_es.SetComponents( 0.0, 0.0, 1.0);
		}
		else{
			_en.SetComponents(-1.0,  0.0, 0.0);
			_et.SetComponents( 0.0, -1.0, 0.0);
			_es.SetComponents( 0.0,  0.0, 1.0);
		}// _end if
	}// (if parallel)
	else{
		_es /= snorm;
		_et  = _es^_en;
			/* t = sxn;
			 * t.t = t.(sxn) = s.(nxt) = s.( nx(sxn) ) = (s.s)(n.n)-(s.n)(n.s)
			 * because s.s = 1; n.n = 1; s.n= 0;
			 * so t.t = 1
			 */
	}// _end if

	//*** build transformation matrix
	build_systransmatrix();
}// _end function

void Cuboid::build_systransmatrix(){
	/*
	 * build transformation matrix b_etwe_en global system (i, j, k) and local system (n, t, s)
	 */

	// *** used to map a vector in local coordinat_es to global coordinat_es
	_map2ijk.SetComponents(	(_en.GetxComp()),(_et.GetxComp()),(_es.GetxComp()),
							(_en.GetyComp()),(_et.GetyComp()),(_es.GetyComp()),
							(_en.GetzComp()),(_et.GetzComp()),(_es.GetzComp()) );
	//	_map2ijk.SetComponents(	(ei&&_en),(ei&&_et),(ei&&_es),
	//							(ej&&_en),(ej&&_et),(ej&&_es),
	//							(ek&&_en),(ek&&_et),(ek&&_es) );

	// *** used to map a global vector to the local coordinat_es
	_map2nts = _map2ijk.GetTranspose();
}// _end function
