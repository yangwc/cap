/*
 * Zone.h
 */
#ifndef ZONE_H_
#define ZONE_H_
#include "../../GB_Tensor.h"


class Zone {
public:
	virtual ~Zone(){;}
	virtual bool if_covered(const GVector& ptloc) = 0;
	virtual Zone* get_new_object() const = 0;
	virtual const GVector& get_latest_maploc() const = 0;
	virtual const GVector& get_latest_normal() const = 0;
};

#endif /* ZONE_H_ */
