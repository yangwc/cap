
#include "SurfCubLayer.h"
#include "../../Function/Function.h"

SurfCubLayer::SurfCubLayer(){
	_fptr_surf = 0;
}

SurfCubLayer::SurfCubLayer( const GVector& location, const GVector& normal, const SVec& layers, function* surffunc) : CuboidLayer(location, normal, layers){
	_fptr_surf = surffunc;
}

SurfCubLayer::SurfCubLayer(const Cuboid& cub, const SVec& layers, function* surffunc) : CuboidLayer(cub, layers){
	_fptr_surf = surffunc;
}

SurfCubLayer::SurfCubLayer(const SurfCubLayer& proto) : CuboidLayer(proto){
	_fptr_surf = proto._fptr_surf-> get_new_obj();
}

inline void SurfCubLayer::get_mapx(const GVector& loc, GVector& locmap){
	GVector r = loc - _loc;
	this-> transform2nts(r);

	/**********************************
	 * get projection
	 **********************************/
	Scalar rn, nx, ny;
	int Istat = _fptr_surf-> get_projection( r.GetyComp(), r.GetxComp(), rn, ny, nx);
	_rec_en.SetComponents(nx,ny,0);
	this-> transform2ijk(_rec_en);
	if( (_rec_en && _en) < _tol){
		rn = -rn;
		_rec_en *= -1;
	}
	if (Istat != 0){
		std::cout<<"WARNING: check the surface normal."<<std::endl;
		std::cout<<"    @"<<loc.GetComp(0)<<","<<loc.GetComp(1)<<","<<loc.GetComp(2)<<std::endl;
	}
//	std::cout<<"x: "; loc.Print();
//	std::cout<<"r: "; r.Print();
//	std::cout<<"n: "; _rec_en.Print();
//	std::cout<<"rn:"<<rn<<std::endl;


	/**********************************
	 * layer applied to -n dir has same
	 * thickness as the one in +n dir,
	 * with const. amp. value 1.0
	 **********************************/

	Scalar xx = 0.5*std::max( _size.GetxComp(), 0.0);
	CuboidLayer::get_mapx( ( rn + xx) , _size.GetxComp(), _ts[0], _ts[0], xx);
	locmap.SetxComponent( xx );

	CuboidLayer::get_mapx( r.GetyComp(), _size.GetyComp(), _ts[1], _ts[2], xx);
	locmap.SetyComponent( xx );

	CuboidLayer::get_mapx( r.GetzComp(), _size.GetzComp(), _ts[3], _ts[4], xx);
	locmap.SetzComponent( xx );

//	std::cout<<"d: "; _size.Print();
//	std::cout<<"xi:"; locmap.Print();
}

//inline int SurfCubLayer::get_projection(const GVector& ro, GVector& n, Scalar& rn, unsigned int Nmax){
//	bool icovered = true;
//	/**********************************
//	 * find projection (x, y) of
//	 * (xn, xt1) on the surface defined
//	 * by function "_fptr_surf"
//	 **********************************/
//	int Icount = 1;
//
//	Scalar xo = ro.GetyComp();
//	Scalar yo = ro.GetxComp();
//	Scalar x  = xo;
//	Scalar y  = _fptr_surf-> get_funcvalue(x);
//	Scalar dy = _fptr_surf-> get_devfuncvalue(x);
//	Scalar Vx = 0;
//	Scalar Vy = yo - y;
//
//	if( std::abs(Vy) <= _tol ){
//		rn = 0;
//		n.SetComponents(1, -dy, 0);
//		n /= n.GetNorm();
//	}
//	else{
//		Scalar xstep = std::max( _ts[0], 0.01*_size.GetNorm() );
//		Scalar VT = Vy*dy, VTo = VT; // (V . T) = 0 if V = N
//		if (VT < 0) xstep *= -1;
//		while( std::abs(VT) > _tol && Icount != Nmax){
//			x  += xstep;
//			y   = _fptr_surf-> get_funcvalue(x);
//			dy  = _fptr_surf-> get_devfuncvalue(x);
//			Vx  = xo - x;
//			Vy  = yo - y;
//			VT  = Vx + Vy*dy;
//			if (VT*VTo < 0) xstep *= -0.3333;
//			VTo = VT;
//			++Icount;
//		}
//
//		if (Icount == Nmax){ Icount = -1; }
//
//		n.SetComponents(Vy, Vx, 0);
//		rn = n.GetNorm();
//		if (rn < _tol){
//			std::cout<<"WARNING: unexpected condition: rn < tol @ get_projection()"<<std::endl;
//			rn = 0;
//			n.SetComponents(1, -dy, 0);
//			n /= n.GetNorm();
//		}
//		else{
//			n /= rn;
//		}
//	}
//	return Icount;
//}// end function
