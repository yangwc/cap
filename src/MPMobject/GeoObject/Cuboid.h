/*
 * Cuboid.h
 */
#ifndef CUBOID_H_
#define CUBOID_H_

#include "Zone.h"

/***********************************************
 *	Cuboid
 ***********************************************/
class Cuboid : public Zone {
public:
	Cuboid(){ _size.SetComponents(0,-1,-1); _tol = GLOBAL_TOLERANCE_ERROR; }
	Cuboid(const GVector& location, const GVector& normal);
	Cuboid(const Cuboid& proto);
	virtual ~Cuboid(){;}

	int reset_data( const GVector& location, const GVector& normal);
	int reset_data( const GVector& location, const GVector& normal, const GVector& size);
	int reset_globalsystem(const GVector& gei,const GVector& gej,const GVector& gek);

	bool if_covered(const GVector& ptloc);
	Zone* get_new_object() const { return new Cuboid(*this); }
	Cuboid* get_this_new_object() const { return new Cuboid(*this); }

	inline void set_size(const GVector& size){_size = size;}
	inline void set_location(const GVector& location){_loc = location;}
	inline void set_tolerance(const Scalar& tol){_tol = tol;}
	inline void set_normal(const GVector& normalin){_en = normalin; _en /= _en.GetNorm(); this-> build_localsystem();}

	// Builds the n,s,t vector
	void build_localsystem();
	// Builds transformation matrix for global and local system
	void build_systransmatrix();
	// Maps a vector to the local coordinate system
	inline void transform2nts(GVector& vec){vec = _map2nts*vec;}
	// Maps a vector to the global coordinate system
	inline void transform2ijk(GVector& vec){vec = _map2ijk*vec;}

	inline const GVector& get_normal() const {return _en;}
	inline const GVector& get_et() const {return _et;}
	inline const GVector& get_es() const {return _es;}
	inline const GVector& get_location() const {return _loc;}
	inline const GVector& get_size() const {return _size;}
	inline const Scalar& get_tolerance() const {return _tol;}

	inline const GVector& get_latest_maploc() const { return _rec_maploc; }
	virtual const GVector& get_latest_normal() const { return _en; }

protected:
//	int myid;
	Scalar _tol;
	GVector _loc, _size, _rec_maploc;
	GVector _en, _es, _et;	// the local coordinate system (n, t, s)
	GTensor _map2ijk, _map2nts; // transformation matrices
};

#endif /* CUBOID_H_ */
