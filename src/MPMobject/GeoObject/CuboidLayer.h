/*
 * Cuboid.h
 */
#ifndef CUBOIDLAYER_H_
#define CUBOIDLAYER_H_

#include <vector>
#include "Cuboid.h"

class CuboidLayer : public Cuboid {
protected:
	typedef std::vector<Scalar> SVec;

public:
	CuboidLayer();
	CuboidLayer(const GVector& location, const GVector& normal, const SVec& layers);
	CuboidLayer(const Cuboid& cub, const SVec& layers);
	CuboidLayer(const CuboidLayer& proto);
	virtual ~CuboidLayer(){;}

	virtual bool if_covered(const GVector& loc);
	virtual Zone* get_new_object() const { return new CuboidLayer(*this); }

protected:
	virtual bool if_covered(const GVector& loc, GVector& locmap);
	virtual void get_mapx(const GVector& loc, GVector& locmap);

	virtual void get_mapx( Scalar r, Scalar l, const Scalar& tp, const Scalar& tn, Scalar& xi) const ;

	MathVec<Scalar> _ts;
};



#endif // CUBOIDLAYER_H_
