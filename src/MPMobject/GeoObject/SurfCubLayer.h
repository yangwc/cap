/*
 * SurfCubLayer.h
 */
#ifndef SURFCUBLAYER_H_
#define SURFCUBLAYER_H_

#include "CuboidLayer.h"
class function;
/***********************************************
 *	SurfCubLayer
 ***********************************************/
class SurfCubLayer : public CuboidLayer {
protected:
	typedef std::vector<Scalar> SVec;
public:
	SurfCubLayer();
	SurfCubLayer(const GVector& location, const GVector& normal, const SVec& layers, function* surffunc);
	SurfCubLayer(const Cuboid& cub, const SVec& layers, function* surffunc);
	SurfCubLayer(const SurfCubLayer& proto);
	virtual ~SurfCubLayer(){delete _fptr_surf;}

	Zone* get_new_object() const { return new SurfCubLayer(*this); }

	inline const GVector& get_latest_normal() const { return _rec_en; }

protected:
	void get_mapx(const GVector& loc, GVector& locmap);

//	int get_projection(const GVector& ro, GVector& n, Scalar& rn, unsigned int Nmax);

	function* _fptr_surf;
	GVector _rec_en;
};

#endif /* SURFCUBLAYER_H_ */
