/*
 * BCALG.h
 */
#ifndef BCALGORITHM_H_
#define BCALGORITHM_H_

#include "../../GB_Tensor.h"
#include "../../Function/Function.h"

class _mpmnode_base;

/***********************************************
 *	BCalgorithm (virtual class)
 ***********************************************/
class BCalgorithm {
public:
	BCalgorithm(){myid = -1; mytol =GLOBAL_TOLERANCE_PRECISION; _ln_nd = 0;}
	BCalgorithm(const int& id){myid = id; mytol =GLOBAL_TOLERANCE_PRECISION; _ln_nd = 0;}
	BCalgorithm(const int& id, const Scalar& tol){myid = id; mytol =tol; _ln_nd = 0;}
	virtual ~BCalgorithm(){;}

	void assign_node(const _mpmnode_base* nd){ _ln_nd = nd; }
	// *** interface functions
	virtual const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd)=0;
	virtual BCalgorithm& get_algrithm() = 0;
	virtual BCalgorithm* get_new_algrithm() = 0;
	virtual int reset_normal(const GVector& en) = 0;

	// *** implemented common functions
	virtual int get_ID() const {return myid;}
	virtual const Scalar& get_tol() const {return mytol;}

protected:
	int myid;
	Scalar mytol;
	const _mpmnode_base* _ln_nd;
};

#endif /* BCALGORITHM_H_ */
