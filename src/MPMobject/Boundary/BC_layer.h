/*
 * Boundary.h
 */
#ifndef BCLAYER_H_
#define BCLAYER_H_

#include "BC.h"
#include "BCA_layer.h"
#include "../../Function/Function.h"

class BClayer : public BC {
public:
	BClayer(){
		_bc = 0;
		_lfptr_n   = new FNconstant(-1, 0.0);
		_lfptr_t1p = new FNconstant(-1, 0.0);
		_lfptr_t1n = new FNconstant(-1, 0.0);
		_lfptr_t2p = new FNconstant(-1, 0.0);
		_lfptr_t2n = new FNconstant(-1, 0.0);
	}

	BClayer(const BClayer& proto){
		_ownerID = proto._ownerID;

		_bc  = 0; if (proto._bc != 0) _bc  = proto._bc-> get_new_boundary();

		/*** linke of zone object in _bc ***/
		_zone = 0; if (proto._zone != 0){
			_zone = proto._zone-> get_new_object();
			if (_bc != 0) _bc->assign_zone( _zone );
		}

		_lfptr_n   = proto._lfptr_n-> get_new_obj();
		_lfptr_t1p = proto._lfptr_t1p-> get_new_obj();
		_lfptr_t1n = proto._lfptr_t1n-> get_new_obj();
		_lfptr_t2p = proto._lfptr_t2p-> get_new_obj();
		_lfptr_t2n = proto._lfptr_t2n-> get_new_obj();
	}

	BClayer(BC* bc, Zone* zone, std::vector<function*> funcs){
		_ownerID = bc->get_ownerID();

		_bc  = bc;

		/*** linke of zone object in _bc ***/
		_zone = zone;
		if (_bc != 0){
			_bc->assign_zone(_zone);
		}

		_lfptr_n   = funcs[0];
		_lfptr_t1p = funcs[1];
		_lfptr_t1n = funcs[2];
		_lfptr_t2p = funcs[3];
		_lfptr_t2n = funcs[4];
	}

	virtual ~BClayer(){
		_zone = 0; // linke of zone object in _bc
		delete _bc;
		delete _lfptr_n;
		delete _lfptr_t1p;
		delete _lfptr_t1n;
		delete _lfptr_t2p;
		delete _lfptr_t2n;
	}

	inline void assign_zone(Zone* zone){ delete _zone; _zone = zone; if (_bc != 0) _bc->assign_zone( _zone ); }
	inline void assign_boundary(BC* bc){ delete _bc; _bc = bc; _bc-> assign_zone( _zone ); }

	inline BC* get_new_boundary() const { return new BClayer(*this); }

	inline BCalgorithm* refresh_new_node( const node* ln_nd){

		BCalgorithm* alg =  _bc-> refresh_new_node(ln_nd);

		if( alg != 0 ){
			BCampf* algamp = new BCampf( alg->get_ID() , alg);

			algamp-> assign_node( ln_nd );
			algamp-> reset_normal( _zone-> get_latest_normal() );
			algamp-> set_amplification( this-> get_amp_coeff() );
			alg = algamp;
			algamp = 0;
		}
		return alg;
	}
	inline int refresh_new_cell( const cell* ln_ce){ return _bc->refresh_new_cell(ln_ce); }
	inline int refresh_del_node( const node* ln_nd){ return _bc->refresh_del_node(ln_nd); }
	inline int refresh_del_cell( const cell* ln_ce){ return _bc->refresh_del_cell(ln_ce); }

protected:
	inline Scalar get_amp_coeff(){
		const GVector& maploc = _zone-> get_latest_maploc();
		Scalar coef = 1, xi = maploc.GetxComp();
		if (xi > 0) coef *= _lfptr_n-> get_funcvalue(xi);

		xi = maploc.GetyComp();
		if ( xi > 0 ){
			coef *= _lfptr_t2p-> get_funcvalue(xi);
		}
		else if ( xi < 0 ){
			coef *= _lfptr_t1n-> get_funcvalue(-xi);
		}

		xi = maploc.GetzComp();
		if ( xi > 0 ){
			coef *= _lfptr_t2p-> get_funcvalue(xi);
		}
		else if ( xi < 0 ){
			coef *= _lfptr_t2n-> get_funcvalue(-xi);
		}
		coef = std::min( std::max( coef, 0.0), 1.0);
		return coef;
	}

	function *_lfptr_n, *_lfptr_t1p, *_lfptr_t1n, *_lfptr_t2p, *_lfptr_t2n;
	BC* _bc;
};

#endif /* BCLAYER_H_ */
