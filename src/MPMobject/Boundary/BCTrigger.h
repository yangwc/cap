/**************************************************
 * BCTrigger.h
 * 	   - BCTrigger()
 * ------------------------------------------------
 *  + call BCalgorithm to apply boundary condition
 *    on nodes.
 *  + can handle the order of applying bc here.
 * ================================================
 * Created by: YWC @ CEE, UW (FEB 05, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.02.10)
 **************************************************/

#ifndef BCTRIGGER_H_
#define BCTRIGGER_H_

#include <vector>
#include "BCAlg.h"

class BCTrigger{
private:
	typedef std::vector<BCalgorithm*> BCVec;

public:
	BCTrigger(){;}
	~BCTrigger(){;}

	inline void link_boundary(BCalgorithm* bc){ _lns_bc.push_back( bc ); }

	inline GVector apply_boundary(const Scalar& tnow, const Scalar& dt){
		GVector fbc;
		this-> apply_boundary(tnow, dt, fbc);
		return fbc;
	}

	inline void apply_boundary(const Scalar& tnow, const Scalar& dt, GVector& fbc){
		fbc.SetZeros();
		for(std::size_t Ibc=0; Ibc !=_lns_bc.size(); ++Ibc){
			fbc += _lns_bc[Ibc] -> get_rforce( tnow, dt, fbc);
		}
	}

	inline int get_Nboundary(){ return _lns_bc.size(); }

protected:
	std::vector<BCalgorithm*> _lns_bc;
};


#endif /* BCTRIGGER_H_ */
