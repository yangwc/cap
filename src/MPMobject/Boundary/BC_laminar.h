/**************************************************
 * LaminarFlow.h
 * 	   - LaminarFlow()
 * ------------------------------------------------
 *  +
 * ================================================
 * Created by: YWC @ CEE, UW (MAR 09, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.03.09)
 **************************************************/

#ifndef SRC_MPMOBJECT_BOUNDARY_LAMINARFLOW_H_
#define SRC_MPMOBJECT_BOUNDARY_LAMINARFLOW_H_

#include "BC.h"
#include "BCA_laminar.h"

#include <vector>
#include "../../IVmap/IVmap.h"

class LaminarFlow : public BC {
public:
	LaminarFlow( int Idir, int Ind, int Iflow){ _Idir = Idir; _Ind = Ind; _Iflow = Iflow;}
	LaminarFlow(const LaminarFlow& proto){
		 _Idir  = proto._Idir;
		 _Ind   = proto._Ind;
		 _Iflow = proto._Iflow;
	}
	virtual ~LaminarFlow(){
		for(std::size_t Io=0; Io != _objs_alg.size(); ++Io) delete _objs_alg[Io];
	}

	virtual BC* get_new_boundary() const { return new LaminarFlow(*this); }

	virtual BCalgorithm* refresh_new_node( const node* ln_nd){
		BClaminar* alg = 0;
		IntGVector ndid = ln_nd->get_id();

		int key = ndid[_Idir], Itarget = _Ind-_Iflow;
		if(key == _Ind){
			alg = this-> get_algorithm(ndid);
			alg-> assign_node( ln_nd );
		}
		else if( key == Itarget){
			ndid.SetComp( _Idir, _Ind);
			alg = this-> get_algorithm(ndid);
			alg->assign_targetnode( ln_nd );
			alg = 0;
		}
		return alg;
	}

	virtual int refresh_new_cell( const cell* ln_ce){return 0;}
	virtual int refresh_del_node( const node* ln_nd){return 0;}
	virtual int refresh_del_cell( const cell* ln_ce){return 0;}

protected:
	inline BClaminar* get_algorithm(const IntGVector& ndid){
		BClaminar* alg = 0;
		unsigned int Ialg;
		int Istat = _addmap_alg.get_w(ndid, Ialg);
		if (Istat == 0){
			alg = _objs_alg[Ialg];
		}
		else{
			Ialg = _objs_alg.size();
			alg  = new BClaminar( _ownerID, _Idir, _Iflow);
			_objs_alg.push_back(alg);
			_addmap_alg.set(ndid, Ialg);
		}
		return alg;
	}

	int _Idir, _Ind, _Iflow;
	IVmap<unsigned int> _addmap_alg;
	std::vector<BClaminar*> _objs_alg;
};



#endif /* SRC_MPMOBJECT_BOUNDARY_LAMINARFLOW_H_ */
