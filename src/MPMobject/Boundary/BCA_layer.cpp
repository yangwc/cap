/*
 * BCalgorithm.cpp
 */
#include "BCA_layer.h"
#include "../GridNode/_mpmnode_base.h"

/***********************************************
 *	BCampf : BCalgorithm
 ***********************************************/
BCampf::BCampf(const int& id, BCalgorithm* algptr) : BCalgorithm(id){
	mytol = algptr->get_tol();
	myalgptr = algptr;
	myampcoef= 0;
}

BCampf::BCampf(const BCampf& algo){
	myid = algo.get_ID();
	mytol =algo.get_tol();
	myalgptr = algo.get_new_kernel();
	myampcoef= algo.get_ampcoef();
}

const GVector& BCampf::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	_tempvec  = myalgptr->get_rforce( tn, dt, fadd);
	_tempvec *= myampcoef;
//	std::cout<<myid<<": "<<myampcoef<<std::endl;
	return _tempvec;
}
