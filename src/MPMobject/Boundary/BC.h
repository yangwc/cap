/*
 * Boundary.h
 */
#ifndef BOUNDARYCONDITION_H_
#define BOUNDARYCONDITION_H_

#include "../_ownership.h"

#include "../../GB_Tensor.h"
#include "../GridNode/_mpmnode_base.h"
#include "../GridElem/_elem_base.h"

#include "../GeoObject/Zone.h"

class Particle;

class BC : public _ownership {
protected:
	typedef _mpmnode_base node;
	typedef _particle_adaptor<Particle> cell;

public:
	BC(){ _zone = 0; }
	virtual ~BC(){delete _zone;}

	inline void assign_zone(Zone* zone){ delete _zone; _zone = zone; }

	virtual BC* get_new_boundary() const = 0;

	virtual BCalgorithm* refresh_new_node( const node* ln_nd) = 0;
	virtual int refresh_new_cell( const cell* ln_ce) = 0;
	virtual int refresh_del_node( const node* ln_nd) = 0;
	virtual int refresh_del_cell( const cell* ln_ce) = 0;

protected:
	Zone* _zone;
};

#endif /* BOUNDARYCONDITION_H_ */
