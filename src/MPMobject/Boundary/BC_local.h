/*
 * Boundary.h
 */
#ifndef BOUNDARYLOCAL_H_
#define BOUNDARYLOCAL_H_

#include "BC.h"
#include "BCAlg.h"

class BClocal : public BC {

public:
	BClocal(){ _algproto = 0; }
	BClocal(const BClocal& bc){
		_ownerID = bc._ownerID;
		_algproto  = 0; if (bc._algproto != 0)  _algproto  = bc._algproto -> get_new_algrithm();
		_zone = 0; if (bc._zone != 0) _zone = bc._zone-> get_new_object();
	}
	virtual ~BClocal(){
		delete _algproto;
		for(std::size_t Io =0; Io != _objs_alg.size(); ++Io) delete _objs_alg[Io];
	}

	inline void assign_algorithm(BCalgorithm* alg){ delete _algproto; _algproto = alg; }

	inline BC* get_new_boundary() const { return new BClocal(*this); }

	inline BCalgorithm* refresh_new_node( const node* ln_nd){
		BCalgorithm* alg = 0;

		if(_algproto == 0 ||
				_zone == 0){
			return alg;
		}

		if( _zone-> if_covered( ln_nd->get_coord() ) ){
			alg = _algproto-> get_new_algrithm();
			alg-> assign_node( ln_nd );

			_objs_alg.push_back(alg); // maybe better to go local
		}

		return alg;
	}

	inline int refresh_new_cell( const cell* ln_ce){ return 0; }
	inline int refresh_del_node( const node* ln_nd){ return 0; }
	inline int refresh_del_cell( const cell* ln_ce){ return 0; }

protected:
	BCalgorithm* _algproto;
	std::vector<BCalgorithm*> _objs_alg;
};

#endif /* BOUNDARYLOCAL_H_ */
