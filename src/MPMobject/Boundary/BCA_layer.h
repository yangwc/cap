/*
 * BCALG.h
 */
#ifndef BCALGORITHMLAYER_H_
#define BCALGORITHMLAYER_H_

#include "BCAlg.h"

/***********************************************
 *	BCampf : BCalgorithm
 ***********************************************/
class BCampf: public BCalgorithm {
public:
	BCampf() : BCalgorithm(-3) { myalgptr=0; myampcoef=0; }
	BCampf(const int& id, BCalgorithm* algptr);
	BCampf(const BCampf& algo);
	~BCampf(){delete myalgptr; myalgptr=0;}

	// *** interface functions
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCampf(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCampf(*this);}
	inline int reset_normal(const GVector& en) { return myalgptr->reset_normal(en); }

	void set_amplification(const Scalar& amp){if(amp>0){this->myampcoef=amp;}}
	const Scalar& get_ampcoef() const {return this->myampcoef;}
	BCalgorithm* get_new_kernel() const {return this->myalgptr->get_new_algrithm();}

protected:
	GVector _tempvec;
	Scalar myampcoef;
	BCalgorithm* myalgptr;
};

#endif /* BCALGORITHMLAYER_H_ */
