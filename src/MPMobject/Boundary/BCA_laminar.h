/*
 * BCALG.h
 */
#ifndef BCALGORITHMLAMINAR_H_
#define BCALGORITHMLAMINAR_H_

#include "BCAlg.h"

/***********************************************
 *	BClaminar : BCalgorithm
 ***********************************************/
class BClaminar: public BCalgorithm {
public:
	BClaminar() : BCalgorithm(-2) {_ln_tgnd = 0; _Ifdir = _Idir = 0;}
	BClaminar(int id, unsigned int Idir, int Ifdir): BCalgorithm(id) { _Idir = Idir; _Ifdir = Ifdir; _ln_tgnd = 0;}
	BClaminar(const BClaminar& algo);
	~BClaminar(){;}

	inline void assign_targetnode(const _mpmnode_base* nd){ _ln_tgnd = nd; }

	// *** interface functions
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BClaminar(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BClaminar(*this);}

	inline int reset_normal(const GVector& en) { return 0; }

protected:
	GVector _tempvec1;
	int _Idir, _Ifdir;
	const _mpmnode_base* _ln_tgnd;
};

#endif /* BCALGORITHMLAMINAR_H_ */
