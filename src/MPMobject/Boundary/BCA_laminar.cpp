/*
 * BCalgorithm.cpp
 */
#include "BCA_laminar.h"
#include "../GridNode/_mpmnode_base.h"

/***********************************************
 *	BClaminar : BCalgorithm
 ***********************************************/
BClaminar::BClaminar(const BClaminar& algo){
	myid     = algo.myid;
	mytol    = algo.mytol;
	_Idir    = algo._Idir;
	_Ifdir   = algo._Ifdir;
	_ln_tgnd = 0;
}

const GVector& BClaminar::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	GVector& rftot = _tempvec1;
	rftot.SetZeros();

	if ( _ln_tgnd != 0 ){
		Scalar mnb = _ln_tgnd-> get_mass();
		if ( mnb > this-> mytol ){
			Scalar m  = _ln_nd-> get_mass();
			Scalar dp = _ln_tgnd->get_momt()[_Idir];
			if( dp*_Ifdir > 0){
				dp /= mnb;
				dp *= m;
				dp -= _ln_nd-> get_momt()[_Idir];
			}
			else{
				dp = -_ln_nd-> get_momt()[_Idir];
			}
			Scalar f = -_ln_nd-> get_fint()[_Idir];
			f += dp/dt;
			rftot.SetComp(_Idir, f);
		}
	}
	return rftot;
}// end function
