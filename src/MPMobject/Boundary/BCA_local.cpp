/*
 * BCalgorithm.cpp
 */
#include "BCA_local.h"
#include "../GridNode/_mpmnode_base.h"


/***********************************************
 *	BCfixed : BCalgorithm
 ***********************************************/
const GVector& BCfixed::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	GVector& rforce = tempvec;

	rforce  = _ln_nd-> get_momt()/(-dt);
	rforce -= _ln_nd-> get_fint();
	rforce -= _ln_nd-> get_fext();
	rforce -= fadd;
	return rforce;
}

/***********************************************
 *	BCplanar : BCalgorithm
 ***********************************************/
const GVector& BCplanar::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	GVector& rftot = tempvec;
	Scalar& momnamp= tempval;
	rftot.SetZeros();

	//*** check if moving against the boundary
	momnamp = _ln_nd-> get_momt() && en;
	if(momnamp < mytol){
		rftot = (-momnamp/dt)*en;

		//*** check if pushing the boundary
		Scalar& fnamp= tempval;
		fnamp = en && (_ln_nd-> get_fint() + _ln_nd-> get_fext() + fadd );
		if(fnamp < mytol){ rftot += (-fnamp)*en; }
	}// end if (contact);
	return rftot;
}// end function

const GVector& BCplanarPre::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	GVector& rftot = tempvec;
	Scalar& fnamp= tempval;

	//*** check if moving against the boundary
	tempvec  = _ln_nd-> get_momt();
	tempvec /= dt;
	tempvec += _ln_nd-> get_fint();
	tempvec += _ln_nd-> get_fext();
	tempvec += fadd;
	fnamp = tempvec && en;
	if(fnamp < mytol){ rftot = (-fnamp)*en; }
	else{ rftot.SetZeros(); }

	return rftot;
}// end function

/***********************************************
 *	BCplanfri : BCalgorithm
 ***********************************************/
const GVector& BCplanwfri::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	const GVector& momentum = _ln_nd-> get_momt();
	GVector force = _ln_nd-> get_fint();
	force += _ln_nd-> get_fext();
	force += fadd;
	GVector& rftot = tempvec3;
	rftot.SetZeros();

	//*** check if moving against the boundary
	Scalar momnamp = momentum && en;
	if(momnamp < mytol){
		if(momnamp < -mytol){ rftot = (-momnamp/dt)*en; }

		//*** check if pushing the boundary
		Scalar fnamp = en && force;
		fnamp = -fnamp;
		if(fnamp > mytol){
			GVector& rf = tempvec2;
			rf.SetZeros();

			//*** normal reaction force
			rf = (fnamp)*en;

			//*** Coulomb friction force
				GVector nfr;
				Scalar  ff = 0, ffmax = fnamp * _mu;

				GVector pt= momentum - momnamp*en;
				Scalar ptamp = pt.GetNorm();
				//*** Kinetic
				if (ptamp > mytol){
					nfr = pt/ptamp;
					ff  = ptamp/dt;
					if(ff < ffmax){	ff += force && nfr; }
				}

				//*** Static
				else{
					GVector ft = force + rf;
					Scalar ftamp = ft.GetNorm();
					if (ftamp > mytol){
						nfr = ft/ftamp;
						ff  = ftamp;
					}
				}

				//*** set ceiling
				if(ff > ffmax){	ff = ffmax; }

				//*** add friction force to reaction force
				rf += (-ff)*nfr;
				rftot += rf;
		}
	}// end if (contact)
	return rftot;
}// end function

/***********************************************
 *	BCfreecut : BCalgorithm
 ***********************************************/
const GVector& BCfreecut::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	//*** at any free cut boundary, sum of inner force is zero
	GVector& rftot = tempvec;
	rftot.SetZeros();
	Scalar& fnamp=tempval;
	fnamp = en && _ln_nd-> get_fint();
	rftot = (-fnamp)*en;

	return rftot;
}// end function

/***********************************************
 *	BCvel : BCalgorithm
 ***********************************************/
BCvel::BCvel(BCvel& algo){
	myid  = algo.get_ID();
	mytol = algo.get_tol();
	en    = algo.get_normal();
	funcptr = algo.get_new_function();
}

const GVector& BCvel::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	const Scalar& mass = _ln_nd-> get_mass();
	/*
	 * Not necessary. Temporary error check, just in case.
	 * Can be deleted to save tiny percentage of analysis time.
	 */
	if (mass < 0){
		std::cout<<"ERROR: negative mass."<<std::endl;
		tempvec.SetZeros();
		return tempvec;
	}

	this-> get_current_movement_w(tn, dt, tempvec);
	Scalar mn = mass*tempvec.GetComp(1);
	Scalar fn = mass*tempvec.GetComp(2);

	GVector& rftot = tempvec;
	Scalar& momnamp= tempval;
	rftot.SetZeros();

	momnamp = _ln_nd-> get_momt() && en;
	momnamp -= mn;
	rftot = (-momnamp/dt)*en;

	Scalar& fnamp=tempval;
	fnamp = en && ( _ln_nd-> get_fint() + _ln_nd-> get_fext() + fadd );
	fnamp -= fn;
	rftot += (-fnamp)*en;

	return rftot;
}// end function

void BCvel::get_current_movement_w(const Scalar& tn, const Scalar& dt, GVector& mov){
	Scalar vn = this->funcptr->get_funcvalue(tn);
	Scalar an;
	if(dt < 1.e3){
		an = this->funcptr->get_devfuncvalue(tn);
	}
	else{
		Scalar tnn = tn + dt;
		Scalar vnn = this->funcptr->get_funcvalue(tnn);
		an = (vnn - vn)/dt;
	}
	mov.SetComponents(0,vn,an);
}// end function

/***********************************************
 *	BCacc : BCalgorithm
 ***********************************************/
BCacc::BCacc(BCacc& algo){
	myid  = algo.get_ID();
	mytol = algo.get_tol();
	en    = algo.get_normal();
	funcptr = algo.get_new_function();
}

const GVector& BCacc::get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd){
	const Scalar& mass = _ln_nd-> get_mass();
	/*
	 * Not necessary. Temporary error check, just in case.
	 * Can be deleted to save tiny percentage of analysis time.
	 */
	if (mass < 0){
		std::cout<<"ERROR: negative mass."<<std::endl;
		tempvec.SetZeros();
		return tempvec;
	}

	this->get_current_movement_w(tn, dt, tempvec);
	Scalar fn = mass*tempvec.GetComp(2);

	GVector& rftot = tempvec;
	rftot.SetZeros();

	Scalar& fnamp=tempval;
	fnamp = en && ( _ln_nd-> get_fint() + _ln_nd-> get_fext() + fadd );
	fnamp -= fn;
	rftot = (-fnamp)*en;

	return rftot;
}// end function

void BCacc::get_current_movement_w(const Scalar& tn, const Scalar& dt, GVector& mov){
	Scalar an;
	if(dt < 1.e3){
		an = this->funcptr->get_devfuncvalue(tn);
	}
	else{
		Scalar vn = this->funcptr->get_funcvalue(tn);
		Scalar tnn = tn + dt;
		Scalar vnn = this->funcptr->get_funcvalue(tnn);
		an = (vnn - vn)/dt;
	}
	mov.SetComponents(0,0,an);
}// end function
