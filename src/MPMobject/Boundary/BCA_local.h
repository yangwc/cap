/*
 * BCALG.h
 */
#ifndef BCALGORITHMLOCAL_H_
#define BCALGORITHMLOCAL_H_

#include "BCAlg.h"

/***********************************************
 *	BCfixed : BCalgorithm
 ***********************************************/
class BCfixed: public BCalgorithm {
public:
	BCfixed() : BCalgorithm(-2) {;}
	BCfixed(const int& id) : BCalgorithm(id) {;}
	BCfixed(BCfixed& algo){ myid = algo.get_ID(); mytol =algo.get_tol(); }
	~BCfixed(){;}

	// *** interface functions
	//	const GVector& get_rforce(const GVector& momentum, const GVector& iforce, const GVector& eforce, const GVector& bforce, const Scalar& mass, const Scalar& tn, const Scalar& dt);
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCfixed(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCfixed(*this);}
	inline int reset_normal(const GVector& en) { return 0; }

protected:
	GVector tempvec;
};

/***********************************************
 *	BCplanar : BCalgorithm
 ***********************************************/
class BCplanar: public BCalgorithm {
public:
	BCplanar() : BCalgorithm(-2) {;}
	BCplanar(const int& id, GVector& normal) : BCalgorithm(id) { en=normal; }
	BCplanar(BCplanar& algo){ myid = algo.get_ID(); mytol =algo.get_tol(); en = algo.get_normal();}
	~BCplanar(){;}

	// *** interface functions
	//	const GVector& get_rforce(const GVector& momentum, const GVector& iforce, const GVector& eforce, const GVector& bforce, const Scalar& mass, const Scalar& tn, const Scalar& dt);
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCplanar(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCplanar(*this);}
	inline int reset_normal(const GVector& en) { this -> en = en; return 0; }

	// *** functions
	GVector& get_normal(){return en;}

protected:
	Scalar tempval;
	GVector tempvec;
	GVector en;
};

class BCplanarPre: public BCalgorithm {
public:
	BCplanarPre() : BCalgorithm(-2) {;}
	BCplanarPre(const int& id, GVector& normal) : BCalgorithm(id) { en=normal; }
	BCplanarPre(BCplanarPre& algo){ myid = algo.get_ID(); mytol =algo.get_tol(); en = algo.get_normal();}
	~BCplanarPre(){;}

	// *** interface functions
	//	const GVector& get_rforce(const GVector& momentum, const GVector& iforce, const GVector& eforce, const GVector& bforce, const Scalar& mass, const Scalar& tn, const Scalar& dt);
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCplanarPre(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCplanarPre(*this);}
	inline int reset_normal(const GVector& en) { this -> en = en; return 0; }

	// *** functions
	inline GVector& get_normal(){return en;}

protected:
	Scalar tempval;
	GVector tempvec;
	GVector en;
};

/***********************************************
 *	BCplanwfri : BCalgorithm
 ***********************************************/
class BCplanwfri: public BCalgorithm {
public:
	BCplanwfri() : BCalgorithm(-2) {;}
	BCplanwfri(const int& id, const GVector& normal, Scalar mu=0) : BCalgorithm(id) { en=normal; _mu = mu;}
	BCplanwfri(BCplanwfri& algo){ myid = algo.myid; mytol =algo.mytol; en = algo.en; _mu = algo._mu;}
	BCplanwfri(BCplanar& algo, Scalar& fricoef){ myid = algo.get_ID(); mytol =algo.get_tol(); en = algo.get_normal(); _mu = fricoef;}
	~BCplanwfri(){;}

	// *** interface functions
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCplanwfri(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCplanwfri(*this);}
	inline int reset_normal(const GVector& en) { this -> en = en; return 0; }

	// *** functions
	GVector& get_normal(){return en;}
	Scalar& get_frcoef(){return _mu;}

protected:
	Scalar tempval, _mu;
	GVector tempvec1,tempvec2,tempvec3;
	GVector en;
};

/***********************************************
 *	BCfreecut : BCalgorithm
 ***********************************************/
class BCfreecut: public BCalgorithm {
public:
	BCfreecut() : BCalgorithm(-2) {_ihistory =false;}
	BCfreecut(const int& id, GVector& normal) : BCalgorithm(id) { en=normal; _ihistory =false;}
	BCfreecut(BCfreecut& algo){ myid = algo.get_ID(); mytol =algo.get_tol(); en = algo.get_normal(); _ihistory =false;}
	~BCfreecut(){;}

	// *** interface functions
	//	const GVector& get_rforce(const GVector& momentum, const GVector& iforce, const GVector& eforce, const GVector& bforce, const Scalar& mass, const Scalar& tn, const Scalar& dt);
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCfreecut(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCfreecut(*this);}
	inline int reset_normal(const GVector& en) { this -> en = en; return 0; }

	// *** functions
	GVector& get_normal(){return en;}

protected:
	Scalar tempval;
	GVector tempvec, tempvec2;
	GVector en, _ofintt, _ofother, _op;
	Scalar _omi;
	bool _ihistory;
};
/***********************************************
 *	BCvel : BCalgorithm
 ***********************************************/
class BCvel: public BCalgorithm {
public:
	BCvel() : BCalgorithm(-2) { funcptr = 0; }
	BCvel(const int& id, GVector& normal, function* fptr): BCalgorithm(id) { en=normal; funcptr = fptr;}
	BCvel(BCvel& algo);
	~BCvel(){delete funcptr; funcptr = 0;}

	// *** interface functions
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCvel(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCvel(*this);}
	inline int reset_normal(const GVector& en) { this -> en = en; return 0; }

	// *** functions
	GVector& get_normal(){return en;}
	void get_current_movement_w(const Scalar& tn, const Scalar& dt, GVector& mov);
	void assign_new_function(function* functionptr){ funcptr = functionptr;}
	function* get_new_function(){ return funcptr->get_new_obj();}

protected:
	Scalar tempval;
	GVector tempvec;
	GVector en;
	function* funcptr;
};

/***********************************************
 *	BCacc : BCalgorithm
 ***********************************************/
class BCacc: public BCalgorithm {
public:
	BCacc() : BCalgorithm(-2) { funcptr = 0; }
	BCacc(const int& id, GVector& normal, function* fptr): BCalgorithm(id) { en=normal; funcptr = fptr;}
	BCacc(BCacc& algo);
	~BCacc(){delete funcptr; funcptr = 0;}

	// *** interface functions
	//	const GVector& get_rforce(const GVector& momentum, const GVector& iforce, const GVector& eforce, const GVector& bforce, const Scalar& mass, const Scalar& tn, const Scalar& dt);
	const GVector& get_rforce(const Scalar& tn, const Scalar& dt, const GVector& fadd);
	inline BCalgorithm& get_algrithm(){return (*(new BCacc(*this)));}
	inline BCalgorithm* get_new_algrithm(){return new BCacc(*this);}
	inline int reset_normal(const GVector& en) { this -> en = en; return 0; }

	// *** functions
	GVector& get_normal(){return en;}
	void get_current_movement_w(const Scalar& tn, const Scalar& dt, GVector& mov);
	void assign_new_function(function* functionptr){ funcptr = functionptr;}
	function* get_new_function(){ return funcptr->get_new_obj();}

protected:
	Scalar tempval;
	GVector tempvec;
	GVector en;
	function* funcptr;
};
#endif /* BCALGORITHMLOCAL_H_ */
