/**************************************************
 * BCManager.h
 * 	   - BCManager()
 * ------------------------------------------------
 *  +
 * ================================================
 * Created by: YWC @ CEE, UW (FEB 05, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.02.05)
 **************************************************/

#ifndef BCMANAGER_H_
#define BCMANAGER_H_

#include <vector>
#include "../../GB_Tensor.h"
#include "BC.h"
#include "BCTrigger.h"

template <typename _elem, typename _node>
class BCManager {
public:
	BCManager(){;}
	virtual ~BCManager(){
		for (std::vector<BC*>::iterator ivec = _bcs.begin(); ivec != _bcs.end(); ++ivec){
			delete *ivec;
			(*ivec) = 0;
		}
	}

	void add_bconditions( BC* bc ){ _bcs.push_back( bc ); }

	int refresh_new_node( _node* ln_nd){
		BCTrigger* bctr = 0;
		BCalgorithm* alg;

		for (std::vector<BC*>::iterator ibc = _bcs.begin(); ibc != _bcs.end(); ++ibc){
			alg = (*ibc)-> refresh_new_node( ln_nd );
			if (alg != 0){
				if (bctr == 0) bctr = new BCTrigger();
				bctr-> link_boundary( alg );
				alg = 0;
			}
		}
		if (bctr != 0){
			ln_nd-> set_bcondition(bctr);
			bctr = 0;
		}
		return 0;
	}

	int refresh_new_cell( _elem* ln_ce){
		for (std::vector<BC*>::iterator ibc = _bcs.begin(); ibc != _bcs.end(); ++ibc){
			(*ibc)-> refresh_new_cell( ln_ce );
		}
		return 0;
	}

	int refresh_del_node( _node* ln_nd){
		for (std::vector<BC*>::iterator ibc = _bcs.begin(); ibc != _bcs.end(); ++ibc){
			(*ibc)-> refresh_del_node( ln_nd );
		}
		return 0;
	}
	int refresh_del_cell( _elem* ln_ce){
		for (std::vector<BC*>::iterator ibc = _bcs.begin(); ibc != _bcs.end(); ++ibc){
			(*ibc)-> refresh_del_cell( ln_ce );
		}
		return 0;
	}

protected:
	std::vector<BC*> _bcs;
};

#endif /* BCMANAGER_H_ */
