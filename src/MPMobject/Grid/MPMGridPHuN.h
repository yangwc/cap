/**************************************************
 * MPMGridPHuN.h
 * 	   - MPMGridPHuN()
 * ------------------------------------------------
 * 	+ Lumped Grid Model
 * 	+ Hu-Washizu Principle (virtual power)
 * 	+ Node-based assumed field (lumped)
 * ================================================
 * Created by: YWC @ CEE, UW (Feb 03, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.02.05)
 **************************************************/
#ifndef MPMGRIDPHUN_H_
#define MPMGRIDPHUN_H_

#include "MPMGrid.h"

template<typename Node, typename Elem>
class MPMGridPHuN : public MPMGrid< Node, Elem>{
protected:
	typedef typename std::vector<Node*> NVec;
	typedef typename std::vector<Elem*> EVec;
	typedef typename NVec::iterator NVite;
	typedef typename EVec::iterator EVite;

public:
	MPMGridPHuN(){;}
	virtual ~MPMGridPHuN(){;}

	virtual int build();
	virtual int solve(const Scalar& tnow, const Scalar& dt);
};

/**************************************************
 * IMPLEMENTAION
 *     to implement a template class in one header
 * file is the easiest and simplest way to compile
 * correctly without any additional setup.
 * ================================================
 * Modified by: YWC (16.02.05)
 **************************************************/
template<typename Node, typename Elem>
int MPMGridPHuN<Node,Elem>::build(){
	NVec& _objs_nd  = MPMGrid<Node,Elem>::_objs_nd;
	EVec& _objs_adp = MPMGrid<Node,Elem>::_objs_adp;

	// *** preparing node-based smoothing
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)-> init_smooth();
	}

	// *** specific stress (sigma bar)
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> lump_m_specstress();
	}
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)-> smooth_specstress();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> smooth_specstress();
	}

	// *** mi, pi, fiext, fiint (need assumed s)
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)->map_to_node();
	}
	return 0;
}
template<typename Node, typename Elem>
int MPMGridPHuN<Node,Elem>::solve(const Scalar& tnow, const Scalar& dt){
	NVec& _objs_nd  = MPMGrid<Node,Elem>::_objs_nd;
	EVec& _objs_adp = MPMGrid<Node,Elem>::_objs_adp;

	// solve ai, vi
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)->solve(tnow, dt);
	}

	// evaluate assumed strain rate
	// *** velocity gradient
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> lump_m_gradvel();
	}
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)-> smooth_gradvel();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> smooth_gradvel();
	}

	return 0;
}

#endif /* MPMGRIDPHUN_H_ */
