/**************************************************
 * MPMGrid.h
 * 	   - MPMGrid()
 * ------------------------------------------------
 * 	+ Lumped Grid Model
 * ================================================
 * Created by: YWC @ CEE, UW (Apr 30, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.11.16)
 **************************************************/
#ifndef MPMGRID_H_
#define MPMGRID_H_

#include "_INTF_Grid.h"
#include "../ObjManager/Gmanager.h"

template<typename Node, typename Elem>
class MPMGrid : public Grid{
protected:
	typedef typename std::vector<Node*> NVec;
	typedef typename std::vector<Elem*> EVec;
	typedef typename NVec::iterator NVite;
	typedef typename EVec::iterator EVite;

public:
	MPMGrid();
	virtual ~MPMGrid();

	inline void assign_gridmanager(Gmanager<Elem,Node>* gmanager){ _gmanager = gmanager;}

	virtual int reset();
	virtual int reset( const Scalar& tnow, std::vector<Particle*>& ln_freepts );

	virtual int build();
	virtual int solve(const Scalar& tnow, const Scalar& dt);

	virtual int link_particle(Particle* ptptr);

	virtual int update_kinematics(const Scalar& tnow, const Scalar& dt);
	virtual int update_deformation(const Scalar& tnow, const Scalar& dt);

	virtual int link_global();

	inline unsigned int get_Nnode() const { return _objs_nd.size(); }
	const Gnode* get_Node(unsigned int Ind) const ;

protected:
    Gmanager<Elem,Node>* _gmanager;

    std::vector<Elem*> _objs_adp;
	std::vector<Node*> _objs_nd;
};

/**************************************************
 * IMPLEMENTAION
 *     to implement a template class in one header
 * file is the easiest and simplest way to compile
 * correctly without any additional setup.
 * ================================================
 * Modified by: YWC (15.11.16)
 **************************************************/
#include <iostream>
template<typename Node, typename Elem>
MPMGrid<Node,Elem>::MPMGrid() {
	_gmanager = 0;
	_ln_brdg  = 0;
}
template<typename Node, typename Elem>
MPMGrid<Node,Elem>::~MPMGrid() {
	delete _gmanager;
	for (EVite ivec = _objs_adp.begin(); ivec != _objs_adp.end(); ++ivec){
		delete *ivec; *ivec = 0;
	}
	for (NVite ivec = _objs_nd.begin(); ivec != _objs_nd.end(); ++ivec){
		delete *ivec; *ivec = 0;
	}
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::reset(){
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)->clean();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)->clean();
	}
	return 0;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::reset( const Scalar& tnow, std::vector<Particle*>& ln_freepts ){
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite) -> clean();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite) -> refresh_particles( tnow, ln_freepts);
	}
	return 0;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::build(){
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)->map_to_node_ex();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)->map_to_node_in();
	}
	return 0;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::solve(const Scalar& tnow, const Scalar& dt){
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)->solve(tnow, dt);
	}
	return 0;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::link_particle(Particle* ptptr){
	const GVector& ptx = ( ptptr->get_coord() );
	Elem* adptr = _gmanager->get_adplink( ptx, _objs_adp, _objs_nd );
	int Istat = adptr->add_particle(ptptr);
	if (Istat!=0) std::cerr<<"ERROR: particle should not out of support."<<std::endl;
	return Istat;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::update_kinematics(const Scalar& tnow, const Scalar& dt){
	int Istat = 0;
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		Istat = (*ite) -> update_pt_kinematics(tnow, dt);
		if( Istat != 0 ){
			std::cerr<<"ERROR: @ particle kinematic updating."<<std::endl;
			Istat = -1;
			break;
		}
	}
	return Istat;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::update_deformation(const Scalar& tnow, const Scalar& dt){
	int Istat = 0;
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		Istat = (*ite) -> update_pt_deformation(tnow, dt);
		if( Istat != 0 ){
			std::cerr<<"ERROR: @ particle deformation updating."<<std::endl;
			Istat = -1;
			break;
		}
	}
	return Istat;
}
template<typename Node, typename Elem>
int MPMGrid<Node,Elem>::link_global(){
	if (_ln_brdg == 0) return -1;
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		Gnode* ndptr = (*ite);
		if ( ndptr->is_shaped() ) {
			_ln_brdg->update_gridusage( ndptr->get_id(), ndptr);
		}
	}
	return 0;
}

template<typename Node, typename Elem>
const Gnode* MPMGrid<Node,Elem>::get_Node(unsigned int Ind) const {
	Gnode * nd = 0;
	if (Ind < _objs_nd.size()) nd = _objs_nd[Ind];
	return nd;
}

#endif /* MPMGRID_H_ */
