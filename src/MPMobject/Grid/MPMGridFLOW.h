/**************************************************
 * MPMGridFLOW.h
 * 	   - MPMGridFLOW()
 * ------------------------------------------------
 * 	+ Lumped Grid Model
 * 	+ Hu-Washizu Principle (virtual energy)
 * 	+ small displacement & deformation
 * 	+ Node-based assumed field (lumped)
 * ================================================
 * Created by: YWC @ CEE, UW (Jan 12, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.01.12)
 **************************************************/
#ifndef MPMGRIDFLOW_H_
#define MPMGRIDFLOW_H_

#include "MPMGrid.h"
#include "../GridElem/MPMelemFLOW.h"
#include "../GridNode/GnodeFLOW.h"

template<typename Node, typename Elem>
class MPMGridFLOW : public MPMGrid< Node, Elem>{
protected:
	typedef typename std::vector<Node*> NVec;
	typedef typename std::vector<Elem*> EVec;
	typedef typename NVec::iterator NVite;
	typedef typename EVec::iterator EVite;

public:
	MPMGridFLOW(){;}
	virtual ~MPMGridFLOW(){;}

	virtual int update_deformation(const Scalar& tnow, const Scalar& dt){
		int Istat = MPMGrid<Node,Elem>::update_deformation(tnow, dt);
		NVec& _objs_nd  = MPMGrid<Node,Elem>::_objs_nd;
		EVec& _objs_adp = MPMGrid<Node,Elem>::_objs_adp;

		if (Istat != 0) return Istat;
		// *** preparing node-based smoothing
		for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
			(*ite)-> init_smooth();
		}
		// *** material state
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> lump_m_matstate();
		}
		for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
			(*ite)-> smooth_matstate();
		}
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> smooth_matstate();
		}
		// *** specific stress (sigma bar)
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> lump_m_specstress();
		}
		for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
			(*ite)-> smooth_specstress();
		}
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> smooth_specstress();
		}
		return Istat;
	}
};

#endif /* MPMGRIDFLOW_H_ */
