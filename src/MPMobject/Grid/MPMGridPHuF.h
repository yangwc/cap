/**************************************************
 * MPMGridPHuF.h
 * 	   - MPMGridPHuF()
 * ------------------------------------------------
 * 	+ Lumped Grid Model
 * 	+ Hu-Washizu Principle (virtual power)
 * 	+ smoothed cell-based assumed field (flux smoothing algorithm)
 * ================================================
 * Created by: YWC @ CEE, UW (Feb 05, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.02.05)
 **************************************************/
#ifndef MPMGRIDPHUF_H_
#define MPMGRIDPHUF_H_

#include "MPMGrid.h"

template<typename Node, typename Elem>
class MPMGridPHuF : public MPMGrid< Node, Elem>{
protected:
	typedef typename std::vector<Node*> NVec;
	typedef typename std::vector<Elem*> EVec;
	typedef typename NVec::iterator NVite;
	typedef typename EVec::iterator EVite;

public:
	MPMGridPHuF(){;}
	virtual ~MPMGridPHuF(){;}

	virtual int build();
	virtual int solve(const Scalar& tnow, const Scalar& dt);
};

/**************************************************
 * IMPLEMENTAION
 *     to implement a template class in one header
 * file is the easiest and simplest way to compile
 * correctly without any additional setup.
 * ================================================
 * Modified by: YWC (16.02.05)
 **************************************************/
template<typename Node, typename Elem>
int MPMGridPHuF<Node,Elem>::build(){
	EVec& _objs_adp = MPMGrid<Node,Elem>::_objs_adp;

	// *** specific stress (sigma bar)
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> local_smooth_preparation();
		(*ite)-> local_smooth_specstress();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> smooth_preparation();
		(*ite)-> smooth_specstress();
	}

	// *** mi, pi, fiext, fiint (need assumed s)
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)->map_to_node();
	}
	return 0;
}
template<typename Node, typename Elem>
int MPMGridPHuF<Node,Elem>::solve(const Scalar& tnow, const Scalar& dt){
	NVec& _objs_nd  = MPMGrid<Node,Elem>::_objs_nd;
	EVec& _objs_adp = MPMGrid<Node,Elem>::_objs_adp;

	// solve ai, vi
	for (NVite ite = _objs_nd.begin(); ite != _objs_nd.end(); ++ite){
		(*ite)->solve(tnow, dt);
	}

	// evaluate assumed strain rate
	// *** velocity gradient
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> local_smooth_gradvel();
	}
	for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
		(*ite)-> smooth_gradvel();
	}
	return 0;
}

#endif /* MPMGRIDPHUF_H_ */
