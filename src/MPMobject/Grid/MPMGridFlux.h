/**************************************************
 * MPMGrid.h
 * 	   - MPMGrid()
 * ------------------------------------------------
 * 	+ Lumped Grid Model
 * 	+ Hu-Washizu Principle (virtual energy)
 * 	+ small displacement & deformation
 * 	+ smoothed cell-based assumed field (flux smoothing algorithm)
 * ================================================
 * Created by: YWC @ CEE, UW (Apr 30, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.11.16)
 **************************************************/
#ifndef MPMGRIDFLUX_H_
#define MPMGRIDFLUX_H_

#include "MPMGrid.h"

template<typename Node, typename Elem>
class MPMGridFlux : public MPMGrid< Node, Elem>{
protected:
	typedef typename std::vector<Elem*> EVec;
	typedef typename EVec::iterator EVite;

public:
	MPMGridFlux(){;}
	virtual ~MPMGridFlux(){;}
	virtual int update_deformation(const Scalar& tnow, const Scalar& dt){
		int Istat = MPMGrid<Node,Elem>::update_deformation(tnow, dt);
		if (Istat != 0) return Istat;

		EVec& _objs_adp = MPMGrid<Node,Elem>::_objs_adp;
		// *** material state
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> local_smooth_preparation();
			(*ite)-> local_smooth_matstate();
		}
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> smooth_preparation();
		}
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> smooth_matstate();
		}
		// *** specific stress (sigma bar)
		for (EVite ite = _objs_adp.begin(); ite != _objs_adp.end(); ++ite){
			(*ite)-> smooth_specstress();
		}
		return Istat;
	}
};
#endif /* MPMGRIDFLUX_H_ */
