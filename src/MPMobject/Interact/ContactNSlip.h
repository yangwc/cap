#ifndef CONTACTNSLIP_H_
#define CONTACTNSLIP_H_

#include "InteractIntf.h"
namespace contact_nslip{

class Contact : public OInteract{
public:
	Contact(){;}
	~Contact(){;}

	void interact(const Scalar& dt, const unsigned int& Nspace, ndvec& spadds);
};

}
#endif /* CONTACTNSLIP_H_ */
