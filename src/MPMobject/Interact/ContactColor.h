#ifndef CONTACTCOLOR_H_
#define CONTACTCOLOR_H_

#include "InteractIntf.h"
namespace contact_color{

class Contact : public OInteract{
public:
	Contact(){;}
	~Contact(){;}

	void interact(const Scalar& dt, const unsigned int& Nspace, ndvec& spadds);
private:
};

}
#endif /* CONTACTNSLIP_H_ */
