#include "ContactColor.h"
using namespace contact_color;

void Contact::interact(const Scalar& dt, const unsigned int& Nspace, ndvec& spadds){
	Scalar micm = 0;
	GVector picm, ficm;

	for (unsigned int Ispa = 0; Ispa != Nspace; ++Ispa){
		Gnode*& ndptr = spadds[Ispa];
		micm += ndptr->get_mass();
		picm += ndptr->get_momt();
		ficm += ndptr->get_fint() + ndptr->get_fext();
	}// end for

	GVector vicm = picm; vicm /= micm;
	GVector aicm = ficm; aicm /= micm;

	GVector& fi = ficm; // rename ficm as fi
	GVector dfi;
	Scalar pn, fn, Rn;
	for (unsigned int Ispa = 0; Ispa != Nspace; ++Ispa){
		Gnode*& ndptr = spadds[Ispa];
		const Scalar& mi = ndptr->get_mass();
		const GVector& pi = ndptr->get_momt();
		const GVector& ni = ndptr->get_n();

		pn = (vicm*mi - pi) && ni;
//		if ( pn < 1.e-12 ){
			Rn = pn/dt;

			fi = ndptr->get_fint(); fi += ndptr->get_fext();
			fn = (aicm*mi - fi) && ni;
//			if ( fn < 0 ) Rn += fn;
			Rn += fn;
			dfi = ni*Rn;
			ndptr->add_fext(dfi);
//		}
	}// end for
}
