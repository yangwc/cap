#include "ContactNSlip.h"
using namespace contact_nslip;

void Contact::interact(const Scalar& dt, const unsigned int& Nspace, ndvec& spadds){
	Scalar micm = 0;
	GVector picm, ficm;

	for (unsigned int Ispa = 0; Ispa != Nspace; ++Ispa){
		Gnode*& ndptr = spadds[Ispa];
		micm += ndptr->get_mass();
		picm += ndptr->get_momt();
		ficm += ndptr->get_fint() + ndptr->get_fext();
	}// end for

	GVector vicm = picm; vicm /= micm;
	GVector aicm = ficm; aicm /= micm;

	GVector& fi = ficm;
	GVector dfi;
	for (unsigned int Ispa = 0; Ispa != Nspace; ++Ispa){
		Gnode*& ndptr = spadds[Ispa];
		const Scalar& mi = ndptr->get_mass();
		const GVector& pi = ndptr->get_momt();
		fi = ndptr->get_fint(); fi += ndptr->get_fext();

		dfi = (mi*vicm - pi)/dt + (mi*aicm - fi);
		ndptr->add_fext(dfi);
	}// end for
}
