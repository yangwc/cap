/**************************************************
 * PTFilter.h
 * 	   - PTFilter()
 * ------------------------------------------------
 * 	+ interface of particle filters
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef OWNERSHIP_H_
#define OWNERSHIP_H_


class _ownership{
public:
	_ownership(){ _ownerID = -1; }
	virtual ~_ownership(){;}

	inline void set_owner(int id){ _ownerID = id; }
	inline bool if_free() const { return _ownerID < 0; }
	inline bool is_accessible( int id ) const { return ( _ownerID == id || this-> if_free() ); }
	inline int get_ownerID() const { return _ownerID; }
protected:
	int _ownerID;
};


#endif /* OWNERSHIP_H_ */
