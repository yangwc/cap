/**************************************************
 * _base_ptbound.h
 * 	   - _base_ptbound()
 * ------------------------------------------------
 * 	+ particle eater ( particle filter )
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef BASEPTBOUND_H_
#define BASEPTBOUND_H_

#include "_INTF_PTFilter.h"
#include "../_ownership.h"
#include "../GeoObject/Cuboid.h"

class _base_ptbound : public PTFilter{
//protected:
//	typedef _particle_adaptor<Particle> cell;

public:
	_base_ptbound(){_czone = 0;}
	virtual ~_base_ptbound(){delete _czone;}

	inline void assign_zone(Cuboid* czone){ delete _czone; _czone = czone; }

//	virtual int refresh_new_cell( cell* ln_ce) = 0;
//	virtual int refresh_del_cell( cell* ln_ce) = 0;
protected:
	Cuboid* _czone;
	std::vector<cell*> _lns_ce;
};

#endif /* BASEPTBOUND_H_ */
