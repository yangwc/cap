/**************************************************
 * PTProducer.h
 * 	   - PFproducer()
 * 	   - PFprodCT()
 * ------------------------------------------------
 * 	+ particle producers ( particle filter )
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef PTPRODUCER_H_
#define PTPRODUCER_H_

#include "_base_ptbound.h"

#include "../LiteFac/Objinfo.h"
#include "../../Function/Function.h"

class PFproducer : public _base_ptbound{
	/***************************************************************************
	 * Center, Constant Area, Particle Producer, Particle Filter
	 ***************************************************************************/
public:
	PFproducer(){ init_self(); }
	PFproducer(const PFproducer& pfilt);
	virtual ~PFproducer(){delete _fptr_vn; _fptr_vn = 0; delete _objinfo; _objinfo = 0;}

	inline void assign_object_info(Objinfo* oinfo){ delete _objinfo; _objinfo = oinfo; }

	inline int set_vn_func(function* func){ delete _fptr_vn; _fptr_vn = func; return 0; }
	inline int set_start_ID(long long int IstaID){ _IptID = IstaID; return 0;}
	inline int set_front_dist(Scalar rn){ _rnpt = rn; while(_rnpt >= 0) _rnpt -= _drnpt; return 0; }

	virtual int set_discretize_info(Scalar drn_pt, unsigned int Nseg_t1, unsigned int Nseg_t2);

	virtual int filter_pt(const Scalar& tnow, const Scalar& dt, PtVec& objs_pt, PtVec& lns_free_pt);
	virtual PTFilter* get_new_filter() const {return new PFproducer(*this);}

	virtual int refresh_new_cell( cell* ln_ce){ return 0; }
	virtual int refresh_del_cell( cell* ln_ce){ return 0; }

protected:

	virtual void init_self(); // setup some initial value that generate nothing
	virtual void create_pt_pattern();

	function* _fptr_vn;
	Scalar _rnpt, _drnpt;

	Objinfo* _objinfo;
	long long int _IptID;

	std::vector<GVector> _ptxn_pattern;
	Scalar _ptVo;

private:
	unsigned int _Nsegt1, _Nsegt2;

};

class PFprodCT : public PFproducer{
	/***************************************************************************
	 * Corner, Time-dependent, Particle Producer, Particle Filter
	 ***************************************************************************/
public:
	PFprodCT(){ init_self();}

	PFprodCT(const PFprodCT& pfilt);
	~PFprodCT(){delete _fptr_lt1; _fptr_lt1 = 0; delete _fptr_lt2; _fptr_lt2 = 0;}

	inline void set_t1length_func(function* func){ delete _fptr_lt1; _fptr_lt1 = func; }
	inline void set_t2length_func(function* func){ delete _fptr_lt2; _fptr_lt2 = func; }

	int set_discretize_info(Scalar drn_pt, unsigned int Nseg_t1, unsigned int Nseg_t2);
	int set_discretize_info(Scalar drn_pt, Scalar dl_t1, Scalar dl_t2);

	int filter_pt(const Scalar& tnow, const Scalar& dt, std::vector<Particle*>& ptptrs);
	PTFilter* get_new_filter() const {return new PFprodCT(*this);}

private:

	void init_self(); // setup some initial value that generate nothing
	void create_pt_pattern(const Scalar& tnow);

	function *_fptr_lt1, *_fptr_lt2;
	Scalar _dlt1, _dlt2;
};
#endif /* PTPRODUCER_H_ */
