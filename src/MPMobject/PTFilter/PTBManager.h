/**************************************************
 * PTBManager.h
 * 	   - PTBManager()
 * ------------------------------------------------
 *  +
 * ================================================
 * Created by: YWC @ CEE, UW (FEB 05, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.02.05)
 **************************************************/

#ifndef PTBMANAGER_H_
#define PTBMANAGER_H_

#include <vector>
#include "../../GB_Tensor.h"
#include "_INTF_PTFilter.h"

template <typename _elem>
class PTBManager {
public:
	PTBManager(){;}
	virtual ~PTBManager(){;}

	void link_ptfilter( PTFilter* ptf ){ _lns_ptfs.push_back( ptf); }

	int refresh_new_cell( _elem* ln_ce){
		for (std::vector<PTFilter*>::iterator iptf = _lns_ptfs.begin(); iptf != _lns_ptfs.end(); ++iptf){
			(*iptf)-> refresh_new_cell( ln_ce );
		}
		return 0;
	}

	int refresh_del_cell( _elem* ln_ce){
		for (std::vector<PTFilter*>::iterator iptf = _lns_ptfs.begin(); iptf != _lns_ptfs.end(); ++iptf){
			(*iptf)-> refresh_del_cell( ln_ce );
		}
		return 0;
	}

protected:
	std::vector<PTFilter*> _lns_ptfs;
};

#endif /* PTBMANAGER_H_ */
