/**************************************************
 * PTProducer.h
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#include "PTProducer.h"
#include "../../Material/matpack.h"

/********************************************
 * PFproducer
 ********************************************/
PFproducer::PFproducer(const PFproducer& proto){
	_ownerID = proto._ownerID;

	if (proto._czone != 0) _czone = proto._czone-> get_this_new_object();
	else _czone = 0;

	if(proto._fptr_vn != 0) _fptr_vn = proto._fptr_vn-> get_new_obj();
	else _fptr_vn = new FNconstant(-1,0);

	if(proto._objinfo != 0) _objinfo = proto._objinfo-> get_new_object();
	else _objinfo = 0;

	_IptID = proto._IptID;
	_rnpt  = proto._rnpt;
	this -> set_discretize_info(proto._drnpt, proto._Nsegt1, proto._Nsegt2);
}

inline void PFproducer::init_self(){
	_objinfo = 0;
	_fptr_vn = new FNconstant(-1,0);
	_IptID   = 0;
	_rnpt    = -1.e-10;
	this -> set_discretize_info(1,2,2);
}

int PFproducer::set_discretize_info(Scalar drn_pt, unsigned int Nseg_t1, unsigned int Nseg_t2){
	int Istat = -1;
	if( drn_pt >  0 && Nseg_t1 != 0 && Nseg_t2 != 0){
		_drnpt  = drn_pt;
		_Nsegt1 = Nseg_t1;
		_Nsegt2 = Nseg_t2;
		if(_czone != 0) this -> create_pt_pattern();
		Istat = 0;
	}
	return Istat;
}

void PFproducer::create_pt_pattern(){
	_ptxn_pattern.assign((_Nsegt1*_Nsegt2), GVector());

	if(_czone == 0) return;

	const GVector& size = _czone-> get_size();
	Scalar dt1 = size.GetComp(1)/_Nsegt1, Lbt1 = 0.5*size.GetComp(1);
	Scalar dt2 = size.GetComp(2)/_Nsegt2, Lbt2 = 0.5*size.GetComp(2);

	_ptVo = dt1*dt2*_drnpt;

	Scalar xt1;
	std::vector<Scalar> xt2s(_Nsegt2);
	for (unsigned int It2 = 0; It2 != _Nsegt2; ++It2) xt2s[It2] = (0.5+It2)*dt2 - Lbt2;

	unsigned int Ipt = 0;
	GVector xnpt;
	for (unsigned int It1 = 0; It1 != _Nsegt1; ++It1){
		xt1 = (0.5+It1)*dt1 - Lbt1;
		for (unsigned int It2 = 0; It2 != _Nsegt2; ++It2){
			Scalar& xt2 = xt2s[It2];
			xnpt.SetComponents(0,xt1,xt2);
			_czone->transform2ijk(xnpt);
			_ptxn_pattern[Ipt] = _czone->get_location() + xnpt;
			Ipt++;
		}// end for (xpt in t2)
	}// end for (xpt in t1)
}

int PFproducer::filter_pt(const Scalar& tnow, const Scalar& dt, PtVec& objs_pt, PtVec& lns_free_pt){
	Scalar an = 0, vn = _fptr_vn->get_funcvalue(tnow);
	if (dt < 1.e12) an = _fptr_vn->get_devfuncvalue(tnow);
	else an = (_fptr_vn->get_funcvalue(tnow + dt) + vn)/dt;

	Scalar rnptn  = _rnpt;
	Scalar rnptn1 = rnptn + vn*dt + 0.5*an*dt*dt;
	const GVector& en = _czone-> get_normal();
	GVector dxpt;
	Particle* ptptr = 0;
	while (rnptn1 >= 0){
		dxpt = en * rnptn;
		for (std::vector<GVector>::iterator ivec=_ptxn_pattern.begin(); ivec != _ptxn_pattern.end(); ++ivec){
			ptptr = _objinfo->get_new_particle((*ivec)+dxpt, tnow);
			ptptr->reset_Iaddress(objs_pt.size());
			ptptr->assign_volume(_ptVo);
			ptptr->set_velocity( en*vn );
			objs_pt.push_back(ptptr);
			lns_free_pt.push_back(ptptr);
			ptptr = 0;
		}
		rnptn  -= _drnpt;
		rnptn1 -= _drnpt;
	}
	_rnpt = rnptn1;
	return 0;
}

/********************************************
 * PFprodCT
 ********************************************/

PFprodCT::PFprodCT(const PFprodCT& proto){
	this -> init_self();

	_ownerID = proto._ownerID;
	_czone = 0;
	if(proto._czone != 0) _czone = proto._czone->get_this_new_object();
	if(proto._fptr_vn != 0) _fptr_vn = proto._fptr_vn->get_new_obj();
	if(proto._fptr_lt1 != 0) _fptr_lt1 = proto._fptr_lt1->get_new_obj();
	if(proto._fptr_lt2 != 0) _fptr_lt2 = proto._fptr_lt2->get_new_obj();
	if(proto._objinfo != 0) _objinfo = proto._objinfo->get_new_object();

	_IptID = proto._IptID;
	_rnpt  = proto._rnpt;
	this -> set_discretize_info(proto._drnpt, proto._dlt1, proto._dlt2);
}

inline void PFprodCT::init_self(){
	_objinfo = 0;
	_fptr_vn = new FNconstant(-1,0);
	_fptr_lt1 = new FNconstant(1,0);
	_fptr_lt2 = new FNconstant(1,0);
	_IptID   = 0;
	_rnpt    = -1.e-10;

	Scalar dl = 0.5;
	this -> set_discretize_info(1,dl,dl);
}

int PFprodCT::set_discretize_info(Scalar drn_pt, unsigned int Nseg_t1, unsigned int Nseg_t2){
	/****************************************************
	 * find dlt1 and dlt2 based on Lt1 and Lt2 at t = 0
	 ****************************************************/
	int Istat = -2;
	if( drn_pt >  0 && Nseg_t1 != 0 && Nseg_t2 != 0){
		Scalar dlt1 = _fptr_lt1->get_funcvalue(0)/Nseg_t1;
		Scalar dlt2 = _fptr_lt2->get_funcvalue(0)/Nseg_t2;
		Istat = this -> set_discretize_info(drn_pt, dlt1, dlt2);
	}
	return Istat;
}

int PFprodCT::set_discretize_info(Scalar drn_pt, Scalar dl_t1, Scalar dl_t2){
	int Istat = -1;
	if( drn_pt >  0 && dl_t1 > 0 && dl_t2 > 0){
		_drnpt  = drn_pt;
		_dlt1 = dl_t1;
		_dlt2 = dl_t2;
		Istat = 0;
	}
	return Istat;
}

void PFprodCT::create_pt_pattern(const Scalar& tnow){
	Scalar lt1 = _fptr_lt1->get_funcvalue(tnow);
	Scalar lt2 = _fptr_lt2->get_funcvalue(tnow);
	unsigned int Nseg_t1 = (unsigned int) (lt1/_dlt1 +0.5);
	if (Nseg_t1 == 0) Nseg_t1 = 1;
	unsigned int Nseg_t2 = (unsigned int) (lt2/_dlt2 +0.5);
	if (Nseg_t2 == 0) Nseg_t2 = 1;
	Scalar dt1 = lt1/Nseg_t1;
	Scalar dt2 = lt2/Nseg_t2;

	_ptxn_pattern.assign((Nseg_t1*Nseg_t2), GVector());
	_ptVo = dt1*dt2*_drnpt;

	Scalar xt1;
	std::vector<Scalar> xt2s(Nseg_t2);
	for (unsigned int It2 = 0; It2 != Nseg_t2; ++It2) xt2s[It2] = (0.5+It2)*dt2;

	unsigned int Ipt = 0;
	GVector xnpt;
	for (unsigned int It1 = 0; It1 != Nseg_t1; ++It1){
		xt1 = (0.5+It1)*dt1;
		for (unsigned int It2 = 0; It2 != Nseg_t2; ++It2){
			Scalar& xt2 = xt2s[It2];
			xnpt.SetComponents(0,xt1,xt2);
			_czone->transform2ijk(xnpt);
			_ptxn_pattern[Ipt] = _czone->get_location() + xnpt;
			Ipt++;
		}// end for (xpt in t2)
	}// end for (xpt in t1)
}

int PFprodCT::filter_pt(const Scalar& tnow, const Scalar& dt, std::vector<Particle*>& ptptrs){
	Scalar an = 0, vn = _fptr_vn->get_funcvalue(tnow);
	if (dt < 1.e12) an = _fptr_vn->get_devfuncvalue(tnow);
	else an = (_fptr_vn->get_funcvalue(tnow + dt) + vn)/dt;

	bool ineedpattern = true;
	Scalar rnptn  = _rnpt;
	Scalar rnptn1 = rnptn + (vn + 0.5*an*dt)*dt;
	GVector dxpt;
	const GVector& en = _czone-> get_normal();
	Particle* ptptr = 0;
	while (rnptn1 >= 0){
		if (ineedpattern){
			this -> create_pt_pattern( tnow );
			ineedpattern = false;
		}
		dxpt = en * rnptn;
		for (std::vector<GVector>::iterator ivec=_ptxn_pattern.begin(); ivec != _ptxn_pattern.end(); ++ivec){
			ptptr = _objinfo->get_new_particle((*ivec)+dxpt, tnow);
			ptptr->reset_Iaddress(ptptrs.size());
			ptptr->assign_volume(_ptVo);
			ptptr->set_velocity( en*vn );
			ptptrs.push_back(ptptr);
			ptptr = 0;
		}
		rnptn  -= _drnpt;
		rnptn1 -= _drnpt;
	}
	_rnpt = rnptn1;
	return 0;
}

