/**************************************************
 * PTEater.h
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#include "PTEater.h"

PFeater::PFeater(const PFeater& proto){
	_czone = 0; if (proto._czone != 0) _czone = proto._czone-> get_this_new_object();
	_ownerID = proto._ownerID;
}

int PFeater::filter_pt(const Scalar& tnow, const Scalar& dt, PtVec& objs_pt, PtVec& lns_free_pt){
	// ************************************
	// *** check number of particles.
	unsigned int Ngpt = objs_pt.size();
	if (Ngpt == 0) return 0;

	// ************************************
	// *** check number of cells.
	unsigned int Nce = _lns_ce.size();
	if (Nce == 0) return 0;

	int Ipt, Igoto;
	// ************************************
	// *** eat particles.
	std::vector<unsigned int> I0pts, Icpts;
	bool ieat = false;
	unsigned int Icp, Ncp;
	for (unsigned int Ice =0; Ice != Nce; ++Ice){
		Icpts.clear();

		// ************************************
		// *** eat particles in a cell.
		cell& ce    = *_lns_ce[Ice];
		PtVec& cpts = ce._admin_sync_particles();
		Ncp         = ce._admin_get_Nparticle();
		for (Icp = 0; Icp != Ncp; ++Icp){
			ieat = _czone -> if_covered( cpts[Icp]-> get_coord() );
			if (ieat){
				Ipt = cpts[Icp]-> get_Iadd();
				I0pts.push_back( Ipt );
				Icpts.push_back( Icp );
				delete objs_pt[Ipt];
				objs_pt[Ipt] = 0;
				cpts[Icp] = 0;
			}// end if (ieat)
		}// end for (particles)

		if ( Icpts.empty() ) continue;

		// ************************************
		// *** compact cell particle container.
		Ipt   = Ncp-1;
		Igoto = -1;
		for (unsigned int Ie =0; Ie != Icpts.size(); ++Ie ){
			while (cpts[Ipt] == 0){
				Ipt--;
				if(Ipt == -1) break;
			}
			Igoto = Icpts[Ie];
			if (Ipt < Igoto) break;

			cpts[Igoto] = cpts[Ipt];
			cpts[Ipt] = 0;
		}// end for (empty pointer)
		ce._admin_reset_Nparticle( Ncp-Icpts.size() );
	}

//	for (std::vector<Particle*>::iterator ivec = objs_pt.begin(); ivec != objs_pt.end(); ++ivec){
//		ieat = _czone -> if_covered( (*ivec)->get_coord() );
//		if (ieat){
//			Iptadd  = (*ivec)->get_Iadd();
//			I0pts.push_back( Iptadd );
//			delete (*ivec);
//			(*ivec) = 0;
//		}// end if (ieat)
//	}// end for (particles)

	// ************************************
	// *** check if ate any particles
	unsigned int Neat = I0pts.size();
	if (Neat == 0) return 0;

	// ************************************
	// *** compact particle container.
	Ipt   = Ngpt-1;
	Igoto = -1;
	for (unsigned int Ie =0; Ie != Neat; ++Ie ){
		while (objs_pt[Ipt] == 0){
			Ipt--;
			if(Ipt == -1) break;
		}

		Igoto = I0pts[Ie];
		if (Ipt < Igoto) continue;

		objs_pt[Igoto] = objs_pt[Ipt];
		objs_pt[Ipt] = 0;
		objs_pt[Igoto] -> reset_Iaddress(Igoto);
	}// end for (empty pointer)
	objs_pt.resize( (Ngpt-Neat) );
	return 0;
}

int PFeater::refresh_new_cell( cell* ln_ce){
	bool icover;
	std::vector<GVector> xcor;
	ln_ce-> get_xcorners(xcor);
	for(unsigned int Ic=0; Ic != xcor.size(); ++Ic){
		icover = _czone-> if_covered( xcor[Ic] );
		if(icover){
			_lns_ce.push_back(ln_ce);
			break;
		}
	}
	return 0;
}
