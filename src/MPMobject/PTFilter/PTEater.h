/**************************************************
 * PTEater.h
 * 	   - PFeater()
 * ------------------------------------------------
 * 	+ particle eater ( particle filter )
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef PTEATER_H_
#define PTEATER_H_

#include "_base_ptbound.h"

class PFeater : public _base_ptbound {
public:
	PFeater(){;}
	PFeater(const PFeater& pfilt);
	~PFeater(){;}

	int filter_pt(const Scalar& tnow, const Scalar& dt, PtVec& objs_pt, PtVec& lns_free_pt);
	PTFilter* get_new_filter() const {return new PFeater(*this);}

	virtual int refresh_new_cell( cell* ln_ce);
	virtual int refresh_del_cell( cell* ln_ce){ return 0; }
};

#endif /* PTEATER_H_ */
