#ifndef GNODEFLOW_H_
#define GNODEFLOW_H_

#include "Gnode.h"

class GnodeFLOW : public Gnode {
public:
	GnodeFLOW(const IntGVector& id, const GVector& coord, ShpSKH* skhptr) : Gnode(id, coord, skhptr){;}
	~GnodeFLOW(){;}

	inline void init_smooth(unsigned int Lstat){ _mmst.resize(Lstat); this-> init_smooth(); }
	inline void init_smooth(){ _msb = 0; _mmst = 0; }

	inline void add_m_matstate( const MathVec<Scalar>& mmstp ){
		if (_mmst.size() == 0) this->init_smooth( mmstp.size() );
		_mmst += mmstp;
	}
	inline void smooth_matstate(){ _mst = _mmst / _mass; }
	inline const MathVec<Scalar>& get_matstate() const { return _mst; }

	inline void add_m_specstress( const GSymmTensor& msbp ){ _msb += msbp; }
	inline void smooth_specstress(){ _sb = _msb / _mass; }
	inline const GSymmTensor& get_specstress() const { return _sb; }

protected:
	GSymmTensor _msb, _sb;
	MathVec<Scalar> _mmst,_mst;
};

#endif /* GNODEFLOW_H_ */
