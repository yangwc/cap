#include "_mpmnode_base.h"

_mpmnode_base::_mpmnode_base( const IntGVector& id, const GVector& coord ) {
	_id = id;
	_coord = coord;
	_obj_bc = 0;
	_tol_precision = GLOBAL_TOLERANCE_PRECISION;
	this-> clean();
}

void _mpmnode_base::clean(){
	_mass = 0;
	_fext.SetZeros();
	_fint.SetZeros();
	_fbc.SetZeros();
	_momt.SetZeros();

//	_acc.SetZeros();
//	_vel.SetZeros();
//	_velo.SetZeros();
//	_dvel.SetZeros();
//	_disp.SetZeros();
}

void _mpmnode_base::solve(const Scalar& tnow, const Scalar& dt){
	if ( _mass == 0 ) return ;

	if ( _mass < 0 ) {
		std::cout<<"my mass < 0: "<<_mass<<std::endl;
		return ;
	}

	_acc  = _fint + _fext ;

	if ( _obj_bc != 0 ){
//		std::cout<<"*** ";
//		this->_coord.Print();
		_obj_bc-> apply_boundary(tnow, dt, _fbc);
		_acc += _fbc;
	}

	_acc /= _mass;
	_vel  = _momt/_mass;

	_acc.admin_dumperror(_tol_precision* std::max( (_vel.GetNorm()/dt), (_fint.GetNorm()/_mass) ) );
	_dvel = _acc*dt;

	_disp = dt*( _vel + _dvel );
	_vel += _dvel;
}

int _mpmnode_base::get_Nboundary() const {
	int Nbc = 0;
	if (_obj_bc != 0) Nbc = _obj_bc-> get_Nboundary();
	return Nbc;
}
