#ifndef MPMNODEBASE_H_
#define MPMNODEBASE_H_

#include <vector>
#include <string>

#include "../../GB_Tensor.h"
#include "../Boundary/BCTrigger.h"

class _mpmnode_base {
public:
	_mpmnode_base(const IntGVector& id, const GVector& coord);
	virtual ~_mpmnode_base(){ delete _obj_bc; }

	inline void set_bcondition(BCTrigger* bctri){ _obj_bc = bctri; }

	inline void reset_coord(const GVector& coord){_coord = coord;}

	inline void add_mass(const Scalar& m){ _mass += m; }
	inline void add_momt(const GVector& p){ _momt += p; }
	inline void add_fext(const GVector& f){ _fext += f; }
	inline void add_fint(const GVector& f){ _fint += f; }

	virtual void clean();
	virtual void solve(const Scalar& tnow, const Scalar& dt);

	inline const Scalar&  get_mass() const {return _mass;}
	inline const GVector& get_momt() const {return _momt;}
	inline const GVector& get_fint() const {return _fint;}
	inline const GVector& get_fext() const {return _fext;}
	inline const GVector& get_fbc()  const {return _fbc;}

	inline const GVector& get_coord()const {return _coord;}
	inline const GVector& get_vel()  const {return _vel;}
	inline const GVector& get_acc()  const {return _acc;}

	inline const GVector& get_disp() const {return _disp;}
	inline const GVector& get_dvel() const {return _dvel;}

	int get_Nboundary() const;
	inline const IntGVector& get_id() const {return _id;}

protected:
	IntGVector _id;

	Scalar _mass, _tol_precision;
	GVector _momt, _fext, _fint, _fbc;
	GVector _coord, _vel, _acc, _disp, _dvel;

	BCTrigger* _obj_bc;
};

#endif /* MPMNODEBASE_H_ */
