#ifndef GNODE_H_
#define GNODE_H_


#include "_mpmnode_base.h"
#include "../Shape/Shape.h"

class Gnode : public _mpmnode_base {
protected:
	typedef ShpSKH::skeytype SKtype;

public:
	Gnode(const IntGVector& id, const GVector& coord, ShpSKH* skhptr) : _mpmnode_base(id, coord){
		_shpkey = skhptr;
	}
	virtual ~Gnode(){ delete _shpkey; }

	// ****************************************** //
	// 1. Geometry info ------------------------- //
	// ****************************************** //
public:
	inline void update_shape( const SKtype& akey){ _shpkey->add_key(akey); }
	inline bool is_shaped(){return _shpkey->if_shaped(); } // also used in contact
	inline SKtype get_shapekey(){ return _shpkey->get_key(); }
protected:
	ShpSKH* _shpkey;

	// ****************************************** //
	// 2. Contact algorithm in the literature --- //
	// ****************************************** //
public:
//	inline void update_n(const GVector& n){ _n += n; }
	inline const GVector& get_n(){ return _n; }
//	void clean();
protected:
	GVector _n;

	// ****************************************** //
	// 3. Time integration scheme --------------- //
	// ****************************************** //
//public:
//	inline void set_theta(const Scalar& val){_theta = val;}

//protected:
//	Scalar _theta;

	// ****************************************** //
	// Major part of original MPM --------------- //
	// ****************************************** //
//public:
	// n @ 2
//	void clean();
	// _disp = dt*(_vel + (_theta*_dvel)); @ 3
//	void solve(const Scalar& tnow, const Scalar& dt);
};

#endif /* GNODE_H_ */
