#include "SMPMelemO0FluxSm.h"

/********************************************
 * general data for smoothing and anti-locking
 ********************************************/
int SMPMelemO0FluxSm::local_smooth_preparation(){
	if (_Npt == 0) return 0;

	this-> refresh_active_neighbor();

	Scalar zero = 0;
	_flux_coeff.assign( zero, _Nneighbor);
	_dxedge.assign( zero, _Nneighbor);

	_i_need_slope_msta = true;

	_me  = _lns_pt[0]-> get_mass();
	_xpo = _lns_pt[0]-> get_coord();

	if (_Npt == 1) return 0;

	_xpo *= _me;
	for (unsigned int Ip=1; Ip != _Npt; ++Ip){
		const Scalar& mp = _lns_pt[Ip]-> get_mass();
		_me  += mp;
		_xpo += _lns_pt[Ip]-> get_coord()*mp;
	}
	_xpo /= _me;

	return 0;
}

int SMPMelemO0FluxSm::smooth_preparation(){
	if (_Npt == 0) return 0;

	Scalar clayer = 1; clayer /= _Nneighbor;

	unsigned int Inb, Idir;
	for (unsigned int Iside=0; Iside != 2; ++Iside){
		for(Idir=0; Idir != 3; ++Idir){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				const SMPMelemO0FluxSm& elem = *(this-> access_neighbor(Inb));
				_flux_coeff[Inb]  = ( elem._me/ (elem._me + _me) );
				_dxedge[Inb]      = (1-_flux_coeff[Inb])*( elem._xpo[Idir] - _xpo[Idir] );
				_flux_coeff[Inb] *= clayer;
			}
		}
	}
	return 0;
}

/********************************************
 * material states
 ********************************************/
int SMPMelemO0FluxSm::local_smooth_matstate(){
	if (_Npt == 0) return 0;

	_lns_pt[0]-> get_state( _mstao.stdvector() );

	if (_Npt == 1) return 0;

	_mstao *= _lns_pt[0]-> get_mass();

	MathVec<Scalar> mmsta;
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_lns_pt[Ipt]-> get_state( mmsta.stdvector() );
		mmsta  *= _lns_pt[Ipt]-> get_mass();
		_mstao += mmsta;
	}
	_mstao /= _me;
	return 0;
}

int SMPMelemO0FluxSm::smooth_matstate(){
	if (_Npt == 0) return 0;

	MathVec<Scalar> mstao, msta, dmsta;
	mstao.assign( _mstao );

	unsigned int Inb, Iside;
	for (unsigned int Idir=0; Idir != 3; ++Idir){
		for (Iside=0; Iside != 2; ++Iside){
			Inb = this-> get_neighbor_index(Idir, Iside);
			if ( _iacts_neighbor[Inb] ){
				SMPMelemO0FluxSm& elem = *( this-> access_neighbor(Inb) );
				elem.get_edge_matstate(Idir, 1-Iside, dmsta);
				this-> get_edge_matstate(Idir, Iside, msta);
				dmsta -= msta;
				dmsta.bw_mindis( elem._mstao - _mstao );
				mstao += dmsta*_flux_coeff[Inb];
			}
		}
	}

	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> reset_state( mstao.stdvector() );

	return 0;
}


void SMPMelemO0FluxSm::update_slope_matstate(){
	bool iactl, iactr;
	unsigned int Inbl, Inbr;
	for(unsigned int Idir=0; Idir != 3; ++Idir){
		MathVec<Scalar>& muse = _msta1[Idir];

		Inbl = this-> get_neighbor_index( Idir, 0);
		Inbr = this-> get_neighbor_index( Idir, 1);
		iactl = _iacts_neighbor[Inbl];
		iactr = _iacts_neighbor[Inbr];

		if ( ! (iactl || iactr)  ){
			muse.zeros( _mstao.size() );
		}
		else if ( iactl && iactr  ){
			const SMPMelemO0FluxSm& eleml = *( this-> access_neighbor(Inbl) );
			const SMPMelemO0FluxSm& elemr = *( this-> access_neighbor(Inbr) );
			muse.assign( elemr._mstao );
			muse -= eleml._mstao;
			muse /= ( elemr._xpo[Idir] - eleml._xpo[Idir] );
		}
		else if ( iactl ){
			const SMPMelemO0FluxSm& eleml = *( this-> access_neighbor(Inbl) );
			muse.assign( _mstao );
			muse -= eleml._mstao;
			muse /= ( _xpo[Idir] - eleml._xpo[Idir] );
		}
		else{
			const SMPMelemO0FluxSm& elemr = *( this-> access_neighbor(Inbr) );
			muse.assign( elemr._mstao );
			muse -= _mstao;
			muse /= ( elemr._xpo[Idir] - _xpo[Idir] );
		}
	}
}

MathVec<Scalar>& SMPMelemO0FluxSm::get_slope_matstate(unsigned int Idir, unsigned int Iside){
	if( _i_need_slope_msta ){
		this-> update_slope_matstate();
		_i_need_slope_msta = false;
	}
	return _msta1[Idir];
}

void SMPMelemO0FluxSm::get_edge_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& msta){
	unsigned int Inb = this-> get_neighbor_index(Idir, Iside);
	const MathVec<Scalar>& msta1 = this-> get_slope_matstate(Idir, Iside);
	msta.assign( _mstao );
	msta += msta1*_dxedge[Inb];
}

/********************************************
 * specific stress
 ********************************************/
inline int SMPMelemO0FluxSm::local_smooth_specstress(){
	if (_Npt == 0) return 0;

	_sbo = _lns_pt[0]-> get_mat_specstress();

	if (_Npt == 1) return 0;

	_sbo *= _lns_pt[0]-> get_mass();
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_sbo += _lns_pt[Ipt] -> get_mat_specstress()*_lns_pt[Ipt]-> get_mass();
	}
	_sbo /= _me;
	return 0;
}

int SMPMelemO0FluxSm::smooth_specstress(){
	if (_Npt == 0) return 0;

	int Istat = this-> local_smooth_specstress();

	// update assumed specific stress.
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> set_assumed_specstress(_sbo);

	return Istat;
}
