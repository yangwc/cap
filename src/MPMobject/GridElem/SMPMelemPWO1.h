/**************************************************
 * SMPMelemPWO1.h
 * 	   - SMPMelemPWO1()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised linear function
 *  + g(x,y,z) = go + g1(x-xo) + g2(y-yo) + g3(z-zo)
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.11.16)
 **************************************************/

#ifndef SMPMELEMPWO1_H_
#define SMPMELEMPWO1_H_

#include "MPMelem.h"


class SMPMelemPWO1 : public MPMelem{
private:
	typedef Gnode Node;

public:
	SMPMelemPWO1(ShpUnit* shp) : MPMelem(shp){;}
	~SMPMelemPWO1(){;}

	int update_pt_deformation(const Scalar& tnow, const Scalar& dt){
		// status index, 0 means no bad happened.
		int Istat = 0;

		//nothing need to be done when there's no particle in the element.
		if (_Npt == 0) return Istat;

		// update particle deformation with velocity strain.
		Istat = MPMelem::update_pt_deformation(tnow, dt);
		if (Istat != 0) return Istat;

		// nothing need to be smoothed when there's only one particle in the element.
		if (_Npt == 1) return Istat;

		// prepare to smooth
		Istat = this -> smooth_preparation();
		if (Istat != 0) return Istat;

		// start smoothing material states.
		Istat = this -> smooth_mat_state_field();
		if (Istat != 0) return Istat;

		// start smoothing specific stress, sigma bar, field.
		Istat = this -> smooth_specstress_field();

		return Istat;
	}

protected:
	std::vector< GVector > _xis_pt;

	inline void allocate_ptvecs(){
		_mpmelem_base<Gnode, Particle>::allocate_ptvecs();
		unsigned int Nallocate = 1;
		_xis_pt.insert( _xis_pt.end(), Nallocate, GVector() );
	}
	inline void extract_ptdata( int Ipt, const GVector& ptxi ){
		_mpmelem_base<Gnode, Particle>::extract_ptdata(Ipt, ptxi);
		_xis_pt[Ipt] = ptxi;
	}

	Scalar _me;
	GVector _xipo, _Ixio;
	IntGVector _i0Io;

	inline int smooth_preparation(){
		_me   = _lns_pt[0]->get_mass();
		_xipo = _xis_pt[0];
		_i0Io = 1;

		if (_Npt == 1) return 0;

		_xipo *= _me;
		_Ixio  = ( _xis_pt[0]*_xipo );

		GVector mxi;
		for (unsigned int Ip=1; Ip != _Npt; ++Ip){
			const Scalar& mp  = _lns_pt[Ip]->get_mass();
			const GVector& xip = _xis_pt[Ip];
			mxi    = ( xip*mp );
			_me   += mp;
			_xipo += mxi;
			_Ixio += xip*mxi;
		}
		GVector tol = _Ixio*_tol_precision;
		_Ixio -= _xipo*_xipo/_me;
		_xipo /= _me;

		for (unsigned int Id=0; Id != 3; ++Id){
			if ( _Ixio[Id] > tol[Id] ){
				_i0Io.SetComp( Id ,0);
			}
			else{
				_Ixio.SetComp( Id ,0);
			}
		}

		return 0;
	}

	inline int smooth_mat_state_field(){
		MathVec<Scalar> _mstao;
		std::vector< MathVec<Scalar> > _msta1;

		_msta1.assign(3, MathVec<Scalar>() );

		_lns_pt[0]-> get_state( _mstao.stdvector() );
		// _msta1 will not be used if _Npt = 1

		/*********************************
		 * nothing need to be done when
		 * there's only one point.
		 *********************************/
		if (_Npt == 1) return 0;

		/*********************************
		 * prepare for smoothing
		 *********************************/
		unsigned int Idir;

		MathVec<Scalar> erro;
		std::vector< MathVec<Scalar> > err1(3);

		_mstao *= _lns_pt[0]-> get_mass();
		erro.assign( _mstao );
		erro.bw_abs();

		const GVector& xip = _xis_pt[0];
		for ( Idir = 0; Idir != 3; ++Idir ){
			if (_i0Io[Idir] == 0 ){
				_msta1[Idir].assign( _mstao*xip[Idir] );
				MathVec<Scalar>& e1 = err1[Idir];
				e1.assign( _msta1[Idir] );
				e1.bw_abs();
			}
		}

		/*********************************
		 * looping over particles
		 *********************************/
		MathVec<Scalar> mmsta;
		for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
			_lns_pt[Ipt] -> get_state( mmsta.stdvector() );
			mmsta  *= _lns_pt[Ipt]->get_mass();
			_mstao += mmsta;
			erro.bw_maxabs( _mstao );

			const GVector& xip = _xis_pt[Ipt];
			for ( Idir = 0; Idir != 3; ++Idir ){
				if ( _i0Io[Idir] == 0 ){
					_msta1[Idir] += mmsta*xip[Idir];
					err1[Idir].bw_maxabs( _msta1[Idir] );
				}
			}
		}

		/*********************************
		 * get vo and v1 and drop error
		 *********************************/
		erro *= _tol_precision;
	//	_mstao.bw_cutout(erro);
		for ( Idir = 0; Idir != 3; ++Idir ){
			if (_i0Io[Idir] == 0 ){
				MathVec<Scalar>& m1 = _msta1[Idir];
	//			MathVec<Scalar>& e1 = err1[Idir];
				m1 -= _mstao*_xipo[Idir];
	//			e1 *= _tol_precision;
	//			m1.bw_cutout( e1 );
				m1 /= _Ixio[Idir];
			}
		}
		_mstao /= _me;


//		int aa;
//		std::cin>>aa;
		// reset original material states with this smoothed (averaged) field.
		MathVec<Scalar>& msta = mmsta;
		for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt){
			const GVector& xip = _xis_pt[Ipt];
			msta = _mstao;
			for ( Idir = 0; Idir != 3; ++Idir ){
				if (_i0Io[Idir] == 0 ) msta += _msta1[Idir] *( xip[Idir] - _xipo[Idir] );
			}
			_lns_pt[Ipt]->reset_state( msta.stdvector() );
		}
		return 0;
	}

	inline int smooth_specstress_field(){
		std::vector< GSymmTensor > _sb1;
		_sb1.assign(3, GSymmTensor() );
		GSymmTensor _sbo;

		_sbo = _lns_pt[0]-> get_mat_specstress();
		// _sb1 will not be used if _Npt = 1

		/*********************************
		 * nothing need to be done when
		 * there's only one point.
		 *********************************/
		if (_Npt == 1) return 0;

		/*********************************
		 * prepare for smoothing
		 *********************************/
		unsigned int Idir;

		_sbo *= _lns_pt[0]-> get_mass();

		const GVector& xip = _xis_pt[0];
		for ( Idir = 0; Idir != 3; ++Idir ){
			if (_i0Io[Idir] == 0 ) _sb1[Idir] = _sbo*xip[Idir];
		}

		/*********************************
		 * looping over particles
		 *********************************/
		GSymmTensor msb;
		for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
			msb = _lns_pt[Ipt]-> get_mat_specstress() * _lns_pt[Ipt]-> get_mass();
			_sbo += msb;
			const GVector& xip = _xis_pt[Ipt];
			for ( Idir = 0; Idir != 3; ++Idir ){
				if ( _i0Io[Idir] == 0 ) _sb1[Idir] += msb*xip[Idir];
			}
		}

		/*********************************
		 * get vo and v1
		 *********************************/
		for ( Idir = 0; Idir != 3; ++Idir ){
			if (_i0Io[Idir] == 0 ){
				GSymmTensor& m1 = _sb1[Idir];
				m1 -= _sbo*_xipo[Idir];
				m1 /= _Ixio[Idir];
			}
		}
		_sbo /= _me;

		// update assumed specific stress.
		GSymmTensor& sb = msb;
		for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt){
			const GVector& xip = _xis_pt[Ipt];
			sb = _sbo;
			for ( Idir = 0; Idir != 3; ++Idir ){
				if (_i0Io[Idir] == 0 ) sb += _sb1[Idir] *( xip[Idir] - _xipo[Idir] );
			}
			_lns_pt[Ipt]->set_assumed_specstress(sb);
		}
		return 0;
	}
};

#endif /* SMPMELEMPWO0_H_ */
