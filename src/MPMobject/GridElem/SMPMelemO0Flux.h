/**************************************************
 * SMPMelemO0Flux.h
 * 	   - SMPMelemO0Flux()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised constant function
 *  + constant flux
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO0FLUX_H_
#define SMPMELEMO0FLUX_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"


class SMPMelemO0Flux : public _neighbor3d<SMPMelemO0Flux>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO0Flux(ShpUnit* shp): _mpmelem_base(shp) {;}
	virtual ~SMPMelemO0Flux(){;}

	int local_smooth_preparation();
	int smooth_preparation();
	int local_smooth_matstate();
	int smooth_matstate();

	int smooth_specstress();

protected:
	int local_smooth_specstress();
	// for flux
	MathVec<Scalar> _flux_coeff, _mstao;
	GSymmTensor _sbo;
	Scalar _me;
};

#endif /* SMPMELEMO0FLUX_H_ */
