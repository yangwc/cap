/**************************************************
 * SMPMelemPWO0.h
 * 	   - SMPMelemPWO0()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised constant function
 *  + Identical to the cell-based anti-locking
 *    algorithm methioned in Carter et. al (2012)
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.11.16)
 **************************************************/

#ifndef SMPMELEMPWO0_H_
#define SMPMELEMPWO0_H_

#include "MPMelem.h"

class SMPMelemPWO0 : public MPMelem{
public:
	SMPMelemPWO0(ShpUnit* shp) : MPMelem(shp){;}
	~SMPMelemPWO0(){;}

	int update_pt_deformation(const Scalar& tnow, const Scalar& dt){
		// status index, 0 means no bad happened.
		int Istat = 0;

		//nothing need to be done when there's no particle in the element.
		if (_Npt == 0) return Istat;

		// update particle deformation with velocity strain.
		Istat = MPMelem::update_pt_deformation(tnow, dt);
		if (Istat != 0) return Istat;

		// nothing need to be smoothed when there's only one particle in the element.
		if (_Npt == 1) return Istat;

		// start smoothing material states.
		Istat = this -> smooth_mat_state_field();
		if (Istat != 0) return Istat;

		// start smoothing specific stress, sigma bar, field.
		Istat = this -> smooth_specstress_field();

		return Istat;
	}

protected:
	inline int smooth_mat_state_field(){
		// construct constant field using average values.
		MathVec<Scalar> mstao;
		_lns_pt[0] -> get_state( mstao.stdvector() );

		std::vector<Scalar> msta;
		unsigned int Ipt;
		for (Ipt = 1; Ipt != _Npt; ++Ipt){
			_lns_pt[Ipt] -> get_state(msta);
			mstao += msta;
		}
		mstao /= _Npt;

		// reset original material states with this smoothed (averaged) field.
		for (Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]->reset_state( mstao.stdvector() );

		return 0;
	}

	inline int smooth_specstress_field(){
		// construct constant field using average values.
		GSymmTensor sbo;
		unsigned int Ipt;
		for (Ipt = 0; Ipt != _Npt; ++Ipt){
			sbo += _lns_pt[Ipt] -> get_mat_specstress();
		}
		sbo /= _Npt;

		// update assumed specific stress.
		for (Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]->set_assumed_specstress(sbo);

		return 0;
	}
};

#endif /* SMPMELEMPWO0_H_ */
