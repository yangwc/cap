/**************************************************
 * SMPMelemO0FluxO1E.h
 * 	   - SMPMelemO0FluxO1E()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised constant function
 *  + constant flux
 *  + linear limiter (slope of secant line)
 *  + tuning coefficient
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO0FLUXO1E_H_
#define SMPMELEMO0FLUXO1E_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"

class SMPMelemO0FluxO1E : public _neighbor3d<SMPMelemO0FluxO1E>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO0FluxO1E(ShpUnit* shp, Scalar camp = 1.1): _mpmelem_base(shp) {
		_msta1.assign(3, MathVec<Scalar>() );
		_camp = std::max( 0.0, camp );
	}
	virtual ~SMPMelemO0FluxO1E(){;}

	int local_smooth_preparation();
	int smooth_preparation();
	int local_smooth_matstate();
	int smooth_matstate();

	int smooth_specstress();

protected:
	void update_correction_matstate();
	void get_correction_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& dmsta);

	int local_smooth_specstress();

	MathVec<Scalar> _flux_coeff, _dxedge, _mstao;
	GSymmTensor _sbo;

	std::vector< MathVec<Scalar> > _msta1;

	Scalar _me, _camp;
	GVector _xpo;

	bool _i_need_correction_msta;
};

#endif /* SMPMELEMO0FLUXO1E_H_ */
