#include "SMPMelemO0FluxO2E.h"

/********************************************
 * general data for smoothing and anti-locking
 ********************************************/
int SMPMelemO0FluxO2E::local_smooth_preparation(){
	if (_Npt == 0) return 0;

	this-> refresh_active_neighbor();

	Scalar zero = 0;
	_flux_coeff.assign( zero, _Nneighbor);
	_dxedge.assign( zero, _Nneighbor);

	_i_need_correction_msta = true;

	_me  = _lns_pt[0]-> get_mass();
	_xpo = _lns_pt[0]-> get_coord();
	_llo.SetZeros();

	if (_Npt == 1) return 0;

	GVector mx;
	_llo  = _xpo*_xpo*_me;
	_xpo *= _me;
	for (unsigned int Ip=1; Ip != _Npt; ++Ip){
		const Scalar&  mp = _lns_pt[Ip]-> get_mass();
		const GVector& xp = _lns_pt[Ip]-> get_coord();
		mx    = mp*xp;
		_me  += mp;
		_xpo += mx;
		_llo += mx*xp;
	}
	_xpo /= _me;
	_llo /= _me;
	_llo -= _xpo*_xpo;

	return 0;
}

int SMPMelemO0FluxO2E::smooth_preparation(){
	if (_Npt == 0) return 0;

	Scalar clayer = 1; clayer /= _Nneighbor;

	unsigned int Inb, Idir;
	for (unsigned int Iside=0; Iside != 2; ++Iside){
		for(Idir=0; Idir != 3; ++Idir){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				const SMPMelemO0FluxO2E& elem = *(this-> access_neighbor(Inb));
				_flux_coeff[Inb]  = ( elem._me/ (elem._me + _me) );
				_dxedge[Inb]      = (1-_flux_coeff[Inb])*( elem._xpo[Idir] - _xpo[Idir] );
				_flux_coeff[Inb] *= clayer;
			}
		}
	}

	return 0;
}

/********************************************
 * material states
 ********************************************/
int SMPMelemO0FluxO2E::local_smooth_matstate(){
	if (_Npt == 0) return 0;

	_lns_pt[0]-> get_state( _mstao.stdvector() );

	if (_Npt == 1) return 0;

	_mstao *= _lns_pt[0]-> get_mass();

	MathVec<Scalar> mmsta;
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_lns_pt[Ipt]-> get_state( mmsta.stdvector() );
		mmsta  *= _lns_pt[Ipt]-> get_mass();
		_mstao += mmsta;
	}
	_mstao /= _me;
	return 0;
}

int SMPMelemO0FluxO2E::smooth_matstate(){
	if (_Npt == 0) return 0;

	MathVec<Scalar> mstao, cdmsta, dmstao;
	MathVec<Scalar>& cdmsta2 = dmstao; // order of calculation matters or declare a new object.

	mstao.assign( _mstao );

	unsigned int Inb, Iside;
	for (unsigned int Idir=0; Idir != 3; ++Idir){
		for (Iside=0; Iside != 2; ++Iside){
			Inb = this-> get_neighbor_index(Idir, Iside);
			if ( _iacts_neighbor[Inb] ){
				SMPMelemO0FluxO2E& elem = *( this-> access_neighbor(Inb) );

				elem.get_correction_matstate(Idir, 1-Iside, cdmsta);
				this-> get_correction_matstate(Idir, Iside, cdmsta2);
				cdmsta -= cdmsta2;
				cdmsta *= _camp;

				dmstao.assign( elem._mstao );
				dmstao -= _mstao;

				cdmsta += dmstao;

				cdmsta.bw_mindis( dmstao );
				mstao += cdmsta*_flux_coeff[Inb];
			}
		}
	}

	// update assumed specific stress.
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> reset_state( mstao.stdvector() );

	return 0;
}

inline void SMPMelemO0FluxO2E::update_correction_matstate(){
	 // status of update
	if( _i_need_correction_msta ) _i_need_correction_msta = false;
	else return;

	unsigned int Idir, Inbl, Inbr, Nv = _mstao.size();
	bool iactl, iactr;

	MathVec<Scalar> mr, ml;
	Scalar dxr, dxl, Lr, Ll;

	std::vector< SMPMelemO0FluxO2E* > i_refresh(3);
	for( Idir=0; Idir != 3; ++Idir){ // update curvature and slope of cells
		MathVec<Scalar>& muse = _msta1[Idir];
		MathVec<Scalar>& ause = _msta2[Idir];

		Inbl = this-> get_neighbor_index( Idir, 0);
		Inbr = this-> get_neighbor_index( Idir, 1);
		iactl = _iacts_neighbor[Inbl];
		iactr = _iacts_neighbor[Inbr];

		i_refresh[Idir] = 0;

		if ( iactl && iactr  ){
			const SMPMelemO0FluxO2E& eleml = *( this-> access_neighbor(Inbl) );
			const SMPMelemO0FluxO2E& elemr = *( this-> access_neighbor(Inbr) );

            dxr = (elemr._xpo[Idir] - _xpo[Idir]);
            dxl = (eleml._xpo[Idir] - _xpo[Idir]);
            mr.assign(elemr._mstao); mr -= _mstao; mr /= dxr;
            ml.assign(eleml._mstao); ml -= _mstao; ml /= dxl;
            Lr = (elemr._llo[Idir] - _llo[Idir]) / dxr;
            Ll = (eleml._llo[Idir] - _llo[Idir]) / dxl;

            ause.assign(mr); ause -= ml; ause /= (dxr - dxl + Lr - Ll);

            mr -= ause*Lr;
            ml -= ause*Ll;

            muse.assign(ause); muse *= (-dxr-dxl); muse += mr; muse += ml; muse *= 0.5;
		}
		else if ( iactl ){
			i_refresh[Idir] = this-> access_neighbor(Inbl);
			SMPMelemO0FluxO2E& eleml = *i_refresh[Idir];
			muse.assign( _mstao );
			muse -= eleml._mstao;
			muse /= ( _xpo[Idir] - eleml._xpo[Idir] );
			ause.zeros( Nv );
		}
		else if ( iactr ){
			i_refresh[Idir] = this-> access_neighbor(Inbr);
			SMPMelemO0FluxO2E& elemr = *i_refresh[Idir];
			muse.assign( elemr._mstao );
			muse -= _mstao;
			muse /= ( elemr._xpo[Idir] - _xpo[Idir] );
			ause.zeros( Nv );
		}
		else { // ( not (iactl || iactr)  )
			muse.zeros( Nv );
			ause.zeros( Nv );
		}
	}

	for( Idir=0; Idir != 3; ++Idir){ // update curvature for edge cells
		if ( i_refresh[Idir] != 0){
			Scalar& dx = dxr; Scalar& L = Lr;

			MathVec<Scalar>& muse = _msta1[Idir];
			MathVec<Scalar>& ause = _msta2[Idir];
			SMPMelemO0FluxO2E& elemnb = *i_refresh[Idir];
			elemnb.update_correction_matstate();

			ause.assign( elemnb._msta2[Idir] );

			dx = (elemnb._xpo[Idir] - _xpo[Idir]);
	        L  = (elemnb._llo[Idir] - _llo[Idir]) / dx;

	        muse -= ause*(dx + L);
		}
	}
}

inline void SMPMelemO0FluxO2E::get_correction_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& dmsta){
	this-> update_correction_matstate();
	unsigned int Inb = this-> get_neighbor_index(Idir, Iside);
	const MathVec<Scalar>& m = _msta1[Idir];
	const MathVec<Scalar>& a = _msta2[Idir];
	const Scalar& dx = _dxedge[Inb];
	dmsta.assign( m ); dmsta *= dx;
	dmsta += a*( dx*dx - _llo[Idir] );
}


/********************************************
 * specific stress
 ********************************************/
inline int SMPMelemO0FluxO2E::local_smooth_specstress(){
	if (_Npt == 0) return 0;

	_sbo = _lns_pt[0]-> get_mat_specstress();

	if (_Npt == 1) return 0;

	_sbo *= _lns_pt[0]-> get_mass();
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_sbo += _lns_pt[Ipt] -> get_mat_specstress()*_lns_pt[Ipt]-> get_mass();
	}
	_sbo /= _me;
	return 0;
}

int SMPMelemO0FluxO2E::smooth_specstress(){
	if (_Npt == 0) return 0;

	int Istat = this-> local_smooth_specstress();

	// update assumed specific stress.
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> set_assumed_specstress(_sbo);

	return Istat;
}
