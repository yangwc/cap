/**************************************************
 * _mpmelem_base.h
 * 	   - _mpmelem_base()
 * ------------------------------------------------
 *  + Original MPM element.
 * 	+ Adaptor between particles and Nodes.
 * 	+ For lumped (weighted average) Node model.
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (16.05.11)
 **************************************************/

#ifndef MPMELEMBASE_H_
#define MPMELEMBASE_H_

#include "_elem_base.h"

template<typename Node, typename Particle>
class _mpmelem_base : public _elem_base<Node, Particle> {
private:
	typedef typename std::vector<Node*> NVec;
	typedef typename NVec::iterator NVite;

public:
	_mpmelem_base(ShpUnit* shp);
	virtual ~_mpmelem_base(){;}

	void map_to_node_ex();
	void map_to_node_in();

	int update_pt_kinematics(const Scalar& tnow, const Scalar& dt);
	int update_pt_deformation(const Scalar& tnow, const Scalar& dt);

protected:
	virtual void allocate_ptvecs();
	virtual void extract_ptdata( int Ipt, const GVector& ptxi );

	std::vector< std::vector<GVector> > _gNxis_pt, _mdgNxis_pt;
	std::vector< std::vector<Scalar> > _Ns_pt, _mdNs_pt;
//	std::vector< std::vector<Scalar> > _shpdat_pt;

	Scalar _tol_precision;
};

template<typename Node, typename Particle>
_mpmelem_base<Node,Particle>::_mpmelem_base( ShpUnit* shp ) : _elem_base<Node, Particle>(shp) {
	_tol_precision = GLOBAL_TOLERANCE_PRECISION;
}
//template<typename Node, typename Particle>
//void _mpmelem_base<Node,Particle>::map_to_node(){
//	if ( _elem_base<Node, Particle>::_Npt == 0) return;
//
//	unsigned int Ind = 0;
//	for (NVite ivec = _elem_base<Node, Particle>::_lns_nd.begin(); ivec != _elem_base<Node, Particle>::_lns_nd.end(); ++ivec){
//		(*ivec)->update_shape( _elem_base<Node, Particle>::_shape->get_shapekey(Ind) );
//		Ind++;
//	}
//
//	GVector ndfi, ndfe;
//	std::vector<GVector> gNs, gNxis;
//	std::vector<Scalar> Ns;
//	Scalar mN;
//	for (unsigned int Ipt=0; Ipt != _elem_base<Node, Particle>::_Npt; ++Ipt){
//		Particle* ptptr = _elem_base<Node, Particle>::_lns_pt[Ipt];
//		_elem_base<Node, Particle>::_shape -> get_modN( _shpdat_pt[Ipt], Ns, gNxis);
//		_elem_base<Node, Particle>::_shape -> get_GNo( gNxis, gNs);
//
//		const Scalar ptm = ptptr->get_mass();
//		const GVector& ptv = ptptr->get_velocity();
//		const GVector& ptb = ptptr->get_bodyforce();
//		const GSymmTensor pts = ptptr->get_specstress();
//
//		for (unsigned int Ind=0; Ind != _elem_base<Node, Particle>::_Nnd; ++Ind){
//			mN = Ns[Ind]*ptm;
//			const GVector& gN = gNs[Ind];
//			Node* gdptr = _elem_base<Node, Particle>::_lns_nd[Ind];
//
//			gdptr->add_mass( mN );
//			gdptr->add_momt( ptv*mN );
//			ndfi = -ptm*(pts*gN);
//			ndfe =  ptb*mN;
//			gdptr->add_fext(ndfe);
//			gdptr->add_fint(ndfi);
//	//		gdptr->update_n(gN);
//		}
//	}
//}
template<typename Node, typename Particle>
void _mpmelem_base<Node,Particle>::map_to_node_ex(){
	if ( _elem_base<Node, Particle>::_Npt == 0) return;

	unsigned int Ind = 0;
	for (NVite ivec = _elem_base<Node, Particle>::_lns_nd.begin(); ivec != _elem_base<Node, Particle>::_lns_nd.end(); ++ivec){
		(*ivec)->update_shape( _elem_base<Node, Particle>::_shape->get_shapekey(Ind) );
		Ind++;
	}


	Scalar mN;
	for (unsigned int Ipt=0; Ipt != _elem_base<Node, Particle>::_Npt; ++Ipt){
		Particle* ptptr = _elem_base<Node, Particle>::_lns_pt[Ipt];

		const std::vector<Scalar>& Ns = _mdNs_pt[Ipt];

		const Scalar ptm = ptptr->get_mass();
		const GVector& ptv = ptptr->get_velocity();
		const GVector& ptb = ptptr->get_bodyforce();

		for (unsigned int Ind=0; Ind != _elem_base<Node, Particle>::_Nnd; ++Ind){
			mN = Ns[Ind]*ptm;

			Node* gdptr = _elem_base<Node, Particle>::_lns_nd[Ind];

			gdptr->add_mass( mN );
			gdptr->add_momt( ptv*mN );
			gdptr->add_fext(ptb*mN);
		}
	}
}
template<typename Node, typename Particle>
void _mpmelem_base<Node,Particle>::map_to_node_in(){
	if ( _elem_base<Node, Particle>::_Npt == 0) return;

	GVector ndfi, vi;
	std::vector<GVector> gNs;

	GTensor gradv;
	unsigned int Ind;
	for (unsigned int Ipt=0; Ipt != _elem_base<Node, Particle>::_Npt; ++Ipt){
		Particle* ptptr = _elem_base<Node, Particle>::_lns_pt[Ipt];

		_elem_base<Node, Particle>::_shape -> get_GNo( _mdgNxis_pt[Ipt], gNs);

		gradv.SetZeros();
		for ( Ind=0; Ind<_elem_base<Node, Particle>::_Nnd; ++Ind){
			Node* ndptr = _elem_base<Node, Particle>::_lns_nd[Ind];
			vi = ndptr -> get_momt() / ndptr -> get_mass();
			gradv += vi & gNs[Ind];
		}

		const Scalar ptm = ptptr->get_mass();
		const GSymmTensor pts = ptptr->get_specstress( _elem_base<Node, Particle>::_shape->get_char_length(), gradv);

		for (Ind=0; Ind != _elem_base<Node, Particle>::_Nnd; ++Ind){
			const GVector& gN = gNs[Ind];
			Node* ndptr = _elem_base<Node, Particle>::_lns_nd[Ind];

			ndptr->add_fint( -ptm*(pts*gN) );
	//		ndptr->update_n(gN);
		}
	}
}
template<typename Node, typename Particle>
int _mpmelem_base<Node,Particle>::update_pt_kinematics(const Scalar& tnow, const Scalar& dt){
	int Istat = 0;
	Particle* ptptr = 0;
	Node* ndptr = 0;
	GVector dv,du;
	for (unsigned int Ipt = 0; Ipt < _elem_base<Node, Particle>::_Npt; ++Ipt){

		const std::vector<Scalar>& Ns = _Ns_pt[Ipt];

		ptptr = _elem_base<Node, Particle>::_lns_pt[Ipt];

		dv.SetZeros(); du.SetZeros();
		for (unsigned int Ind = 0; Ind < _elem_base<Node, Particle>::_Nnd; ++Ind){
			ndptr = _elem_base<Node, Particle>::_lns_nd[Ind];

			const Scalar& N = Ns[Ind];

			dv += ( ndptr->get_dvel() )*N;
			du += ( ndptr->get_disp() )*N;
		}
		Istat = ptptr -> update_kinamatics( dt, du, dv);
		if(Istat != 0) break;
	}// end for (particles)
	return Istat;
}
template<typename Node, typename Particle>
int _mpmelem_base<Node,Particle>::update_pt_deformation(const Scalar& tnow, const Scalar& dt){
	int Istat = 0;

	Node* ndptr;
	unsigned int Ind;
	std::vector<GVector> xnds( _elem_base<Node, Particle>::_Nnd );
	for ( Ind=0; Ind<_elem_base<Node, Particle>::_Nnd; ++Ind){
		ndptr = _elem_base<Node, Particle>::_lns_nd[Ind];
		xnds[Ind] = ndptr->get_coord() + ndptr->get_disp();
	}

	Particle* ptptr;
	GTensor gradv;
	std::vector<GVector> gNs;
	for (unsigned int Ipt = 0; Ipt < _elem_base<Node, Particle>::_Npt; ++Ipt){

		_elem_base<Node, Particle>::_shape -> get_GN( _gNxis_pt[Ipt], xnds, gNs);
//		_elem_base<Node, Particle>::_shape -> get_GNo( _gNxis_pt[Ipt], gNs);

		ptptr = _elem_base<Node, Particle>::_lns_pt[Ipt];

		gradv.SetZeros();
		for ( Ind=0; Ind<_elem_base<Node, Particle>::_Nnd; ++Ind){
			gradv += ( _elem_base<Node, Particle>::_lns_nd[Ind] -> get_vel() )& gNs[Ind];
		}
		Istat = ptptr -> update_configuration( dt, gradv);
		if(Istat != 0) break;
	}// end for (particles)
	return Istat;
}
template<typename Node, typename Particle>
inline void _mpmelem_base<Node,Particle>::allocate_ptvecs(){
	unsigned int Nallocate = 1;
	Particle* ptlink = 0; _elem_base<Node, Particle>::_lns_pt.insert(_elem_base<Node, Particle>::_lns_pt.end(), Nallocate, ptlink);

//	_shpdat_pt.insert(_shpdat_pt.end(), Nallocate, std::vector<Scalar>() );
	_Ns_pt.insert(          _Ns_pt.end(), Nallocate, std::vector<Scalar>() );
	_gNxis_pt.insert(    _gNxis_pt.end(), Nallocate, std::vector<GVector>() );
	_mdNs_pt.insert(      _mdNs_pt.end(), Nallocate, std::vector<Scalar>() );
	_mdgNxis_pt.insert(_mdgNxis_pt.end(), Nallocate, std::vector<GVector>() );
}
template<typename Node, typename Particle>
inline void _mpmelem_base<Node,Particle>::extract_ptdata( int Ipt, const GVector& ptxi ){
	std::vector<Scalar> shpdat_pt;
	_elem_base<Node, Particle>::_shape -> get_shapedata(ptxi, shpdat_pt);
	_elem_base<Node, Particle>::_shape -> get_N(shpdat_pt, _Ns_pt[Ipt], _gNxis_pt[Ipt]);
	_elem_base<Node, Particle>::_shape -> get_modN(shpdat_pt, _mdNs_pt[Ipt], _mdgNxis_pt[Ipt]);
}

#endif /* MPMELEMBASE_H_ */
