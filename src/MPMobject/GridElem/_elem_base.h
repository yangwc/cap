/**************************************************
 * _elem_base.h
 * 	   - _elem_base<Node,Particle>()
 * ------------------------------------------------
 *  +
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.11.16)
 **************************************************/

#ifndef ELEMBASE_H_
#define ELEMBASE_H_

#include <vector>
#include "../Shape/Shape.h"

template<typename Particle>
class _particle_adaptor {
public:
	_particle_adaptor();
	_particle_adaptor(ShpUnit* shp);
	virtual ~_particle_adaptor(){delete _shape; _shape = 0;}

	inline void assign_id(const IntGVector& id){_id = id;}

	int refresh_particles( const Scalar& tnow, std::vector<Particle*>& ln_freepts );
	int add_particle(Particle* pin);

	inline void clean(){ _Npt = 0;}

	inline bool is_empty(){ return _Npt == 0; }

	inline void get_xcorners(std::vector<GVector>& xcorners){ _shape->get_xcorners(xcorners); }

	inline std::vector<Particle*>& _admin_sync_particles(){ return _lns_pt; }
	inline unsigned int _admin_get_Nparticle(){ return _Npt; }
	inline void _admin_reset_Nparticle(unsigned int Npt){ _Npt = Npt; }

protected:
	virtual void allocate_ptvecs();
	virtual void extract_ptdata( int Ipt, const GVector& ptxi ){;}

	IntGVector _id;
	ShpUnit* _shape;

	unsigned int _Npt;
	std::vector<Particle*> _lns_pt;
};

template<typename Node, typename Particle>
class _elem_base : public _particle_adaptor<Particle> {
public:
	_elem_base();
	_elem_base(ShpUnit* shp);
	virtual ~_elem_base(){;}

	int assign_nodelink(unsigned int Ind, Node* ndptr);

protected:
	unsigned int _Nnd;
	std::vector<Node*> _lns_nd;
};

/**************************************************
 * IMPLEMENTAION
 *     to implement a template class in one header
 * file is the easiest and simplest way to compile
 * correctly without any additional setup.
 * ================================================
 * Modified by: YWC (15.11.16)
 **************************************************/
template<typename Particle>
_particle_adaptor<Particle>::_particle_adaptor() {
	_shape = 0;
	_Npt = 0;
}

template<typename Particle>
_particle_adaptor<Particle>::_particle_adaptor( ShpUnit* shp ) {
	_shape = shp;
	_Npt = 0;
}

template<typename Particle>
int _particle_adaptor<Particle>::refresh_particles( const Scalar& tnow, std::vector<Particle*>& ln_freepts ){
	unsigned int Ipt = 0;
	Particle* ptptr  = 0;
	GVector ptxi;
	while (Ipt != _Npt){
		ptptr = _lns_pt[Ipt];
		_shape -> get_mapcoord( ptptr -> get_coord(), ptxi);

		if ( _shape -> if_covered(ptxi) ){
			ptptr -> set_time( tnow );
			this -> extract_ptdata( Ipt, ptxi );
			Ipt++;
		}
		else{
			ln_freepts.push_back( ptptr );
			_Npt--;
			_lns_pt[Ipt] = _lns_pt[_Npt];
		}
	}
	return 0;
}

template<typename Particle>
int _particle_adaptor<Particle>::add_particle(Particle* ptptr){
	_Npt++;
	if ( _Npt > _lns_pt.size() ) this -> allocate_ptvecs();
	unsigned int Ipt = _Npt - 1;

	GVector ptxi; _shape -> get_mapcoord( ptptr -> get_coord(), ptxi);

	int Istat = 0;
	if ( _shape -> if_covered(ptxi) ){
		_lns_pt[Ipt] = ptptr;
		this -> extract_ptdata( Ipt, ptxi );
	}
	else{
		_Npt--;
		Istat = -1;
	}
	return Istat;
}

template<typename Particle>
void _particle_adaptor<Particle>::allocate_ptvecs(){
	unsigned int Nallocate = 1;
	Particle* ptlink = 0; _lns_pt.insert(_lns_pt.end(), Nallocate, ptlink);
}

template<typename Node, typename Particle>
_elem_base<Node,Particle>::_elem_base(){
	_Nnd = 0;
}

template<typename Node, typename Particle>
_elem_base<Node,Particle>::_elem_base( ShpUnit* shp ) : _particle_adaptor<Particle>(shp) {
	_Nnd = _particle_adaptor<Particle>::_shape->get_Nnode();
	_lns_nd.assign(_Nnd, 0);
}

template<typename Node, typename Particle>
int _elem_base<Node,Particle>::assign_nodelink(unsigned int Ind, Node* ndptr){
	if (Ind >= _Nnd) {
		std::cout<< "WARNING: link #"<<Ind<<" is out of range."<<std::endl;
		return -1;
	}
	if (_lns_nd[Ind] != 0){
		std::cout<< "WARNING: link #"<<Ind<<" is overwritten."<<std::endl;
	}
	_lns_nd[Ind] = ndptr;
	return 0;
}

#endif /* ELEMBASE_H_ */
