#include "SMPMelemO1FluxS.h"

/********************************************
 * general data for smoothing and anti-locking
 ********************************************/
int SMPMelemO1FluxS::local_smooth_preparation(){
	if (_Npt == 0) return 0;

	this-> refresh_active_neighbor();

	_flux_coeff.assign( 0.0, _Nneighbor);
	_dxiedge.assign( 0.0, _Nneighbor);
	_osci_coeff.assign( 0.0, _Nneighbor);
	_i_raw_slope_msta = true;

	_me   = _lns_pt[0]->get_mass();
	_xio  = _xis_pt[0];
	_i0Io = 1;

	if (_Npt == 1) return 0;

	_xio  *= _me;
	_Ixio  = (_xis_pt[0]*_xio );

	GVector mxi;
	for (unsigned int Ip=1; Ip != _Npt; ++Ip){
		const Scalar& mp   = _lns_pt[Ip]->get_mass();
		const GVector& xip = _xis_pt[Ip];
		mxi    = ( xip*mp );
		_me   += mp;
		_xio  += mxi;
		_Ixio += xip*mxi;
	}
	GVector tol = _Ixio*_tol_precision;
	_Ixio -= _xio*_xio/_me;
	_xio  /= _me;

	for (unsigned int Id=0; Id != 3; ++Id){
		if ( _Ixio[Id] > tol[Id] ){
			_i0Io.SetComp( Id ,0);
		}
		else{
			_Ixio.SetComp( Id ,0);
		}
	}

	return 0;
}

int SMPMelemO1FluxS::smooth_preparation(){
	if (_Npt == 0) return 0;

	Scalar clayer = 1; clayer /= (2*_Nneighbor);

	GVector Lxi = _Ixio*(_ca/_me); Lxi = Lxi.bw_sqrt();

	unsigned int Inb, Idir;
	for (unsigned int Iside=0; Iside != 2; ++Iside){
		for(Idir=0; Idir != 3; ++Idir){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				const SMPMelemO1FluxS& elem = *(this-> access_neighbor(Inb));
				_flux_coeff[Inb]  = ( elem._me/ ( (_ca+1)*(_me + elem._me) ) );
				_flux_coeff[Inb] *= clayer;
				_dxiedge[Inb]     = ( ( (int) Iside )*2-1 )*Lxi[Idir];
				if((_i0Io[Idir]+elem._i0Io[Idir]) == 0){
					_osci_coeff[Inb]  = ( elem._Ixio[Idir]/ ( (_Ixio[Idir] + elem._Ixio[Idir]) ) );
					_osci_coeff[Inb] *= clayer;
				}
			}
		}
	}

	return 0;
}

/********************************************
 * material states
 ********************************************/
int SMPMelemO1FluxS::local_smooth_matstate(){
	if (_Npt == 0) return 0;

	_lns_pt[0]-> get_state( _mstao.stdvector() );
	// _msta1 will not be used if _Npt = 1

	/*********************************
	 * nothing need to be done when
	 * there's only one point.
	 *********************************/
	if (_Npt == 1) return 0;

	/*********************************
	 * prepare for smoothing
	 *********************************/
	unsigned int Idir;

	MathVec<Scalar> erro;
	std::vector< MathVec<Scalar> > err1(3);

	_mstao *= _lns_pt[0]-> get_mass();
	erro.assign( _mstao );
	erro.bw_abs();

	const GVector& xip = _xis_pt[0];
	for ( Idir = 0; Idir != 3; ++Idir ){
		if (_i0Io[Idir] == 0 ){
			_msta1[Idir].assign( _mstao*xip[Idir] );
			MathVec<Scalar>& e1 = err1[Idir];
			e1.assign( _msta1[Idir] );
			e1.bw_abs();
		}
	}

	/*********************************
	 * looping over particles
	 *********************************/
	MathVec<Scalar> mmsta;
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_lns_pt[Ipt] -> get_state( mmsta.stdvector() );
		mmsta  *= _lns_pt[Ipt]->get_mass();
		_mstao += mmsta;
		erro.bw_maxabs( _mstao );

		const GVector& xip = _xis_pt[Ipt];
		for ( Idir = 0; Idir != 3; ++Idir ){
			if ( _i0Io[Idir] == 0 ){
				_msta1[Idir] += mmsta*xip[Idir];
				err1[Idir].bw_maxabs( _msta1[Idir] );
			}
		}
	}

	/*********************************
	 * get vo and v1 and drop error
	 *********************************/
	erro *= _tol_precision;
	_mstao.bw_cutout(erro);
	for ( Idir = 0; Idir != 3; ++Idir ){
		if (_i0Io[Idir] == 0 ){
			MathVec<Scalar>& m1 = _msta1[Idir];
			MathVec<Scalar>& e1 = err1[Idir];
			m1 -= _mstao*_xio[Idir];
			e1 *= _tol_precision;
			m1.bw_cutout( e1 );
			m1 /= _Ixio[Idir];

			_smsta1[Idir].assign( _msta1[Idir] );
		}
	}
	_mstao /= _me;

	return 0;
}

int SMPMelemO1FluxS::smooth_matstate(){
	if (_Npt == 0) return 0;
	unsigned int Idir;

	/*********************************
	 * prepare for updating
	 *********************************/
	MathVec<Scalar> mstao;
	mstao.assign( _mstao );
	std::vector< MathVec<Scalar> > msta1(3);
	for ( Idir = 0; Idir != 3; ++Idir ){
		if (_i0Io[Idir] == 0 ) msta1[Idir].assign(_msta1[Idir]);
	}

	/*********************************
	 * update vo and v1 (flux)
	 *********************************/
	MathVec<Scalar> msta, dmsta;
	unsigned int Inb;
	for (unsigned int Iside=0; Iside != 2; ++Iside){
		for(Idir=0; Idir != 3; ++Idir){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				SMPMelemO1FluxS& elem = *(this-> access_neighbor(Inb));
				elem.get_edge_matstate(Idir, 1-Iside, dmsta);
				this-> get_edge_matstate(Idir, Iside, msta);
				dmsta -= msta;
				mstao += dmsta*_flux_coeff[Inb];
				if ( _i0Io[Idir] == 0 ){
					msta1[Idir] += dmsta*( _flux_coeff[Inb]*_ca/_dxiedge[Inb] );
				}
			}
		}
	}

	/*********************************
	 * update particle values
	 *********************************/
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt){
		const GVector& xip = _xis_pt[Ipt];
		msta.assign( mstao );
		for(Idir=0; Idir != 3; ++Idir){
			if (_i0Io[Idir] == 0 ) msta += msta1[Idir] *( xip[Idir] - _xio[Idir] );
		}
		_lns_pt[Ipt]->reset_state( msta.stdvector() );
	}
	return 0;
}

int SMPMelemO1FluxS::smooth_oscillation_matstate(){
	/*********************************
	 * update v1
	 *********************************/
	MathVec<Scalar> mm1;
	unsigned int Inb, Iside, Is;

	for(unsigned int Idir=0; Idir != 3; ++Idir){
		if (_i0Io[Idir] == 1 ) continue;
		const MathVec<Scalar>& m1   = _msta1[Idir];
		MathVec<Scalar>& sm1   = _smsta1[Idir];

		for ( Iside=0; Iside != 2; ++Iside){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				SMPMelemO1FluxS& elem = *(this-> access_neighbor(Inb));
				if (elem._i0Io[Idir] == 1 ) continue;

				const MathVec<Scalar>& m1nb = elem._msta1[Idir];

				mm1.assign( m1nb );
				mm1 *= m1;

				Scalar& mcoef = _osci_coeff[Inb];

				for (Is=0; Is != mm1.size(); ++Is){
					if (mm1[Is] < 0){
						sm1[Is] += ( m1nb[Is] - m1[Is] )*mcoef;
					}
				}
			}
		}
	}
	return 0;
}

void SMPMelemO1FluxS::get_edge_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& msta){
	if (_i_raw_slope_msta ){
		_i_raw_slope_msta = false;
		this-> smooth_oscillation_matstate();
	}
	unsigned int Inb = this-> get_neighbor_index( Idir, Iside);
	msta.assign( _mstao );
	if (_i0Io[Idir] == 0 ) msta += _smsta1[Idir]*_dxiedge[Inb];
}

/********************************************
 * specific stress
 ********************************************/
inline int SMPMelemO1FluxS::local_smooth_specstress(){
	if (_Npt == 0) return 0;

	_sbo = _lns_pt[0]-> get_mat_specstress();
	// _sb1 will not be used if _Npt = 1

	/*********************************
	 * nothing need to be done when
	 * there's only one point.
	 *********************************/
	if (_Npt == 1) return 0;

	/*********************************
	 * prepare for smoothing
	 *********************************/
	unsigned int Idir;

	_sbo *= _lns_pt[0]-> get_mass();

	const GVector& xip = _xis_pt[0];
	for ( Idir = 0; Idir != 3; ++Idir ){
		if (_i0Io[Idir] == 0 ) _sb1[Idir] = _sbo*xip[Idir];
	}

	/*********************************
	 * looping over particles
	 *********************************/
	GSymmTensor msb;
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		msb = _lns_pt[Ipt]-> get_mat_specstress() * _lns_pt[Ipt]-> get_mass();
		_sbo += msb;
		const GVector& xip = _xis_pt[Ipt];
		for ( Idir = 0; Idir != 3; ++Idir ){
			if ( _i0Io[Idir] == 0 ) _sb1[Idir] += msb*xip[Idir];
		}
	}

	/*********************************
	 * get vo and v1
	 *********************************/
	for ( Idir = 0; Idir != 3; ++Idir ){
		if (_i0Io[Idir] == 0 ){
			GSymmTensor& m1 = _sb1[Idir];
			m1 -= _sbo*_xio[Idir];
			m1 /= _Ixio[Idir];
		}
	}
	_sbo /= _me;

	return 0;
}

int SMPMelemO1FluxS::smooth_specstress(){
	if (_Npt == 0) return 0;

	this-> local_smooth_specstress();

	/*********************************
	 * update particle values
	 *********************************/
	unsigned int Idir;
	GSymmTensor sb;

	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt){
		const GVector& xip = _xis_pt[Ipt];
		sb = _sbo;
		for(Idir=0; Idir != 3; ++Idir){
			if (_i0Io[Idir] == 0 ) sb += _sb1[Idir] *( xip[Idir] - _xio[Idir] );
		}
		_lns_pt[Ipt]->set_assumed_specstress( sb );
	}
	return 0;
}
