/**************************************************
 * SMPMelemO1FluxS.h
 * 	   - SMPMelemO1FluxS()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised linear function
 *  + linear flux
 *  + oscillation filter
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO1FLUXS_H_
#define SMPMELEMO1FLUXS_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"

class SMPMelemO1FluxS : public _neighbor3d<SMPMelemO1FluxS>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO1FluxS(ShpUnit* shp): _mpmelem_base(shp) {
		_ca = 3;
		_msta1.assign(3, MathVec<Scalar>() );
		_sb1.assign(3, GSymmTensor() );
		_smsta1.assign(3, MathVec<Scalar>() );
	}
	virtual ~SMPMelemO1FluxS(){;}

	int local_smooth_preparation();
	int smooth_preparation();

	int local_smooth_matstate();
	int smooth_matstate();


	int smooth_specstress();

protected:
	inline void allocate_ptvecs(){
		_mpmelem_base<Gnode, Particle>::allocate_ptvecs();
		unsigned int Nallocate = 1;
		_xis_pt.insert( _xis_pt.end(), Nallocate, GVector() );
	}
	inline void extract_ptdata( int Ipt, const GVector& ptxi ){
		_mpmelem_base<Gnode, Particle>::extract_ptdata(Ipt, ptxi);
		_xis_pt[Ipt] = ptxi;
	}

	void get_edge_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& msta);
	int smooth_oscillation_matstate();

	int local_smooth_specstress();
	// for flux
	std::vector< GVector > _xis_pt;
	MathVec<Scalar> _flux_coeff, _osci_coeff, _dxiedge, _mstao;

	std::vector< MathVec<Scalar> > _msta1, _smsta1;
	std::vector< GSymmTensor > _sb1;

	bool _i_raw_slope_msta;

	GSymmTensor _sbo;
	Scalar _me;
	GVector _xio, _Ixio;
	IntGVector _i0Io;

	int _ca;
};

#endif /* SMPMELEMO1FLUXS_H_ */
