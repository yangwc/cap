/**************************************************
 * _neighbor.h
 * 	   - _neighbor()
 * ------------------------------------------------
 *  + a template for any type of objects which
 *    has 6 neighbors in three directions.
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 9, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.11.09)
 **************************************************/
#ifndef NEIGHBOR_H_
#define NEIGHBOR_H_

#include <vector>

template <typename _obj>
class _neighbor3d{
public:
	_neighbor3d(){
		_Nneighbor = 6;
		_obj* ptr = 0;
		_lns_neighbor.assign(_Nneighbor, ptr);
	}
	virtual ~_neighbor3d(){;}
	inline void link_neighbor(unsigned int Idir, unsigned int Iside, _obj* neighbor){
		unsigned int Iadd = this -> get_neighbor_index(Idir, Iside);
		_lns_neighbor[Iadd] = neighbor;
	}
	inline const std::vector<bool>& refresh_active_neighbor(){
		_iacts_neighbor.assign(_Nneighbor,0);
		for(unsigned int Iadd=0; Iadd != _Nneighbor; ++Iadd){
			if( _lns_neighbor[Iadd] != 0 ){
				if( !_lns_neighbor[Iadd] -> is_empty() ) _iacts_neighbor[Iadd] = true;
			}
		}
		return _iacts_neighbor;
	}
	inline _obj* access_neighbor(unsigned int Idir, unsigned int Iside){
		unsigned int Iadd = this -> get_neighbor_index(Idir, Iside);
		return _lns_neighbor[Iadd];
	}
	inline _obj* access_neighbor(unsigned int Iadd){ return _lns_neighbor[Iadd]; }
	inline unsigned int get_neighbor_index(unsigned int Idir, unsigned int Iside) const {
		unsigned int Iadd = 2*Idir + Iside;
		return Iadd;
	}
	inline bool i_active(unsigned int Idir, unsigned int Iside) const {
		unsigned int Iadd = this -> get_neighbor_index(Idir, Iside);
		return _iacts_neighbor[Iadd];
	}
	inline bool i_active(unsigned int Iadd) const { return _iacts_neighbor[Iadd]; }
protected:
	unsigned int _Nneighbor;
	std::vector<_obj*> _lns_neighbor;
	std::vector<bool> _iacts_neighbor;
};

#endif /* ELEM_NEIGHBOR_H_ */
