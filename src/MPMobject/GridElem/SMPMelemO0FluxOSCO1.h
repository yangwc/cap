/**************************************************
 * SMPMelemO0FluxOSCO1.h
 * 	   - SMPMelemO0FluxOSCO1()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised constant function
 *  + constant flux
 *  + linear limiter (slope of secant line)
 *  + tuning coefficient
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO0FLUXOSCO1_H_
#define SMPMELEMO0FLUXOSCO1_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"

class SMPMelemO0FluxOSCO1 : public _neighbor3d<SMPMelemO0FluxOSCO1>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO0FluxOSCO1(ShpUnit* shp, Scalar camp = 1.0): _mpmelem_base(shp) {
		_msta1.assign(3, MathVec<Scalar>() );
		_i_fluxs.resize(3);
		_camp = std::max( 0.0, camp );
	}
	virtual ~SMPMelemO0FluxOSCO1(){;}

	int local_smooth_preparation();
	int smooth_preparation();
	int local_smooth_matstate();
	int smooth_matstate();

	int smooth_specstress();

protected:
	void update_correction_matstate();
	void get_correction_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& dmsta);

	int local_smooth_specstress();

	MathVec<Scalar> _flux_coeff, _dxedge, _mstao;
	GSymmTensor _sbo;
	std::vector< MathVec<bool> > _i_fluxs;
	std::vector< MathVec<Scalar> > _msta1;

	Scalar _me, _camp;
	GVector _xpo;

	bool _i_need_correction_msta;
};

#endif /* SMPMELEMO0FLUXOSCO1_H_ */
