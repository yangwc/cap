/**************************************************
 * MPMelemFLOW.h
 * 	   - MPMelemFLOW()
 * ------------------------------------------------
 *  +
 * ================================================
 * Created by: YWC @ CEE, UW (Jan 13, 2016)
 * ------------------------------------------------
 * Modified by: YWC (16.01.13)
 **************************************************/

#ifndef MPMELEMFLOW_H_
#define MPMELEMFLOW_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"
#include "../../MPMcomputer/Particle.h"
#include "../GridNode/GnodeFLOW.h"

class MPMelemFLOW : public _neighbor3d<MPMelemFLOW>, public _mpmelem_base<GnodeFLOW, Particle> {
public:
	MPMelemFLOW(ShpUnit* shp) : _mpmelem_base(shp){;}

	virtual ~MPMelemFLOW(){;}

	void lump_m_matstate();
	void smooth_matstate();
	void lump_m_specstress();
	void smooth_specstress();
};

#endif /* MPMELEMFLOW_H_ */
