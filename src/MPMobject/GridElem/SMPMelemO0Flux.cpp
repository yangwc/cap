#include "SMPMelemO0Flux.h"

/********************************************
 * general data for smoothing and anti-locking
 ********************************************/
int SMPMelemO0Flux::local_smooth_preparation(){
	if (_Npt == 0) return 0;

	this-> refresh_active_neighbor();
	Scalar zero = 0;
	_flux_coeff.assign( zero, _Nneighbor);

	_me = _lns_pt[0]-> get_mass();
	for (unsigned int Ip=1; Ip != _Npt; ++Ip) _me   += _lns_pt[Ip]->get_mass();
	return 0;
}

int SMPMelemO0Flux::smooth_preparation(){
	if (_Npt == 0) return 0;

	unsigned int Inb, Idir;
	for (unsigned int Iside=0; Iside != 2; ++Iside){
		for(Idir=0; Idir != 3; ++Idir){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				const SMPMelemO0Flux& elem = *(this-> access_neighbor(Inb));
				_flux_coeff[Inb] = ( elem._me/ ( _Nneighbor * (elem._me + _me) ) );
			}
		}
	}

	return 0;
}

/********************************************
 * material states
 ********************************************/
int SMPMelemO0Flux::local_smooth_matstate(){
	if (_Npt == 0) return 0;

	_lns_pt[0]-> get_state( _mstao.stdvector() );

	if (_Npt == 1) return 0;

	_mstao *= _lns_pt[0]-> get_mass();

	MathVec<Scalar> mmsta;
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_lns_pt[Ipt]-> get_state( mmsta.stdvector() );
		mmsta  *= _lns_pt[Ipt]-> get_mass();
		_mstao += mmsta;
	}
	_mstao /= _me;
	return 0;
}

int SMPMelemO0Flux::smooth_matstate(){
	if (_Npt == 0) return 0;

	MathVec<Scalar> mstao;
	mstao.assign( _mstao );

	for (unsigned int Inb=0; Inb != _Nneighbor; ++Inb){
		if ( _iacts_neighbor[Inb] ){
			const SMPMelemO0Flux& elem = *( this-> access_neighbor(Inb) );
			mstao += (elem._mstao - _mstao)*_flux_coeff[Inb];
		}
	}
	// update assumed specific stress.
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> reset_state( mstao.stdvector() );

	return 0;
}

/********************************************
 * specific stress
 ********************************************/
inline int SMPMelemO0Flux::local_smooth_specstress(){
	if (_Npt == 0) return 0;

	_sbo = _lns_pt[0]-> get_mat_specstress();

	if (_Npt == 1) return 0;

	_sbo *= _lns_pt[0]-> get_mass();
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_sbo += _lns_pt[Ipt] -> get_mat_specstress()*_lns_pt[Ipt]-> get_mass();
	}
	_sbo /= _me;
	return 0;
}

int SMPMelemO0Flux::smooth_specstress(){
	if (_Npt == 0) return 0;

	int Istat = this-> local_smooth_specstress();

	// update assumed specific stress.
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> set_assumed_specstress(_sbo);

	return Istat;
}
