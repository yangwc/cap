#include "MPMelemFLOW.h"


void MPMelemFLOW::lump_m_matstate(){
	if (_Npt == 0) return;

	MathVec<Scalar> mmsta;
	unsigned int Ind;
	for (unsigned int Ipt=0; Ipt != _Npt; ++Ipt){
		Particle* ptptr = _lns_pt[Ipt];
		std::vector<Scalar>& Ns = _Ns_pt[Ipt];

		ptptr-> get_state( mmsta.stdvector() );
		mmsta *= ptptr-> get_mass();

		for ( Ind=0; Ind != _Nnd; ++Ind){
			_lns_nd[Ind]-> add_m_matstate( mmsta*Ns[Ind] );
		}
	}
}

void MPMelemFLOW::smooth_matstate(){
	if (_Npt == 0) return;

	MathVec<Scalar> msta;
	unsigned int Ind;
	for (unsigned int Ipt = 0; Ipt < _Npt; ++Ipt){
		const std::vector<Scalar>& Ns = _Ns_pt[Ipt];

		msta = ( _lns_nd[0]-> get_matstate() ) * Ns[0];
		for ( Ind=1; Ind != _Nnd; ++Ind){
			msta += ( _lns_nd[Ind]-> get_matstate() ) * Ns[Ind];
		}

		_lns_pt[Ipt]-> reset_state( msta.stdvector() );
	}// end for (particles)
}

void MPMelemFLOW::lump_m_specstress(){
	if (_Npt == 0) return;

	GSymmTensor msb;
	unsigned int Ind;
	for (unsigned int Ipt=0; Ipt != _Npt; ++Ipt){
		Particle* ptptr = _lns_pt[Ipt];
		std::vector<Scalar>& Ns = _Ns_pt[Ipt];

		msb  = ptptr-> get_mat_specstress();
		msb *= ptptr-> get_mass();

		for ( Ind=0; Ind != _Nnd; ++Ind){
			_lns_nd[Ind]-> add_m_specstress( msb*Ns[Ind] );
		}
	}
}

void MPMelemFLOW::smooth_specstress(){
	if (_Npt == 0) return;

	GSymmTensor sb;
	unsigned int Ind;
	for (unsigned int Ipt = 0; Ipt < _Npt; ++Ipt){
		const std::vector<Scalar>& Ns = _Ns_pt[Ipt];

		sb = ( _lns_nd[0]-> get_specstress() ) * Ns[0];
		for ( Ind=1; Ind != _Nnd; ++Ind){
			sb += ( _lns_nd[Ind]-> get_specstress() ) * Ns[Ind];
		}

		_lns_pt[Ipt]-> set_assumed_specstress( sb );
	}// end for (particles)
}
