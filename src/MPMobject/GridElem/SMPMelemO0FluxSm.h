/**************************************************
 * SMPMelemO0FluxSm.h
 * 	   - SMPMelemO0FluxSm()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised constant function
 *  + constant flux
 *  + linear limiter (slope of secant line)
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO0FLUXSM_H_
#define SMPMELEMO0FLUXSM_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"


class SMPMelemO0FluxSm : public _neighbor3d<SMPMelemO0FluxSm>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO0FluxSm(ShpUnit* shp): _mpmelem_base(shp) {
		_msta1.assign(3, MathVec<Scalar>() );
	}
	virtual ~SMPMelemO0FluxSm(){;}

	int local_smooth_preparation();
	int smooth_preparation();
	int local_smooth_matstate();
	int smooth_matstate();

	int smooth_specstress();

protected:
	void update_slope_matstate();
	MathVec<Scalar>& get_slope_matstate(unsigned int Idir, unsigned int Iside);
	void get_edge_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& msta);

	int local_smooth_specstress();

	MathVec<Scalar> _flux_coeff, _dxedge, _mstao;
	GSymmTensor _sbo;

	std::vector< MathVec<Scalar> > _msta1;

	Scalar _me;
	GVector _xpo;

	bool _i_need_slope_msta;
};

#endif /* SMPMELEMO0FLUXSM_H_ */
