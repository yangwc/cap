/**************************************************
 * SMPMelemO0FluxOSCO0.h
 * 	   - SMPMelemO0FluxOSCO0()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised constant function
 *  + constant flux
 *  + linear limiter (slope of secant line)
 *  + tuning coefficient
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO0FLUXOSCO0_H_
#define SMPMELEMO0FLUXOSCO0_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"

class SMPMelemO0FluxOSCO0 : public _neighbor3d<SMPMelemO0FluxOSCO0>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO0FluxOSCO0(ShpUnit* shp): _mpmelem_base(shp) {
		_i_fluxs.resize(3);
	}
	virtual ~SMPMelemO0FluxOSCO0(){;}

	int local_smooth_preparation();
	int smooth_preparation();
	int local_smooth_matstate();
	int smooth_matstate();

	int smooth_specstress();

protected:
	void update_correction_matstate();

	int local_smooth_specstress();

	MathVec<Scalar> _flux_coeff, _mstao;
	GSymmTensor _sbo;

	std::vector< MathVec<bool> > _i_fluxs;

	Scalar _me;

	bool _i_need_correction_msta;
};

#endif /* SMPMELEMO0FLUXOSCO0_H_ */
