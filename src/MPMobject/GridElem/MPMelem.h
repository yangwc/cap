/**************************************************
 * MPMelem.h
 * 	   - MPMelem()
 * ------------------------------------------------
 *  + Original MPM element.
 * 	+ Adaptor between particles and Gnodes.
 * 	+ For lumped (weighted average) Gnode model.
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.11.13)
 **************************************************/

#ifndef MPMELEM_H_
#define MPMELEM_H_
#include "_neighbor.h"
#include "_mpmelem_base.h"
#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"

class MPMelem : public _neighbor3d<MPMelem>, public _mpmelem_base<Gnode, Particle> {
public:
	MPMelem(ShpUnit* shp) : _mpmelem_base(shp){;}
	virtual ~MPMelem(){;}
};

#endif /* MPMELEM_H_ */
