/**************************************************
 * SMPMelemO1Flux.h
 * 	   - SMPMelemO1Flux()
 * ------------------------------------------------
 *  + Smoothed MPM ()
 *  + cell-based piece-wised linear function
 *  + linear flux
 * ================================================
 * Created by: YWC @ CEE, UW (Nov 16, 2015)
 * ------------------------------------------------
 * Modified by: YWC (16.03.07)
 **************************************************/

#ifndef SMPMELEMO1FLUX_H_
#define SMPMELEMO1FLUX_H_

#include "_neighbor.h"
#include "_mpmelem_base.h"

#include "../../MPMcomputer/Particle.h"
#include "../GridNode/Gnode.h"

class SMPMelemO1Flux : public _neighbor3d<SMPMelemO1Flux>, public _mpmelem_base<Gnode, Particle> {
public:
	SMPMelemO1Flux(ShpUnit* shp): _mpmelem_base(shp) {
		_ca = 3;
		_msta1.assign(3, MathVec<Scalar>() );
		_sb1.assign(3, GSymmTensor() );
	}
	virtual ~SMPMelemO1Flux(){;}

	int local_smooth_preparation();
	int smooth_preparation();

	int local_smooth_matstate();
	int smooth_matstate();

	int smooth_specstress();

protected:
	inline void allocate_ptvecs(){
		_mpmelem_base<Gnode, Particle>::allocate_ptvecs();
		unsigned int Nallocate = 1;
		_xis_pt.insert( _xis_pt.end(), Nallocate, GVector() );
	}
	inline void extract_ptdata( int Ipt, const GVector& ptxi ){
		_mpmelem_base<Gnode, Particle>::extract_ptdata(Ipt, ptxi);
		_xis_pt[Ipt] = ptxi;
	}

	void get_edge_matstate(unsigned int Idir, unsigned int Iside, MathVec<Scalar>& msta);
	int local_smooth_specstress();

	// for flux
	std::vector< GVector > _xis_pt;
	MathVec<Scalar> _flux_coeff, _dxiedge, _mstao;

	std::vector< MathVec<Scalar> > _msta1;
	std::vector< GSymmTensor > _sb1;

	GSymmTensor _sbo;
	Scalar _me;
	GVector _xipo, _Ixio;
	IntGVector _i0Io;

	int _ca;
};

#endif /* SMPMELEMO1FLUX_H_ */
