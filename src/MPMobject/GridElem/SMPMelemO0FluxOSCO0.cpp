#include "SMPMelemO0FluxOSCO0.h"

/********************************************
 * general data for smoothing and anti-locking
 ********************************************/
int SMPMelemO0FluxOSCO0::local_smooth_preparation(){
	if (_Npt == 0) return 0;

	this-> refresh_active_neighbor();

	Scalar zero = 0;
	_flux_coeff.assign( zero, _Nneighbor);

	_i_need_correction_msta = true;

	_me  = _lns_pt[0]-> get_mass();

	if (_Npt == 1) return 0;

	for (unsigned int Ip=1; Ip != _Npt; ++Ip){
		_me  += _lns_pt[Ip]-> get_mass();
	}

	return 0;
}

/********************************************
 * material states
 ********************************************/
int SMPMelemO0FluxOSCO0::smooth_preparation(){
	if (_Npt == 0) return 0;

	Scalar clayer = 1; clayer /= _Nneighbor;

	unsigned int Inb, Idir;
	for (unsigned int Iside=0; Iside != 2; ++Iside){
		for(Idir=0; Idir != 3; ++Idir){
			Inb = this-> get_neighbor_index( Idir, Iside);
			if (_iacts_neighbor[Inb]){
				const SMPMelemO0FluxOSCO0& elem = *(this-> access_neighbor(Inb));
				_flux_coeff[Inb]  = ( clayer*elem._me/ (elem._me + _me) );
			}
		}
	}
	return 0;
}

int SMPMelemO0FluxOSCO0::local_smooth_matstate(){
	if (_Npt == 0) return 0;

	_lns_pt[0]-> get_state( _mstao.stdvector() );

	if (_Npt == 1) return 0;

	_mstao *= _lns_pt[0]-> get_mass();

	MathVec<Scalar> mmsta;
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_lns_pt[Ipt]-> get_state( mmsta.stdvector() );
		mmsta  *= _lns_pt[Ipt]-> get_mass();
		_mstao += mmsta;
	}
	_mstao /= _me;
	return 0;
}

int SMPMelemO0FluxOSCO0::smooth_matstate(){
	if (_Npt == 0) return 0;

	MathVec<Scalar> mstao, dmstao;

	this-> update_correction_matstate();
	mstao.assign( _mstao );

	unsigned int Inb, Iside;
	for (unsigned int Idir=0; Idir != 3; ++Idir){
		MathVec<bool>& ifluxs = _i_fluxs[Idir];
		for (Iside=0; Iside != 2; ++Iside){
			Inb = this-> get_neighbor_index(Idir, Iside);
			if ( _iacts_neighbor[Inb] ){
				SMPMelemO0FluxOSCO0& elem = *( this-> access_neighbor(Inb) );
				elem.update_correction_matstate();

				dmstao.assign( elem._mstao );
				dmstao -= _mstao;
				dmstao *= (ifluxs+elem._i_fluxs[Idir]);
				dmstao *= _flux_coeff[Inb];

				mstao  += dmstao;
			}
		}
	}

	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> reset_state( mstao.stdvector() );

	return 0;
}


void SMPMelemO0FluxOSCO0::update_correction_matstate(){
	if( !_i_need_correction_msta ) return;
	_i_need_correction_msta = false;

	bool iactl, iactr;
	unsigned int Inbl, Inbr;

	for(unsigned int Idir=0; Idir != 3; ++Idir){
		MathVec<bool>& ifluxs = _i_fluxs[Idir];

		ifluxs.zeros(_mstao.size());

		Inbl = this-> get_neighbor_index( Idir, 0);
		Inbr = this-> get_neighbor_index( Idir, 1);
		iactl = _iacts_neighbor[Inbl];
		iactr = _iacts_neighbor[Inbr];

		if ( iactl && iactr  ){
			const SMPMelemO0FluxOSCO0& eleml = *( this-> access_neighbor(Inbl) );
			const SMPMelemO0FluxOSCO0& elemr = *( this-> access_neighbor(Inbr) );

			MathVec<Scalar> dvr;
			MathVec<Scalar> dvl;
			dvr.assign(elemr._mstao); dvr -= _mstao;
			dvl.assign(eleml._mstao); dvl -= _mstao;
			(dvr*dvl).bw_gt(0, ifluxs.stdvector());
		}
	}
}

/********************************************
 * specific stress
 ********************************************/
inline int SMPMelemO0FluxOSCO0::local_smooth_specstress(){
	if (_Npt == 0) return 0;

	_sbo = _lns_pt[0]-> get_mat_specstress();

	if (_Npt == 1) return 0;

	_sbo *= _lns_pt[0]-> get_mass();
	for (unsigned int Ipt = 1; Ipt != _Npt; ++Ipt){
		_sbo += _lns_pt[Ipt] -> get_mat_specstress()*_lns_pt[Ipt]-> get_mass();
	}
	_sbo /= _me;
	return 0;
}

int SMPMelemO0FluxOSCO0::smooth_specstress(){
	if (_Npt == 0) return 0;

	int Istat = this-> local_smooth_specstress();

	// update assumed specific stress.
	for (unsigned int Ipt = 0; Ipt != _Npt; ++Ipt) _lns_pt[Ipt]-> set_assumed_specstress(_sbo);

	return Istat;
}
