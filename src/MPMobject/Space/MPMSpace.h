/**************************************************
 * MPMSpace.h
 * 	   - MPMSpace()
 * ------------------------------------------------
 * 	+ the MPM space
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.21)
 **************************************************/
#ifndef MPMSPACE_H_
#define MPMSPACE_H_

#include "SpaceIntf.h"

class MPMSpace : public Space {
public:
	MPMSpace(int ID) : Space(ID){}
	MPMSpace(MPMSpace* proto) : Space(proto) {}
	virtual ~MPMSpace() {}

	inline int link_particle(const Scalar& tnow){
		/*** 1. reset grid ***/
		int Istat = 0;
		std::vector<Particle*>* pts = 0;
		if ( _ln_freepts.capacity() != 0){
			Istat = _grid->reset( tnow, _ln_freepts );
			if (Istat != 0) return -1;
			pts = &_ln_freepts;
		}
		else{
			Istat = _grid->reset();
			if (Istat != 0) return -1;
			_ln_freepts.resize(2,0);
			pts = &_objs_pt;
		}

		/*** 2. link particle to grid ***/
		Particle* ptptr = 0;
		for (std::vector<Particle*>::iterator ivec = pts -> begin(); ivec != pts -> end(); ++ivec){
			ptptr = (*ivec);
			ptptr -> set_time( tnow );
			Istat = _grid -> link_particle(ptptr);
			if (Istat != 0) return -2;
		}

		/*** 3. all particles are linked, no free ones ***/
		_ln_freepts.clear();
		return Istat;
	}

	inline int build_grid(){
		int Istat = _grid -> build();
		if (Istat != 0) return -1;
		Istat     = _grid -> link_global();
		if (Istat != 0) return -2;
		return Istat;
	}

	inline bool update_pt_kinematics(const Scalar& tnow, const Scalar& dt){
		int Istat = _grid -> update_kinematics(tnow, dt);
		return (Istat == 0);
	}
	inline bool update_pt_deformation(const Scalar& tnow, const Scalar& dt){
		int Istat = _grid -> update_deformation(tnow, dt);
		return (Istat == 0);
	}
};

#endif /* MPMSPACE_H_ */
