#ifndef LNKMNGR_H_
#define LNKMNGR_H_

#include "../../IVmap/IVmap.h"
#include <vector>

template <class _obj>
class LNKmngr {
protected:
	typedef std::vector<unsigned int> uintvec;
	typedef std::vector<_obj*> olnvec;
	typedef std::vector<olnvec> olnsvec;

public:
	virtual ~LNKmngr(){;}

	virtual
	unsigned int
	get_address(const IntGVector& oid, _obj* olink, uintvec& Nsps, olnsvec& olnsv ) = 0;
};

template <class _obj>
class FLNKmngr : public LNKmngr<_obj> {
protected:
	typedef std::vector<unsigned int> uintvec;
	typedef std::vector<_obj*>        olnvec;
	typedef std::vector<olnvec>       olnsvec;

public:
	virtual ~FLNKmngr(){;}

	inline
	unsigned int
	get_address(const IntGVector& oid, _obj* olink, uintvec& Nsps, olnsvec& olnsv ){
		unsigned int Iadd;
		int Istat = _addmap_obj.get_w(oid, Iadd);
		if(Istat == 0){
			unsigned int& Nsp = Nsps[Iadd];
			olnvec& olnks = olnsv[Iadd];

			if(Nsp == olnks.size()){
				olnks.push_back( olink );
			}
			else{
				olnks[Nsp] = olink;
			}
			Nsp++;
		}
		else{
			Iadd = Nsps.size();
			_addmap_obj.set( oid, Iadd );
			Nsps.push_back(1);
			olnvec olnks(1,olink);
			olnsv.push_back( olnks );
		}
		return Iadd;
	}

private:
	IVmap<unsigned int> _addmap_obj;
};

#endif /* LNKMNGR_H_ */
