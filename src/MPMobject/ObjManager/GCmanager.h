#ifndef GCMANAGER_H_
#define GCMANAGER_H_

#include "../Shape/Shape.h"
#include "../../IVmap/IVmap.h"
#include <vector>
#include "../LiteFac/ElemMaker.h"

template <class _pgadp>
class GCmanager {
private:
	typedef std::vector<_pgadp*> advec;

public:
	virtual ~GCmanager(){;}
	virtual _pgadp* get_adplink(const GVector& ptx, const GVector& spacing, advec& adps) = 0;
	virtual _pgadp* get_adplink(const IntGVector& adpid, const GVector& spacing, advec& adps) = 0;
};

template <class _pgadp>
class FGCmanager : public GCmanager<_pgadp> {
private:
	typedef std::vector<_pgadp*> advec;

public:
	FGCmanager(AdMaker<_pgadp>* admaker, ShpCCP* shmaker){
		_admaker = admaker;
		_shapkit = shmaker;
		_adidstp = _shapkit->get_Nids();
		_adidstp -= 1;
	};
	virtual ~FGCmanager(){
		delete _admaker;
		delete _shapkit;
	}

	inline _pgadp* get_adplink(const GVector& ptx, const GVector& spacing, advec& adps){
		IntGVector adpid = (ptx/spacing).floor(); adpid *= _adidstp;
		_pgadp* adptr = this -> get_adplink(adpid, spacing, adps);
		return adptr;
	}

	inline _pgadp* get_adplink(const IntGVector& adpid, const GVector& spacing, advec& adps){
		_pgadp* adptr = 0;
		unsigned int Iadd;
		int Istat = _addmap_ad.get_w(adpid, Iadd);
		if(Istat == 0){
			adptr = adps[ Iadd ];
		}
		else{
			adptr = allocate(adpid, spacing, adps);
		}
		return adptr;
	}

protected:

	inline _pgadp* allocate(const IntGVector& adpid, const GVector& spacing, advec& adps){
		/*** related to shape ***/
		GVector adsc(adpid); adsc /= (GVector) _adidstp; adsc += 0.5; adsc *= spacing;

		ShpUnit* shptr = _shapkit->get_new_shpunit(adsc, spacing);
		_pgadp* adptr  = _admaker->get_new(adsc, shptr);
		shptr = 0;

		_addmap_ad.set( adpid, adps.size() );
		adps.push_back(adptr);

		return adptr;
	}

	IVmap<unsigned int> _addmap_ad;
	AdMaker<_pgadp>* _admaker;
	ShpCCP* _shapkit;

	IntGVector _adidstp;
};


#endif /* GMANAGER_H_ */
