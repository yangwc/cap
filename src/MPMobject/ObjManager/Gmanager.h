#ifndef GMANAGER_H_
#define GMANAGER_H_

#include "../Shape/Shape.h"
#include "../LiteFac/NodeMaker.h"
#include "../LiteFac/ElemMaker.h"
#include "../../IVmap/IVmap.h"
#include <vector>

#include "../Boundary/BCManager.h"
#include "../PTFilter/PTBManager.h"

template <typename _elem, typename _node>
class Gmanager {
private:
	typedef std::vector<_elem*> emvec;
	typedef std::vector<_node*> ndvec;

public:
	virtual ~Gmanager(){;}
	virtual _elem* get_adplink(const GVector& ptx, emvec& adps, ndvec& nds) = 0;
	virtual _elem* get_adplink(const IntGVector& adpid, emvec& adps, ndvec& nds) = 0;

	virtual void add_bconditions( BC* bcptr ){ _bcmngr.add_bconditions(bcptr); }
	virtual void link_ptfilters( PTFilter* ptf ){ _ptfmngr.link_ptfilter(ptf); }

protected:
	BCManager<_elem, _node> _bcmngr;
	PTBManager<_elem> _ptfmngr;
};


template <typename _elem, typename _node, typename _elemmaker, typename _nodemaker>
class FRCGmanager : public Gmanager<_elem, _node> {
	/*******************************************
	 * Floating (dynamic) grid manager
	 * (create when necessary)
	 * _elem: element (particle-grid node adaptor)
	 * _node: grid node
	 *******************************************/
private:
	typedef std::vector<_elem*> emvec;
	typedef std::vector<_node*> ndvec;

public:
	FRCGmanager(const GVector& spacing, _elemmaker* admaker, _nodemaker* ndmaker, ShpCCP* shmaker){
		_spacing = spacing;
		_admaker = admaker;
		_ndmaker = ndmaker;
		_shapkit = shmaker;
		_adidstp = _shapkit->get_Nids();
		_adidstp -= 1;
	};
	virtual ~FRCGmanager(){
		delete _admaker;
		delete _ndmaker;
		delete _shapkit;
	}


	inline _elem*
	get_adplink(const GVector& ptx, emvec& adps, ndvec& nds){
		IntGVector adpid = (ptx/_spacing).floor(); adpid *= _adidstp;
		_elem* adptr = this -> get_adplink(adpid, adps, nds);
		return adptr;
	}

	inline _elem*
	get_adplink(const IntGVector& adpid, emvec& adps, ndvec& nds){
		_elem* adptr = 0;
		unsigned int Iadd;
		int Istat = _addmap_ad.get_w(adpid, Iadd);
		if(Istat == 0){
			adptr = adps[ Iadd ];
		}
		else{
			adptr = allocate(adpid, adps, nds);
		}
		return adptr;
	}
protected:
	IVmap<unsigned int> _addmap_ad, _addmap_nd;
	_elemmaker* _admaker;
	_nodemaker* _ndmaker;
	ShpCCP* _shapkit;

	IntGVector _adidstp;
	GVector _spacing;

	inline _elem* allocate(const IntGVector& adpid, emvec& adps, ndvec& nds){
		/*** related to shape ***/
		GVector adsc(adpid); adsc /= (GVector) _adidstp; adsc += 0.5; adsc *= _spacing;
		ShpUnit* shptr = _shapkit -> get_new_shpunit(adsc, _spacing);

		/*** create new adaptor|element|cell ***/
		_elem* new_elm  = _admaker -> get_new(adpid, adsc, shptr);
		shptr = 0;

		/*** inform boundaries about new cell ***/
		Gmanager<_elem, _node>::_bcmngr.refresh_new_cell( new_elm );

		/*** inform particle filter about new cell ***/
		Gmanager<_elem, _node>::_ptfmngr.refresh_new_cell( new_elm );

		/*** assign nodes to the new created adaptor ***/
		_node* gdptr;
		std::vector<IntGVector> xs;
		_shapkit-> get_new_nodeids(xs);
		for (unsigned int Ind=0; Ind != xs.size(); ++Ind ){
			gdptr = this -> get_ndlink( adpid, adsc, xs[Ind], nds);
			new_elm -> assign_nodelink(Ind, gdptr);
		}

		/*** assign the adptor's neighbors info ***/
		this-> refresh_adp_neighbors( adpid, new_elm, adps);

		/*** restore the adaptor ***/
		_addmap_ad.set( adpid, adps.size() );
		adps.push_back(new_elm);

		return new_elm;
	}


	inline _node*
	get_ndlink(const IntGVector& adpid, const GVector& adcentroid, const IntGVector& locndid, ndvec& nds){
		_node* gridnodeptr=0;
		IntGVector ndid = locndid + adpid;
		unsigned int Iadd;
		int Istat = _addmap_nd.get_w(ndid, Iadd);
		if(Istat == 0){
			gridnodeptr = nds[ Iadd ];
		}
		else{
			// calculate node's coordinate
			GVector coord;
			_shapkit->get_ndcoord_w( locndid, _spacing, coord );
			coord += adcentroid;

			// create a new node
			gridnodeptr = _ndmaker-> get_new(ndid, coord, _shapkit->get_new_shpkeyhandler() );

			// inform boundaries about new node
			Gmanager<_elem, _node>::_bcmngr.refresh_new_node( gridnodeptr );

			// restore new node
			_addmap_nd.set( ndid, nds.size() );
			nds.push_back(gridnodeptr);
		}
		return gridnodeptr;
	}

	inline int refresh_adp_neighbors( const IntGVector& adpid, _elem* new_elm, emvec& adps){
		IntGVector neiborid;
		unsigned int Iadd;
		int Istat, idx;
		for(unsigned int Idir=0; Idir != 3; ++Idir){
			neiborid = adpid;
			idx  = adpid.GetComp(Idir);
			// negative side and positive side
			for(unsigned int Iside=0; Iside != 2; ++Iside){
				neiborid.SetComp(Idir, idx + Iside*2-1);
				Istat = _addmap_ad.get_w( neiborid, Iadd);
				if(Istat == 0){
					_elem* nbr_elm = adps[ Iadd ];
					nbr_elm -> link_neighbor(Idir, 1-Iside, new_elm);
					new_elm -> link_neighbor(Idir, Iside, nbr_elm);
				}
			}
		}
		return 0;
	}
};


#endif /* GMANAGER_H_ */
