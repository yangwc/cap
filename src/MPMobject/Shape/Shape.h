#ifndef SHAPE_H_
#define SHAPE_H_

#include "../../GB_Tensor.h"

class ShpCCP;	// CCP: central control platform. (factory)
class ShpUnit;	// unit: shape function and geometric
class ShpSKH;	// SKH: shape key handler.
class ShpSKD;	// SKD: shape key decoder. (return geometric properties from the key)

/****************
 * ShpCCP
 * - factory
 ****************/
class ShpCCP {
public:
	virtual ~ShpCCP(){;}

	virtual int get_new_nodeids(std::vector<IntGVector>& ndids) = 0;
	virtual int get_nodeid_w( const unsigned int& Ind, IntGVector& id) = 0;
	virtual int get_ndcoord_w( const IntGVector& ndid, const GVector& dim, GVector& xnd ) = 0;

	virtual ShpUnit* get_new_shpunit(const GVector& xcs, const GVector& dim) const = 0;
	virtual ShpSKH* get_new_shpkeyhandler() const = 0;
	virtual ShpSKD* get_new_shpkeydecoder() const = 0;

	inline unsigned int get_Nnode(){ return _Nnd; }
	inline const IntGVector& get_Nids(){ return _Nids; }

protected:
	IntGVector _Nids;
	unsigned int _Nnd;
};

/****************
 *  ShpUnit
 *  - Shape function
 ****************/
class ShpUnit {
public:
	virtual ~ShpUnit(){;}

	virtual Scalar get_volume() = 0;
	virtual Scalar get_char_length() = 0;
	virtual const GVector& get_char_dim() = 0;
	virtual const GVector& get_centroid() = 0;

	virtual void get_mapcoord( const GVector& x, GVector& xi ) = 0;
	virtual GVector get_mapcoord( const GVector& x ) = 0;

	virtual void get_shapedata(const GVector& xip, std::vector<Scalar>& shpdat) const = 0;
	virtual void get_modN(const std::vector<Scalar>& shpdat, std::vector<Scalar>& N, std::vector<GVector>& gNxi) const = 0;

	virtual void get_N(const std::vector<Scalar>& shpdat, std::vector<Scalar>& N, std::vector<GVector>& gNxi) const = 0;
	virtual void get_N(const GVector& xip, std::vector<Scalar>& N, std::vector<GVector>& gNxi) = 0;
	virtual void get_N(const GVector& xip, std::vector<Scalar>& N) = 0;
	virtual void get_GNxi(const GVector& xipt, std::vector<GVector>& gNxi) = 0;

	virtual void get_GN(const GVector& ptxi, const std::vector<GVector>& xnds, std::vector<GVector>& gN) = 0;
	virtual void get_GN(const std::vector<GVector>& gNxi, const std::vector<GVector>& xnds, std::vector<GVector>& gN) = 0;
	virtual void get_GNo(const GVector& ptxi, std::vector<GVector>& gN) = 0;
	virtual void get_GNo(const std::vector<GVector>& gNxi, std::vector<GVector>& gN) = 0;

	virtual void get_xcorners(std::vector<GVector>& xcorners) = 0;

	virtual unsigned int get_shapekey(const unsigned int& Ind) = 0;
	virtual ShpUnit* get_new_obj() = 0;

	virtual void get_Inodes(unsigned int Idir, unsigned int Iside, std::vector<unsigned int> Inds) = 0;

	inline unsigned int get_Nnode(){ return _Nnd; }


	virtual bool if_covered(const GVector& xi) = 0;

protected:
	unsigned int _Nnd;
};

/****************
 *  ShpSKH
 *  - Shape Key
 *  	Handler
 ****************/
class ShpSKH{
public:
	typedef unsigned int skeytype;

public:
	ShpSKH(){ _key = 0;}
	virtual ~ShpSKH(){;}

	virtual void reset(){ _key = 0; }
	virtual void add_key( const skeytype& key){ _key += key; }
	virtual const skeytype& get_key(){ return _key; }
	virtual bool if_shaped(){ return _key != 0; }

protected:
	skeytype _key;
};

/****************
 *  ShpSKD
 *  - Shape Key
 *  	Decoder
 ****************/
class ShpSKD{
public:
	typedef unsigned int skeytype;

public:
	virtual ~ShpSKD(){;}
	virtual std::vector<int>* get_new_normalkeys(const skeytype& key) = 0;
};
#endif /* SHAPE_H_ */
