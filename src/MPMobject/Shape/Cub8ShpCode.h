#include <vector>
#include <iostream>
/**************************************
 *   error report
 **************************************/

void print_first_group_err(const int& Igp1){
	std::cout<<"ERROR: undefined first group number "<<Igp1<<" of node surface index."<<std::endl;
}

void print_second_group_err(const int& Igp2, const int& Igp1){
	std::cout<<"ERROR: undefined second group number "<<Igp2<<" of node surface index.("<<Igp1<<")"<<std::endl;
}

/**************************************
 *   node surface key: second classify
 **************************************/
std::vector<int>* get_ptr_nkeys_1cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP1-1
	case   1: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case   2: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case   4: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case   8: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case  16: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case  32: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case  64: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 128: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_2cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP2-1
	case   3: nkey[0]=11; nkey[1]=-1; nkey[2]=-1; break;
	case   6: nkey[0]= 1; nkey[1]=11; nkey[2]=-1; break;
	case   9: nkey[0]=-1; nkey[1]=11; nkey[2]=-1; break;
	case  12: nkey[0]=11; nkey[1]= 1; nkey[2]=-1; break;
	case  17: nkey[0]=-1; nkey[1]=-1; nkey[2]=11; break;
	case  34: nkey[0]= 1; nkey[1]=-1; nkey[2]=11; break;
	case  48: nkey[0]=11; nkey[1]=-1; nkey[2]= 1; break;
	case  68: nkey[0]= 1; nkey[1]= 1; nkey[2]=11; break;
	case  96: nkey[0]= 1; nkey[1]=11; nkey[2]= 1; break;
	case 136: nkey[0]=-1; nkey[1]= 1; nkey[2]=11; break;
	case 144: nkey[0]=-1; nkey[1]=11; nkey[2]= 1; break;
	case 192: nkey[0]=11; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP2-2
	case   5: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case  10: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case  18: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case  24: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case  33: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case  36: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case  66: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case  72: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case  80: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	case 129: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case 132: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 160: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	//*** GP2-3
	case  20: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  40: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  65: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 130: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_3cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP3-1
	case   7: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  11: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  13: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case  14: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case  19: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  25: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  35: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  38: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  49: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case  50: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case  70: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case  76: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case  98: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 100: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 112: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 137: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 140: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 145: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 152: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 176: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 196: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 200: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 208: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 224: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP3-2
	case  21: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  22: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  28: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case  41: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  42: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  44: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case  52: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case  56: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case  67: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  69: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case  73: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case  81: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case  84: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case  97: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 104: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 131: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case 134: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 138: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 146: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 148: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 162: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 168: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 193: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 194: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP3-3
	case  26: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  37: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  74: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  82: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  88: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 133: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 161: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 164: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_4cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP4-1
	case  15: nkey[0]= 0; nkey[1]= 0; nkey[2]=-1; break;
	case  51: nkey[0]= 0; nkey[1]=-1; nkey[2]= 0; break;
	case 102: nkey[0]= 1; nkey[1]= 0; nkey[2]= 0; break;
	case 153: nkey[0]=-1; nkey[1]= 0; nkey[2]= 0; break;
	case 204: nkey[0]= 0; nkey[1]= 1; nkey[2]= 0; break;
	case 240: nkey[0]= 0; nkey[1]= 0; nkey[2]= 1; break;
	//*** GP4-2
	case  27: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  39: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  78: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 114: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 141: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 177: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 216: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 228: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP4-3
	case  30: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case  45: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case  53: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case  58: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case  75: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case  83: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case  86: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case  89: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case  92: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 101: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case 106: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case 120: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	case 135: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case 149: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case 154: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case 163: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case 166: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case 169: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case 172: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 180: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	case 197: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 202: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 210: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	case 225: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	//*** GP4-4
	case  23: nkey[0]= 0; nkey[1]=-1; nkey[2]=-1; break;
	case  29: nkey[0]=-1; nkey[1]= 0; nkey[2]=-1; break;
	case  43: nkey[0]= 0; nkey[1]=-1; nkey[2]=-1; break;
	case  46: nkey[0]= 1; nkey[1]= 0; nkey[2]=-1; break;
	case  54: nkey[0]= 1; nkey[1]=-1; nkey[2]= 0; break;
	case  57: nkey[0]=-1; nkey[1]=-1; nkey[2]= 0; break;
	case  71: nkey[0]= 1; nkey[1]= 0; nkey[2]=-1; break;
	case  77: nkey[0]= 0; nkey[1]= 1; nkey[2]=-1; break;
	case  99: nkey[0]= 1; nkey[1]=-1; nkey[2]= 0; break;
	case 108: nkey[0]= 1; nkey[1]= 1; nkey[2]= 0; break;
	case 113: nkey[0]= 0; nkey[1]=-1; nkey[2]= 1; break;
	case 116: nkey[0]= 1; nkey[1]= 0; nkey[2]= 1; break;
	case 139: nkey[0]=-1; nkey[1]= 0; nkey[2]=-1; break;
	case 142: nkey[0]= 0; nkey[1]= 1; nkey[2]=-1; break;
	case 147: nkey[0]=-1; nkey[1]=-1; nkey[2]= 0; break;
	case 156: nkey[0]=-1; nkey[1]= 1; nkey[2]= 0; break;
	case 178: nkey[0]= 0; nkey[1]=-1; nkey[2]= 1; break;
	case 184: nkey[0]=-1; nkey[1]= 0; nkey[2]= 1; break;
	case 198: nkey[0]= 1; nkey[1]= 1; nkey[2]= 0; break;
	case 201: nkey[0]=-1; nkey[1]= 1; nkey[2]= 0; break;
	case 209: nkey[0]=-1; nkey[1]= 0; nkey[2]= 1; break;
	case 212: nkey[0]= 0; nkey[1]= 1; nkey[2]= 1; break;
	case 226: nkey[0]= 1; nkey[1]= 0; nkey[2]= 1; break;
	case 232: nkey[0]= 0; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP4-5
	case  60: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case  85: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 105: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 150: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 170: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 195: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	//*** GP4-6
	case  90: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 165: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_5cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP5-1
	case  31: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  47: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  55: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  59: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  79: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 103: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case 110: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 115: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 118: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 143: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 155: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case 157: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 179: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 185: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 205: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 206: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 217: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 220: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 230: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 236: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 241: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 242: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 244: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 248: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP5-2
	case  61: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  62: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  87: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case  93: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 107: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case 109: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 117: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 121: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 124: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 151: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case 158: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 171: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case 174: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 182: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 186: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 188: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 199: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 203: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 211: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 213: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 214: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	case 227: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 233: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 234: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP5-3
	case  91: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case  94: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 122: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 167: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case 173: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 181: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 218: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 229: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_6cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP6-1
	case  63: nkey[0]=11; nkey[1]=-1; nkey[2]=-1; break;
	case 111: nkey[0]= 1; nkey[1]=11; nkey[2]=-1; break;
	case 119: nkey[0]= 1; nkey[1]=-1; nkey[2]=11; break;
	case 159: nkey[0]=-1; nkey[1]=11; nkey[2]=-1; break;
	case 187: nkey[0]=-1; nkey[1]=-1; nkey[2]=11; break;
	case 207: nkey[0]=11; nkey[1]= 1; nkey[2]=-1; break;
	case 221: nkey[0]=-1; nkey[1]= 1; nkey[2]=11; break;
	case 238: nkey[0]= 1; nkey[1]= 1; nkey[2]=11; break;
	case 243: nkey[0]=11; nkey[1]=-1; nkey[2]= 1; break;
	case 246: nkey[0]= 1; nkey[1]=11; nkey[2]= 1; break;
	case 249: nkey[0]=-1; nkey[1]=11; nkey[2]= 1; break;
	case 252: nkey[0]=11; nkey[1]= 1; nkey[2]= 1; break;
	//*** GP6-2
	case  95: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case 123: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case 126: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case 175: nkey[0]=11; nkey[1]=11; nkey[2]=-1; break;
	case 183: nkey[0]=11; nkey[1]=-1; nkey[2]=11; break;
	case 189: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case 219: nkey[0]=-1; nkey[1]=11; nkey[2]=11; break;
	case 222: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 231: nkey[0]= 1; nkey[1]=11; nkey[2]=11; break;
	case 237: nkey[0]=11; nkey[1]= 1; nkey[2]=11; break;
	case 245: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	case 250: nkey[0]=11; nkey[1]=11; nkey[2]= 1; break;
	//*** GP6-3
	case 125: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 190: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 215: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;
	case 235: nkey[0]=11; nkey[1]=11; nkey[2]=11; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_7cell(const int& Igp2, const int& Igp1){
	int nkey[]={0,0,0};
	switch(Igp2){
	//*** GP7-1
	case 127: nkey[0]= 1; nkey[1]=-1; nkey[2]=-1; break;
	case 191: nkey[0]=-1; nkey[1]=-1; nkey[2]=-1; break;
	case 223: nkey[0]=-1; nkey[1]= 1; nkey[2]=-1; break;
	case 239: nkey[0]= 1; nkey[1]= 1; nkey[2]=-1; break;
	case 247: nkey[0]= 1; nkey[1]=-1; nkey[2]= 1; break;
	case 251: nkey[0]=-1; nkey[1]=-1; nkey[2]= 1; break;
	case 253: nkey[0]=-1; nkey[1]= 1; nkey[2]= 1; break;
	case 254: nkey[0]= 1; nkey[1]= 1; nkey[2]= 1; break;

	default:
		print_second_group_err(Igp2, Igp1);
		break;
	}// end switch

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(nkey, nkey+3);
	return nkeyptr;
}// end function

std::vector<int>* get_ptr_nkeys_8cell(const int& Igp2, const int& Igp1){
	if(Igp2 != 255){
		print_second_group_err(Igp2, Igp1);
	}

	std::vector<int>* nkeyptr = new std::vector<int>(3);
	nkeyptr->assign(3,0);
	return nkeyptr;
}// end function

/**************************************
 *   node surface key: first classify
 **************************************/
std::vector<int>* get_ptr_nkeys(const int& Igp1, const int& Igp2){
	std::vector<int>* nkeyptr=0;
	switch(Igp1){
	case 1:
		nkeyptr = get_ptr_nkeys_1cell(Igp2, Igp1);
		break;
	case 2:
		nkeyptr = get_ptr_nkeys_2cell(Igp2, Igp1);
		break;
	case 4:
		nkeyptr = get_ptr_nkeys_4cell(Igp2, Igp1);
		break;
	case 8:
		nkeyptr = get_ptr_nkeys_8cell(Igp2, Igp1);
		break;
	case 3:
		nkeyptr = get_ptr_nkeys_3cell(Igp2, Igp1);
		break;
	case 5:
		nkeyptr = get_ptr_nkeys_5cell(Igp2, Igp1);
		break;
	case 6:
		nkeyptr = get_ptr_nkeys_6cell(Igp2, Igp1);
		break;
	case 7:
		nkeyptr = get_ptr_nkeys_7cell(Igp2, Igp1);
		break;
	default:
		std::cout<<"ERROR: undefined first group number "<<Igp1<<" of node surface index."<<std::endl;
		break;
	}// end switch
	return nkeyptr;
}// end function


