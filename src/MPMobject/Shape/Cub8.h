/*
 * Cub8.h
 *
 *  Created on: May 14, 2014
 *      Author: wenchia
 */

#ifndef CUB8_H_
#define CUB8_H_

#include "_INTF_Shape.h"

class Cub8Unit;	// unit: shape function and geometric
class Cub8SKH;	// SKH: shape key handler.
class Cub8SKD;	// SKD: shape key decoder. (return geometric properties from the key)
class Cub8CCP;	// CCP: central control platform. (factory)

class Cub8Unit : public ShpUnit {
public:
	Cub8Unit(const GVector& xc, const GVector& dim, Scalar Lcut, Scalar Nmin, Scalar dNmin);
	Cub8Unit(Cub8Unit* proto);
	virtual ~Cub8Unit(){;}

	inline void set_modcoef(Scalar Lcut, Scalar Nmin, Scalar dNmin){ _Lcut= Lcut; _Nmin= Nmin; ; _dNmin= dNmin;}

	inline Scalar get_volume(){ return _dim.bw_product(); }
	inline Scalar get_char_length(){ return _dim.bw_sum() / 3.0; }
	inline const GVector& get_char_dim(){ return _dim; };
	inline const GVector& get_centroid(){ return _xc; };

	inline void get_mapcoord( const GVector& x, GVector& xi ){ xi = (x-_xc)*2/_dim; }
	inline GVector get_mapcoord( const GVector& x ){ GVector xi; get_mapcoord( x, xi ); return xi; }

	void get_shapedata(const GVector& xip, std::vector<Scalar>& shpdat) const;

	void get_modN(const std::vector<Scalar>& shpdat, std::vector<Scalar>& N, std::vector<GVector>& gNxi) const;

	void get_N(const std::vector<Scalar>& shpdat, std::vector<Scalar>& N, std::vector<GVector>& gNxi) const;
	void get_N(const GVector& xip, std::vector<Scalar>& N, std::vector<GVector>& gNxi);
	void get_N(const GVector& xip, std::vector<Scalar>& N);
	void get_GNxi(const GVector& xipt, std::vector<GVector>& gN);

	void get_GN(const GVector& ptxi, const std::vector<GVector>& xnds, std::vector<GVector>& gN);
	void get_GN(const std::vector<GVector>& gNxi, const std::vector<GVector>& xnds, std::vector<GVector>& gN);
	void get_GNo(const GVector& xipt, std::vector<GVector>& gN);
	void get_GNo(const std::vector<GVector>& gNxi, std::vector<GVector>& gN);

	void get_xcorners(std::vector<GVector>& xcorners);

//	bool get_shapfval(const GVector& ptx, std::vector<Scalar>& Ns, std::vector<GVector>& gNxis);
	inline unsigned int get_shapekey(const unsigned int& Ind){ return _shpkeys[Ind]; }
	virtual ShpUnit* get_new_obj(){ return new Cub8Unit(this); }

	void get_Inodes(unsigned int Idir, unsigned int Iside, std::vector<unsigned int> Inds);

	inline bool if_covered(const GVector& xi){ return !( xi.any_GT( 1 +_tol) || xi.any_LT( - 1 -_tol) );}

protected:
	void init_setup();

	void get_localcoord(int Ikey, MathVec<int>& xloc);

	void get_N1d(const GVector& xipt, std::vector< std::vector<Scalar> >& N1ds);
	void get_dN1d(const GVector& xipt, std::vector< std::vector<Scalar> >& dN1ds);
//	void get_modN1d(const GVector& xipt, std::vector< std::vector<Scalar> >& N1ds);
//	void get_moddN1d(const GVector& xipt, std::vector< std::vector<Scalar> >& dN1ds);

	Scalar _tol, _Lcut, _hLcut, _dNmin, _Nmin;
	GVector _xc, _dim;

	std::vector<unsigned int> _shpkeys;
};

class Cub8SKD : public ShpSKD {
public:
	typedef unsigned int skeytype;

public:
	Cub8SKD(){;}
	virtual ~Cub8SKD(){;}

	std::vector<int>* get_new_normalkeys(const skeytype& key);
};

class Cub8CCP : public ShpCCP {
public:
	Cub8CCP(Scalar Lcut=0.167, Scalar Nmin=1e-3, Scalar dNmin=0){ _Nids.SetComponents(2,2,2); _Nnd = 8; _Lcut= Lcut; _Nmin= Nmin; _dNmin= dNmin; }
	virtual ~Cub8CCP(){;}

	int get_new_nodeids(std::vector<IntGVector>& ndids);
	int get_nodeid_w( const unsigned int& Ind, IntGVector& id);
	int get_ndcoord_w( const IntGVector& ndid, const GVector& dim, GVector& xnd );

	inline ShpUnit* get_new_shpunit(const GVector& xcs, const GVector& dim) const { return new Cub8Unit(xcs, dim, _Lcut, _Nmin, _dNmin); }
	inline ShpSKH*  get_new_shpkeyhandler() const { return new ShpSKH(); }
	inline ShpSKD*  get_new_shpkeydecoder() const { return new Cub8SKD(); }

protected:
	Scalar _Lcut, _Nmin, _dNmin;
};

#endif /* CUB8_H_ */
