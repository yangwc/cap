/*
 * Cub8.cpp
 *
 *  Created on: May 14, 2014
 *      Author: wenchia
 */

#include "Cub8.h"
#include "Cub8ShpCode.h"
#include <algorithm>

std::vector<int>* Cub8SKD::get_new_normalkeys(const skeytype& key){
	unsigned int Igp1 = key / 10000;
	unsigned int Igp2 = key - Igp1*10000;

	std::vector<int>* nkey = get_ptr_nkeys(Igp1, Igp2);
	return nkey;
}

int Cub8CCP::get_new_nodeids(std::vector<IntGVector>& ndids){
	/******************************************
	 * Ind = id_0 * 1 + id_1 * 2 + id_2 * 4
	 * id  = (xi_i + 1) / 2
	 * xi_i (local coord. of node i) = +1 or -1;
	 ******************************************/
	ndids.resize(_Nnd);
	unsigned int Ind=0;
	for (unsigned int Iz = 0; Iz!=2; ++Iz){
		for (unsigned int Iy = 0; Iy!=2; ++Iy){
			for (unsigned int Ix = 0; Ix!=2; ++Ix){
				ndids[Ind].SetComponents(Ix,Iy,Iz);
				Ind++;
			}
		}
	}
	return 0;
}

int Cub8CCP::get_nodeid_w( const unsigned int& Ind, IntGVector& id){
	/******************************************
	 * Ind = id_0 * 1 + id_1 * 2 + id_2 * 4
	 ******************************************/
	if(Ind > 7) return -1;

	unsigned int idi, II = Ind;
	idi = II / 4;
	id.SetzComp(idi);
	II -= 4*idi;
	idi = II / 2;
	id.SetyComp(idi);
	idi = II - 2*idi;
	id.SetxComp(idi);
	return 0;
}

int Cub8CCP::get_ndcoord_w( const IntGVector& ndid, const GVector& dim, GVector& xnd ){
	/******************************************
	 * xi = 2*id - 1; x = xo + (xi - 0)/2 * L;
	 * here xo = 0;
	 ******************************************/
	xnd = (GVector) ndid;
	xnd *= 2;
	xnd -= 1;
	xnd *= dim; xnd *= 0.5;
	return 0;
}

Cub8Unit::Cub8Unit(const GVector& xc, const GVector& dim, Scalar Lcut, Scalar Nmin, Scalar dNmin) {
	_xc    = xc;
	_dim   = dim;
	_Lcut  = Lcut;
	_dNmin = dNmin;
	_Nmin  = Nmin;
	this-> init_setup();
}

Cub8Unit::Cub8Unit(Cub8Unit* proto) {
	_xc    = proto->_xc;
	_dim   = proto->_dim;
	_Lcut  = proto->_Lcut;
	_dNmin = proto->_dNmin;
	_Nmin  = proto->_Nmin;
	this-> init_setup();
}

inline void Cub8Unit::init_setup(){
	_Nnd = 8;
	_tol = GLOBAL_TOLERANCE_PRECISION;

	unsigned int shpkey[]={10001,10002,10008,10004, 10016,10032,10128,10064};
	_shpkeys.assign(shpkey, shpkey+8);
}

inline void Cub8Unit::get_shapedata(const GVector& xip, std::vector<Scalar>& shpdat) const {
	shpdat.resize(6);
	Scalar xi;
	for(unsigned int Id=0; Id != 3; ++Id){
		xi = xip.GetComp( Id );
		shpdat[Id]   = 0.5*(1 + xi);
		shpdat[3+Id] = 0.5;
	}
}

inline void Cub8Unit::get_N(const std::vector<Scalar>& shpdat, std::vector<Scalar>& N, std::vector<GVector>& gNxi) const{
	if ( N.size() < _Nnd) N.assign( _Nnd, 0);
	if ( gNxi.size() < _Nnd) gNxi.assign( _Nnd, GVector() );

	Scalar  Nxs[] = {1-shpdat[0], shpdat[0]};
	Scalar  Nys[] = {1-shpdat[1], shpdat[1]};
	Scalar  Nzs[] = {1-shpdat[2], shpdat[2]};
	Scalar dNxs[] = { -shpdat[3], shpdat[3]};
	Scalar dNys[] = { -shpdat[4], shpdat[4]};
	Scalar dNzs[] = { -shpdat[5], shpdat[5]};
	unsigned int Ind=0, Ix, Iy;
	for (unsigned int Iz = 0; Iz!=2; ++Iz){
		const Scalar& Nz  = Nzs[Iz];
		const Scalar& dNz = dNzs[Iz];
		for ( Iy = 0; Iy!=2; ++Iy){
			const Scalar& Ny  = Nys[Iy];
			const Scalar& dNy = dNys[Iy];
			for ( Ix = 0; Ix!=2; ++Ix){
				const Scalar& Nx  = Nxs[Ix];
				const Scalar& dNx = dNxs[Ix];
				N[Ind] = Nx*Ny*Nz;
				gNxi[Ind].SetComponents( dNx*Ny*Nz, Nx*dNy*Nz, Nx*Ny*dNz );
				Ind++;
			}
		}
	}
}

inline void Cub8Unit::get_modN(const std::vector<Scalar>& shpdat, std::vector<Scalar>& Ns, std::vector<GVector>& gNxis) const{
	std::vector<Scalar> modshpdat = shpdat;
	Scalar Le;
	for (unsigned int Id = 0; Id!=3; ++Id){
		const Scalar& N = shpdat[Id];
		if (N < _Nmin) modshpdat[Id] = _Nmin;
		else if ( N > (1-_Nmin) ) modshpdat[Id] = 1-_Nmin;

		Le = 0.5 - std::abs( N-0.5 );
		if (Le < _Lcut) modshpdat[Id+3] = (0.5-_dNmin)*Le/_Lcut + _dNmin;
	}
	this-> get_N(modshpdat, Ns, gNxis);
}

inline void Cub8Unit::get_N(const GVector& xipt, std::vector<Scalar>& N){
	if ( N.size() < _Nnd) N.assign( _Nnd, 0);

	std::vector< std::vector<Scalar> > N1ds;
	this->get_N1d(xipt, N1ds);

	const std::vector<Scalar>& Nxs = N1ds[0];
	const std::vector<Scalar>& Nys = N1ds[1];
	const std::vector<Scalar>& Nzs = N1ds[2];
	unsigned int Ind=0, Ix, Iy;
	for (unsigned int Iz = 0; Iz!=2; ++Iz){
		const Scalar& Nz = Nzs[Iz];
		for ( Iy = 0; Iy!=2; ++Iy){
			const Scalar& Ny = Nys[Iy];
			for ( Ix = 0; Ix!=2; ++Ix){
				N[Ind] = Nxs[Ix]*Ny*Nz;
				Ind++;
			}
		}
	}
}

inline void Cub8Unit::get_N(const GVector& xipt, std::vector<Scalar>& N, std::vector<GVector>& gNxi){
	if ( N.size() < _Nnd) N.assign( _Nnd, 0);
	if ( gNxi.size() < _Nnd) gNxi.assign( _Nnd, GVector() );

	std::vector< std::vector<Scalar> > N1ds;
	std::vector< std::vector<Scalar> > dN1ds;

	this-> get_N1d(xipt,N1ds);
	this-> get_dN1d(xipt,dN1ds);

	const std::vector<Scalar>& Nxs = N1ds[0];
	const std::vector<Scalar>& Nys = N1ds[1];
	const std::vector<Scalar>& Nzs = N1ds[2];
	const std::vector<Scalar>& dNxs = dN1ds[0];
	const std::vector<Scalar>& dNys = dN1ds[1];
	const std::vector<Scalar>& dNzs = dN1ds[2];
	unsigned int Ind=0, Ix, Iy;
	for (unsigned int Iz = 0; Iz!=2; ++Iz){
		const Scalar& Nz  = Nzs[Iz];
		const Scalar& dNz = dNzs[Iz];
		for ( Iy = 0; Iy!=2; ++Iy){
			const Scalar& Ny  = Nys[Iy];
			const Scalar& dNy = dNys[Iy];
			for ( Ix = 0; Ix!=2; ++Ix){
				const Scalar& Nx  = Nxs[Ix];
				const Scalar& dNx = dNxs[Ix];
				N[Ind] = Nx*Ny*Nz;
				gNxi[Ind].SetComponents( dNx*Ny*Nz, Nx*dNy*Nz, Nx*Ny*dNz );
				Ind++;
			}
		}
	}
}

inline void Cub8Unit::get_GNxi(const GVector& xipt, std::vector<GVector>& gNxi){
	if ( gNxi.size() < _Nnd) gNxi.assign( _Nnd, GVector() );

	std::vector< std::vector<Scalar> > N1ds;
	std::vector< std::vector<Scalar> > dN1ds;

	this-> get_N1d(xipt,N1ds);
	this-> get_dN1d(xipt,dN1ds);

	const std::vector<Scalar>& Nxs = N1ds[0];
	const std::vector<Scalar>& Nys = N1ds[1];
	const std::vector<Scalar>& Nzs = N1ds[2];
	const std::vector<Scalar>& dNxs = dN1ds[0];
	const std::vector<Scalar>& dNys = dN1ds[1];
	const std::vector<Scalar>& dNzs = dN1ds[2];
	unsigned int Ind=0, Ix, Iy;
	for (unsigned int Iz = 0; Iz!=2; ++Iz){
		const Scalar& Nz  = Nzs[Iz];
		const Scalar& dNz = dNzs[Iz];
		for ( Iy = 0; Iy!=2; ++Iy){
			const Scalar& Ny  = Nys[Iy];
			const Scalar& dNy = dNys[Iy];
			for ( Ix = 0; Ix!=2; ++Ix){
				const Scalar& Nx  = Nxs[Ix];
				const Scalar& dNx = dNxs[Ix];
				gNxi[Ind].SetComponents( dNx*Ny*Nz, Nx*dNy*Nz, Nx*Ny*dNz );
				Ind++;
			}
		}
	}
}

void Cub8Unit::get_GN(const GVector& xipt, const std::vector<GVector>& xnds, std::vector<GVector>& gN){
	std::vector<GVector> gNxi;
	this -> get_GNxi( xipt, gNxi);
	this -> get_GN( gNxi, xnds, gN);
}

void Cub8Unit::get_GN(const std::vector<GVector>& gNxi, const std::vector<GVector>& xnds, std::vector<GVector>& gN){
	Scalar xmeg = xnds[0].GetNorm();
	GTensor dxdxi = gNxi[0] & xnds[0]; // exi o ex
	for(unsigned int Ind=1; Ind != _Nnd; ++Ind){
		dxdxi += gNxi[Ind] & xnds[Ind];
		xmeg   = std::max(xmeg,xnds[Ind].GetNorm());
	}
	dxdxi.admin_dumperror(xmeg*GLOBAL_TOLERANCE_PRECISION*0.5);

	GTensor& dxidx = dxdxi;
	dxidx = dxdxi.GetInverse(); // ex o exi

	if ( gN.size() < _Nnd) gN.assign( _Nnd, GVector() );
	for(unsigned int Ind=0; Ind != _Nnd; ++Ind) gN[Ind] = dxidx * gNxi[Ind]; // ex o exi . exi -> ex
}

inline void Cub8Unit::get_GNo(const GVector& xipt, std::vector<GVector>& gNo){
	this -> get_GNxi( xipt, gNo);
	GVector dxidx = 2.0/_dim;
	for(unsigned int Ind=0; Ind != _Nnd; ++Ind) gNo[Ind] *= dxidx;
}

inline void Cub8Unit::get_GNo(const std::vector<GVector>& gNxi, std::vector<GVector>& gNo){
	if ( gNo.size() != _Nnd) gNo.assign( _Nnd, GVector() );
	GVector dxidx = 2.0/_dim;
	for(unsigned int Ind=0; Ind != _Nnd; ++Ind) {
		gNo[Ind] = gNxi[Ind]*dxidx;
	}
}

inline void Cub8Unit::get_xcorners(std::vector<GVector>& xcorners){
	xcorners.assign(_Nnd, _xc);

	GVector Ls = 0.5*_dim;
	IntGVector signs;

	unsigned int Ind=0;
	for (unsigned int Iz = 0; Iz!=2; ++Iz){
		for (unsigned int Iy = 0; Iy!=2; ++Iy){
			for (unsigned int Ix = 0; Ix!=2; ++Ix){
				signs.SetComponents((2*Ix-1),(2*Iy-1),(2*Iz-1));
				xcorners[Ind] += Ls*signs;
				Ind++;
			}
		}
	}
}

inline void Cub8Unit::get_localcoord(int Ind, MathVec<int>& xloc){
	int Iz = Ind/4;
	Ind -= Iz*4;
	int Iy = Ind/2;
	Ind -= Iy*2;
	int Ix = Ind;

	Ix *= 2; Ix--;
	Iy *= 2; Iy--;
	Iz *= 2; Iz--;

	xloc.resize(3);
	xloc[0] =  Ix; xloc[1] =  Iy; xloc[2] =  Iz;
}// end function


void Cub8Unit::get_Inodes(unsigned int Idir, unsigned int Iside, std::vector<unsigned int> Inds){
	Inds.assign(4,0);
	switch(Idir*2+Iside){
	case 0:
		Inds[0] = 0; Inds[1] = 2; Inds[2] = 4; Inds[3] = 6;
		break;
	case 1:
		Inds[0] = 1; Inds[1] = 3; Inds[2] = 5; Inds[3] = 7;
		break;
	case 2:
		Inds[0] = 0; Inds[1] = 1; Inds[2] = 4; Inds[3] = 5;
		break;

	case 3:
		Inds[0] = 2; Inds[1] = 3; Inds[2] = 6; Inds[3] = 7;
		break;

	case 4:
		Inds[0] = 0; Inds[1] = 1; Inds[2] = 2; Inds[3] = 3;
		break;

	case 5:
		Inds[0] = 4; Inds[1] = 5; Inds[2] = 6; Inds[3] = 7;
		break;
	default:
		std::cout<<"WARNING: unknown key @ (dir,side): "<< Idir<<", "<<Iside<<std::endl;
		break;
	}
}

inline void Cub8Unit::get_N1d(const GVector& xipt, std::vector< std::vector<Scalar> >& N1ds){
	N1ds.resize(3);
	Scalar xi;
	for(unsigned int Id=0; Id != 3; ++Id){
		std::vector<Scalar>& Nxs = N1ds[Id];
		Nxs.resize(2);
		xi  = xipt.GetComp(Id);
		Nxs[0] = 0.5*(1 - xi);
		Nxs[1] = 0.5*(1 + xi);
	}
}

inline void Cub8Unit::get_dN1d(const GVector& xipt, std::vector< std::vector<Scalar> >& dN1ds){
	dN1ds.resize(3);
	for(unsigned int Id=0; Id != 3; ++Id){
		std::vector<Scalar>& dNxs = dN1ds[Id];
		dNxs.resize(2);
		dNxs[0] = -0.5;
		dNxs[1] =  0.5;
	}
}

//inline void Cub8Unit::get_modN1d(const GVector& xipt, std::vector< std::vector<Scalar> >& N1ds){
//	N1ds.resize(3);
//	Scalar xi, axi, cc;
//	for(unsigned int Id=0; Id != 3; ++Id){
//		std::vector<Scalar>& Nxs = N1ds[Id];
//		Nxs.resize(2);
//		xi  = xipt.GetComp(Id);
//		axi = std::abs( xi );
//		if (axi >_Lcut){
//			cc = ( (1-axi)*(1-axi) / (4-4*_Lcut) - 0.25*(1+_Lcut) )*(1-2*_dNmin) - _dNmin*axi;
//			if (xi < 0 ) cc *= -1;
//			Nxs[0] = 0.5 + cc;
//			Nxs[1] = 0.5 - cc;
//		}
//		else{
//			Nxs[0] = 0.5*(1 - xi);
//			Nxs[1] = 0.5*(1 + xi);
//		}
//	}
//}
//
//inline void Cub8Unit::get_moddN1d(const GVector& xipt, std::vector< std::vector<Scalar> >& dN1ds){
//	dN1ds.resize(3);
//	Scalar axi, c1;
//	for(unsigned int Id=0; Id != 3; ++Id){
//		std::vector<Scalar>& dNxs = dN1ds[Id];
//		dNxs.resize(2);
//		axi = std::abs( xipt.GetComp(Id) );
//		if (axi >_Lcut){
//			c1 = (1-axi)*(1-2*_dNmin)/(2-2*_Lcut) + _dNmin;
//	        dNxs[0] = -c1;
//	        dNxs[1] =  c1;
//		}
//		else{
//			dNxs[0] = -0.5;
//			dNxs[1] =  0.5;
//		}
//	}
//}
//inline void Cub8Unit::get_modN(const GVector& xipt, std::vector<Scalar>& N, std::vector<GVector>& gNxi){
//	if ( N.size() < _Nnd) N.assign( _Nnd, 0);
//	if ( gNxi.size() < _Nnd) gNxi.assign( _Nnd, GVector() );
//
//	std::vector< std::vector<Scalar> > N1ds;
//	std::vector< std::vector<Scalar> > dN1ds;
//
//	this-> get_N1d(xipt,N1ds);
//	this-> get_moddN1d(xipt,dN1ds);
//
//	Scalar bot = 1e-3;
//	for (int Id=0; Id != 3; ++Id){
//		if(N1ds[Id][0] < bot ){
//			N1ds[Id][0] = bot;
//			N1ds[Id][1] = 1-bot;
//		}
//		else if(N1ds[Id][1] < bot ){
//			N1ds[Id][1] = bot;
//			N1ds[Id][0] = 1-bot;
//		}
//	}
//
//	const std::vector<Scalar>& Nxs = N1ds[0];
//	const std::vector<Scalar>& Nys = N1ds[1];
//	const std::vector<Scalar>& Nzs = N1ds[2];
//
//	const std::vector<Scalar>& dNxs = dN1ds[0];
//	const std::vector<Scalar>& dNys = dN1ds[1];
//	const std::vector<Scalar>& dNzs = dN1ds[2];
//	unsigned int Ind=0, Ix, Iy;
//	for (unsigned int Iz = 0; Iz!=2; ++Iz){
//		const Scalar& Nz  = Nzs[Iz];
//		const Scalar& dNz = dNzs[Iz];
//		for ( Iy = 0; Iy!=2; ++Iy){
//			const Scalar& Ny  = Nys[Iy];
//			const Scalar& dNy = dNys[Iy];
//			for ( Ix = 0; Ix!=2; ++Ix){
//				const Scalar& Nx  = Nxs[Ix];
//				const Scalar& dNx = dNxs[Ix];
//				N[Ind] = Nx*Ny*Nz;
//				gNxi[Ind].SetComponents( dNx*Ny*Nz, Nx*dNy*Nz, Nx*Ny*dNz );
//				Ind++;
//			}
//		}
//	}
//}
