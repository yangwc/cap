#ifndef ELMMAKER_H_
#define ELMMAKER_H_

#include "../../GB_Tensor.h"

template <class _elm>
class BasicElmMaker {
public:
	BasicElmMaker(){;}
	virtual ~BasicElmMaker(){;}

	inline void set_dimension(const GVector& dim){ _dim = dim; }

	virtual inline _elm* get_new(const IntGVector& adpid, const GVector& coord, ShpUnit* shp){
		_elm* elmptr = new _elm(shp);
		elmptr->assign_id( adpid );
		return elmptr;
	}
protected:
	GVector _dim;
};

#endif /* ELMMAKER_H_ */
