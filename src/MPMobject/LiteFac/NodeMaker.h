#ifndef NODEMAKER_H_
#define NODEMAKER_H_

#include <vector>
#include "../../GB_Tensor.h"

class ShpSKH;

template <class _node>
class BasicNodeMaker {
public:
	BasicNodeMaker(){ _theta = 0.0; }
	~BasicNodeMaker(){;}

	inline void set_Cintg(Scalar theta){ _theta = theta; }

	_node* get_new(const IntGVector& id, const GVector& coord, ShpSKH* skh){
		_node* ndptr = new _node(id, coord, skh);
		return ndptr;
	}

private:
	Scalar _theta;
};
#endif /* NODEMAKER_H_ */
