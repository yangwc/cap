/**************************************************
 * Objinfo.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 17, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#include "Objinfo.h"

//#include "DeformObjs.h"

Objinfo::Objinfo( unsigned int objid) {
	_objid = objid;
	_material = 0;


	_Xref = 0; _vel = 0; _bforce = 0;

	_Iptnew = 0;
	_Rdamp = 0.02;
}

Objinfo::Objinfo(const Objinfo& proto){
	_objid = proto._objid;

	_material = 0; _Xref = 0; _vel = 0; _bforce = 0;
	if (proto._material != 0) _material = proto._material->get_new_obj();
	if (proto._Xref != 0) _Xref = new GVector(*proto._Xref);
	if (proto._vel != 0) _vel = new GVector(*proto._vel);
	if (proto._bforce != 0) _bforce = new GVector(*proto._bforce);

	_Iptnew = proto._Iptnew;
	_Rdamp = proto._Rdamp;
}

Objinfo::~Objinfo() {
	delete _material; _material = 0;
	delete _Xref;_Xref = 0;
	delete _vel; _vel = 0;
	delete _bforce; _bforce = 0;
}

int Objinfo::get_object_info(Particle* ptptr, const Scalar& tnow){
	/*** Know nothing about other object ***/
	if ( ptptr->get_objectID() != _objid) return -1;

	/******************************************************
	 * Basic setup
	 ******************************************************/
	ptptr->assign_material( _material->get_new_obj() );
	ptptr->set_damping_ratio(_Rdamp);

	/******************************************************
	 * Setup object status
	 ******************************************************/
	if (_vel != 0) ptptr->set_velocity( *_vel );
	if (_bforce != 0) ptptr->set_bodyforce( *_bforce );

	// default reference point is (0,0,0)
	GVector xpt = ptptr->get_coord();
	if (_Xref != 0) xpt += *_Xref; ptptr->set_coord( xpt );

	/******************************************************
	 * Tracking maximum particle id to prevent duplication
	 * when creating particles automatically.
	 ******************************************************/
	int ptidnext = ptptr->get_ID()+1;
	if (_Iptnew < ptidnext) _Iptnew = ptidnext;
	return 0;
}

Particle* Objinfo::get_new_particle(const GVector& xpt, const Scalar& tnow){
	Particle* ptptr = new Particle(_Iptnew, 0);
	ptptr->set_time(tnow);
	ptptr->set_coord(xpt);
	_Iptnew++;
	/******************************************************
	 * Basic setup
	 ******************************************************/
	ptptr->assign_objectID(_objid);
	ptptr->assign_material( _material->get_new_obj() );
	ptptr->set_damping_ratio(_Rdamp);

	/******************************************************
	 * Setup object status
	 ******************************************************/
	if (_vel != 0) ptptr->set_velocity( *_vel );
	if (_bforce != 0) ptptr->set_bodyforce( *_bforce );
	return ptptr;
}

