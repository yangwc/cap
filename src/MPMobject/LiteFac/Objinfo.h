/**************************************************
 * Objinfo.h
 * 	   - Objinfo()
 * ------------------------------------------------
 * 	+ Object information center
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 17, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef OBJINFO_H_
#define OBJINFO_H_

#include "../../MPMcomputer/Particle.h"
#include "../../Function/Function.h"

class Objinfo {
public:
	Objinfo(unsigned int objid);
	Objinfo(const Objinfo& proto);
	virtual ~Objinfo();

	int assign_material(Material* mat){delete _material; _material = mat; return 0;}

	int set_damping_ratio(const Scalar Rdamp){_Rdamp = std::max(Rdamp, 0.0); return 0;}
	int set_start_ptID(long long int idsta){_Iptnew = idsta; return 0;}
	int set_velocity(GVector* vec){delete _vel; _vel = vec; return 0;}
	int set_bodyforce(GVector* vec){delete _bforce; _bforce = vec; return 0;}
	int reset_original_point(GVector* vec){delete _Xref; _Xref = vec; return 0;}

	int get_object_info(Particle* ptptr, const Scalar& tnow);
	Particle* get_new_particle(const GVector& xpt, const Scalar& tnow);

	inline Objinfo* get_new_object() const { return new Objinfo(*this); }
	inline Material* get_new_material() const { return _material->get_new_obj();}
	inline std::vector<Scalar> get_property() const { return _material -> get_property(); }
	inline void get_property(std::vector<Scalar>& props) const { _material -> get_property( props ); }
	inline unsigned int get_matcode() const { return _material ->_get_code(); }
	inline unsigned int get_Nstatevals() const { return _material->get_Nstatevals(); }

private:
	void setup_velocity(Particle* ptptr, const Scalar& tnow);
	void setup_bforce(Particle* ptptr, const Scalar& tnow);

	long long int _Iptnew;
	unsigned int _objid;

	Material* _material;
	GVector *_Xref, *_vel, *_bforce;
	Scalar _Rdamp; // damping ratio

};

#endif /* OBJINFO_H_ */
