/**************************************************
 * O_odb.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#include "O_odb.h"

/***********************************************
 *	outodb : public Output
 ***********************************************/
outodb::outodb(){
	_Lint  = sizeof(int);
	_Lreal = sizeof(Scalar);
	this -> _myversion();
	_sep = ",";
}
outodb::outodb(const std::string& bname, const std::string& ftype){
	_Lint  = sizeof(int);
	_Lreal = sizeof(Scalar);
	this -> _myversion();
	_sep = ",";
	this -> openfile(bname, ftype, true);
}

void outodb::print_header( int objID,
		unsigned int matcode, const std::vector<Scalar>& props, unsigned int Nstate,
		const std::vector<Scalar>& stimes){
	std::string vname;
	unsigned int Ldata;

	this -> print_sysinfo(_Lint, _Lreal);

	/*****************/
	/*** object ID & material info***/
	_ofile.write( (char *) &objID, _Lint );
	_ofile.write( (char *) &matcode, _Lint );

	Ldata = props.size();
	_ofile.write( (char *) &Ldata, _Lint );
	this -> printvals_bin(props, &_ofile);

	/*************************/
	/*** Ldata & data name ***/
	Ldata  = 11; // ptID, mass,  x.1, x.2, x.3,  v.1, v.2, v.3,  b.1, b.2, b.3
	Ldata += Nstate;
	_ofile.write( (char *) &Ldata, _Lint );
	_ofile<< "pt_ID"
		  <<_sep<< "mass"
		  <<_sep<< "x_1" <<_sep<< "x_2" <<_sep<< "x_3"
		  <<_sep<< "v_1" <<_sep<< "v_2" <<_sep<< "v_3"
		  <<_sep<< "b_1" <<_sep<< "b_2" <<_sep<< "b_3";
	for(unsigned int Istate = 0; Istate != Nstate; ++Istate){
		_ofile<<_sep<< "state_" << (Istate+1);
	}
	_ofile<< std::endl;

	/**************************/
	/*** Nstep & step times ***/
	unsigned int Nt = stimes.size();
	_ofile.write( (char *) &Nt, _Lint );
	for (std::vector<Scalar>::const_iterator ivec = stimes.begin(); ivec != stimes.end(); ++ivec){
		_ofile.write( (char *) &(*ivec), _Lreal );
//		_ofile<<_sep<< (*ivec);
	}
//	_ofile<< std::endl;
}


void outodb::print_onestep(const Scalar& tnow, const Ptpvec& ptptrs){
	/*
	 * writes information for current time step
	 */
	unsigned int auint = ptptrs.size();
	_ofile.write( (char *) &auint, _Lint );
	/********************
	 * Particle data
	 ********************/
	Particle* ptptr = 0;
	for(Ptpvec::const_iterator ipt =ptptrs.begin(); ipt != ptptrs.end(); ++ipt){
		ptptr = (*ipt);
		_temp_scas.clear();
		ptptr->get_state(_temp_scas);
		/*--- general output ---*/
		auint = ptptr->get_ID(); _ofile.write( (char *) &auint, _Lint );
		_ofile.write( (char *) &(ptptr->get_mass()), _Lreal );
		this -> printtensor_bin( ptptr->get_coord(), &_ofile);
		this -> printtensor_bin( ptptr->get_velocity(), &_ofile);
		this -> printtensor_bin( ptptr->get_bodyforce(), &_ofile);
		this -> printvals_bin(_temp_scas, &_ofile);
	}// end for (pt)
	ptptr = 0;
}
