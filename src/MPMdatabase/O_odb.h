/**************************************************
 * O_odb.h
 * ------------------------------------------------
 * 	+ output information of particles
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.01.27)
 **************************************************/
#ifndef OUTODB_H_
#define OUTODB_H_

#include "../MPMcomputer/Particle.h"
#include "pval_pt.h"

#include "Output.h"

class outodb : public Output, public PVal{
protected:
	typedef std::vector<Particle*> Ptpvec;

public:
	outodb();
	outodb(const std::string& bname,const std::string& ftype=".odb");
	virtual ~outodb(){;}

    void print_header( int objID,
    		unsigned int matcode, const std::vector<Scalar>& props, unsigned int Nstate,
    		const std::vector<Scalar>& stimes);
    void print_onestep(const Scalar& tnow, const Ptpvec& ptptrs);

private:
    void _myversion(){_version = "1.0.0";}
    std::string _sep;
    std::vector<Scalar> _temp_scas;
    unsigned int _Lint, _Lreal;
};
#endif // OUTODB_H_

