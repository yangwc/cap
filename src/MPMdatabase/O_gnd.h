/**************************************************
 * O_gnd.h
 * ------------------------------------------------
 * 	+ output data of grid nodes
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#ifndef OUTGND_H_
#define OUTGND_H_

#include "../MPMcomputer/Grid.h"

#include "Output.h"

class outgnd : public Output, public PVal{
public:
	outgnd();
	outgnd(const std::string& bname,const std::string& ftype=".gnd");
	virtual ~outgnd(){;}

	int open(const std::string& bname,const std::string& ftype=".gnd");

    void print_header( int objID, const std::vector<Scalar>& stimes);
    void print_onestep(const Scalar& tnow, const Grid* grid);

private:
    void _myversion(){_version = "1.0.0";}
    std::string _sep; //, _srcfname, _exeversion,
};

#endif // OUTGND_H_

