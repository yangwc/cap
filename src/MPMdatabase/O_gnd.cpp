/**************************************************
 * O_gnd.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#include "O_gnd.h"

/***********************************************
 *	outgnd : public Output
 ***********************************************/
outgnd::outgnd(){
	_myversion();
	_msg = "";
	_sep = ",";
}
outgnd::outgnd(const std::string& bname, const std::string& ftype){
	_myversion();
	_msg = "";
	_sep = ",";
	this -> open(bname, ftype);
}

int outgnd::open(const std::string& bname,const std::string& ftype){
	this -> openfile(bname, ftype);
	_ofile.precision(6);
	_ofile << std::scientific;
	return 0;
}

void outgnd::print_header( int objID, const std::vector<Scalar>& stimes){
	std::string vname;
	unsigned int Ldata;

	/*****************/
	/*** object ID ***/
	_ofile<< objID << std::endl;

	/*************************/
	/*** Ldata & data name ***/
//	Ldata = 5; // Ind, x.x, x.y, x.z, Nbc
//	ofile<< Ldata <<_sep<< "Ind" <<_sep<< "x_1" <<_sep<< "x_2" <<_sep<< "x_3" <<_sep<< "Nbc";

	// Ind, x.x, x.y, x.z, Nbc,
	// mi,
	// pi.x, pi.y, pi.z,
	// fiint.x, fiint.y, fiint.z,
	// fiext.x, fiext.y, fiext.z,
	// fibc.x, fibc.y, fibc.z,
	Ldata = 18;
	_ofile<< Ldata <<_sep<< "Ind" <<_sep<< "x_1" <<_sep<< "x_2" <<_sep<< "x_3" <<_sep<< "Nbc";
	_ofile<<_sep<< "mi" <<_sep<< "pi_1" <<_sep<< "pi_2" <<_sep<< "pi_3";
	_ofile<<_sep<< "fiin_1" <<_sep<< "fiin_2" <<_sep<< "fiin_3";
	_ofile<<_sep<< "fiex_1" <<_sep<< "fiex_2" <<_sep<< "fiex_3";
	_ofile<<_sep<< "fibc_1" <<_sep<< "fibc_2" <<_sep<< "fibc_3";
	_ofile<< std::endl;

	/**************************/
	/*** Nstep & step times ***/
	_ofile<< stimes.size();
	for (std::vector<Scalar>::const_iterator ivec = stimes.begin(); ivec != stimes.end(); ++ivec){
		_ofile<<_sep<< (*ivec);
	}
	_ofile<< std::endl;
}

void outgnd::print_onestep(const Scalar& tnow, const Grid* grid){
	/*********************************************
	 * writes information for current time step
	 *********************************************/
	unsigned int Nnd = grid->get_Nnode();
	_ofile << Nnd << std::endl;

	/********************
	 * Particle data
	 ********************/
	const Gnode* ndptr = 0;
	unsigned int Jnd = 0;
	for(unsigned int Ind = 0; Ind != Nnd; ++Ind){
		ndptr = grid-> get_Node(Ind);
		/*--- general output ---*/
		_ofile<< Jnd << _sep;
		this -> printtensor( ndptr->get_coord(), _sep, &_ofile);
		_ofile<< _sep;
		_ofile<< ndptr-> get_Nboundary();
		_ofile<< _sep;
		_ofile<< ndptr-> get_mass();
		_ofile<< _sep;
		this -> printtensor( ndptr->get_momt(), _sep, &_ofile);
		_ofile<< _sep;
		this -> printtensor( ndptr->get_fint(), _sep, &_ofile);
		_ofile<< _sep;
		this -> printtensor( ndptr->get_fext(), _sep, &_ofile);
		_ofile<< _sep;
		this -> printtensor( ndptr->get_fbc(), _sep, &_ofile);
		/*--- end line ---*/
		_ofile<< std::endl;
		++Jnd;
	}// end for (pt)
	ndptr = 0;
}
