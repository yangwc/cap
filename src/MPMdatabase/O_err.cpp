/**************************************************
 * O_err.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#include "O_err.h"
#include <ctime>

outerr::outerr(){
	this -> _myversion();
	this -> appendfile("new_error", ".err");
	this -> _print_welcome();
}

outerr::outerr(const std::string& bname, const std::string& ftype){
	this -> _myversion();
	this -> appendfile(bname, ftype);
	this -> _print_welcome();
}

void outerr::_print_welcome(){
	time_t tsta; time(&tsta);
	std::string tstr = asctime (localtime(&tsta) );
	_ofile << "__________________________________________"<<std::endl;
	_ofile << "*** ERROR MESSAGE *** "<<tstr.substr(4,15)<<" ***"<<std::endl;
}
