/**************************************************
 * O_log.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (DEC 22, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.22)
 **************************************************/
#include "O_log.h"
#include <ctime>

outlog::outlog(const std::string& infnm){
	this -> _myversion();
	this -> appendfile("mpm", ".log");
	this -> _print_welcome(infnm);
}

void outlog::_print_welcome(const std::string& infnm){
	time_t tsta; time(&tsta);
	std::string tstr = asctime (localtime(&tsta) );
	_ofile << "[ "<<tstr.substr(4,15)<<" ]: "<<infnm<<std::endl;
}
