/**************************************************
 * O_log.h
 * ------------------------------------------------
 * 	+ output log messages
 * ================================================
 * Created by: YWC @ CEE, UW (DEC 22, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.22)
 **************************************************/
#ifndef OUTLOG_H_
#define OUTLOG_H_

#include "Output.h"
class outlog : public Output{
  public:
	outlog(const std::string& infnm);
	~outlog(){;}

  private:
	void _myversion(){_version = "3.0.0";}
	void _print_welcome(const std::string& infnm);
};
#endif // OUTLOG_H_

