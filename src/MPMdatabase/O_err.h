/**************************************************
 * O_err.h
 * ------------------------------------------------
 * 	+ output error messages
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#ifndef OUTERR_H_
#define OUTERR_H_

#include "Output.h"
class outerr : public Output{
  public:
	outerr();
	outerr(const std::string& bname, const std::string& ftype=".err");
	~outerr(){;}

  private:
	void _myversion(){_version = "1.0.0";}
	void _print_welcome();
};
#endif // OUTERR_H_

