#ifndef PVAL_H_
#define PVAL_H_

#include <ostream>
#include <vector>

class PVal{
public:
	virtual ~PVal(){;}
protected:
	/******************************************
	 * ascii output
	 ******************************************/
	template <class _Val>
	inline void printval(const _Val& aval, std::ostream* optr) const { (*optr)<<aval<<"\t"; }
	template <class _GTensor>
	inline void printtensor(const _GTensor& aten, std::ostream* optr) const { aten.Print(optr); }
	template <class _GTensor>
	inline void printtensor(const _GTensor& aten, const std::string& sep, std::ostream* optr) const { aten.Print(optr,sep); }
	template <class _Val>
	inline void printvals(const std::vector<_Val>& vals, const std::string& sep, std::ostream* optr) const {
		std::ostream& out = (*optr);
		typename std::vector<_Val>::const_iterator ivec = vals.begin();
		if( ivec != vals.end() ){
			out<<(*ivec);
			++ivec;
			while(ivec != vals.end()){
				out<<sep<<(*ivec);
				++ivec;
			}
		}
	}
	template <class _Val>
	inline void printvals(const std::vector<_Val>& vals, std::ostream* optr) const {
		this -> printvals(vals, "\t", optr);
		(*optr)<<"\t";
	}

	/******************************************
	 * binary output
	 ******************************************/
	template <class _Val>
	inline void printval_bin(const _Val& aval, std::ostream* optr) const { optr->write( (char*) aval, sizeof(aval) ); }
	template <class _GTensor>
	inline void printtensor_bin(const _GTensor& aten, std::ostream* optr) const { aten.PrintBin(optr); }
	template <class _Val>
	inline void printvals_bin(const std::vector<_Val>& vals, std::ostream* optr) const {
		unsigned int Ldat = sizeof(vals[0]);
		std::ostream& out = (*optr);
		for( typename std::vector<_Val>::const_iterator ivec = vals.begin(); ivec != vals.end(); ++ivec ){
			out.write( (char*) &(*ivec), Ldat );
		}
	}
};
#endif /* PVAL_H_ */
