#ifndef PVAL_PT_H_
#define PVAL_PT_H_

#include <string>
#include "../MPMcomputer/Particle.h"

#include "pval.h"

class PTPVal : public PVal{
public:
	virtual ~PTPVal(){;}
	virtual int print(Particle* ptptr, const std::string& sep, std::ostream* optr) const = 0;
	virtual int print(Particle* ptptr, std::ostream* optr) const = 0;
	virtual PTPVal* get_new_obj() const = 0;

	inline const std::string& get_name() const { return _strname; }
	inline unsigned int get_Nvalue() const { return _Nval; }

protected:
	std::string _strname;
	unsigned int _Nval;
};

class PTPvel : public PTPVal{
public:
	PTPvel(){ _strname = "Velocity"; _Nval = 3; }
	inline int print(Particle* ptptr, std::ostream* optr) const{
		this -> printtensor( ptptr->get_velocity(), optr);
		return 0;
	}
	inline int print(Particle* ptptr, const std::string& sep, std::ostream* optr) const{
		this -> printtensor( ptptr->get_velocity(), sep, optr);
		return 0;
	}
	inline PTPVal* get_new_obj() const { return new PTPvel(); }
};

class PTPmass : public PTPVal{
public:
	PTPmass(){ _strname = "Mass"; _Nval = 1; }
	inline int print(Particle* ptptr, std::ostream* optr) const {
		this -> printval( ptptr->get_mass(), optr);
		return 0;
	}
	inline int print(Particle* ptptr, const std::string& sep, std::ostream* optr) const {
		(*optr)<<ptptr->get_mass();
		return 0;
	}
	inline PTPVal* get_new_obj() const { return new PTPmass(); }
};

class PTPdensity : public PTPVal{
public:
	PTPdensity(){ _strname = "Density"; _Nval = 1; }
	inline int print(Particle* ptptr, std::ostream* optr) const {
		this -> printval( ptptr->get_density(), optr);
		return 0;
	}
	inline int print(Particle* ptptr, const std::string& sep, std::ostream* optr) const {
		(*optr)<<ptptr->get_density();
		return 0;
	}
	inline PTPVal* get_new_obj() const { return new PTPdensity(); }
};

class PTPstress : public PTPVal{
public:
	PTPstress(){ _strname = "Stress"; _Nval = 6; }
	virtual ~PTPstress(){;}
	inline int print(Particle* ptptr, std::ostream* optr) const {
		this -> printtensor( ptptr->get_stress(), optr);
		return 0;
	}
	inline int print(Particle* ptptr, const std::string& sep, std::ostream* optr) const {
		this -> printtensor( ptptr->get_stress(), sep, optr);
		return 0;
	}
	inline PTPVal* get_new_obj() const { return new PTPstress(); }
};
#endif /* VALGET_PT_H_ */
