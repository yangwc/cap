/**************************************************
 * O_obj.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#include "O_obj.h"

/***********************************************
 *	outobj : public Output
 ***********************************************/
outobj::outobj(){
	_myversion();
	_sep = ",";
}
outobj::outobj(const std::string& bname, const std::string& ftype){
	_myversion();
	_sep = ",";
	this -> open(bname, ftype);
}

outobj::~outobj(){
	for (std::vector<PTPVal*>::iterator ivec = outkeys.begin(); ivec != outkeys.end(); ++ivec){
		delete (*ivec); (*ivec) = 0;
	}
}

int outobj::open(const std::string& bname,const std::string& ftype){
	this -> openfile(bname, ftype);
	_ofile.precision(6);
	_ofile << std::scientific;
	return 0;
}

void outobj::print_header( int objID, const std::vector<Scalar>& stimes){
	std::string vname;
	unsigned int Nvcomp, Ldata;
	unsigned int Iout, Ivar;

	/*****************/
	/*** object ID ***/
	_ofile<< objID << std::endl;

	/*************************/
	/*** Ldata & data name ***/
	Ldata = 4; // ptID, x.x, x.y, x.z
	for(Iout = 0; Iout< outkeys.size(); ++Iout){
		Ldata += outkeys[Iout]->get_Nvalue();
	}// end for (out)
	_ofile<< Ldata <<_sep<< "pt_ID" <<_sep<< "x_1" <<_sep<< "x_2" <<_sep<< "x_3";

	for(Iout = 0; Iout< outkeys.size(); ++Iout){
		Nvcomp = outkeys[Iout]->get_Nvalue();
		vname  = outkeys[Iout]->get_name();
		for(Ivar = 0; Ivar != Nvcomp; ++Ivar){ _ofile<<_sep<< vname << "_"<< (Ivar+1); }
	}// end for (out)
	_ofile<< std::endl;

	/**************************/
	/*** Nstep & step times ***/
	_ofile<< stimes.size();
	for (std::vector<Scalar>::const_iterator ivec = stimes.begin(); ivec != stimes.end(); ++ivec){
		_ofile<<_sep<< (*ivec);
	}
	_ofile<< std::endl;
}


void outobj::print_onestep(const Scalar& tnow, const Ptpvec& ptptrs){
	/*
	 * writes information for current time step
	 */
	_ofile << ptptrs.size() << std::endl;

	/********************
	 * Particle data
	 ********************/
	Particle* ptptr = 0;
	unsigned int Iout;
	for(Ptpvec::const_iterator ipt =ptptrs.begin(); ipt != ptptrs.end(); ++ipt){
		ptptr = (*ipt);
		/*--- general output ---*/
		_ofile<< ptptr->get_ID() << _sep;
		this -> printtensor( ptptr->get_coord(), _sep, &_ofile);

		/*--- other output ---*/
		for(Iout = 0; Iout< outkeys.size(); ++Iout){
			_ofile<<_sep;
			outkeys[Iout] -> print( ptptr, _sep, &_ofile);
		}// end for (out)

		/*--- end line ---*/
		_ofile<< std::endl;
	}// end for (pt)
	ptptr = 0;
}
