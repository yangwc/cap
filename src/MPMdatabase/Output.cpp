/**************************************************
 * Output.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#include "Output.h"
#include <sys/stat.h>

bool i_file_exist(std::string& filename){
	struct stat buff;
	return ( stat(filename.c_str(),&buff) == 0 );
}
/***********************************************
 *	Output
 ***********************************************/
Output::Output(){
	_myversion();
	_msg = "";
}

Output::Output(const std::string& bname, const std::string& ftype){
	_myversion();
	_msg = "";
	openfile(bname, ftype);
}

std::string Output::opennewfile(const std::string& bname, const std::string& ftype){
	/*********************************
	 * Return used basename.
	 *********************************/
	this -> close();
	_basename = bname;
	_filetype = ftype;
	_filename = _basename+_filetype;
	unsigned int II=1;

	while( i_file_exist(_filename) ){
		std::stringstream midname;
		midname<<"_"<<II;
		std::cout<<"WARNING: "<<_filename<<" has already existed. File named ";
		_basename = bname+midname.str();
		_filename = _basename+_filetype;
		std::cout<<_filename<<" will be used."<<std::endl;
	}// end while

	_ofile.open(_filename.c_str(),std::ios::out|std::ios::trunc);
	if (!_ofile.is_open()){
		std::cerr<<"Failed at opening file "<<_filename<<"."<<std::endl;
		return "";
	}
	else{return _basename;} // end if
}// end function (Output::opennewfile)

void Output::openfile(const std::string& bname, const std::string& ftype, bool ibin){
	if (_ofile.is_open()){_ofile.close();}
	_basename = bname;
	_filetype = ftype;
	_filename = (_basename+_filetype);
	if( i_file_exist(_filename) ){
		std::cout<<"WARNING: "<<_filename<<" will be overwritten."<<std::endl;
	}
	if (ibin){
		_ofile.open(_filename.data(),std::ios::out|std::ios::trunc|std::ios::binary);
	}
	else{
		_ofile.open(_filename.data(),std::ios::out|std::ios::trunc);
	}
	if (!_ofile.is_open()){std::cerr<<"Failed at opening file "<<_filename<<"."<<std::endl;}
}// end function (Output::openfile)

void Output::appendfile(const std::string& bname, const std::string& ftype, bool ibin){
	if (_ofile.is_open()){_ofile.close();}
	_basename = bname;
	_filetype = ftype;
	_filename = (_basename+_filetype);
	if (ibin){
		_ofile.open(_filename.data(),std::ios::out|std::ios::app|std::ios::binary);
	}
	else{
		_ofile.open(_filename.data(),std::ios::out|std::ios::app);
	}
	if (!_ofile.is_open()){std::cerr<<"Failed at opening file "<<_filename<<"."<<std::endl;}
}// end function (Output::openfile)

void Output::print_sysinfo(unsigned int Lint, unsigned int Lreal){
	if(Lint == 0)  Lint  = sizeof(int);
	if(Lreal == 0) Lreal = sizeof(double);
	_ofile<<Lint<<','<<Lreal
		  <<": *** sizes (byte) of integers and real numbers used in the output ***"
		  <<std::endl;
}
