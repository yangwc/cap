/**************************************************
 * O_obj.h
 * ------------------------------------------------
 * 	+ output response of particles
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#ifndef OUTOBJ_H_
#define OUTOBJ_H_

#include "../MPMcomputer/Particle.h"
#include "pval_pt.h"

#include "Output.h"

class outobj : public Output, public PVal{
protected:
	typedef std::vector<Particle*> Ptpvec;

public:
	outobj();
	outobj(const std::string& bname,const std::string& ftype=".obj");
	virtual ~outobj();

	int open(const std::string& bname,const std::string& ftype=".obj");

	inline void set_outkeys(const std::vector<PTPVal*>& okeys) {outkeys = okeys;}

    void print_header( int objID, const std::vector<Scalar>& stimes);
    void print_onestep(const Scalar& tnow, const Ptpvec& ptptrs);

private:
    void _myversion(){_version = "1.0.0";}

    std::string _sep;
    std::vector<PTPVal*> outkeys;
};

#endif // OUTOBJ_H_

