/**************************************************
 * Output.h
 * ------------------------------------------------
 * 	+ I/O tool
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2013)
 * ------------------------------------------------
 * Modified by: YWC (14.12.19)
 **************************************************/
#ifndef OUTPUT_H_
#define OUTPUT_H_

#include <string>
#include <fstream>
#include <vector>
#include "../GB_Tensor.h" // just for Scalar
#include "pval.h"

bool i_file_exist(std::string& filename);

class Output{
public:
	Output();
	Output(const std::string& bname,const std::string& ftype="");
	virtual ~Output(){ this -> close(); }

	std::string opennewfile(const std::string& bname, const std::string& ftype);
	void openfile(const std::string& bname, const std::string& ftype, bool ibin=false);
	void appendfile(const std::string& bname, const std::string& ftype, bool ibin=false);

	void print_sysinfo(unsigned int Lint=0, unsigned int Lreal=0);
	inline void add_msg(std::string msg){_msg += msg;}
	inline void print(){_ofile<<_msg;}
	template <class _msg>
	inline void print(_msg msg ){_ofile<<msg;}
	inline void close(){if(_ofile.is_open()){_ofile.close();}}

	inline const std::string& get_filename(){ return _filename; }

protected:
    std::string _version;
    std::string _basename, _filename, _filetype;
    std::ofstream _ofile;

    std::string _msg;

private:
    void _myversion(){_version = "1.0.0";}
};

#endif // OUTPUT_H_

