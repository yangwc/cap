/*
 * main.cpp
 */

#include <string>
#include <fstream>
#include <iostream>
#include <ctime>

#include "MPMfactory/mpm_factory.h"
#include "MPMcomputer/MPMcomputer.h"
#include "MPMdatabase/O_log.h"

int main(int argc, char **argv){
	std::vector<std::string> fnames;
	std::string filename;
	/****************************
	 *		get filename list
	 ****************************/
	if (argc == 1){
		std::string infile = "infile";
		std::ifstream ifile(infile.c_str());
		if (ifile.is_open()){
			while (getline(ifile,filename)) {fnames.push_back(filename);}
		}
		ifile.close();
	}
	else{
		for(int II=1; II<argc; ++II){ fnames.push_back(std::string(argv[II])); }
	}

	/****************************
	 *		MAIN
	 ****************************/
	outlog* olog = 0;
	int Istat=-1001;
	for(unsigned int II=0; II<fnames.size(); II++){
		filename = fnames[II];

		if (olog != 0){ olog->print("    * uncompleted\n"); delete olog; olog = 0; }
		olog = new outlog(filename);

		/****************************
		 *		read filename
		 ****************************/
		mpmfactory* mpmfac = new mpmfactory();
		mpmfac->set_order(filename, Istat);
		if (Istat < 0) {delete mpmfac; mpmfac = 0; continue;}
		olog->print("    - mpm version: "+mpmfac->get_mpm_version()+"\n");
		olog->print("    - input file: "+mpmfac->get_infilename()+"\n");

		/****************************
		 *		make MPM computer
		 ****************************/
		MPM* mympm = mpmfac->make_new_model(Istat);
		delete mpmfac; mpmfac = 0;
		if (Istat < 0) {delete mympm; mympm = 0; continue; }

		/****************************
		 *		Run analysis
		 ****************************/
		time_t tsta,tend;
		time(&tsta);

		mympm->run();

		time (&tend);
		Scalar timeelapsed = difftime(tend,tsta);

		/****************************
		 *		Say goodbye
		 ****************************/
		delete mympm; mympm = 0;
		std::cout<<"MPM analysis complete after "<<timeelapsed<<" seconds."<<std::endl;
		olog->print("    - ana. time: "); olog->print(timeelapsed); olog->print(" (sec)\n");
		olog->close(); delete olog; olog = 0;
	}// end for (filenames)
	if (olog != 0){ olog->print("    * uncompleted\n"); delete olog; olog = 0; }
	if (Istat == -1001){ std::cout<<"!!!NO INPUT!!! Nothing is done."<<std::endl; }
	return Istat;
}// end main
