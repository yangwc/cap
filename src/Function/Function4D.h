/*
 * Function4D.h
 *
 *  Created on: Oct 20, 2014
 *      Author: wenchia
 */

#ifndef FUNCTION4D_H_
#define FUNCTION4D_H_

#include "Function.h"

class Function4D {
	/***************************
	 * 4D: x, y, z, t
	 ***************************/
public:
	Function4D(){;}
	virtual ~Function4D(){;}

	virtual Scalar get_funcvalue(const GVector& xyz, const Scalar& t)=0;
//	virtual Scalar get_devfuncvalue(const GVector& xyz, const Scalar& t)=0;
	virtual Function4D* get_new_obj()=0;
//	virtual std::vector<Scalar> get_parameters()=0;
	virtual inline int get_ID(){ return _id; }
	virtual inline int get_Ncoefmax(){ return _Ncoef; }

protected:
	int _id, _Ncoef;
};

//class F4linear1D : public Function4D{
//public:
//	F4linear1D(){;}
//	virtual ~F4linear1D(){;}
//
//	virtual Scalar get_funcvalue(const GVector& xyz, const Scalar& t)=0;
////	virtual Scalar get_devfuncvalue(const GVector& xyz, const Scalar& t)=0;
//	virtual Function4D* get_new_obj()=0;
//	virtual std::vector<Scalar> get_parameters()=0;
//
//};

class F4composed : public Function4D{
	/***************************
	 * 4D: x, y, z, t
	 * f(x,y,z,t) = fx(x)fy(y)fz(z)ft(t)
	 * default functions of elements are all g(a) = 1
	 ***************************/
public:
	F4composed(){_fx = 0, _fy = 0, _fz = 0, _ft = 0;}
	F4composed(F4composed& proto);
	~F4composed(){delete _fx; delete _fy; delete _fz; delete _ft;}

	inline int set_fx(function* func){delete _fx; _fx = func; return 0;}
	inline int set_fy(function* func){delete _fy; _fy = func; return 0;}
	inline int set_fz(function* func){delete _fz; _fz = func; return 0;}
	inline int set_ft(function* func){delete _ft; _ft = func; return 0;}
	inline int set_finction(unsigned int Idim, function* func){
		int Istat = -1;
		switch (Idim){
		case 1: Istat = set_fx(func); break;
		case 2: Istat = set_fy(func); break;
		case 3: Istat = set_fz(func); break;
		case 4: Istat = set_ft(func); break;
		}
		return Istat;
	}

	Scalar get_funcvalue(const GVector& xyz, const Scalar& t);
	Function4D* get_new_obj(){return new F4composed(*this);}
//	std::vector<Scalar> get_parameters();
private:
	function *_fx, *_fy, *_fz, *_ft;

};

#endif /* FUNCTION4D_H_ */
