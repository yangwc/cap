/*
 * Function4D.cpp
 *
 *  Created on: Oct 20, 2014
 *      Author: wenchia
 */

#include "Function4D.h"

F4composed::F4composed(F4composed& proto){
	_fx = proto._fx->get_new_obj();
	_fy = proto._fy->get_new_obj();
	_fz = proto._fz->get_new_obj();
	_ft = proto._ft->get_new_obj();
}
Scalar F4composed::get_funcvalue(const GVector& xyz, const Scalar& t){
	Scalar val = 1.0;
	if(_fx != 0) val*=_fx->get_funcvalue(xyz.GetxComp());
	if(_fy != 0) val*=_fy->get_funcvalue(xyz.GetyComp());
	if(_fz != 0) val*=_fz->get_funcvalue(xyz.GetzComp());
	if(_ft != 0) val*=_ft->get_funcvalue(t);
	return val;
}
