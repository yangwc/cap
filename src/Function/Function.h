/*
 * Function.h
 */

#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <cmath>

#include "../GB_Tensor.h"

class function{
public:
	function(){
		_id = -1;
		_Ncoef = 0;
		_Nite = 1000;
		_tol = GLOBAL_TOLERANCE_ERROR;
		_dxite = 0.1;
	}
	virtual ~function(){;}

	inline void set_Nmaxite(unsigned int Nmax) { _Nite = Nmax; }
	inline void set_tolerance(Scalar tol) { _tol = std::max( 0.0, tol); }
	inline void set_dxite(Scalar dx) { _dxite = std::max( 0.0, dx); }

	virtual Scalar get_funcvalue(const Scalar& x) const =0;
	virtual Scalar get_devfuncvalue(const Scalar& x) const =0;
	virtual function* get_new_obj() const = 0;
	virtual void get_tvector(const Scalar& x, Scalar& vtx, Scalar& vty) const;
	virtual int get_projection(const Scalar& xo, const Scalar& yo, Scalar& rn, Scalar& nx, Scalar& ny);

	inline int get_ID() const { return _id; }
	inline int get_Ncoefmax() const { return _Ncoef; }


protected:
	int _id, _Ncoef;
	unsigned int _Nite;
	Scalar _tol, _dxite;

};

class FNconstant : public function{
	/*
	 * constant.
	 * ---------------
	 * f(x) = yo
	 */
public:
	FNconstant(){ _id=-1; m_yo=0; _Ncoef=1;}
	FNconstant(int ID, const Scalar& yo){ _id=ID; m_yo=yo; _Ncoef=1; }
	FNconstant(const FNconstant& func);

	inline Scalar get_funcvalue(const Scalar& x) const { return m_yo; }
	inline Scalar get_devfuncvalue(const Scalar& x) const { return 0.0; }
	inline function* get_new_obj() const {return new FNconstant(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_yo;
};

class FNsin : public function{
	/*
	 * sin
	 * ---------------
	 * f(x) = amp*sine(2*PI*(x-xo)/T) + yo
	 */
public:
	FNsin();
	FNsin(int ID, const Scalar& T, const Scalar& amp);
	FNsin(int ID, const Scalar& T, const Scalar& amp, const Scalar& xo, const Scalar& yo);
	FNsin(const FNsin& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNsin(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_w, m_amp, m_xo, m_yo;
};

class FNcos : public function{
	/*
	 * sine.
	 * ---------------
	 * f(x) = amp*sine(2*PI*(x-xo)/T) + yo
	 */
public:
	FNcos();
	FNcos(int ID, const Scalar& T, const Scalar& amp);
	FNcos(int ID, const Scalar& T, const Scalar& amp, const Scalar& xo, const Scalar& yo);
	FNcos(const FNcos& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNcos(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_w, m_amp, m_xo, m_yo;
};

class FNlramp : public function{
	/*
	 * linear ramp.
	 * ---------------
	 *        { yo               , if x <= xs
	 * f(x)	= { yo + m*(x  - xs) , if xs< x < xe
	 * 		  { yo + m*(xe - xs) , if x >= xe
	 */
public:
	FNlramp();
	FNlramp(int ID, const Scalar& yo, const Scalar& m, const Scalar& xs, const Scalar& xe);
	FNlramp(const FNlramp& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNlramp(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_yo, m_ye, m_m, m_xs, m_xe;
};

class FNline : public function{
	/*
	 * line.
	 * ---------------
	 * f(x)	= yo + m*(x  - xo)
	 */
public:
	FNline();
	FNline(int ID, const Scalar& m, const Scalar& xo, const Scalar& yo);
	FNline(const FNline& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNline(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_m, m_xo, m_yo;
};

class FNO2poly : public function{
	/*
	 * line.
	 * ---------------
	 * f(x)	= ao + a1*x + a2*x^2
	 */
public:
	FNO2poly();
	FNO2poly(int ID, const Scalar& ao, const Scalar& a1, const Scalar& a2);
	FNO2poly(const FNO2poly& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNO2poly(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	std::vector<Scalar> _an;
};

class FNexp : public function{
	/*
	 * line.
	 * ---------------
	 * f(x)	= yo + exp(a(x-xo))
	 */
public:
	FNexp();
	FNexp(int ID, const Scalar& a, const Scalar& xo, const Scalar& yo);
	FNexp(const FNexp& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNexp(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_a, m_xo, m_yo;
};

class FNrect : public function{
	/*
	 * rectangular.
	 * ---------------
	 *        { yo , if x < xs
	 * f(x)	= { yt , if xs<= x < xe
	 * 		  { yo , if x >= xe
	 */
public:
	FNrect();
	FNrect(int ID, const Scalar& yo, const Scalar& yt, const Scalar& xs, const Scalar& xe);
	FNrect(const FNrect& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	function* get_new_obj() const {return new FNrect(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar m_yo, m_yt, m_xs, m_xe;
};

class FNhcir : public function{
	/*
	 * half circle
	 * ---------------
	 * f(x) = sign* (r^2 - x^x)^0.5
	 */
public:
	FNhcir();
	FNhcir(int ID, const Scalar& r, const Scalar& sign);
	FNhcir(const FNhcir& func);

	Scalar get_funcvalue(const Scalar& x) const;
	Scalar get_devfuncvalue(const Scalar& x) const;
	void get_tvector(const Scalar& x, Scalar& vtx, Scalar& vty) const;
	int get_projection(const Scalar& xo, const Scalar& yo, Scalar& rn, Scalar& nx, Scalar& ny);
	function* get_new_obj() const {return new FNhcir(*this);}
	std::vector<Scalar> get_parameters() const;

private:
	Scalar _r;
	int _sign;
};

#endif /* FUNCTION_H_ */
