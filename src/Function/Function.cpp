/*
 * Function.cpp
 */
#include "Function.h"

inline void function::get_tvector(const Scalar& x, Scalar& vtx, Scalar& vty) const{
	vtx = 1;
	vty = this-> get_devfuncvalue(x);
}

inline int function::get_projection(const Scalar& xo, const Scalar& yo, Scalar& rn, Scalar& nx, Scalar& ny){
	int Istat = 0;
	/**********************************
	 * find projection (x, y) of
	 * (xo, xo) on the function surface
	 **********************************/
	unsigned int Icount = 1;

	Scalar x  = xo;
	Scalar y  = this-> get_funcvalue(x);
	Scalar Vx = 0;
	Scalar Vy = yo - y;

	Scalar vtx, vty;
	this-> get_tvector( x, vtx, vty);

	if( std::abs(Vy) <= _tol ){
		rn = 0;
		Scalar ss = 1.0/std::sqrt( vtx*vtx + vty*vty );
		nx =  vty*ss;
		ny = -vtx*ss;
	}
	else{
		Scalar xstep = _dxite;
		Scalar VT = Vy*vty, VTo = VT; // (V . T) = 0 if V = N
		if (VT < 0) xstep *= -1;
		while( std::abs(VT) > _tol && Icount != _Nite){
			x  += xstep;
			y   = this-> get_funcvalue(x);

			this-> get_tvector( x, vtx, vty);

			Vx  = xo - x;
			Vy  = yo - y;
			VT  = Vx*vtx + Vy*vty;
			if (VT*VTo < 0) xstep *= -0.3333;
			VTo = VT;
			++Icount;
		}

		if (Icount == _Nite){ Istat = -1; }

		rn = std::sqrt( Vx*Vx + Vy*Vy );
		if (rn < _tol){
			std::cout<<"WARNING: unexpected condition: rn < tol @ get_projection()"<<std::endl;
			rn = 0;
			Scalar ss = 1.0/std::sqrt(vtx*vtx+vty*vty);
			nx =  vty*ss;
			ny = -vtx*ss;
		}
		else{
			Scalar ss = 1.0/rn;
			nx = Vx*ss;
			ny = Vy*ss;
		}
	}
	return Istat;
}// end function
/***********************************************
 *	FNsin : function
 ***********************************************/
FNconstant::FNconstant(const FNconstant& func){
	_id = func.get_ID();
	m_yo = func.m_yo;
	_Ncoef=1;
}

inline std::vector<Scalar> FNconstant::get_parameters() const {
	std::vector<Scalar> paras(1);
	paras[0] = m_yo;
	return paras;
}
/***********************************************
 *	FNsin : function
 ***********************************************/
FNsin::FNsin(){
	_id = -1;
	m_w = 2*PI;
	m_amp = 1;
	m_xo = 0;
	m_yo = 0;
	_Ncoef = 4;
}

FNsin::FNsin(int ID, const Scalar& T, const Scalar& amp){
	_id = ID;
	m_w = 2*PI/T;
	m_amp = amp;
	m_xo = 0;
	m_yo = 0;
	_Ncoef = 4;
}

FNsin::FNsin(int ID, const Scalar& T, const Scalar& amp, const Scalar& xo, const Scalar& yo){
	_id = ID;
	m_w = 2*PI/T;
	m_amp = amp;
	m_xo = xo;
	m_yo = yo;
	_Ncoef = 4;
}

FNsin::FNsin(const FNsin& func){
	_id = func.get_ID();
	m_w = func.m_w;
	m_amp = func.m_amp;
	m_xo = func.m_xo;
	m_yo = func.m_yo;
	_Ncoef = 4;
}

inline Scalar FNsin::get_funcvalue(const Scalar& x) const{
	Scalar y = m_amp*std::sin( m_w*( x - m_xo ) ) + m_yo;
	return y;
}

inline Scalar FNsin::get_devfuncvalue(const Scalar& x) const{
	Scalar dy = m_w*m_amp*std::cos( m_w*( x - m_xo ) );
	return dy;
}

std::vector<Scalar> FNsin::get_parameters() const {
	std::vector<Scalar> paras(4);
	paras[0] = m_w;
	paras[1] = m_amp;
	paras[2] = m_xo;
	paras[3] = m_yo;
	return paras;
}

/***********************************************
 *	FNcos : function
 ***********************************************/
FNcos::FNcos(){
	_id = -1;
	m_w = 2*PI;
	m_amp = 1;
	m_xo = 0;
	m_yo = 0;
	_Ncoef = 4;
}

FNcos::FNcos(int ID, const Scalar& T, const Scalar& amp){
	_id = ID;
	m_w = 2*PI/T;
	m_amp = amp;
	m_xo = 0;
	m_yo = 0;
	_Ncoef = 4;
}

FNcos::FNcos(int ID, const Scalar& T, const Scalar& amp, const Scalar& xo, const Scalar& yo){
	_id = ID;
	m_w = 2*PI/T;
	m_amp = amp;
	m_xo = xo;
	m_yo = yo;
	_Ncoef = 4;
}

FNcos::FNcos(const FNcos& func){
	_id = func.get_ID();
	m_w = func.m_w;
	m_amp = func.m_amp;
	m_xo = func.m_xo;
	m_yo = func.m_yo;
	_Ncoef = 4;
}

inline Scalar FNcos::get_funcvalue(const Scalar& x) const{
	Scalar y = m_amp*std::cos( m_w*( x - m_xo ) ) + m_yo;
	return y;
}

inline Scalar FNcos::get_devfuncvalue(const Scalar& x) const{
	Scalar dy = -m_w*m_amp*std::sin( m_w*( x - m_xo ) );
	return dy;
}

std::vector<Scalar> FNcos::get_parameters() const {
	std::vector<Scalar> paras(4);
	paras[0] = m_w;
	paras[1] = m_amp;
	paras[2] = m_xo;
	paras[3] = m_yo;
	return paras;
}

/***********************************************
 *	FNlramp : function
 ***********************************************/
FNlramp::FNlramp(){
	_id =-1; m_yo = 0; m_m  = 1;
	m_xs = 0; m_xe = 1; m_ye = 1;
	_Ncoef = 4;
}
FNlramp::FNlramp(int ID, const Scalar& yo, const Scalar& m, const Scalar& xs, const Scalar& xe){
	_id = ID;
	m_yo = yo;
	m_m  = m;
	m_xs = xs;
	m_xe = xe;
	m_ye = yo + m*(xe - xs);
	_Ncoef = 4;
}

FNlramp::FNlramp(const FNlramp& func){
	_id = func.get_ID();
	m_yo = func.m_yo;
	m_m  = func.m_m;
	m_xs = func.m_xs;
	m_xe = func.m_xe;
	m_ye = m_yo + m_m*(m_xe - m_xs);
	_Ncoef = 4;
}

inline Scalar FNlramp::get_funcvalue(const Scalar& x) const{
	Scalar y = m_yo;
	if(x >= m_xe){ y = m_ye; }
	else { y += m_m*(x - m_xs); }
	return y;
}

inline Scalar FNlramp::get_devfuncvalue(const Scalar& x) const{
	Scalar dy = 0.0;
	if(x > m_xs && x < m_xe){
		dy = m_m;
	}
	return dy;
}

inline std::vector<Scalar> FNlramp::get_parameters() const {
	std::vector<Scalar> paras(4);
	paras[0] = m_yo;
	paras[1] = m_m;
	paras[2] = m_xs;
	paras[3] = m_xe;
	return paras;
}

/***********************************************
 *	FNline : function
 ***********************************************/
FNline::FNline(){
	_id =-1; m_m  = 1;
	m_xo = 0; m_yo = 0;
	_Ncoef = 3;
}
FNline::FNline(int ID, const Scalar& m, const Scalar& xo, const Scalar& yo){
	_id = ID;
	m_yo = yo;
	m_m  = m;
	m_xo = xo;
	_Ncoef = 3;
}

FNline::FNline(const FNline& func){
	_id = func.get_ID();
	m_m  = func.m_m;
	m_xo = func.m_xo;
	m_yo = func.m_yo;
	_Ncoef = 3;
}

inline Scalar FNline::get_funcvalue(const Scalar& x) const {
	Scalar y = m_yo + m_m*(x-m_xo);
	return y;
}

inline Scalar FNline::get_devfuncvalue(const Scalar& x) const {
	Scalar dy = m_m;
	return dy;
}

inline std::vector<Scalar> FNline::get_parameters() const {
	std::vector<Scalar> paras(3);
	paras[0] = m_m;
	paras[1] = m_xo;
	paras[2] = m_yo;
	return paras;
}

/***********************************************
 *	FNO2poly: function
 ***********************************************/
FNO2poly::FNO2poly(){
	_id =-1;
	_Ncoef = 3;
	_an.assign(3, 0);
}
FNO2poly::FNO2poly(int ID, const Scalar& ao, const Scalar& a1, const Scalar& a2){
	_id = ID;
	_Ncoef = 3;
	_an.assign(3, 0);
	_an[0] = ao; _an[1] = a1; _an[2] = a2;
}

FNO2poly::FNO2poly(const FNO2poly& func){
	_id = func.get_ID();
	_Ncoef = 3;
	_an  = func._an;
}

inline Scalar FNO2poly::get_funcvalue(const Scalar& x) const{
	Scalar y  = _an[0] + _an[1]*x + _an[2]*x*x;
	return y;
}

inline Scalar FNO2poly::get_devfuncvalue(const Scalar& x) const {
	Scalar dy = _an[1] + 2*_an[2]*x;
	return dy;
}

inline std::vector<Scalar> FNO2poly::get_parameters() const {
	return _an;
}

/***********************************************
 *	FNexp : function
 ***********************************************/
FNexp::FNexp(){
	_id =-1; m_a  = 1;
	m_xo = 0; m_yo = 0;
	_Ncoef = 3;
}
FNexp::FNexp(int ID, const Scalar& a, const Scalar& xo, const Scalar& yo){
	_id = ID;
	m_a  = a;
	m_yo = yo;
	m_xo = xo;
	_Ncoef = 3;
}

FNexp::FNexp(const FNexp& func){
	_id = func.get_ID();
	m_a  = func.m_a;
	m_xo = func.m_xo;
	m_yo = func.m_yo;
	_Ncoef = 3;
}

inline Scalar FNexp::get_funcvalue(const Scalar& x) const{
	Scalar y = m_yo + std::exp(m_a*(x-m_xo));
	return y;
}

inline Scalar FNexp::get_devfuncvalue(const Scalar& x) const{
	Scalar dy = m_a*std::exp(m_a*(x-m_xo));
	return dy;
}

inline std::vector<Scalar> FNexp::get_parameters() const {
	std::vector<Scalar> paras(3);
	paras[0] = m_a;
	paras[1] = m_xo;
	paras[2] = m_yo;
	return paras;
}

/***********************************************
 *	FNrect : function
 ***********************************************/
FNrect::FNrect(){
	_id =-1; m_yo = 0; m_yt = 1;
	m_xs = 0; m_xe = 1;
	_Ncoef = 4;
}
FNrect::FNrect(int ID, const Scalar& yo, const Scalar& yt, const Scalar& xs, const Scalar& xe){
	_id = ID;
	m_yo = yo;
	m_yt = yt;
	m_xs = xs;
	m_xe = xe;
	_Ncoef = 4;
}

FNrect::FNrect(const FNrect& func){
	_id = func.get_ID();
	m_yo = func.m_yo;
	m_yt = func.m_yt;
	m_xs = func.m_xs;
	m_xe = func.m_xe;
	_Ncoef = 4;
}

inline Scalar FNrect::get_funcvalue(const Scalar& x) const{
	Scalar y = m_yo;
	if(x < m_xe && x >= m_xs){ y = m_yt; }
	return y;
}

inline Scalar FNrect::get_devfuncvalue(const Scalar& x) const{
	Scalar dy = 0.0;
	return dy;
}

std::vector<Scalar> FNrect::get_parameters() const {
	std::vector<Scalar> paras(4);
	paras[0] = m_yo;
	paras[1] = m_yt;
	paras[2] = m_xs;
	paras[3] = m_xe;
	return paras;
}

/***********************************************
 *	FNhcir : function
 ***********************************************/
FNhcir::FNhcir(){
	_id   =-1;
	_r    = 10;
	_sign = 0;

	_Ncoef = 2;
}
FNhcir::FNhcir(int ID, const Scalar& r, const Scalar& sign){
	_id = ID;
	_r  = std::max( 0.0, r );
	if (sign < 0) _sign = -1;
	else _sign = 1;

	_Ncoef = 2;
}

FNhcir::FNhcir(const FNhcir& func){
	_id   = func.get_ID();

	_r    = func._r;
	_sign = func._sign;

	_Ncoef = 2;
}

Scalar FNhcir::get_funcvalue(const Scalar& x) const {
	Scalar y = _sign*std::sqrt( std::max(0.0,_r*_r - x*x) );
	return y;
}

Scalar FNhcir::get_devfuncvalue(const Scalar& x) const {
	Scalar dy, yy = _r*_r - x*x;
	if (yy > 0) dy = (-_sign)*x/std::sqrt(yy);
	else dy = 0;
	return dy;
}

inline void FNhcir::get_tvector(const Scalar& x, Scalar& vtx, Scalar& vty) const {
	Scalar yy = _r*_r - x*x;
	if (yy >= 0){
		vtx = std::sqrt( yy );
		vty = (-_sign)*x;
	}
	else{
		vtx = 1;
		vty = 0;
	}
}

inline int FNhcir::get_projection(const Scalar& xo, const Scalar& yo, Scalar& rn, Scalar& nx, Scalar& ny){
	int Istat = 0;
	Scalar vox = xo;
	Scalar voy = yo;
	Scalar rx = std::sqrt(vox*vox + voy*voy);
	if (rx <= _tol){
		nx = 1;
		ny = 0;
		rn = 0;
	}
	else if( std::abs(vox) > _r){
		nx = 0;
		if (voy < 0){
			rn = -voy;
			ny = -1;
		}
		else{
			ny = 1;
			rn = voy;
		}

	}
	else{
		if (voy*_sign > 0){
			nx = vox/rx;
			ny = voy/rx;
			rn = rx-_r;
			if(rn < 0){
				rn = -rn;
				nx = -nx;
				ny = -ny;
			}
		}
		else{
			rn = rx + _r;
		}
	}
	return Istat;
}// end function

inline std::vector<Scalar> FNhcir::get_parameters() const {
	std::vector<Scalar> paras(2);
	paras[0] = _r;
	paras[1] = _sign;
	return paras;
}
