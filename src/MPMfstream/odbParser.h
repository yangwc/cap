/**************************************************
 * odbParser.h
 * 	   - odbParser()
 * ------------------------------------------------
 * 	+ fstream for odb file
 * ================================================
 * Created by: YWC @ CEE, UW (Feb 13, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.02.13)
 **************************************************/

#ifndef ODBPARSER_H_
#define ODBPARSER_H_

#include <string>
#include <vector>
#include "binfstream.h"

#include "../Base/StrParser.h"

template <class _unint, class _double>
class odbParser {
private:
	typedef typename std::vector<_unint> uiVec;
	typedef typename std::vector<_double> dbVec;
	typedef typename std::vector< dbVec* > DVec;
	typedef typename DVec::iterator itDV;

public:
	odbParser(){
		_Luint = sizeof(_unint); _Ldoub= sizeof(_double);
		_Nstat=0; _objID = 0; _matcode = 0;
	}
	virtual ~odbParser(){ this -> close(); }

	inline int open(std::string ifnm){
		int Istat = -1;
		if ( _ifile.open(ifnm) ){
			Istat = this -> _CHECK_size();
			if (Istat != 0){ _ifile.close(); Istat = -2; }
			else{
				_ifile.getvalue( _objID, _Luint);
				_ifile.getvalue( _matcode, _Luint);

				unsigned int len;
				_ifile.getvalue( len, _Luint);
				_ifile.getvalues( _matparas, len, _Ldoub);

				_ifile.getvalue( len, _Luint);
				_ifile.getvalues( _tstps, len, _Ldoub);

				_ifile.getvalue( len, _Luint);

				std::string aline; StrParser strp;
				_ifile.getline( aline );
				_vnms  = strp.split( aline );
				_Nstat = len - (1+1+3+3+3);
			}
		}
		return Istat;
	}

	inline int read_step(std::vector<_unint>& ptIDs, std::vector<_double>& ptms, DVec& datas){
		_unint Npt;
		_ifile.getvalue( Npt, _Luint);
		ptIDs.assign( Npt, 0);
		ptms.assign( Npt, 0);
		this -> _format_ptdata(Npt, datas);

		std::vector<_double>& xs = *datas[0];
		std::vector<_double>& vs = *datas[1];
		std::vector<_double>& bs = *datas[2];
		std::vector<_double>& sts = *datas[3];

		unsigned int Ivec = 0, Ist = 0;
		for (unsigned int Ipt = 0; Ipt != Npt; ++Ipt){
			_ifile.getvalue( ptIDs[Ipt], _Luint);
			_ifile.getvalue(  ptms[Ipt], _Ldoub);
			_ifile.getvalues(  xs[Ivec], 3, _Ldoub);
			_ifile.getvalues(  vs[Ivec], 3, _Ldoub);
			_ifile.getvalues(  bs[Ivec], 3, _Ldoub);
			_ifile.getvalues(  sts[Ist], _Nstat, _Ldoub);

			Ivec += 3;
			Ist  += _Nstat;
		}
		return 0;
	}
	inline void close(){ _ifile.close(); }

private:
	inline int _format_ptdata(unsigned int Npt, DVec& datas){
		unsigned int Lfmt = 4, Ld = datas.size();
		if ( Ld != Lfmt ){
			for ( itDV iv = datas.begin(); iv != datas.end(); ++iv){
				delete (*iv); (*iv) = 0;
			}
			datas.assign( Lfmt, new std::vector<double>* );
		}
		datas[0] -> assign( Npt*3, 0);
		datas[1] -> assign( Npt*3, 0);
		datas[2] -> assign( Npt*3, 0);
		datas[3] -> assign( Npt*_Nstat, 0);
		return 0;
	}

	inline int _CHECK_size(){
		int Istat = -100;
		std::string sysinfo;
		if( _ifile.getline(sysinfo) ){
			StrParser strp;
			std::vector<std::string> infs = strp.split(sysinfo);
			unsigned int size = strp.convert<unsigned int>(infs[0]);
			if ( size == sizeof(_unint) ){
				size = strp.convert<unsigned int>(infs[1]);
				if ( size == sizeof(_double) ) Istat = 0;
			}
		}
		return Istat;
	}

private:
	binfstream _ifile;
	unsigned int _Luint, _Ldoub, _Nstat;
	_unint _objID, _matcode;
	std::vector<_double> _tstps, _matparas;
	std::vector<std::string> _vnms;
};

#endif /* ODBPARSER_H_ */
