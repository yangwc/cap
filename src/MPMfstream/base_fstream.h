/**************************************************
 * _fstream.h
 * 	   - _fstream()
 * ------------------------------------------------
 * 	+ basic fstream adapter
 * ================================================
 * Created by: YWC @ CEE, UW (Feb 04, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.02.04)
 **************************************************/

#ifndef BASEFSTREAM_H_
#define BASEFSTREAM_H_

#include <string>
#include <fstream>

class base_fstream {
public:
	base_fstream(){;}
	base_fstream( const std::string& ifnm ){ this -> open(ifnm); }
	virtual ~base_fstream(){ this -> close(); }

	inline bool open(const std::string& ifnm){
		this -> close();
		_ifile.open( ifnm.c_str() );
		return _ifile.is_open();
	}
	inline void close(){ if ( _ifile.is_open() ) _ifile.close(); }

	inline bool getline(std::string& str){ return (bool) std::getline(_ifile, str); }
	inline bool is_open(){ return _ifile.is_open(); }

protected:
	std::ifstream _ifile;
};

#endif /* BASEFSTREAM_H_ */
