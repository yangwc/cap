/**************************************************
 * inpParser.h
 * 	   - inpParser()
 * ------------------------------------------------
 * 	+ Parser of inp file (mpm input file)
 * ================================================
 * Created by: YWC @ CEE, UW (Jan 26, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.01.28)
 **************************************************/

#ifndef INPPARSER_H_
#define INPPARSER_H_

#include <string>
#include <vector>
#include <iostream>
#include <ciso646>
#include <algorithm>

#include "../Base/StrParser.h"
#include "base_fstream.h"

class inpParser : public base_fstream{
public:
	inpParser(){ _Istr = 0; _skips = "/#@"; }
	inpParser(const std::string& ifnm){
		_Istr = 0;
		_skips = "/#@";
		_ifile.open( ifnm.c_str() ) ;
	}
	virtual ~inpParser(){;}

	inline int read_line(){
		int Istat = -1;
		if( std::getline(_ifile, _temp_str) ){
			_temp_str = _strp.clean(_temp_str);
			_instrs = _strp.split(_temp_str);
			Istat = _instrs.size();
		}
		else{
			_instrs.clear();
		}
		_Istr = 0;
		return Istat;
	}

	inline bool read_next_line(){
		_instrs.clear(); _Istr = 0;

		int Istat = -1;
		while( std::getline(_ifile, _temp_str) ){
			_temp_str = _strp.clean(_temp_str);
			_instrs = _strp.split(_temp_str);
			Istat = _instrs.size();
			if (Istat != 0 and
					_skips.find( _instrs[0][0] ) > _skips.size() ){
				break;
			}
		}
		return (Istat != -1);
	}

	template <class _type>
	inline void get_data(std::vector<_type>& odata){
		odata = _strp.batch_convert<_type>(_instrs);
	}

	template <class _type>
	inline std::vector<_type>* get_new_data(){
		std::vector<_type>* optr = new std::vector<_type>;
		*optr = _strp.batch_convert<_type>(_instrs);
		return optr;
	}

	template <class _type>
	inline void get_surplus(std::vector<_type>& odata, unsigned int Iend){
		Iend = std::min(Iend +_Istr, (unsigned int) _instrs.size() );
		odata = _strp.batch_convert<_type>( std::vector<std::string>
						( _instrs.begin() + _Istr , _instrs.begin() + Iend ) );
		_Istr = Iend;
	}

	template <class _type>
	inline void get_surplus(std::vector<_type>& odata){
		this -> get_surplus<_type>( odata, _instrs.size() );
	}

	template <class _type>
	inline std::vector<_type>* get_new_surplus(unsigned int Iend){
		Iend = std::min(Iend +_Istr, (unsigned int) _instrs.size() );
		std::vector<_type>* optr = new std::vector<_type>;

		*optr = _strp.batch_convert<_type>( std::vector<std::string>
					( _instrs.begin() + _Istr, _instrs.begin() + Iend ) );
		_Istr = Iend;
		return optr;
	}

	template <class _type>
	inline std::vector<_type>* get_new_surplus(){
		return this -> get_new_surplus<_type> ( _instrs.size() );
	}

	template <class _type>
	inline _type get_value( unsigned int Iv){
		this -> CHECK_index( Iv );
		_type oval = _strp.convert<_type>(_instrs[Iv]);
		return oval;
	}

	template <class _type>
	inline _type get_next_value(){
		this -> CHECK_index();
		_type oval = _strp.convert<_type>(_instrs[_Istr]);
		_Istr ++;
		return oval;
	}

	inline const std::string& get_next_value(){
		this -> CHECK_index();
		unsigned int Iostr = _Istr;
		_Istr ++;
		return _instrs[Iostr];
	}

	inline unsigned int get_Ninput(){ return _instrs.size(); }

	inline bool is_endcmd(){
		std::string& key = _instrs[0];
		if(key.size() != 3) return false;
		return ( _strp.uppercase(key) == "END");
	}

	inline void CHECK_index(){
		try{
			if ( _Istr == _instrs.size() ) throw -1;
		}
		catch( int Istat){
			std::cerr<<"ERROR: insufficient input data. No more value can be extracted."<<std::endl;
		}
	}

	inline void CHECK_index(unsigned int Iv){
		try{
			if (Iv >= _instrs.size()) throw -1;
		}
		catch( int Istat){
			std::cerr<<"ERROR: insufficient input data. Given index is over the size of the array."<<std::endl;
		}
	}

private:
	StrParser _strp;
	std::string _temp_str, _skips;
	std::vector<std::string> _instrs;
	unsigned int _Istr;
};

#endif /* INPPARSER_H_ */
