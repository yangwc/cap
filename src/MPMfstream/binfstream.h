/**************************************************
 * binfstream.h
 * 	   - binfstream()
 * ------------------------------------------------
 * 	+ fstream for binary file
 * ================================================
 * Created by: YWC @ CEE, UW (Feb 02, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.02.13)
 **************************************************/

#ifndef BINFSTREAM_H_
#define BINFSTREAM_H_

#include <string>
#include <vector>
#include "base_fstream.h"

class binfstream : public base_fstream {
public:
	binfstream(){;}
	binfstream( const std::string& ifnm ){ this -> open(ifnm); }
	virtual ~binfstream(){;}

	template <class _T>
	inline void getvalue(_T& val){ this -> getvalue<_T>( val, sizeof(_T) ); }
	template <class _T>
	inline void getvalue(_T& val, unsigned int vsize){ _ifile.read( (char*) val, vsize ); }
	template <class _T>
	inline int getvalues(std::vector<_T>& vals, unsigned int Nval){ return this -> getvalues<_T>( vals, Nval, sizeof(_T) ); }
	template <class _T>
	inline int getvalues(std::vector<_T>& vals, unsigned int Nval, unsigned int vsize){
		vals.assign(Nval, 0);
		int Istat = 0;
		_T val;
		for (unsigned int Ival = 0; Ival != Nval; ++Ival){
			if ( !_ifile.read( (char*) val, vsize ) ) { Istat = -1; break; }
		}
		return Istat;
	}

};

#endif /* BINFSTREAM_H_ */
