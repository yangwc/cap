/**************************************************
 * GBridge.cpp
 * ================================================
 * Created by: YWC @ CEE, UW (May 5, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.04.27)
 **************************************************/
#include "GBridge.h"

GBridge::GBridge(LNKmngr<Gnode>* mngrptr, OInteract* algptr) {
	_gmanager = mngrptr;
	_alg = algptr;
}

GBridge::~GBridge() {
	delete _gmanager;
	delete _alg;
}

void GBridge::reset(){
	for (std::vector<unsigned int>::iterator ivec = _Nsps.begin(); ivec != _Nsps.end(); ++ivec){
		(*ivec) = 0;
	}
	_Nmod = 0;
}

void GBridge::interact(const Scalar& dt){
	if (_Nmod != 0){
		for (unsigned int II = 0; II != _Nmod; ++II){
			unsigned int& Iadd = _Imods[II];
			_alg -> interact(dt, _Nsps[Iadd], _gadds[Iadd]);
		}
	}
}

void GBridge::update_gridusage(const IntGVector& ndid, Gnode* ndlink){
	unsigned int Iadd = _gmanager -> get_address(ndid, ndlink, _Nsps, _gadds);
	if (_Nsps[Iadd] == 2){
		if(_Nmod == _Imods.size()){
			_Imods.push_back( Iadd );
		}
		else{
			_Imods[_Nmod] = Iadd;
		}
		_Nmod++;
	}
}

