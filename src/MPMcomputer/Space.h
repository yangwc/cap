/**************************************************
 * Space.h
 * 	   - Space()
 * ------------------------------------------------
 * 	+ interface of space
 * ================================================
 * Created by: YWC @ CEE, UW (Apr 27, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.04.27)
 **************************************************/
#ifndef SPACE_H_
#define SPACE_H_

#include <map>
#include <string>
#include <vector>
#include <list>

#include "../GB_Tensor.h"

#include "../MPMdatabase/O_obj.h"
#include "../MPMdatabase/O_gnd.h"
#include "../MPMdatabase/O_odb.h"

#include "Particle.h"
#include "Grid.h"
#include "PTFilter.h"

class Space {
public:
	Space(){ _ID = -1; _grid = 0; _fgnd = 0; _fobj = 0; _fodb = 0; }
	Space(int ID){ _ID = ID; _grid = 0; _fgnd = 0; _fobj = 0; _fodb = 0; }
	Space(Space* proto){
		_ID   = proto -> _ID;
		_grid = proto -> _grid; proto -> _grid = 0;
		_fgnd = proto -> _fgnd; proto -> _fgnd = 0;
		_fobj = proto -> _fobj; proto -> _fobj = 0;
		_fodb = proto -> _fodb; proto -> _fodb = 0;
		_objs_pt = proto -> _objs_pt; proto -> _objs_pt.clear();
		_objs_ptfilt = proto -> _objs_ptfilt; proto -> _objs_ptfilt.clear();
	}
	virtual ~Space(){
		for (std::vector<Particle*>::iterator ivec = _objs_pt.begin(); ivec!= _objs_pt.end(); ++ivec){
			delete (*ivec); (*ivec)=0;
		}
		for (std::vector<PTFilter*>::iterator ivec = _objs_ptfilt.begin(); ivec!= _objs_ptfilt.end(); ++ivec){
			delete (*ivec); (*ivec)=0;
		}
		delete _grid; _grid = 0;
		delete _fgnd; _fgnd = 0;
		delete _fobj; _fobj = 0;
		delete _fodb; _fodb = 0;
	}

	inline void assign_gridmodel(Grid* grid){_grid = grid;}
	inline void assign_particles(const std::vector<Particle*>& particlelist){_objs_pt = particlelist;}
	inline void assign_ptfilters(const std::vector<PTFilter*>& ptfilters){_objs_ptfilt = ptfilters;}
	inline void assign_objfile(outobj* filein) {_fobj = filein; }
	inline void assign_gndfile(outgnd* filein) {_fgnd = filein; }
	inline void assign_odbfile(outodb* filein) {_fodb = filein; }

	virtual int link_particle(const Scalar& tnow) = 0;
	virtual int build_grid() = 0;

	inline int solve_grid(const Scalar& tnow, const Scalar& dt){ return _grid->solve(tnow, dt); }

//	virtual bool update_particle(const Scalar& tnow, const Scalar& dt) = 0;
	virtual bool update_pt_kinematics(const Scalar& tnow, const Scalar& dt) = 0;
	virtual bool update_pt_deformation(const Scalar& tnow, const Scalar& dt) = 0;

	inline void filter_pt(const Scalar& tnow, const Scalar& dt){
		for (std::vector<PTFilter*>::iterator ivec = _objs_ptfilt.begin(); ivec!= _objs_ptfilt.end(); ++ivec){
			(*ivec) -> filter_pt(tnow, dt, _objs_pt, _ln_freepts);
		}
	}

	inline void print_onestep(const Scalar& tnow){
		Grid* const gptr = _grid;
		if ( _fobj != 0 ) _fobj->print_onestep( tnow, _objs_pt );
		if ( _fgnd != 0 ) _fgnd->print_onestep( tnow, gptr );
		if ( _fodb != 0 ) _fodb->print_onestep( tnow, _objs_pt );
	}
	inline void close_ofile(){
		if (_fobj != 0) _fobj->close();
		if (_fgnd != 0) _fgnd->close();
		if (_fodb != 0) _fodb->close();
	}

	inline int get_ID(){return _ID;}

protected:
	int _ID;
	std::vector<Particle*> _objs_pt, _ln_freepts;
	std::vector<PTFilter*> _objs_ptfilt;
	Grid* _grid;

	outobj* _fobj;
	outgnd* _fgnd;
	outodb* _fodb;
};

#endif /* SPACE_H_ */
