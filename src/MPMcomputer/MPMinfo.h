#ifndef MPMINFO_H_
#define MPMINFO_H_

#include <string>

std::string _major_ = "1.0.0";

std::string _minor_ = "0";
//std::string _info_  = "";

std::string _version_ = _major_+'.'+_minor_;//+'.'+_info_;

#endif /* MPMINFO_H_ */
