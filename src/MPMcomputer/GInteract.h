#ifndef OINTERACT_H_
#define OINTERACT_H_

#include <vector>
#include "../GB_Tensor.h"
#include "../MPMobject/GridNode/Gnode.h"

class OInteract {
protected:
	typedef std::vector< Gnode* > ndvec;

public:
	virtual ~OInteract(){;}
	virtual void interact(const Scalar& dt, const unsigned int& Nspace, ndvec& spadds) = 0;
};

#endif /* OINTERACT_H_ */
