#ifndef MPMCOMPUTE_H
#define MPMCOMPUTE_H

#include "../GB_Tensor.h"
#include "MPMos.h"

class MPM {
  public:
	MPM();
	~MPM();

	inline void assign_operatingsystem(MPMos* os){_obj_os = os;}
	inline void assign_timestep(const Scalar& dt){ if(dt > 0){_dt = dt;} }
	inline void assign_duration(const Scalar& t){ if(t > 0){_t_end = t;} }

	inline void set_starttime(const Scalar& to){ if(to >= 0){_t_sta = to;} }
	inline void set_rectimeo(const Scalar& to){ if(to >= 0){_t_srec = to;} }
	inline void set_recstep(unsigned int Nout){ _Nstep_rec = Nout; }

	void run();

  private: 
	unsigned int _Nstep_rec;
	Scalar _dt, _t_sta, _t_end, _t_srec;
	MPMos* _obj_os;
};

#endif
