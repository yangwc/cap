/**************************************************
 * Particle.h
 * ------------------------------------------------
 * 	+ Material Point
 * ================================================
 * Created by: YWC @ CEE, UW (XXX XX, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.11.24)
 **************************************************/
#ifndef PARTICLE_H_
#define PARTICLE_H_

#include <map>
#include <vector>
#include <string>
#include <sstream>

#include "../GB_Tensor.h"
#include "../Material/Material.h"

class Particle {
	// ****************************************** //
	// stabilization (integration points) ------- //
	// ****************************************** //
public:
	inline void
	assign_material(Material* mat){
		delete _material;
		_material = mat;
		_mass = _Vo*_material->get_density();
		this-> update_damping_coefficient();
	}
	inline void
	set_damping_ratio(const Scalar& Rc){
		_Rdamp = std::max(Rc,0.0);
		if (_material != 0) this-> update_damping_coefficient();
	}
	inline void
	update_damping_coefficient(){
		_csy = _material-> get_spec_viscosity();
		if (_csy > GLOBAL_TOLERANCE_ERROR){
			_csy *= (1+_Rdamp);
		}
		else{
			_csy = 2*_Rdamp* ( 2*std::sqrt( _material-> get_spec_shear_modulus() ) );
			_cdi = 2*_Rdamp* _material->get_wavespeed() - _csy;
		}
	}

	inline int
	update_configuration( const Scalar& dt, GTensor& gradv, const Scalar& Jt = 1 ){
		_material -> update_state( dt, _tnow, Jt, gradv );
		return 0;
	}

	// specstress: specific stress, i.e. sigma-bar
	inline void
	set_assumed_specstress( const GSymmTensor& sb ){
		_i_assumed   = true;
		_specstress  = sb;
	}
	inline GSymmTensor get_specstress(const Scalar& dx, const GTensor& gradv) const {
		GSymmTensor specstress = gradv.GetSymmPart();
		specstress *= _csy*dx;

		GVector dig = specstress.Diag();
		dig += ( gradv.Diag() * (_cdi*dx) );
		specstress.Diag(dig);

		specstress += this-> get_specstress();
		return specstress;
	}
protected:
	Scalar _Rdamp, _cdi, _csy;

	// ****************************************** //
	// smoothing particle field ----------------- //
	// ****************************************** //
public:
	inline void reset_velocity(const GVector& vel){ _vel = vel; }
	inline void reset_state( const std::vector<Scalar>& stat ){ _material -> set_state(stat);  }

	// ****************************************** //
	// modified for using assumed stress -------- //
	// ****************************************** //
public:

	// default as assumed stress if available
	inline const GSymmTensor& get_specstress() const {
		if (_i_assumed) return _specstress;
		else return _material -> get_specstress();
	}
	inline const GSymmTensor& get_stress() const {
		if (_i_assumed) return ( _specstress*_material->get_density() );
		else return _material -> get_stress();
	}
	inline const GSymmTensor& get_mat_specstress() const {
		return _material -> get_specstress();
	}
	inline const GSymmTensor& get_mat_stress() const {
		return _material -> get_stress();
	}

	inline unsigned int get_Nstatevals() const { return _material->get_Nstatevals(); }
	inline void get_state( std::vector<Scalar>& states )  const { _material->get_state(states); }
protected:
	bool _i_assumed;
	GSymmTensor _specstress;

	// ****************************************** //
	// Major part of original MPM --------------- //
	// ****************************************** //
public:
	Particle(unsigned int id, unsigned int Iadd){
		_id   = id;
		_Iadd = Iadd;
		_obid = 0;
		_mass = 0;
		_Vo   = 0;
		_material = 0;
		_tnow = 0;

		// Anti-locking algorithm in the literature
		_i_assumed = false;

		// Stabilization (artificial viscosity)
		_Rdamp = 0.02;
		_cdi = _csy = 0;
	}

	~Particle(){delete _material; _material = 0;}

	inline void assign_objectID(unsigned int id){ _obid = id;}
	inline void assign_volume(const Scalar& vol){ _Vo = vol; if (_material != 0) _mass = _Vo*_material->get_density(); }

	inline void set_time(Scalar tnow){ _tnow = tnow;}
	inline void set_coord(const GVector& x){_coord = x;}
	inline void set_velocity(const GVector& vel){ _vel = vel; }
	inline void set_bodyforce(const GVector& bf){ _bforce = bf;}
	inline int  set_state(const std::vector<Scalar>& stat){ return _material -> set_state(stat); }

	inline void reset_Iaddress(unsigned int Iadd){_Iadd = Iadd;}


	inline int
	update_kinamatics( const Scalar& dt, const GVector& du, const GVector& dv){
		_vel   += dv; _coord += du; return 0;
	}

	inline const GVector& get_bodyforce() const { return _bforce; }

	inline const GVector& get_velocity() const {return _vel;}
	inline const GVector& get_coord() const {return _coord;}
	inline const Scalar& get_mass() const {return _mass;}
	inline const Scalar& get_volume() const {return _Vo;}
	inline const Scalar& get_density() const {return _material->get_density();}
	inline Scalar get_soundspeed() const { return _material->get_wavespeed(); }

	inline unsigned int get_objectID(){return _obid;}
	inline int get_materialID(){return _material->get_id();}
	inline int get_ID(){return _id;}
	inline int get_Iadd(){return _Iadd;}

protected:
	unsigned int _id, _Iadd, _obid;
	Scalar _mass, _Vo;
	Material* _material;

	GVector _coord, _vel, _bforce;

	/***********************
	 * waiting list
	 ***********************/
	Scalar _tnow;

	GVector _temp_vec;
	GSymmTensor _temp_symts;
};

#endif /* PARTICLE_H_ */
