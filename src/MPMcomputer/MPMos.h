/**************************************************
 * MPMos.h
 * 	   - MPMos()
 * ------------------------------------------------
 * 	+ major platform of the MPM model
 * ================================================
 * Created by: YWC @ CEE, UW (Apr 27, 2015)
 * ------------------------------------------------
 * Modified by: YWC (15.09.18)
 **************************************************/
#ifndef MPMOS_H_
#define MPMOS_H_

#include <map>
#include <string>
#include <vector>
#include <list>

#include "../GB_Tensor.h"
#include "Space.h"
#include "GBridge.h"

class MPMos {
public:
	MPMos(){_gbrdg = 0;}
	~MPMos();

	int add_space(Space* space);

	inline void set_gridbridge(GBridge* gbrdg){_gbrdg = gbrdg;}

	int build_grid(const Scalar& tnow, const Scalar& dt);

	int update_particle(const Scalar& tnow, const Scalar& dt);

	void print_onestep(const Scalar& tnow);

	void end_update();

private:
	typedef std::map<int, int> mapadd;

	std::vector<Space*> _objs_space;
	mapadd _lns_spaddmap;

	GBridge* _gbrdg;
};

#endif /* MPMOS_H_ */
