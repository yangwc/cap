#include "MPMcomputer.h"
#include <iostream>
#include <string>
#include <ciso646>

MPM::MPM(){
	_dt = 0.0;
	_t_sta  = 0.0;
	_t_end  = 0.0;

	_t_srec = 0.0;
	_Nstep_rec = 0;

	_obj_os = 0;
}

MPM::~MPM(){
	delete _obj_os; _obj_os = 0;
}

void MPM::run(){
	if(_t_end <= _t_sta){
		std::cout << "!!! WARNING !!! Analysis ends before or when it starts! NOTHING will be done." << std::endl;
		return;
	}
	if(_t_end < _t_srec){
		std::cout << "!!! WARNING !!! Data start being recorded after all analysis is done! NOTHING will be done." << std::endl;
		return;
	}
	/****************************************************
	 *********** Initial setup & declaration ************
	 ****************************************************/
	// *** writes progress to console
	bool ioprogress = true;

	// *** Time related
	Scalar dt     = _dt;
	Scalar tnow   = _t_sta;

	// time step in MPM can be a very small number compared to analyzing duration
	unsigned long long int Ntstep = (unsigned long long int) ((_t_end - _t_sta)/dt +0.01);
	unsigned long long int Istep = 0;

	// *** Output related
	int Nrecon = (int) ((_t_srec - _t_sta)/dt +0.01);
	if (Nrecon < 0){ Nrecon = 0;}
	unsigned int Nostep = _Nstep_rec;
	unsigned int Iostep = Nostep - Nrecon;

	// *** Model related
	MPMos& os = *_obj_os;

	// *** state index
	int Istat = -1;

	/****************************************************
	 *************** begin MPM algorithm ****************
	 ****************************************************/

	/*************************
	 *     build initial grid
	 *************************/
	Istat = os.build_grid(tnow, dt);

	/*************************
	 *     main loop
	 *************************/
	while (  Istep != Ntstep and Istat == 0){
		/*************************
		 *    Report on Screen
		 *    Write to files
		 *************************/
		if( Iostep == Nostep ){
			// report to screen
			if (ioprogress){ std::cout<<Istep<<"/"<< Ntstep<<"--("<< tnow<<")"<<std::endl; }
			// print to file
			os.print_onestep(tnow);
			// reset counter
			Iostep = 0;
		}//end if ( output )

		/*************************
		 *     update particle
		 *************************/
		Istat = os.update_particle(tnow, dt);
		if( Istat != 0 ) break;

		/*************************
		 *    Ready 4 Next
		 *************************/
		Iostep++;
		Istep++;
		tnow += dt;

		/*************************
		 *     build current grid
		 *     (grid at final step
		 *     is build for output,
		 *     and assumed strain
		 *     and stress)
		 *************************/
		Istat = os.build_grid(tnow, dt);
	}// end while (time steps)
	/****************************************************
	 *    Report last step on screen
	 ****************************************************/
	if (ioprogress){ std::cout<<Istep<<"/"<< Ntstep<<"--("<< tnow<<")--end"<< std::endl; }

	/****************************************************
	 *    Record last step to files
	 ****************************************************/
	if( Istat == 0 ) os.print_onestep(tnow);

	/****************************************************
	 *    Print Warning Message for Error
	 ****************************************************/
	if( Istat != 0 ){ std::cout<<"!!! ERRORs occurred and the analysis has been terminated !!!"<<std::endl; }

	/****************************************************
	 *    Close os
	 ****************************************************/
	os.end_update();
}
