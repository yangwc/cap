/**************************************************
 * PTFilter.h
 * 	   - PTFilter()
 * ------------------------------------------------
 * 	+ interface of particle filters
 * ================================================
 * Created by: YWC @ CEE, UW (Oct 9, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef PTFILTER_H_
#define PTFILTER_H_

#include <vector>
#include "../GB_Tensor.h"
#include "../MPMobject/_ownership.h"
#include "Particle.h"
#include "../MPMobject/GridElem/_elem_base.h"

class PTFilter : public _ownership{
protected:
	typedef std::vector<Particle*> PtVec;
	typedef _particle_adaptor<Particle> cell;

public:
	virtual ~PTFilter(){;}

	virtual int filter_pt(const Scalar& tnow, const Scalar& dt, PtVec& objs_pt, PtVec& lns_free_pt) = 0;

	virtual PTFilter* get_new_filter() const = 0;

	virtual int refresh_new_cell( cell* ln_ce) = 0;
	virtual int refresh_del_cell( cell* ln_ce) = 0;
};

#endif /* PTFILTER_H_ */
