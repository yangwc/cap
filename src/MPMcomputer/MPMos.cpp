#include "MPMos.h"

MPMos::~MPMos(){
	for(std::vector<Space*>::iterator ivec = _objs_space.begin(); ivec != _objs_space.end(); ++ivec){
		delete (*ivec); (*ivec) = 0;
	}
	delete _gbrdg; _gbrdg = 0;
}

int MPMos::add_space(Space* space){
	int id = space->get_ID();
	mapadd::iterator imap = _lns_spaddmap.find(id);
	if (imap != _lns_spaddmap.end()){
		std::cerr<<"ERROR: space "<<id<<" has existed!!!"<<std::endl;
		return -1;
	}
	_lns_spaddmap[id] = _objs_space.size();
	_objs_space.push_back(space);
	return 0;
}

int MPMos::build_grid(const Scalar& tnow, const Scalar& dt){
	int Istat = -1;

	_gbrdg->reset();
	Space* spptr = 0;
	for (std::vector<Space*>::iterator ivec =_objs_space.begin(); ivec != _objs_space.end(); ++ivec){
		spptr = (*ivec);
		Istat = spptr -> link_particle(tnow); if (Istat != 0) break;
		Istat = spptr -> build_grid(); if (Istat != 0) break;
	}
	if (Istat != 0) return -1;

	_gbrdg -> interact(dt);

	for (std::vector<Space*>::iterator ivec =_objs_space.begin(); ivec != _objs_space.end(); ++ivec){
		Istat = (*ivec) -> solve_grid(tnow, dt);
		if (Istat != 0) break;
	}

	return Istat;
}

int MPMos::update_particle(const Scalar& tnow, const Scalar& dt){
	int Istat = -1;

	Space* spptr = 0;
	bool igood = true;
	for ( std::vector<Space*>::iterator ivec =_objs_space.begin(); ivec != _objs_space.end(); ++ivec){
		spptr = (*ivec);

		igood = spptr->update_pt_kinematics( tnow, dt);
		if (!igood) break;

		igood = spptr->update_pt_deformation( tnow, dt);
		if (!igood) break;

		spptr->filter_pt( tnow, dt);
	}
	if (igood) Istat = 0;

	return Istat;
}

void MPMos::print_onestep(const Scalar& tnow){
	for(std::vector<Space*>::iterator ivec = _objs_space.begin(); ivec != _objs_space.end(); ++ivec){
		(*ivec) -> print_onestep(tnow);
	}
}

void MPMos::end_update(){
	for(std::vector<Space*>::iterator ivec = _objs_space.begin(); ivec != _objs_space.end(); ++ivec){
			(*ivec) -> close_ofile();
	}
}
