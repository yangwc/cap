/**************************************************
 * GBridge.h
 * 	   - GBridge()
 * ------------------------------------------------
 * 	+ Bridge of Grids
 * ================================================
 * Created by: YWC @ CEE, UW (May 5, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/
#ifndef GBRIDGE_H_
#define GBRIDGE_H_

#include <vector>
#include "../MPMobject/ObjManager/LNKmngr.h"
#include "GInteract.h"

class GBridge {
private:
	typedef Gnode* fulladd;
	typedef std::vector< fulladd > faddvec;

public:
	GBridge(LNKmngr<Gnode>* mngrptr, OInteract* algptr);
	virtual ~GBridge();

	void reset();
	void interact(const Scalar& dt);
	void update_gridusage(const IntGVector& ndid, Gnode* ndlink);

private:
	OInteract* _alg;
	LNKmngr<Gnode>* _gmanager;

	std::vector<unsigned int> _Nsps;
	std::vector< faddvec > _gadds;

	unsigned int _Nmod;
	std::vector<unsigned int> _Imods;
};

#endif /* GBRIDGE_H_ */
