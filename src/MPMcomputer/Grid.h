/**************************************************
 * Grid.h
 * 	   - Grid()
 * ------------------------------------------------
 * 	+ Grid interface
 * ================================================
 * Created by: YWC @ CEE, UW (Apr 30, 2014)
 * ------------------------------------------------
 * Modified by: YWC (15.09.23)
 **************************************************/

#ifndef GRID_H_
#define GRID_H_
#include "../GB_Tensor.h"
#include "Particle.h"
#include "GBridge.h"

class Gnode;

class Grid {
public:
	virtual ~Grid(){;}

	// MPM spaces are connected to any other through grids.
	inline void link_bridge(GBridge*  brdglink){ _ln_brdg = brdglink;}

	// RESET grid to its default condition.
	virtual int reset() = 0;

	// RESET grid to its default condition and keep links of particles which still
	// stay in same elements as the previous time step.
	virtual int reset( const Scalar& tnow, std::vector<Particle*>& ln_freepts ) = 0;

	// BUILD variable fields and determine nodal values.
	virtual int build() = 0;

	// SOLVE boundary problems and calculate acceleration field.
	virtual int solve(const Scalar& tnow, const Scalar& dt) = 0;

	// LINK or push back a particle into an element.
	virtual int link_particle(Particle* ptptr) = 0;

	// UPDATE kinematics.
	virtual int update_kinematics(const Scalar& tnow, const Scalar& dt) = 0;

	// UPDATE deformation.
	virtual int update_deformation(const Scalar& tnow, const Scalar& dt) = 0;

	// UPDATE status of grid nodes to "bridge"s. (to interact with other spaces|grids)
	virtual int link_global() = 0;

	// GET read-only links of grid nodes. (for output)
	virtual unsigned int get_Nnode() const = 0;
	virtual const Gnode* get_Node(unsigned int Ind) const = 0;

protected:
    GBridge*  _ln_brdg;
};

#endif /* GRID_H_ */
