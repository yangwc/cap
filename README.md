* Continuum Analysis Program (CAP) is a code developed as part of Wen-Chia Yang's Ph.D. work: *Study of Tsunami-Induced Fluid and Debris Load on Bridges using the Material Point Method* (2016)
* Characteristics: material point method; single-threaded; enhanced boundary treatment; anti-locking algorithm; smoothing (stabilization) algorithm.
* Manual and sample examples are temporary unavailable.
* Your comments and suggestions are appreciated.